<?php

defined('BASEPATH') OR exit('No direct script access allowed');

/*
  | -------------------------------------------------------------------------
  | URI ROUTING
  | -------------------------------------------------------------------------
  | This file lets you re-map URI requests to specific controller functions.
  |
  | Typically there is a one-to-one relationship between a URL string
  | and its corresponding controller class/method. The segments in a
  | URL normally follow this pattern:
  |
  |	example.com/class/method/id/
  |
  | In some instances, however, you may want to remap this relationship
  | so that a different class/function is called than the one
  | corresponding to the URL.
  |
  | Please see the user guide for complete details:
  |
  |	https://codeigniter.com/user_guide/general/routing.html
  |
  | -------------------------------------------------------------------------
  | RESERVED ROUTES
  | -------------------------------------------------------------------------
  |
  | There are three reserved routes:
  |
  |	$route['default_controller'] = 'welcome';
  |
  | This route indicates which controller class should be loaded if the
  | URI contains no data. In the above example, the "welcome" class
  | would be loaded.
  |
  |	$route['404_override'] = 'errors/page_missing';
  |
  | This route will tell the Router which controller/method to use if those
  | provided in the URL cannot be matched to a valid route.
  |
  |	$route['translate_uri_dashes'] = FALSE;
  |
  | This is not exactly a route, but allows you to automatically route
  | controller and method names that contain dashes. '-' isn't a valid
  | class or method name character, so it requires translation.
  | When you set this option to TRUE, it will replace ALL dashes in the
  | controller and method URI segments.
  |
  | Examples:	my-controller/index	-> my_controller/index
  |		my-controller/my-method	-> my_controller/my_method
 */

/*  Socal Routing */
$route['default_controller'] = 'Frontend';
$route['404_override'] = 'Login/ERROR_404';
$route['translate_uri_dashes'] = FALSE;

// $route['Login/Facebook'] = "Login/Facebook";
// $route['Login/FacebookLoginDetails'] = "Login/FacebookLoginDetails";
// $route['Login/Google'] = "Login/Google";
// $route['Login/GoogleLoginDetails'] = "Login/GoogleLoginDetails";
// $route['Login/Twitter'] = "Login/Twitter";
// $route['Login/TwitterDetails'] = "Login/TwitterDetails";
/* */


$route['Login/Admin'] = "Login/Admin";
$route['Login/CheckAdminLogin'] = "Login/CheckAdminLogin";
$route['Login/AdminLoginout'] = "Login/AdminLoginout";
$route['Login/AdminLoginout'] = "Login/AdminLoginout";
$route['Logout'] = "Login/Logout";
$route['Login/(.+)'] = "Login/index";

$route['home'] = "Frontend/index";
$route['registration'] = "Frontend/Registration";
$route['dashboard'] = "Frontend/dashboard";
$route['shipping'] = "Frontend/shipping";
$route['recieve'] = "Frontend/recieve";
$route['track'] = "Frontend/track";
$route['addressbook'] = "Frontend/addressbook";
$route['myWallet'] = "Frontend/myWallet";
$route['forgotPassword'] = "Frontend/forgotPassword";
$route['resetpassword/(:any)'] = "Frontend/resetPassword/$1";
$route['activateUserAccount'] = "Frontend/activateUserAccount";

$route['choose-your-courier'] = "Frontend/documentSubmit";
$route['thank-you'] = "Frontend/thankyou";

$route['logout'] = 'Frontend/logout';

// $route['user_dashboard'] = 'Users/index';
// $route['UserLoginout'] = 'Login/UserLoginout';
