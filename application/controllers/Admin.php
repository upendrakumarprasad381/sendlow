<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Admin extends CI_Controller {

    private $segment2;
    private $segment3;
    private $segment4;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Dubai');
        IsLogedAdmin();
        $this->load->Model('Database');
        $this->segment2 = $this->uri->segment(2);
        $this->segment3 = $this->uri->segment(3);
        $this->segment4 = $this->uri->segment(4);
    }

    public function Helper() {
        $status = $_REQUEST['function'];
        switch ($status) {
            default :
                $_REQUEST['function']($_REQUEST);
        }
    }

    public function index() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'index');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function UserManagement() {
        //  $CondutaionArray = array('id' => 1);
        $ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;
        $Session = $this->session->userdata('Admin');
        if (in_array($Session->id, $ADMIN_PERMISSION_ARRAY)) {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `admin_login` WHERE archive=:archive ";
        } else {
            $CondutaionArray_dept = array('archive' => 0, 'id' => $Session->id);
            $Qry_dept_id = "SELECT * FROM `admin_login` WHERE archive=:archive and id=:id";

            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `admin_login` WHERE archive=:archive ";
        }
        $UserArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
        // print_r($Qry);die();
        $data['UserArray'] = $UserArray;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'UserManagement', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function walletTransactionReport() {
        $data['Filter'] = '';
        if (isset($_POST['Filter'])) {
            $data['Filter'] = $_POST;
        }
        $Qry = "SELECT * FROM  wallet_transactions";
        $walletArray = $this->Database->select_qry_array($Qry);
        $data['walletArray'] = $walletArray;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'walletTransactionReport', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function couriers() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $Id = $json['Id'];
                $json['lastupdate'] = date('Y-m-d H:i:s');
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('courier_company_master', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('courier_company_master', $json, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `courier_company_master` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['courier'] = $Array[0];
                }
            }

            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddCouriers', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $ConditionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `courier_company_master` WHERE archive=0";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['courier'] = $Array;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'couriers', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function aboutUs() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'aboutUs');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function privacyPolicy() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'privacyPolicy');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function termsAndConditions() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'termsAndConditions');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function deliveryInformation() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'deliveryInformation');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function testimonials() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $array = $json;
                $array['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $array['Id'];
                unset($json['Id']);

                if (!empty($_REQUEST['NewImage'])) {
                    $ImageName = 'client' . uniqid() . '.png';
                    $UploadPath = HOME_DIR . 'uploads/testimonials/' . $ImageName;
                    if (file_put_contents($UploadPath, file_get_contents($_REQUEST['NewImage']))) {
                        $oldFile = HOME_DIR . (!empty($_REQUEST['OldImage']) ? $_REQUEST['OldImage'] : '');
                        if (is_file($oldFile)) {
                            unlink($oldFile);
                        }
                        $array['image'] = $ImageName;
                    }
                }

                if ($Id == '') {
                    $Result = $this->Database->insert('testimonials', $array);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('testimonials', $array, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $CondutaionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `testimonials` WHERE id=:id";
                $testimonialArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($testimonialArray) > 0) {
                    $data['Id'] = $Id;
                    $data['testimonialArray'] = $testimonialArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddTestimonials', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `testimonials` WHERE archive=:archive";
            $testimonialArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['testimonialArray'] = $testimonialArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'testimonials', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function companyAccountInfo() {
        if (isset($_REQUEST['json'])) {
            $Json = json_decode($_REQUEST['json'], true);
            $UserId = $Json['UserId'];
            unset($Json['UserId']);
            $Json['lastupdate'] = date('Y-m-d H:i:s');
            $Json['timestamp'] = date('Y-m-d H:i:s');
            if ($UserId > 0) {
                $Result = 1;
                $CondArray = array('id' => $UserId);
                $this->Database->update('company_account_info', $Json, $CondArray);
            } else {
                $Result = $this->Database->insert('company_account_info', $Json);
            }
            if ($Result) {
                echo json_encode(array('status' => 'success', 'message' => 'Successfully'));
            }
            exit;
        }
        $data['accountArray'] = '';
        $data['accountId'] = '';
        // if (is_numeric($UserId)) {
        // $CondutaionArray = array('id' => $UserId);
        $Qry = "SELECT * FROM `company_account_info`";
        $accountArray = $this->Database->select_qry_array($Qry);
        $data['accountArray'] = $accountArray[0];
        // }
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'companyAccountInfo', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function parcel() {
        //  $CondutaionArray = array('id' => 1);
        $ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;
        $Session = $this->session->userdata('Admin');
        if (in_array($Session->id, $ADMIN_PERMISSION_ARRAY)) {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `admin_login` WHERE archive=:archive ";
        } else {
            $CondutaionArray_dept = array('archive' => 0, 'id' => $Session->id);
            $Qry_dept_id = "SELECT * FROM `admin_login` WHERE archive=:archive and id=:id";

            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `admin_login` WHERE archive=:archive ";
        }
        $UserArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
        // print_r($Qry);die();
        $data['UserArray'] = $UserArray;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'UserManagement', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function AddNewUser() {
        if (isset($_REQUEST['json'])) {
            //print_r('hiii');
            $Json = json_decode($_REQUEST['json'], true);
            $UserId = $Json['UserId'];
            $email = $Json['email'];

            $CondutaionArray_Email = array('email' => $email);
            if ($UserId) {
                $Qry_email = "SELECT * FROM `admin_login` WHERE email=:email AND id!=$UserId";
            } else {
                $Qry_email = "SELECT * FROM `admin_login` WHERE email=:email ";
            }
            $email_exist = $this->Database->select_qry_array($Qry_email, $CondutaionArray_Email);
            if ($email_exist) {
                echo json_encode(array('status' => 'error', 'message' => 'This Email Or Username Already Exist'));
                exit;
            }
            //email validation
            if (isset($Json['user_type']) && $Json['user_type'] != 0) {
                if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {

                    echo json_encode(array('status' => 'error', 'message' => 'Invalid email format'));
                    exit;
                }
            }
            //end email validation
            //   $Json['created_by'] = $adminsession->id;
            unset($Json['UserId']);
            $Json['lastupdate'] = date('Y-m-d H:i:s');
            if (isset($Json['password'])) {
                $Json['password'] = md5($Json['password']);
            }
            if ($UserId > 0) {
                $Result = 1;
                $CondArray = array('id' => $UserId);
                $this->Database->update('admin_login', $Json, $CondArray);
            } else {
                $Result = $this->Database->insert('admin_login', $Json);
            }
            if ($Result) {
                echo json_encode(array('status' => 'success', 'message' => 'Successfully'));
            }
            exit;
        }

        $UserId = base64_decode($this->segment3);
        $data['UserArray'] = '';
        $data['UserId'] = '';
        if (is_numeric($UserId)) {
            $CondutaionArray = array('id' => $UserId);
            $Qry = "SELECT * FROM `admin_login` WHERE id=:id";
            $UserArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['UserArray'] = $UserArray[0];
            $data['UserId'] = $UserId;
        }
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'AddNewUser', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function userpermission() {
        if (isset($_POST['SetUserId'])) {
            $permission = !empty($_POST['permission']) && is_array($_POST['permission']) ? $_POST['permission'] : array();
            $SetUserId = !empty($_POST['SetUserId']) ? $_POST['SetUserId'] : '';
            $cond = array('user_id' => $SetUserId);
            $this->Database->delete('user_permission', $cond);
            if (!empty($permission)) {
                $date = date('Y-m-d H:i:s');
                for ($i = 0; $i < count($permission); $i++) {
                    $d = $permission[$i];
                    $addPer = array(
                        'user_id' => $SetUserId,
                        'menu_id' => $d['MenuId'],
                        'additional_json' => !empty($d['actionpermission']) ? json_encode($d['actionpermission']) : '',
                        'timestamp' => $date,
                    );
                    $this->Database->insert('user_permission', $addPer);
                }
            }
            die(json_encode(array('status' => true, 'message' => 'successfully')));
        }
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'userpermission');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function country() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_POST['json']) && is_array($_POST['json'])) {
                $json = $_POST['json'];
                $json['courier_id'] = !empty($json['courier_id']) ? json_encode($json['courier_id']) : '';

                $json['lastupdate'] = date('Y-m-d H:i:s');
                if (empty($json['id'])) {
                    $json['timestamp'] = $json['lastupdate'];
                    $Result = $this->Database->insert('country', $json);
                } else {
                    $this->Database->update('country', $json, ['id' => $json['id']]);
                }
                $return = array('status' => 'success');

                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `country` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['country'] = $Array[0];
                }
            }

            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddCountry', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $ConditionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `country` WHERE archive=0 order by id ASC";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['country'] = $Array;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'country', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function city() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('city', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('city', $json, $CondArray);
                }

                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `city` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['city'] = $Array[0];
                }
            }

            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddCity', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $ConditionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `city` WHERE archive=0 order by id ASC";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['city'] = $Array;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'city', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function autoDiscount() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('auto_discount', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('auto_discount', $json, $CondArray);
                }

                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `auto_discount` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['discount'] = $Array[0];
                }
            }

            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddDiscount', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $ConditionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `auto_discount` WHERE archive=0 order by id ASC";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['discount'] = $Array;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'discount', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function shipments() {
        // $qry = "SELECT * FROM `register` WHERE archive=0 order by id desc";
        // $array = $this->Database->select_qry_array($qry);
        // $data['users'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'shipments');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    // public function registered_users() {
    //     $qry = "SELECT * FROM `register` WHERE archive=0 order by id desc";
    //     $array = $this->Database->select_qry_array($qry);
    //     $data['users'] = $array;
    //     $this->load->view(ADMIN_DIR . 'includes/header');
    //     $this->load->view(ADMIN_DIR . 'registered_users', $data);
    //     $this->load->view(ADMIN_DIR . 'includes/footer');
    // }

    public function individual() {
        $qry = "SELECT * FROM `register` WHERE archive=0 AND type=1 order by id desc";
        $array = $this->Database->select_qry_array($qry);
        $data['users'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'registered_users', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function smallBusiness() {
        $qry = "SELECT * FROM `register` WHERE archive=0 AND type=3 order by id desc";
        $array = $this->Database->select_qry_array($qry);
        $data['users'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'registered_users', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function company() {
        $qry = "SELECT * FROM `register` WHERE archive=0 AND type=2 order by id desc";
        $array = $this->Database->select_qry_array($qry);
        $data['users'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'registered_users', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function broker() {
        $qry = "SELECT * FROM `register` WHERE archive=0 AND type=4 order by id desc";
        $array = $this->Database->select_qry_array($qry);
        $data['users'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'registered_users', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function ecommerce() {
        $qry = "SELECT * FROM `register` WHERE archive=0 AND type=5 order by id desc";
        $array = $this->Database->select_qry_array($qry);
        $data['users'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'registered_users', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function user_info() {
        $UserId = base64_decode($this->segment3);
        $qry = "SELECT * FROM `register`  WHERE id=:id";
        $CondutaionArray = array('id' => $UserId);
        $UserArray = $this->Database->select_qry_array($qry, $CondutaionArray);
        if (count($UserArray) > 0) {
            $data['UserId'] = $UserId;
            $data['UserInfo'] = $UserArray[0];
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'user_info', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function offers() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'offers');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function add_offers() {
        if (!empty($_POST['json']) && is_array($_POST['json'])) {
            $Json = $_POST['json'];
            $offerId = $Json['id'];
            $Json['start_date'] = date('Y-m-d', strtotime($Json['start_date']));
            $Json['expiry_date'] = date('Y-m-d', strtotime($Json['expiry_date']));
            $Json['lastupdate'] = date('Y-m-d h:m:s');
            if (!empty($coupon_id)) {
                $CondArray = array('id' => $offerId);
                $this->Database->update('offers', $Json, $CondArray);
            } else {
                $this->Database->insert('offers', $Json);
            }
            die(json_encode(array('status' => true, 'message' => 'Successfully')));
        }
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'AddOffer');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function home_banner() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $array = $json;
                $array['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $array['Id'];
                unset($json['Id']);

                if (!empty($_REQUEST['NewBanner'])) {
                    $ImageName = 'home_banner' . uniqid() . '.png';
                    $UploadPath = HOME_DIR . 'uploads/home_banner/' . $ImageName;
                    if (file_put_contents($UploadPath, file_get_contents($_REQUEST['NewBanner']))) {
                        $oldFile = HOME_DIR . (!empty($_REQUEST['OldBanner']) ? $_REQUEST['OldBanner'] : '');
                        if (is_file($oldFile)) {
                            unlink($oldFile);
                        }
                        $array['image'] = $ImageName;
                    }
                }
                unset($array['NewImageBanner']);
                unset($array['OldImageBanner']);
                unset($array['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('home_banner', $array);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('home_banner', $array, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $CondutaionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `home_banner` WHERE id=:id";
                $bannerArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($bannerArray) > 0) {
                    $data['Id'] = $Id;
                    $data['bannerArray'] = $bannerArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddBanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `home_banner` WHERE archive=:archive";
            $bannerArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['bannerArray'] = $bannerArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'HomeBanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function rightBanner() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $array = $json;
                $array['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $array['Id'];
                unset($json['Id']);

                if (!empty($_REQUEST['NewBanner'])) {
                    $ImageName = 'right_banner' . uniqid() . '.png';
                    $UploadPath = HOME_DIR . 'uploads/home_banner/' . $ImageName;
                    if (file_put_contents($UploadPath, file_get_contents($_REQUEST['NewBanner']))) {
                        $oldFile = HOME_DIR . (!empty($_REQUEST['OldBanner']) ? $_REQUEST['OldBanner'] : '');
                        if (is_file($oldFile)) {
                            unlink($oldFile);
                        }
                        $array['image'] = $ImageName;
                    }
                }
                unset($array['NewImageBanner']);
                unset($array['OldImageBanner']);
                unset($array['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('home_right_banner', $array);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('home_right_banner', $array, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $CondutaionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `home_right_banner` WHERE id=:id";
                $bannerArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($bannerArray) > 0) {
                    $data['Id'] = $Id;
                    $data['bannerArray'] = $bannerArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddHomeRightBanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `home_right_banner` WHERE archive=:archive";
            $bannerArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['bannerArray'] = $bannerArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'homeRightBanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function leftBanner() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $array = $json;
                $array['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $array['Id'];
                unset($json['Id']);

                if (!empty($_REQUEST['NewBanner'])) {
                    $ImageName = 'left_banner' . uniqid() . '.png';
                    $UploadPath = HOME_DIR . 'uploads/home_banner/' . $ImageName;
                    if (file_put_contents($UploadPath, file_get_contents($_REQUEST['NewBanner']))) {
                        $oldFile = HOME_DIR . (!empty($_REQUEST['OldBanner']) ? $_REQUEST['OldBanner'] : '');
                        if (is_file($oldFile)) {
                            unlink($oldFile);
                        }
                        $array['image'] = $ImageName;
                    }
                }
                unset($array['NewImageBanner']);
                unset($array['OldImageBanner']);
                unset($array['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('home_left_banner', $array);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('home_left_banner', $array, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $CondutaionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `home_left_banner` WHERE id=:id";
                $bannerArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($bannerArray) > 0) {
                    $data['Id'] = $Id;
                    $data['bannerArray'] = $bannerArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddHomeLeftBanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `home_left_banner` WHERE archive=:archive";
            $bannerArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['bannerArray'] = $bannerArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'homeLeftBanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function importBanner() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $array = $json;
                $array['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $array['Id'];
                unset($json['Id']);

                if (!empty($_REQUEST['NewBanner'])) {
                    $ImageName = 'import_banner' . uniqid() . '.png';
                    $UploadPath = HOME_DIR . 'uploads/home_banner/' . $ImageName;
                    if (file_put_contents($UploadPath, file_get_contents($_REQUEST['NewBanner']))) {
                        $oldFile = HOME_DIR . (!empty($_REQUEST['OldBanner']) ? $_REQUEST['OldBanner'] : '');
                        if (is_file($oldFile)) {
                            unlink($oldFile);
                        }
                        $array['image'] = $ImageName;
                    }
                }
                unset($array['NewImageBanner']);
                unset($array['OldImageBanner']);
                unset($array['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('import_banner', $array);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('import_banner', $array, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $CondutaionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `import_banner` WHERE id=:id";
                $bannerArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($bannerArray) > 0) {
                    $data['Id'] = $Id;
                    $data['bannerArray'] = $bannerArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddHomeImportBanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `import_banner` WHERE archive=:archive";
            $bannerArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['bannerArray'] = $bannerArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'homeImportBanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function exportBanner() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $array = $json;
                $array['lastupdate'] = date('Y-m-d H:i:s');
                $Id = $array['Id'];
                unset($json['Id']);

                if (!empty($_REQUEST['NewBanner'])) {
                    $ImageName = 'export_banner' . uniqid() . '.png';
                    $UploadPath = HOME_DIR . 'uploads/home_banner/' . $ImageName;
                    if (file_put_contents($UploadPath, file_get_contents($_REQUEST['NewBanner']))) {
                        $oldFile = HOME_DIR . (!empty($_REQUEST['OldBanner']) ? $_REQUEST['OldBanner'] : '');
                        if (is_file($oldFile)) {
                            unlink($oldFile);
                        }
                        $array['image'] = $ImageName;
                    }
                }
                unset($array['NewImageBanner']);
                unset($array['OldImageBanner']);
                unset($array['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('export_banner', $array);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('export_banner', $array, $CondArray);
                }
                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $CondutaionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `export_banner` WHERE id=:id";
                $bannerArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($bannerArray) > 0) {
                    $data['Id'] = $Id;
                    $data['bannerArray'] = $bannerArray[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddHomeExportBanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $CondutaionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `export_banner` WHERE archive=:archive";
            $bannerArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $data['bannerArray'] = $bannerArray;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'homeExportBanner', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function walletRequest() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'walletRequest');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function request_info() {
        $requestId = base64_decode($this->segment3);
        $qry = "SELECT wallet_request.*,register.first_name,register.last_name,register.email,register.type,register.mobile FROM `wallet_request` 
        LEFT JOIN register ON register.id=wallet_request.user_id WHERE wallet_request.archive='0' AND wallet_request.id=$requestId";
        $UserArray = $this->Database->select_qry_array($qry);
        if (count($UserArray) > 0) {
            $data['requestId'] = $requestId;
            $data['UserInfo'] = $UserArray[0];
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'request_info', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function updateWalletAmount() {
        if (isset($_POST)) {
            $request_id = $_POST['request_id'];
            $user_id = $_POST['user_id'];
            $request_type = $_POST['request_type'];
            $request_no = '00' . $request_id . date('dmY');
            $transaction_no = $_POST['transaction_id'];
            $recharge_amount = '';
            if ($request_type == 1) {
                $recharge_amount = $_POST['recharge_amount'];
                $wallet = array(
                    'user_id' => $user_id,
                    'transfer_request_id' => $request_id,
                    'transaction_id' => $transaction_no,
                    'order_id' => $request_no,
                    'amount' => $recharge_amount,
                    'payment_type' => 2,
                    'lastupdate' => date('Y-m-d H:i:s'),
                    'timestamp' => date('Y-m-d H:i:s')
                );
            } else {
                $offerId = $_POST['offer_id'];
                $offerDetails = GetOfferByOfferId($offerId);
                $paid_amt = $offerDetails[0]->paid_amount;
                $recharge_amount = $offerDetails[0]->get_amount;
                $wallet = array(
                    'user_id' => $user_id,
                    'transfer_request_id' => $request_id,
                    'offer_id' => $offerId,
                    'transaction_id' => $transaction_no,
                    'order_id' => $request_no,
                    'amount' => $paid_amt,
                    'payment_type' => 2,
                    'lastupdate' => date('Y-m-d H:i:s'),
                    'timestamp' => date('Y-m-d H:i:s')
                );

                $redeemOffer = array(
                    'user_id' => $user_id,
                    'offer_id' => $offerId,
                    'is_redeem' => 1,
                    'lastupdate' => date('Y-m-d H:i:s'),
                    'timestamp' => date('Y-m-d H:i:s')
                );
                $redeemResp = $this->Database->insert('redeemed_offers', $redeemOffer);
            }
            $condition = array('id' => $request_id);
            $approve['is_approved'] = 1;
            $resp = $this->Database->update('wallet_request', $approve, $condition);

            $result = $this->Database->insert('wallet_transactions', $wallet);
            $walletResp = updateMyWallet($user_id, $recharge_amount);
            if ($walletResp) {
                $conduser = array('id' => $result);
                $payment['payment_status'] = 1;
                $resp = $this->Database->update('wallet_transactions', $payment, $conduser);
                if ($resp != 0) {
                    $return = array('status' => 'true', 'message' => 'Amount added to wallet');
                    exit(json_encode($return));
                } else {
                    $return = array('status' => 'false', 'message' => 'Amount not added to wallet');
                    exit(json_encode($return));
                }
            } else {
                $return = array('status' => 'false', 'message' => 'Amount not added to wallet');
                exit(json_encode($return));
            }
        } else {
            $return = array('status' => 'false', 'message' => 'Something went wrong');
            exit(json_encode($return));
        }
    }

    public function approveWallet() {
        $qry = "SELECT wallet_request.*,register.first_name,register.last_name,register.email,register.type,register.mobile FROM `wallet_request` 
        LEFT JOIN register ON register.id=wallet_request.user_id WHERE wallet_request.is_approved != 0 order by wallet_request.id desc";
        $array = $this->Database->select_qry_array($qry);
        $data['request'] = $array;
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'allWalletRequest', $data);
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    // public function addTickets() {
    //     $this->load->view(ADMIN_DIR . 'includes/header');
    //     $this->load->view(ADMIN_DIR . 'AddTickets');
    //     $this->load->view(ADMIN_DIR . 'includes/footer');
    // }

    public function ticketSettings() {
        $segment3 = $this->segment3;
        $Id = base64_decode($this->segment4);
        $data = array();
        if ($segment3 == 'add-new') {
            if (isset($_REQUEST['json'])) {
                $json = json_decode($_REQUEST['json'], true);
                $Id = $json['Id'];
                unset($json['Id']);
                if ($Id == '') {
                    $Result = $this->Database->insert('ticket_settings', $json);
                } else if ($Id > 0) {
                    $Result = 1;
                    $CondArray = array('id' => $Id);
                    $this->Database->update('ticket_settings', $json, $CondArray);
                }

                if ($Result) {
                    $return = array('status' => 'success');
                }
                exit(json_encode($return));
            }
            $data['Id'] = '';
            if (is_numeric($Id)) {
                $ConditionArray = array('id' => $Id);
                $Qry = "SELECT * FROM `ticket_settings` WHERE id=:id ORDER BY id DESC";
                $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
                if (count($Array) > 0) {
                    $data['Id'] = $Id;
                    $data['ticket'] = $Array[0];
                }
            }
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'AddTickets', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        } else {
            $ConditionArray = array('archive' => 0);
            $Qry = "SELECT * FROM `ticket_settings` WHERE archive=0 order by id ASC";
            $Array = $this->Database->select_qry_array($Qry, $ConditionArray);
            $data['ticket'] = $Array;
            $this->load->view(ADMIN_DIR . 'includes/header');
            $this->load->view(ADMIN_DIR . 'ticketSettings', $data);
            $this->load->view(ADMIN_DIR . 'includes/footer');
        }
    }

    public function markup() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'markup');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function booking() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'booking');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function bookingdetails() {
        $this->load->view(ADMIN_DIR . 'includes/header');
        $this->load->view(ADMIN_DIR . 'bookingdetails');
        $this->load->view(ADMIN_DIR . 'includes/footer');
    }

    public function test() {
      

        exit;

        exit;
        $this->load->library("aramex");

        $params = array(
            'Shipments' => array(
                'Shipment' => array(
                    'Shipper' => array(
                        'Reference1' => 'Ref 111111',
                        'Reference2' => 'Ref 222222',
                        'AccountNumber' => '20016',
                        'PartyAddress' => array(
                            'Line1' => 'Mecca St',
                            'Line2' => '',
                            'Line3' => '',
                            'City' => 'Amman',
                            'StateOrProvinceCode' => '',
                            'PostCode' => '',
                            'CountryCode' => 'Jo'
                        ),
                        'Contact' => array(
                            'Department' => '',
                            'PersonName' => 'Michael',
                            'Title' => '',
                            'CompanyName' => 'Aramex',
                            'PhoneNumber1' => '5555555',
                            'PhoneNumber1Ext' => '125',
                            'PhoneNumber2' => '',
                            'PhoneNumber2Ext' => '',
                            'FaxNumber' => '',
                            'CellPhone' => '07777777',
                            'EmailAddress' => 'michael@aramex.com',
                            'Type' => ''
                        ),
                    ),
                    'Consignee' => array(
                        'Reference1' => 'Ref 333333',
                        'Reference2' => 'Ref 444444',
                        'AccountNumber' => '',
                        'PartyAddress' => array(
                            'Line1' => '15 ABC St',
                            'Line2' => '',
                            'Line3' => '',
                            'City' => 'Dubai',
                            'StateOrProvinceCode' => '',
                            'PostCode' => '',
                            'CountryCode' => 'AE'
                        ),
                        'Contact' => array(
                            'Department' => '',
                            'PersonName' => 'Mazen',
                            'Title' => '',
                            'CompanyName' => 'Aramex',
                            'PhoneNumber1' => '6666666',
                            'PhoneNumber1Ext' => '155',
                            'PhoneNumber2' => '',
                            'PhoneNumber2Ext' => '',
                            'FaxNumber' => '',
                            'CellPhone' => '',
                            'EmailAddress' => 'mazen@aramex.com',
                            'Type' => ''
                        ),
                    ),
                    'ThirdParty' => array(
                        'Reference1' => '',
                        'Reference2' => '',
                        'AccountNumber' => '',
                        'PartyAddress' => array(
                            'Line1' => '',
                            'Line2' => '',
                            'Line3' => '',
                            'City' => '',
                            'StateOrProvinceCode' => '',
                            'PostCode' => '',
                            'CountryCode' => ''
                        ),
                        'Contact' => array(
                            'Department' => '',
                            'PersonName' => '',
                            'Title' => '',
                            'CompanyName' => '',
                            'PhoneNumber1' => '',
                            'PhoneNumber1Ext' => '',
                            'PhoneNumber2' => '',
                            'PhoneNumber2Ext' => '',
                            'FaxNumber' => '',
                            'CellPhone' => '',
                            'EmailAddress' => '',
                            'Type' => ''
                        ),
                    ),
                    'Reference1' => 'Shpt 0001',
                    'Reference2' => '',
                    'Reference3' => '',
                    'ForeignHAWB' => 'ABC 000111',
                    'TransportType' => 0,
                    'ShippingDateTime' => time(),
                    'DueDate' => time(),
                    'PickupLocation' => 'Reception',
                    'PickupGUID' => '',
                    'Comments' => 'Shpt 0001',
                    'AccountingInstrcutions' => '',
                    'OperationsInstructions' => '',
                    'Details' => array(
                        'Dimensions' => array(
                            'Length' => 10,
                            'Width' => 10,
                            'Height' => 10,
                            'Unit' => 'cm',
                        ),
                        'ActualWeight' => array(
                            'Value' => 0.5,
                            'Unit' => 'Kg'
                        ),
                        'ProductGroup' => 'EXP',
                        'ProductType' => 'PDX',
                        'PaymentType' => 'P',
                        'PaymentOptions' => '',
                        'Services' => '',
                        'NumberOfPieces' => 1,
                        'DescriptionOfGoods' => 'Docs',
                        'GoodsOriginCountry' => 'Jo',
                        'CashOnDeliveryAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => ''
                        ),
                        'InsuranceAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => ''
                        ),
                        'CollectAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => ''
                        ),
                        'CashAdditionalAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => ''
                        ),
                        'CashAdditionalAmountDescription' => '',
                        'CustomsValueAmount' => array(
                            'Value' => 0,
                            'CurrencyCode' => ''
                        ),
                        'Items' => array(
                        )
                    ),
                ),
            ),
            'ClientInfo' => array(
                'AccountCountryCode' => 'JO',
                'AccountEntity' => 'AMM',
                'AccountNumber' => '20016',
                'AccountPin' => '221321',
                'UserName' => 'reem@reem.com',
                'Password' => '123456789',
                'Version' => '1.0'
            ),
            'Transaction' => array(
                'Reference1' => '001',
                'Reference2' => '',
                'Reference3' => '',
                'Reference4' => '',
                'Reference5' => '',
            ),
            'LabelInfo' => array(
                'ReportID' => 9201,
                'ReportType' => 'URL',
            ),
        );

        $params['Shipments']['Shipment']['Details']['Items'][] = array(
            'PackageType' => 'Box',
            'Quantity' => 1,
            'Weight' => array(
                'Value' => 0.5,
                'Unit' => 'Kg',
            ),
            'Comments' => 'Docs',
            'Reference' => ''
        );
        echo json_encode($params);
        exit;
        $price = $this->aramex->createShipment($bookingId = '10');
    }

}
