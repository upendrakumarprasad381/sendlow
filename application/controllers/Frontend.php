<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Frontend extends CI_Controller {

    private $segment2;
    private $segment3;
    private $segment4;

    public function __construct() {

        parent::__construct();
        date_default_timezone_set('Asia/Dubai');
        error_reporting(1);
        $this->load->Model('Database');
        $this->segment2 = $this->uri->segment(2);
        $this->segment3 = $this->uri->segment(3);
        $this->segment4 = $this->uri->segment(4);
        if (isset($_SESSION['currency'])) {
            
        } else {
            $_SESSION['currency'] = 'AED';
        }
    }

    public function Helper() {
        $status = $_REQUEST['function'];
        switch ($status) {
            default :
                $_REQUEST['function']($_REQUEST);
        }
    }

    public function index() {
        $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'Home');
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    public function Registration() {
        $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'Registration');
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    public function dashboard() {
        $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'Dashboard');
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    public function shipping() {
        $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'shipping');
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    public function recieve() {
        $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'recieve');
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    public function track() {
        $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'Trackid');
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    public function addressbook() {
        $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'AddressBook');
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    public function myWallet() {
        $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'MyWallet');
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    public function forgotPassword() {
        $segment3 = $this->segment3;
        if ($segment3 == 'update') {
            if (!empty($_POST['email'])) {
                $qry = "SELECT * FROM `register` WHERE `email` = '" . $_POST['email'] . "' and archive=0";
                $arr = $this->Database->select_qry_array($qry);
                if (!empty($arr)) {
                    send_email_for_forgot_password($arr[0]->id, $_POST['email']);
                    $json = '{"status":"success","message":"Please check your mail to reset your password"}';
                } else {
                    $json = '{"status":"error","message":"User not found"}';
                }
            }
            echo $json;
            exit;
        } else {
            $this->load->view(FRONTED_DIR . 'Includes/header');
            $this->load->view(FRONTED_DIR . 'ForgotPassword');
            $this->load->view(FRONTED_DIR . 'Includes/footer');
        }
    }

    public function resetPassword($id = '') {
        $segment3 = $this->segment3;
        if ($segment3 == 'update') {
            if ($_REQUEST['id'] != '' && $_REQUEST['password']) {
                $CondArray = array('id' => $_REQUEST['id']);
                $update_array = array('password' => md5($_REQUEST['password']));
                $result = $this->Database->update('register', $update_array, $CondArray);
                if ($result) {
                    $json = '{"status":"success","message":"Password updated successfully"}';
                } else {
                    $json = '{"status":"error","message":"Something went wrong"}';
                }
            }
            echo $json;
            exit;
        } else {
            $id = base64_decode($id);
            $data['id'] = $id;
            $this->load->view(FRONTED_DIR . 'Includes/header');
            $this->load->view(FRONTED_DIR . 'ResetPassword', $data);
            $this->load->view(FRONTED_DIR . 'Includes/footer');
        }
    }

    public function Register() {
        if (isset($_REQUEST) && !empty($_REQUEST)) {
            if ($_REQUEST['type'] == 1) {
                $email = $_REQUEST['ind_email'];
                $phone = $_REQUEST['ind_phone'];
                $check_email = "SELECT * FROM `register` WHERE  `email` LIKE '" . $email . "' AND type=1";
                $sql = $this->Database->select_qry_array($check_email);
                if (count($sql) <= 0) {
                    $qry = "SELECT discount FROM `auto_discount` WHERE archive=0 AND register_type=1";
                    $discount = $this->Database->select_qry_array($qry);
                    $code = $discount[0]->discount;
                    if (!empty($discount)) {
                        $code = $discount[0]->discount;
                    } else {
                        $code = '';
                    }
                    $activationcode = substr(md5(uniqid(rand(), true)), 6, 6);
                    $custid = rand(1000, 9999);
                    $Insert = array(
                        'type' => 1,
                        'customerId' => "Ind" . $custid,
                        'first_name' => $_REQUEST['ind_fname'],
                        'last_name' => $_REQUEST['ind_lname'],
                        'email' => $email,
                        'mobile' => $phone,
                        'password' => md5($_REQUEST['ind_password']),
                        'encr_password' => base64_encode($_REQUEST['ind_password']),
                        'archive' => 0,
                        'is_verify' => 0,
                        'activation_code' => $activationcode,
                        'discount_code' => $code,
                        'timestamp' => date('Y-m-d H:i:s'),
                        'lastupdate' => date('Y-m-d H:i:s')
                    );
                    $result = $this->Database->insert('register', $Insert);
                    if ($result) {
                        // $otp = rand(1000, 9999);
                        // $result = array('OTP' => $otp);
                        // $messagqwe = "your verification code is " . $otp;
                        // send_sms($phonecode . $phone, $messagqwe);
                        // send_sms($phone, $messagqwe);
                        $discountMsg = "Your discount code is" . $code;
                        send_sms($phone, $discountMsg);
                        send_activation_mail($Insert);
                        addNotifaction($result, NEW_USER_NOTIFICATION);
                        $Insert['id'] = $result;
                        $return = array('status' => 'success', 'message' => 'An activation mail has been sent to you registered email. Please vist the link to activate your account');
                        exit(json_encode($return));
                    } else {
                        $return = array('status' => 'success', 'message' => 'Something went wrong');
                        exit(json_encode($return));
                    }
                } else {
                    $return = array('status' => 'success', 'message' => 'Email already exist');
                    exit(json_encode($return));
                }
                exit(json_encode($return));
            } else if ($_REQUEST['type'] == 3) {
                $email = $_REQUEST['bus_email'];
                $phone = $_REQUEST['bus_phone'];
                $check_email = "SELECT * FROM `register` WHERE  `email` LIKE '" . $email . "' AND type=3";
                $sql = $this->Database->select_qry_array($check_email);
                if (count($sql) <= 0) {
                    $qry = "SELECT discount FROM `auto_discount` WHERE archive=0 AND register_type=3";
                    $discount = $this->Database->select_qry_array($qry);
                    if (!empty($discount)) {
                        $code = $discount[0]->discount;
                    } else {
                        $code = '';
                    }
                    $activationcode = substr(md5(uniqid(rand(), true)), 6, 6);
                    $custid = rand(1000, 9999);
                    $Insert = array(
                        'type' => 3,
                        'customerId' => "Com" . $custid,
                        'first_name' => $_REQUEST['bus_fname'],
                        'last_name' => $_REQUEST['bus_lname'],
                        'email' => $_REQUEST['bus_email'],
                        'mobile' => $phone,
                        'password' => md5($_REQUEST['bus_password']),
                        'encr_password' => base64_encode($_REQUEST['bus_password']),
                        'archive' => 0,
                        'is_verify' => 0,
                        'activation_code' => $activationcode,
                        'discount_code' => $code,
                        'timestamp' => date('Y-m-d H:i:s'),
                        'lastupdate' => date('Y-m-d H:i:s')
                    );
                    $result = $this->Database->insert('register', $Insert);
                    if ($result) {
                        // $otp = rand(1000, 9999);
                        // $result = array('OTP' => $otp);
                        // $messagqwe = "your verification code is " . $otp;
                        // send_sms($phonecode . $phone, $messagqwe);
                        // send_sms($phone, $messagqwe);
                        $discountMsg = "Your discount code is" . $code;
                        send_sms($phone, $discountMsg);
                        send_activation_mail($Insert);
                        addNotifaction($result, NEW_USER_NOTIFICATION);
                        $Insert['id'] = $result;
                        $return = array('status' => 'success', 'message' => 'An activation mail has been sent to you registered email. Please vist the link to activate your account');
                        exit(json_encode($return));
                    } else {
                        $return = array('status' => 'success', 'message' => 'Something went wrong');
                        exit(json_encode($return));
                    }
                } else {
                    $return = array('status' => 'success', 'message' => 'Email already exist');
                    exit(json_encode($return));
                }
                exit(json_encode($return));
            } else if ($_POST['type'] == 2) {
                $email = $_POST['comp_email'];
                $phone = $_POST['comp_phone'];
                $check_email = "SELECT * FROM `register` WHERE  `email` LIKE '" . $email . "' AND type=2";
                $sql = $this->Database->select_qry_array($check_email);
                if (count($sql) <= 0) {
                    $qry = "SELECT discount FROM `auto_discount` WHERE archive=0 AND register_type=1";
                    $discount = $this->Database->select_qry_array($qry);
                    $code = $discount[0]->discount;
                    if (!empty($discount)) {
                        $code = $discount[0]->discount;
                    } else {
                        $code = '';
                    }
                    $activationcode = substr(md5(uniqid(rand(), true)), 6, 6);
                    $custid = rand(1000, 9999);
                    $license = $this->uploadDocuments('comp_license');
                    $VAT = $this->uploadDocuments('comp_vat');
                    if (isset($license['upload_data']) && (isset($VAT['upload_data']))) {
                        $Insert = array(
                            'type' => 2,
                            'customerId' => "Busi" . $custid,
                            'first_name' => $_POST['comp_fname'],
                            'last_name' => $_POST['comp_lname'],
                            'email' => $_POST['comp_email'],
                            'mobile' => $_POST['comp_phone'],
                            'license' => $_FILES['comp_license']['name'],
                            'VAT' => $_FILES['comp_vat']['name'],
                            'password' => md5($_POST['comp_password']),
                            'encr_password' => base64_encode($_REQUEST['comp_password']),
                            'archive' => 0,
                            'is_verify' => 0,
                            'activation_code' => $activationcode,
                            'discount_code' => $code,
                            'timestamp' => date('Y-m-d H:i:s'),
                            'lastupdate' => date('Y-m-d H:i:s')
                        );
                        $result = $this->Database->insert('register', $Insert);
                        if ($result) {
                            // $otp = rand(1000, 9999);
                            // $result = array('OTP' => $otp);
                            // $messagqwe = "your verification code is " . $otp;
                            // send_sms($phonecode . $phone, $messagqwe);
                            // send_sms($phone, $messagqwe);
                            $discountMsg = "Your discount code is" . $code;
                            send_sms($phone, $discountMsg);
                            send_activation_mail($Insert);
                            addNotifaction($result, NEW_USER_NOTIFICATION);
                            $Insert['id'] = $result;
                            $return = array('status' => 'success', 'message' => 'An activation mail has been sent to you registered email. Please vist the link to activate your account');
                            exit(json_encode($return));
                        } else {
                            $return = array('status' => 'success', 'message' => 'Something went wrong');
                            exit(json_encode($return));
                        }
                    } else {
                        $return = array('status' => 'success', 'message' => 'Something went wrong');
                        exit(json_encode($return));
                    }
                } else {
                    $return = array('status' => 'success', 'message' => 'Email already exist');
                    exit(json_encode($return));
                }
                exit(json_encode($return));
            }
        }
    }

    public function login() {
        if (isset($_REQUEST['email']) && $_REQUEST['password']) {
            $email = $_REQUEST['email'];
            $password = $_REQUEST['password'];
            $remember = $_REQUEST['remember'];
            $md5_password = md5($password);
            $qry = "SELECT * FROM `register` WHERE `email` LIKE '$email' AND `password` LIKE '$md5_password' AND archive =0";
            $login_array = $this->Database->select_qry_array($qry);
            if (count($login_array) > 0) { //
                if ($login_array[0]->is_verify == 1) {
                    $this->session->unset_userdata('UserLogin');
                    $this->session->set_userdata('UserLogin', $login_array[0]);
                    if ($_REQUEST['remember'] == true) {
                        $this->load->helper('cookie');
                        set_cookie('user_login', $_REQUEST['email'], time() + (10 * 365 * 24 * 60 * 60));
                        set_cookie('userpassword', $_REQUEST['password'], time() + (10 * 365 * 24 * 60 * 60));
                        //setcookie("user_login",$_REQUEST['email'],time()+ (10 * 365 * 24 * 60 * 60));
                        //setcookie("userpassword",$_REQUEST['password'],time()+ (10 * 365 * 24 * 60 * 60));
                        //print_r($_COOKIE);
                    } else {
                        if (isset($_COOKIE["user_login"])) {
                            setcookie("user_login", "");
                            if (isset($_COOKIE["userpassword"])) {
                                setcookie("userpassword", "");
                            }
                        }
                    }
                    $json = '{"status":"success","message":"Login Successfully"}';
                } else {
                    $json = '{"status":"error","message":"Your account is not activated. Please verify your email id first."}';
                }
            } else {
                $json = '{"status":"error","message":"Invalid email or password"}';
            }
        } else {
            $json = '{"status":"error","message":"Email and password is required"}';
        }
        echo $json;
    }

    public function logout() {
        $this->session->unset_userdata('UserLogin');
        // session_destroy();
        redirect(base_url('/'));
    }

    public function activateUserAccount() {
        $emailcode = $_GET['a'];
        $emailarray = explode('_', $emailcode);
        $email = base64_decode($emailarray[0]);
        $activationCode = base64_decode($emailarray[1]);
        $check_email = "SELECT * FROM `register`  WHERE  `email` LIKE '" . $email . "' AND activation_code LIKE '" . $activationCode . "'";
        $sql = $this->Database->select_qry_array($check_email);
        $mailArray = $sql[0];
        if (!empty($sql)) {
            if ($sql[0]->is_verify == 0) {
                $CondArray = array('id' => $sql[0]->id);
                $array['is_verify'] = '1';
                $result = $this->Database->update('register', $array, $CondArray);
                $wallet = array(
                    'user_id' => $sql[0]->id,
                    'account_balance' => 0,
                    'archive' => 0,
                    'lastupdate' => date('Y-m-d H:i:s'),
                    'timestamp' => date('Y-m-d H:i:s')
                );
                $this->Database->insert('my_wallet', $wallet);
                $qry1 = "SELECT discount FROM `auto_discount` WHERE archive=0 AND register_type=$mailArray->type";
                $discount = $this->Database->select_qry_array($qry1);
                $code = $discount[0];
                send_email_for_registration($mailArray, $code);
                $return = array('status' => 'success', 'message' => 'Your account has been activated and the registration is now complete.');
            } else {
                $return = array('status' => 'error', 'message' => 'This account is already active.');
            }
        } else {
            $return = array('status' => 'error', 'message' => 'This account does not exist.');
        }
        $this->data['return'] = $return;
        $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'Home', $this->data);
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    public function saveSenderAddress() {
        if (isset($_REQUEST) && !empty($_REQUEST)) {
            $session = GetSessionArrayLogin();
            $senderAddressId = 0;
            if (!empty($_REQUEST['id'])) {
                $addressId = $_REQUEST['id'];
                $cond = array('id' => $addressId);
                $Insert = array(
                    'full_name' => $_REQUEST['full_name'],
                    'mobile' => $_REQUEST['phone'],
                    'country' => $_REQUEST['country'],
                    'city' => $_REQUEST['city'],
                    'lastupdate' => date('Y-m-d H:i:s')
                );
                $senderAddressId = $this->Database->update('sender_address', $Insert, $cond);
            } else {
                $Insert = array(
                    'user_id' => $session->id,
                    'full_name' => $_REQUEST['full_name'],
                    'mobile' => $_REQUEST['phone'],
                    'country' => $_REQUEST['country'],
                    'city' => $_REQUEST['city'],
                    'archive' => 0,
                    'timestamp' => date('Y-m-d H:i:s'),
                    'lastupdate' => date('Y-m-d H:i:s')
                );
                $senderAddressId = $this->Database->insert('sender_address', $Insert);
            }
            if ($senderAddressId) {
                $return = array('status' => true, 'isLoged' => true, 'message' => 'Added successfully.', 'addressId' => $senderAddressId);
                exit(json_encode($return));
            } else {
                $return = array('status' => false, 'message' => 'Something went wrong');
                exit(json_encode($return));
            }
        } else {
            $return = array('status' => false, 'message' => 'Something went wrong');
            exit(json_encode($return));
        }
        exit(json_encode($return));
    }

    public function removeSenderAddress() {
        $senderAddressId = !empty($_REQUEST['addressId']) ? $_REQUEST['addressId'] : '';
        $update = array(
            'archive' => 1,
        );
        $condUpdate = array(
            'id' => $senderAddressId,
        );
        $this->Database->update('sender_address', $update, $condUpdate);
        die(json_encode(array('status' => true, 'isLoged' => true, 'message' => 'Address deleted successfully.')));
    }

    public function saveRecipientAddress() {
        if (isset($_REQUEST) && !empty($_REQUEST)) {
            $session = GetSessionArrayLogin();
            $senderAddressId = 0;
            if (!empty($_REQUEST['id'])) {
                $addressId = $_REQUEST['id'];
                $cond = array('id' => $addressId);
                $Insert = array(
                    'full_name' => $_REQUEST['full_name'],
                    'mobile' => $_REQUEST['phone'],
                    'country' => $_REQUEST['country'],
                    'city' => $_REQUEST['city'],
                    'lastupdate' => date('Y-m-d H:i:s')
                );
                $recipientAddressId = $this->Database->update('recipient_address', $Insert, $cond);
            } else {
                $Insert = array(
                    'user_id' => $session->id,
                    'full_name' => $_REQUEST['full_name'],
                    'mobile' => $_REQUEST['phone'],
                    'country' => $_REQUEST['country'],
                    'city' => $_REQUEST['city'],
                    'archive' => 0,
                    'timestamp' => date('Y-m-d H:i:s'),
                    'lastupdate' => date('Y-m-d H:i:s')
                );
                $recipientAddressId = $this->Database->insert('recipient_address', $Insert);
            }
            if ($recipientAddressId) {
                $return = array('status' => true, 'isLoged' => true, 'message' => 'Added successfully.', 'addressId' => $recipientAddressId);
                exit(json_encode($return));
            } else {
                $return = array('status' => false, 'message' => 'Something went wrong');
                exit(json_encode($return));
            }
        } else {
            $return = array('status' => false, 'message' => 'Something went wrong');
            exit(json_encode($return));
        }
        exit(json_encode($return));
    }

    public function removeRecipientAddress() {
        $recipientAddressId = !empty($_REQUEST['addressId']) ? $_REQUEST['addressId'] : '';
        $update = array(
            'archive' => 1,
        );
        $condUpdate = array(
            'id' => $recipientAddressId,
        );
        $this->Database->update('recipient_address', $update, $condUpdate);
        die(json_encode(array('status' => true, 'isLoged' => true, 'message' => 'Address deleted successfully.')));
    }

    public function UpdateWalletByCard() {
        if (isset($_POST)) {
            $amount = $_POST['amount'];
            $user_id = $_POST['user_id'];
            $request_no = '00' . $user_id . date('dmY');
            $transaction_no = 'TRN' . $user_id . date('dmY');
            $request = array(
                'user_id' => $user_id,
                'request_type' => 1,
                'recharge_amount' => $amount,
                'lastupdate' => date('Y-m-d H:i:s'),
                'timestamp' => date('Y-m-d H:i:s')
            );
            $reqResp = $this->Database->insert('wallet_card_request', $request);

            $wallet = array(
                'user_id' => $user_id,
                'card_request_id' => $reqResp,
                'transaction_id' => $transaction_no,
                'order_id' => $request_no,
                'amount' => $amount,
                'payment_type' => 1,
                'lastupdate' => date('Y-m-d H:i:s'),
                'timestamp' => date('Y-m-d H:i:s')
            );
            $result = $this->Database->insert('wallet_transactions', $wallet);
            $walletResp = updateMyWallet($user_id, $amount);
            if ($walletResp) {
                $conduser = array('id' => $result);
                $payment['payment_status'] = 1;
                $resp = $this->Database->update('wallet_transactions', $payment, $conduser);
                if ($resp != 0) {
                    $return = array('status' => 'success', 'message' => 'Amount added to wallet');
                    exit(json_encode($return));
                } else {
                    $return = array('status' => 'failure', 'message' => 'Amount not added to wallet');
                    exit(json_encode($return));
                }
            } else {
                $return = array('status' => 'failure', 'message' => 'Amount not added to wallet');
                exit(json_encode($return));
            }
        } else {
            $return = array('status' => 'failure', 'message' => 'Something went wrong');
            exit(json_encode($return));
        }
    }

    public function UpdateWalletByTransfer() {
        if (isset($_POST)) {
            $amount = $_POST['transfer_amount'];
            $user_id = $_POST['userId'];
            $request_no = '00' . $user_id . date('dmY');
            $transaction_no = $_POST['transactionNum'];

            $config['upload_path'] = './uploads/Reciept';
            $config['allowed_types'] = 'gif|jpg|png|pdf';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('walletReciept')) {
                $data = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
            }
            if (isset($data['error'])) {
                $return = array('status' => 'failure', 'message' => $data['error']);
                exit(json_encode($return));
            } else {
                $filename = $data['upload_data'][file_name];
                $request = array(
                    'user_id' => $user_id,
                    'request_type' => 1,
                    'recharge_amount' => $amount,
                    'reciept' => $filename,
                    'transaction_num' => $transaction_no,
                    'lastupdate' => date('Y-m-d H:i:s'),
                    'timestamp' => date('Y-m-d H:i:s')
                );
                $reqResp = $this->Database->insert('wallet_request', $request);
                if ($reqResp) {
                    addNotifaction($reqResp, NEW_RECHARGE_REQUEST);
                    $return = array('status' => 'success', 'message' => 'Request sent successfully');
                    exit(json_encode($return));
                } else {
                    $return = array('status' => 'failure', 'message' => 'Something went wrong');
                    exit(json_encode($return));
                }
            }
        } else {
            $return = array('status' => 'failure', 'message' => 'Something went wrong');
            exit(json_encode($return));
        }
    }

    public function offerWalletByCard() {
        if (isset($_POST)) {
            $paid_amount = $_POST['paid_amount'];
            $get_amount = $_POST['get_amount'];
            $offer_id = $_POST['offer_id'];
            $user_id = $_POST['user_id'];
            $request_no = '00' . $user_id . date('dmY');
            $transaction_no = 'TRN' . $user_id . date('dmY');

            $request = array(
                'user_id' => $user_id,
                'offer_id' => $offer_id,
                'request_type' => 2,
                'recharge_amount' => $paid_amount,
                'lastupdate' => date('Y-m-d H:i:s'),
                'timestamp' => date('Y-m-d H:i:s')
            );
            $reqResp = $this->Database->insert('wallet_card_request', $request);

            $wallet = array(
                'user_id' => $user_id,
                'card_request_id' => $reqResp,
                'offer_id' => $offer_id,
                'transaction_id' => $transaction_no,
                'order_id' => $request_no,
                'amount' => $paid_amount,
                'payment_type' => 1,
                'lastupdate' => date('Y-m-d H:i:s'),
                'timestamp' => date('Y-m-d H:i:s')
            );
            $redeemOffer = array(
                'user_id' => $user_id,
                'offer_id' => $offer_id,
                'is_redeem' => 1,
                'lastupdate' => date('Y-m-d H:i:s'),
                'timestamp' => date('Y-m-d H:i:s')
            );
            $redeemResp = $this->Database->insert('redeemed_offers', $redeemOffer);
            $result = $this->Database->insert('wallet_transactions', $wallet);
            $walletResp = updateMyWallet($user_id, $get_amount);
            if ($walletResp) {
                $conduser = array('id' => $result);
                $payment['payment_status'] = 1;
                $resp = $this->Database->update('wallet_transactions', $payment, $conduser);
                if ($resp != 0) {
                    $return = array('status' => 'success', 'message' => 'Amount added to wallet');
                    exit(json_encode($return));
                } else {
                    $return = array('status' => 'failure', 'message' => 'Amount not added to wallet');
                    exit(json_encode($return));
                }
            } else {
                $return = array('status' => 'failure', 'message' => 'Amount not added to wallet');
                exit(json_encode($return));
            }
        } else {
            $return = array('status' => 'failure', 'message' => 'Something went wrong');
            exit(json_encode($return));
        }
    }

    public function offerWalletByTransfer() {
        if (isset($_POST)) {
            $paid_amount = $_POST['offer_paid_amount'];
            $get_amount = $_POST['offer_get_amount'];
            $offer_id = $_POST['offerId'];
            $user_id = $_POST['offer_userId'];
            $request_no = '00' . $user_id . date('dmY');
            $transaction_no = $_POST['offerTransactionNum'];

            $config['upload_path'] = './uploads/Reciept';
            $config['allowed_types'] = 'gif|jpg|png|pdf';

            $this->load->library('upload', $config);

            if (!$this->upload->do_upload('offerWalletReciept')) {
                $data = array('error' => $this->upload->display_errors());
            } else {
                $data = array('upload_data' => $this->upload->data());
            }

            if (isset($data['error'])) {
                $return = array('status' => 'failure', 'message' => $data['error']);
                exit(json_encode($return));
            } else {
                $filename = $data['upload_data'][file_name];
                $request = array(
                    'user_id' => $user_id,
                    'offer_id' => $offer_id,
                    'request_type' => 2,
                    'recharge_amount' => $paid_amount,
                    'reciept' => $filename,
                    'transaction_num' => $transaction_no,
                    'lastupdate' => date('Y-m-d H:i:s'),
                    'timestamp' => date('Y-m-d H:i:s')
                );
                $reqResp = $this->Database->insert('wallet_request', $request);
                if ($reqResp) {
                    addNotifaction($reqResp, NEW_RECHARGE_REQUEST);
                    $return = array('status' => 'success', 'message' => 'Request sent successfully');
                    exit(json_encode($return));
                } else {
                    $return = array('status' => 'failure', 'message' => 'Something went wrong');
                    exit(json_encode($return));
                }
            }
        } else {
            $return = array('status' => 'failure', 'message' => 'Something went wrong');
            exit(json_encode($return));
        }
    }

    public function documentSubmit() {




        $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'Shipping', $response);
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    function uploadDocuments($filename = '') {
        $config['upload_path'] = './uploads/Register';
        $config['allowed_types'] = 'gif|jpg|png|pdf';

        $this->load->library('upload', $config);

        if (!$this->upload->do_upload($filename)) {
            $error = array('error' => $this->upload->display_errors());
            return $error;
        } else {
            $data = array('upload_data' => $this->upload->data());
            return $data;
        }
    }

    function getValues() {

        $draw = $_POST['draw'];
        $row = $_POST['start'];
        $rowperpage = $_POST['length']; // Rows display per page
        $columnIndex = $_POST['order'][0]['column']; // Column index
        $columnName = $_POST['columns'][$columnIndex]['data']; // Column name
        $columnSortOrder = $_POST['order'][0]['dir']; // asc or desc
        $searchValue = mysqli_real_escape_string($con, $_POST['search']['value']); // Search value

        $data = array(
            "TRACKING_ID" => '<span><img src="<?= base_url() ?>css/images/icnMenu.png"></span>1234234',
            "STATUS" => 'Delivered',
            "RECIPIENT_NAME" => 'John',
            "RECIPIENT_COUNTRY" => 'AE',
            "RECIPIENT_CITY" => 'dubai',
            'SENDER_NAME' => 'John',
            'SENDER_COUNTRY' => 'John',
            'SENDER_CITY' => 'Dubai',
            'RECEIVED_BY' => 'John',
            'CHARGEABLE_WEIGHT' => '2',
        );

        $totalRecords = 2;
        $totalRecordwithFilter = 2;

        ## Response
        $response = array(
            "draw" => intval($draw),
            "iTotalRecords" => $totalRecords,
            "iTotalDisplayRecords" => $totalRecordwithFilter,
            "aaData" => $data
        );

        echo json_encode($response);
    }

    public function thankyou() {
        $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'thankyou');
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    public function test() {
      
        $this->load->library("dhl");
        $price = $this->dhl->calculaterate();
    }

    public function token() {
        $data = array("grant_type" => 'client_credentials', "client_id" => "l72465cfab683d4990a9238a00556d5773", 'client_secret' => '5c6fc48755034b87848829970eb27f98');
        $curl = curl_init("https://apis-sandbox.fedex.com/oauth/token");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);
        $access_token = !empty($response['access_token']) ? $response['access_token'] : '';
        return $access_token;
    }

    public function fedex_rates() {
        $curl = curl_init();
        $params = '{
  "accountNumber": {
    "value": "740561073"
  },
  "requestedShipment": {
    "shipper": {
      "address": {
        "postalCode": 65608,
        "countryCode": "AE"
      }
    },
    "recipient": {
      "address": {
        "postalCode": 682001,
        "countryCode": "IN"
      }
    },
    "shipDateStamp": "2021-12-06",
    "pickupType": "DROPOFF_AT_FEDEX_LOCATION",
    "serviceType": "INTERNATIONAL_PRIORITY",
    "shipmentSpecialServices": {
      "specialServiceTypes": [
        "RETURN_SHIPMENT"
      ],
      "returnShipmentDetail": {
        "returnType": "PRINT_RETURN_LABEL"
      }
    },
    "rateRequestType": [
      "LIST",
      "ACCOUNT"
    ],
    "customsClearanceDetail": {
      "dutiesPayment": {
        "paymentType": "SENDER",
        "payor": {}
      },
      "commodities": [
        {
          "description": "Camera",
          "quantity": 1,
          "quantityUnits": "PCS",
          "weight": {
            "units": "KG",
            "value": 5
          },
          "customsValue": {
            "amount": 100,
            "currency": "USD"
          }
        }
      ]
    },
    "requestedPackageLineItems": [
      {
        "weight": {
          "units": "KG",
          "value": 5
        }
      }
    ]
  }
}';
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apis-sandbox.fedex.com/rate/v1/rates/quotes",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $this->token(),
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 26ece168-66d4-6360-9470-aa9cffd34bf2",
                "x-locale: en_US"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        //echo "cURL Error #:" . $err;
        $response = json_decode($response, true);
        $price = !empty($response['output']['rateReplyDetails'][0]['ratedShipmentDetails'][0]['totalNetFedExCharge']) ? $response['output']['rateReplyDetails'][0]['ratedShipmentDetails'][0]['totalNetFedExCharge'] : '0';
        return $price;
    }

    public function dhl_rates() {
        getCommonSearchFormateAPI();
        exit;
        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://express.api.dhl.com/mydhlapi/test/rates?accountNumber=969277615&originCountryCode=AE&originPostalCode=65608&originCityName=Dubai&destinationCountryCode=IN&destinationPostalCode=682001&destinationCityName=Cochin&weight=5&length=15&width=10&height=5&plannedShippingDate=2021-12-05&isCustomsDeclarable=false&unitOfMeasurement=metric&nextBusinessDay=false",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic YXBYMHhGOGhaNXpTNGQ6QyE2cEojMGtOQDhz",
                "cache-control: no-cache",
                "postman-token: 57824cbe-db7e-dd4b-28a1-c8b88e6c4bf6"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
//            echo "cURL Error #:" . $err;
            return 0;
        } else {
            $response = json_decode($response, true);
            $price = !empty($response['products'][0]['totalPrice'][0]['price']) ? $response['products'][0]['totalPrice'][0]['price'] : '0';
            return $price;
        }
    }

}

?>