<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Login extends CI_Controller {

    private $segment2;
    private $segment3;
    private $segment4;

    public function __construct() {
        parent::__construct();
        date_default_timezone_set('Asia/Dubai');
        $this->load->Model('Database');
        $this->segment2 = $this->uri->segment(2);
        $this->segment3 = $this->uri->segment(3);
        $this->segment4 = $this->uri->segment(4);
    }

    public function Admin() {
        $this->load->view(ADMIN_DIR . 'Login');
    }

   public function CheckAdminLogin() {
       // echo 'testtt';exit;
        $json = array();
        if (isset($_REQUEST['UserName']) && !empty($_REQUEST['UserName'])) {
            $UserName = $_REQUEST['UserName'];
            $Password = $_REQUEST['Password'];
            $Md5Password = md5($Password);
            $Qry = "SELECT * FROM admin_login WHERE email=:email AND password=:password";
            $CondutaionArray = array('email' => $UserName, 'password' => $Md5Password);
            $UserArray = $this->Database->select_qry_array($Qry, $CondutaionArray);
            
            
            if (!empty($UserArray)) {
                $UserArray = $UserArray[0];
                if ($UserArray->archive == 0) {
                    $this->session->set_userdata('Admin', $UserArray);
                    $json = array('status' => 'success', 'RedirectURL' => base_url('Admin'), 'msg' => 'Login successfully');
                } else {
                    $json = array('status' => 'error', 'msg' => 'Your account has been blocked.');
                }
            } else {
                $json = array('status' => 'error', 'msg' => 'Invalid user name and password.'
                );
            }
        } else {
            $json = array('status' => 'error', 'msg' => 'Invalid credentials.'
            );
        }
        echo json_encode($json);
    }
    public function AdminLoginout() {
        $this->session->unset_userdata('Admin');
        redirect(base_url('Login/Admin'));
    }
    
    public function Users() {
        $this->load->view(USER_DIR . 'Login');
    }
    public function UserLoginout() {
        $this->session->unset_userdata('Users');
        redirect(base_url('Login/Users'));
    }
    public function CheckVendorLogin() {
        $json = array();
        if (isset($_REQUEST['UserName']) && !empty($_REQUEST['UserName']) && !empty($_REQUEST['Password'])) {
            $UserName = $_REQUEST['UserName'];
            $Password = $_REQUEST['Password'];
            $Md5Password = md5($Password);
            $Qry = "SELECT * FROM register WHERE email='$UserName' AND password='$Md5Password'";
            $CondutaionArray = array('email' => $UserName, 'password' => $Md5Password);
            $UserArray = $this->Database->select_qry_array($Qry);
            if (!empty($UserArray)) {
                $UserArray = $UserArray[0];
                if ($UserArray->archive == 0 && $UserArray->is_verify == 1) {
                    $this->session->set_userdata('Users', $UserArray);
                    $json = array('status' => 'success', 'RedirectURL' => base_url('user_dashboard'), 'msg' => 'Login successfully');
                } elseif ($UserArray->is_verify == 0) {
                    $json = array('status' => 'error', 'msg' => 'Please verify your account');
                } else {
                    $json = array('status' => 'error', 'msg' => 'Your account has been blocked.');
                }
            } else {
                $json = array('status' => 'error', 'msg' => 'Invalid user name and password.');
            }
        } else {
            $json = array('status' => 'error', 'msg' => 'Invalid credentials.'
            );
        }
        echo json_encode($json);
    }

    public function index() {
        if (isset($_REQUEST['UserName']) && $_REQUEST['UserPassword']) {
            $finalArray = array('status' => 'error', 'message' => 'Invalid user name and password');

            if (!empty($_REQUEST['UserName']) && !empty($_REQUEST['UserPassword'])) {
                $UserPassword = md5($_REQUEST['UserPassword']);
                $UserName = $_REQUEST['UserName'];
                $Mobile = substr($UserName, -9);
                $cond = '';
                if (is_numeric($Mobile)) {
                    $cond = " mobile_no='$Mobile'";
                } else {
                    $cond = " email='$UserName'";
                }
                $Qry = "SELECT * FROM `users_details` WHERE password='$UserPassword' AND $cond";
                $CheckUser = $this->Database->select_qry_array($Qry);
                if (count($CheckUser) > 0) {
                    $CheckUser = $CheckUser[0];
                    if ($CheckUser->archive == 0) {
                        $this->session->set_userdata('user', $CheckUser);
                        $finalArray = array('status' => 'success', 'message' => 'Login successfully');
                    } else {
                        $finalArray = array('status' => 'error', 'message' => 'your id has been blocked. Contact with admin');
                    }
                }
            }
            die(json_encode($finalArray));
        }
          $this->load->view(FRONTED_DIR . 'Includes/header');
        $this->load->view(FRONTED_DIR . 'FrontendLogin');
        $this->load->view(FRONTED_DIR . 'Includes/footer');
    }

    public function Signup() {
        $this->load->view('FrontendSignup');
    }

    public function FrontendSignup() {
        $finaljson = array('status' => 'error', 'message' => 'Invalid URL');
        if (isset($_REQUEST['UserName']) && $_REQUEST['UserEmail']) {
            $Email = $_REQUEST['UserEmail'];
            $Qry = "SELECT * FROM `users_details` WHERE `email` =:email";
            $CondutaionArray = array('email' => $_REQUEST['UserEmail']);
            $CheckEmail = $this->Database->select_qry_array($Qry, $CondutaionArray);
            if (count($CheckEmail) == 0) {
                $InsertArray = array(
                    'user_name' => $_REQUEST['UserName'],
                    'email' => $Email,
                    'password' => md5($_REQUEST['UserPassword']),
                    'mobile_no' => $_REQUEST['UserMobile'],
                    'mobile_code' => $_REQUEST['UserMobileCode'],
                );
                $Result = $this->Database->insert('users_details', $InsertArray);
                $_REQUEST['UserName'] = $Email;
                $this->index();
//                if ($Result) {
//                    $finaljson = array('status' => 'success', 'message' => 'Register successfully');
//                }
            } else {
                $finaljson = array('status' => 'error', 'message' => 'Your email id is already registered.please login');
            }
        }
        die(json_encode($finaljson));
    }

    public function SendRegistrationOTP() {
        if (isset($_REQUEST['UserName']) && $_REQUEST['UserEmail']) {
            $UserEmail = $_REQUEST['UserEmail'];
            $UserMobile = $_REQUEST['UserMobile'];
            $COndMob = '';
            if (is_numeric($UserMobile)) {
                $COndMob = "OR mobile_no='$UserMobile'";
            }
            $Qry = "SELECT * FROM `users_details` WHERE (`email` LIKE '$UserEmail' $COndMob)";
            $CheckEmail = $this->Database->select_qry_array($Qry);
            if (count($CheckEmail) == 0) {
                $_REQUEST['OTP'] = mt_rand(1000, 9999);
                $mobile = $_REQUEST['UserMobileCode'] . $_REQUEST['UserMobile'];
                $msg = 'Dear ' . $_REQUEST['UserName'] . ',<br>' . 'You have one new otp: ' . $_REQUEST['OTP'];
                $msg = EmailUseTemplate($msg);
                $SmsMessage = "Your Gate onetime password is: " . $_REQUEST['OTP'];
                send_sms($mobile, $SmsMessage);
                $return = array(
                    'status' => 'success',
                    'message' => 'A one time password has been sent to this number.',
                    'OTP' => $_REQUEST['OTP'] + 987,
                );
                send_mail($UserEmail, 'Gate one time password', $msg);
            } else {
                $CheckEmail = $CheckEmail[0];
                if ($CheckEmail->social_media_type == 1) {
                    $message = 'Your email id or phone is already registered with Facebook social media.Try to login with social media';
                } else if ($CheckEmail->social_media_type == 2) {
                    $message = 'Your email id or phone is already registered with Google.Try to login with social media';
                } else if ($CheckEmail->social_media_type == 3) {
                    $message = 'Your email id or phone is already registered with Twitter.Try to login with social media';
                } else {
                    $message = 'Your email or mobile is already registered.Try to login';
                }
                $return = array('status' => 'error', 'message' => $message);
            }
            echo json_encode($return);
        }
    }

    public function FroentForgotPassword() {
        if (isset($_REQUEST['EmailId'])) {
            $EmailId = $_REQUEST['EmailId'];
            $Phone = substr($EmailId, -9);
            $COndMob = '';
            if (is_numeric($Phone)) {
                $COndMob = "OR mobile_no='$Phone'";
            }
            $Qry = "SELECT * FROM `users_details` WHERE (`email` ='$EmailId' $COndMob)";
            $CheckEmail = $this->Database->select_qry_array($Qry);
            $final_array = array();
            if (count($CheckEmail) > 0) {
                $CheckEmail[0]->OTP = mt_rand(0000, 9999);
                $Mobile = $CheckEmail[0]->mobile_code . $CheckEmail[0]->mobile_no;
                $msg = 'Dear ' . $CheckEmail[0]->user_name . ',<br>' . 'Your one time password: ' . $CheckEmail[0]->OTP;
                $msg = EmailUseTemplate($msg);
                $SmsMessage = "Your Gate onetime password is: " . $CheckEmail[0]->OTP;
                send_sms($Mobile, $SmsMessage);
                $subject = 'Gate OTP';
                send_mail($EmailId, $subject, $msg);
                $forgot_password_ses = array(
                    'OTP' => $CheckEmail[0]->OTP,
                    'Email Id' => $EmailId,
                    'UserId' => $CheckEmail[0]->id,
                );
                $this->session->set_userdata('forgot_password', $forgot_password_ses);

                $final_array = array('status' => 'success', 'message' => is_numeric($EmailId) ? "You got the one OTP to this mobile number." : "You got the one OTP to this email id.");
            } else {
                $final_array = array('status' => 'error', 'message' => is_numeric($EmailId) ? " Mobile number doesn't exists." : "Email id doesn't exists.");
            }
            die(json_encode($final_array));
        }
    }

    public function frontend_forgot_password() {
        $SessionArray = GetSessionForgotPasswordUser();
        if (empty($SessionArray)) {
            redirect(base_url('Login'));
        }
        $data['dataArray'] = $SessionArray;
        $this->load->view('frontend_forgot_password', $data);
    }

    public function ERROR_404() {
        $data = array();
        $data['URL'] = base_url();
        $CurrentController = $this->router->fetch_class();
        if ($CurrentController == 'Admin') {
            $data['URL'] = base_url('Admin');
        }
        $this->load->view(ADMIN_DIR . '404', $data);
    }

    public function Google() {
        $this->load->library('Google');
        $this->google->Login();
    }

    public function GoogleLoginDetails() {
        if (isset($_GET['code'])) {
            $Code = $_GET['code'];
            $this->load->library('Google');
            $Array = $this->google->UserInfo($Code);
            if (!empty($Array)) {
                $this->CheckSocialMedia($Array);
            }
        } else {
            $this->Google();
        }
    }

    public function Facebook() {
        $this->load->library('Facebook');
        $this->facebook->Login();
    }

    public function FacebookLoginDetails() {
        if (isset($_GET['code'])) {
            $this->load->library('Facebook');
            $state = '';
            if (isset($_GET['state'])) {
                $state = $_GET['state'];
            }
            $UserInfo = $this->facebook->UserInfo($state);
            if (is_array($UserInfo)) {
                $this->CheckSocialMedia($UserInfo);
            }
        }
    }

    public function Twitter() {
        $this->load->library('Twitter');
        $this->twitter->Login();
    }

    public function TwitterDetails() {
        if (isset($_GET['oauth_token']) && $_GET['oauth_verifier']) {
            $this->load->library('Twitter');
            $oauth_token = $_GET['oauth_token'];
            $oauth_verifier = $_GET['oauth_verifier'];
            $UserInfo = $this->twitter->UserInfo($oauth_token, $oauth_verifier);
            if (is_array($UserInfo)) {
                $this->CheckSocialMedia($UserInfo);
            }
        } else {
            $this->Twitter();
        }
    }

    public function CheckSocialMedia($Array) {
        if (isset($Array['SocialType']) && isset($Array['AccountId'])) {
            $Qry = "SELECT * FROM `users_details` WHERE `social_media_id` =:social_media_id AND social_media_type=:social_media_type";
            $CondutaionArray = array('social_media_id' => $Array['AccountId'], 'social_media_type' => $Array['SocialType']);
            $CheckUser = $this->Database->select_qry_array($Qry, $CondutaionArray);
            if ($Array['UserProfilePic'] != '') {
                $ImageName = $Array['AccountId'] . '.jpg';
                $ProfilePic = HOME_DIR . 'files/UserProfilePictures/' . $ImageName;
                if (is_file($ProfilePic)) {
                    unlink($ProfilePic);
                }
                if (count($CheckUser) > 0) {
                    $OldPic = HOME_DIR . 'files/UserProfilePictures/' . $CheckUser[0]->image_url;
                    if (is_file($OldPic)) {
                        unlink($OldPic);
                    }
                }
                if (file_put_contents($ProfilePic, file_get_contents($Array['UserProfilePic']))) {
                    
                }
            }
            if (count($CheckUser) == 0) {
                if ($Array['Email'] == '') {
                    die(json_encode(array('status' => 'error', 'message' => 'Email id not exists')));
                }
                $Qry = "SELECT * FROM `users_details` WHERE `email` =:email";
                $CondutaionArray = array('email' => $Array['Email']);
                $CheckEmail = $this->Database->select_qry_array($Qry, $CondutaionArray);
                if (count($CheckEmail) == 0) {
                    $InsertArray = array(
                        'user_name' => $Array['AccountName'],
                        'email' => $Array['Email'],
                        'social_media_id' => $Array['AccountId'],
                        'social_media_type' => $Array['SocialType'],
                        'birthday' => date('Y-m-d', strtotime($Array['Birthday'])),
                        'gender' => ucfirst($Array['Gender']) == 'Male' ? 1 : 2,
                        'mobile_no' => 0,
                        'mobile_code' => 0,
                        'lastupdate' => date('Y-m-d H:i:s'),
                    );
                    $UserId = $this->Database->insert('users_details', $InsertArray);
                } else {
                    $UserId = $CheckEmail[0]->id;
                }
            } else {
                $UserId = $CheckUser[0]->id;
            }
            $InsertArray = array(
                'social_media_id' => $Array['AccountId'],
                'social_media_type' => $Array['SocialType'],
            );
            if (count($CheckUser) > 0) {
                if ($Array['Birthday'] != '') {
                    $InsertArray['birthday'] = date('Y-m-d', strtotime($Array['Birthday']));
                }if ($Array['Gender'] != '' && $CheckUser[0]->gender != '') {
                    $InsertArray['gender'] = ucfirst($Array['Gender']) == 'Male' ? 1 : 2;
                }
            }
            if (isset($ImageName)) {
                $InsertArray['image_url'] = $ImageName;
            }
            $CondArray = array('id' => $UserId);
            $this->Database->update('users_details', $InsertArray, $CondArray);
            if (!isset($UserId) || !is_numeric($UserId)) {
                die(json_encode(array('status' => 'error', 'message' => 'User Id Issue')));
            }
            $Qry = "SELECT * FROM `users_details` WHERE `id` =:id";
            $CondutaionArray = array('id' => $UserId);
            $CheckUser = $this->Database->select_qry_array($Qry, $CondutaionArray);
            $CheckUser = $CheckUser[0];
            if ($CheckUser->archive == 0) {
                $this->session->set_userdata('user', $CheckUser);
                $AdditionalSession = GetAdditionalSession();
                if (isset($AdditionalSession['CallBackURL']) && $AdditionalSession['CallBackURL'] != '') {
                    $Unnset = ['CallBackURL'];
                    UnsetAdditionalSession($Unnset);
                    header('Location: ' . filter_var($AdditionalSession['CallBackURL'], FILTER_SANITIZE_URL));
                } else {
                    header('Location: ' . filter_var(base_url(), FILTER_SANITIZE_URL));
                }
            } else if ($CheckUser->archive == 1) {
                die(json_encode(array('status' => false, 'message' => 'Your account has been blocked by Gate admin. contact with admin')));
            }
        }
    }

    public function Logout() {
        $this->session->unset_userdata('user');
        redirect(base_url());
    }

}
