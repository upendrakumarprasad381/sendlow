<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Services extends CI_Controller {

    public $login_type = '';
    public $success = 200;
    public $error = 401;

    public function __construct() {
        parent::__construct();
        $Header = $this->input->request_headers();
        date_default_timezone_set('Asia/Dubai');
        $this->load->Model('Database');
        $User = isset($Header['User-Agents']) ? $Header['User-Agents'] : '';
        $Authtoken = isset($Header['Authtoken']) ? $Header['Authtoken'] : '';
        $LanguageCode = isset($Header['Language-Code']) ? $Header['Language-Code'] : '';
        $this->language = strtolower($LanguageCode) == 'ar' ? 'AR' : 'EN';

        $_REQUEST['languagecode'] = $this->language;
        if ($User != 'com.cargarage.com' || $Authtoken != 'Alwafaa!1234') {
            header('Content-Type: application/json');
            die(json_encode(array("response" => array("status" => false, "message" => "Auth Failed"))));
        }
        
        if ($this->language == 'AR') {
            $this->load->language("common", "app_arabic");
        } else {
            $this->load->language("common", "app_englist");
        }
        $method = $this->router->fetch_method();
        $this->login_type = !empty($Header['Logintype']) ? $Header['Logintype'] : 0;
        //   send_mail('upendra@alwafaagroup.com', "power group - $method", 'ff');
        header('Content-Type: application/json');
    }

    public function login() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $today = date('Y-m-d H:i:s');
        $email = !empty($json['email']) ? $json['email'] : '';
        $password = !empty($json['password']) ? $json['password'] : '';
        $password = md5($password);
        if (empty($email)) {

            die(json_encode(array("code" => $this->error, "response" => array("status" => false, "message" => $this->lang->line("Please_enter_a_valid_email")))));
        }
$mob =   substr($email, 1);

        $qry = "SELECT * FROM `users` WHERE (email LIKE '$email' or mobile='$mob') AND password='$password' ";
        $Array = $this->Database->select_qry_array($qry);
        if (count($Array) > 1 || empty($Array)) {

            die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("Invalid_username_and_pswrd")))));
        }

        $InsertArray = array(
            'device_id' => $json['device_id'],
            'lastupdate' => $today,
        );
        $Array = $Array[0];
        $customerId = $Array->id;
        $cond = array('id' => $customerId);
        $this->Database->update('users', $InsertArray, $cond);
        $cArray = GetuserdetailsBy($customerId);
        $result['user_details'] = empty($cArray) ? new stdClass() : $cArray;

        die(json_encode(array("code" => $this->success, "response" => array("status" => true,"message" => $this->lang->line("successfully_Login")), 'result' => $result)));
    }

 public function logout() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $customerId = !empty($json['user_id']) ? $json['user_id'] : '';
        $InsertArray = array(
            'device_id' => '',
            'lastupdate' => date('Y-m-d H:i:s'),
        );
        $cond = array('id' => $customerId);
        $this->Database->update('users', $InsertArray, $cond);
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Logout_successfully")))));
    }

    public function user_details() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $customerId = !empty($json['customer_id']) ? $json['customer_id'] : '';
        $cArray = GetCustomerArrayByIdAPIRespone($customerId);
        $result['customer'] = empty($cArray) ? new stdClass() : $cArray;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>  $this->lang->line("success")), 'result' => $result)));
    }



    public function register() {
        $json = file_get_contents('php://input');
    //    send_mail('upendra@alwafaagroup.com,navaranjitha@alwafaagroup.com', "power group -register ", json_encode($json));
        // send_mail('navaranjitha@alwafaagroup.com', 'response', json_encode($json));
        $json = json_decode($json, TRUE);
        
        // $myfile = fopen("register.txt", "a") or die("Unable to open file!");
        // $date = new DateTime();
        // $date = $date->format("y:m:d h:i:s");
        
        // fwrite($myfile,$date);
        // fwrite($myfile,'-------------------------------');
        // fwrite($myfile,$json);
        // fclose($myfile);

        if (!is_array($json)) {
            $json = json_decode($json, true);
        }
        $today = date('Y-m-d H:i:s');
        $phone = !empty($json['mobile_number']) ? $json['mobile_number'] : '';
        $phonecode = !empty($json['mobile_code']) ? $json['mobile_code'] : '';
        //   $phonecode = !empty($json['mobile_code']) ? str_replace('+', '', $json['mobile_code']) : '';
        $email = !empty($json['email']) ? $json['email'] : '';
        //password validation
        $password = !empty($json['password']) ? $json['password'] : '';


        $qry = "SELECT * FROM `users` WHERE email LIKE '$email' and archive='0' ";
        $Array = $this->Database->select_qry_array($qry);
        if (!empty($Array)) {
            die(json_encode(array("code" => $this->success, "response" => array("status" => false,"message" => $this->lang->line("already_register_this_email")))));
        }
        $qry1 = "SELECT * FROM `admin_login` WHERE email LIKE '$email' and archive='0'  ";
        $Array1 = $this->Database->select_qry_array($qry1);
        if (!empty($Array1)) {
            die(json_encode(array("code" => $this->success, "response" => array("status" => false,"message" => $this->lang->line("already_register_this_email")))));
        }
        
        $qry = "SELECT * FROM `admin_login` WHERE email LIKE '$email'  ";
        $Array = $this->Database->select_qry_array($qry);
        if (!empty($Array)) {
            die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("already_register_this_email")))));
        }



        $isVerify = !empty($json['isVerify']) ? $json['isVerify'] : '';
        if (empty($isVerify)) {
            $otp = rand(10000, 99999);
            $mailHtml = "<p>To authenticate, please use the following..OTP: $otp</p>";
            $mailHtml = $mailHtml . "<p>Do not share this OTP with anyone.</p>";
            //$mailHtml = emailTemplate($mailHtml);
           // send_mail($email, 'Fix Your Car', $mailHtml);
             $otp = '0000';
            $result = array('OTP' => $otp);
            // $otp = rand(1000, 9999);
            // $result = array('OTP' => (string) $otp);
            // $messagqwe = "Your verification code is " . $otp;

            // send_sms($phonecode . $phone, $messagqwe);
          
            $array['otp'] = $otp;
            $array['name'] = $json['full_name'];
            $array['email'] = $email;
            send_email_for_otp($array);
            die(json_encode(array("response" => array("status" => true, "message" =>  $this->lang->line("otp_send_successfully")), 'result' => $result)));
        } else {
            $full_name = !empty($json['full_name']);
            $Inst['name'] = !empty($json['full_name']) ? $json['full_name'] : '';
            $Inst['email'] = $email;
            $Inst['password'] = !empty($json['password']) ? md5($json['password']) : '';
            $Inst['mobile_code'] = $phonecode;
            $Inst['mobile'] = $phone;
            $Inst['device_id'] = !empty($json['device_id']) ? $json['device_id'] : '';

            $Inst['is_verify'] = $isVerify;
            $Inst['lastupdate'] = $today;
            $Inst['timestamp'] = $today;
            $pass= !empty($json['password']) ? $json['password'] : '';

            $customerId = $this->Database->insert('users', $Inst);
            if(!empty($customerId))
            {
                send_register_mail($customerId,$pass);
                send_admin_register_mail($customerId,'1');
            }

            $cArray = GetuserdetailsBy($customerId);
            $result['customer'] = empty($cArray) ? new stdClass() : $cArray;
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_registered")), 'result' => $result)));
        }
    }

    public function Category_list() {

        $qry = "SELECT * FROM `category_details` where archive='0'  and status='0' ORDER BY order_view ASC";
        $cArray = $this->Database->select_qry_array($qry);
        
        $return = [];$special_off=[];$count_n=0;
        $qry1 = "SELECT * FROM `offer_banner`  where archive='0' and status='0' ORDER BY id DESC";
        $cArray1 = $this->Database->select_qry_array($qry1);
        $offerimage = [];
        for ($k = 0; $k < count($cArray1); $k++) {
            $dryr = $cArray1[$k];
           
            $offerimage[$k] = base_url() . 'uploads/offer_banner/' .$dryr->image;
        }
        for ($i = 0; $i < count($cArray); $i++) {
            $dft = $cArray[$i];
            $image = !empty($dft->category_image) ?$dft->category_image:'';
            $image_array=explode(',',$image); $image_url=[];
            for($j=0;$j<count($image_array);$j++)
            {
                $image_url[$j]=!empty($image_array[$j]) ? base_url() . "files/category_image/" . $image_array[$j]: '';
            }
            $return[$i] = array(
                'category_id' => $dft->category_id,
                'category_name' => $this->language == 'AR' ?!empty($dft->category_ar)?$dft->category_ar :$dft->category: $dft->category ,
                'description' => $this->language == 'AR' ? !empty($dft->description_ar)?$dft->description_ar :$dft->description: $dft->description,
                'layout_type'=>isset($dft->layout_type)?(int)$dft->layout_type :'',
                'image_url' => !empty($image_url) ? $image_url: ''
                );
            if($i==count($cArray)-1)
            {
                $return[$i+1]=array(
                'category_id' => "0",
                'category_name' => $this->lang->line("special_offer") ,
                'description' => '',
                'layout_type'=>6,
                'image_url' => !empty($offerimage) ? $offerimage: ''
                );
            }
            }

             //   $result_merge = array_merge($return, $special_off);

        $qry1 = "SELECT * FROM `home_banner`  where archive='0' and status='0' ORDER BY id DESC";
        $cArray1 = $this->Database->select_qry_array($qry1);
        $bannerimage = [];
        for ($k = 0; $k < count($cArray1); $k++) {
            $dryr = $cArray1[$k];
           
            $bannerimage[$k] = array('image_url'=>base_url() . 'uploads/home_banner/' .$dryr->image,
            'url'=>!empty($dryr->url)?$dryr->url:""
            );
        }

        $result['banner_image'] = $bannerimage;
        $result['category'] = $return;

        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

   


   public function getEmirates() {
         $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $garage_type = !empty($json['garage_type']) ? $json['garage_type'] : '';
        $qry = "SELECT * FROM `emirates` where archive='0'";
        $cArray = $this->Database->select_qry_array($qry);
$return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $get_garages=get_garages_emirate($dft->id,$garage_type);
            $count_garages = isset($get_garages->count_garage)?($get_garages->count_garage):'0';
            $return[$i] = array(
                'id' => $dft->id,
                'emirate' => $this->language == 'AR' ?!empty($dft->emirate_ar)?$dft->emirate_ar :$dft->emirate: $dft->emirate ,
                'garage_count'=>$count_garages
                );
        
            }
        $result['Emirates'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>  $this->lang->line("success")), 'result' => $result)));
    }
     public function getRegion() {
          $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $emirate_id = !empty($json['emirate_id']) ? $json['emirate_id'] : '';
                $garage_type = isset($json['garage_type']) ? $json['garage_type'] : '';

        $qry = "SELECT * FROM `regions` where archive='0' and emirate_id='".$emirate_id."'";
        $cArray = $this->Database->select_qry_array($qry);
        $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
             $get_garages=get_garages_region($dft->id,$garage_type);
            $count_garages = !empty($get_garages->count_garage)?($get_garages->count_garage):'0';
            $return[$i] = array(
                'id' => $dft->id,
                'region' => $this->language == 'AR' ?!empty($dft->region_ar)?$dft->region_ar :$dft->region: $dft->region ,
                'emirate'=>$dft->emirate_id,
                'garage_count'=>$count_garages

                );
        
            }
        $result['Region'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>  $this->lang->line("success")), 'result' => $result)));
    }
    
 public function getYear() {
        $qry = "SELECT * FROM `year` where archive='0' order by year desc";
        $cArray = $this->Database->select_qry_array($qry);
        $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $return[$i] = array(
                'id' => $dft->id,
                'year' => $this->language == 'AR' ?!empty($dft->year_ar)?$dft->year_ar :$dft->year: $dft->year ,
                );
        
            }
        $result['year'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>  $this->lang->line("success")), 'result' => $result)));
    }
    public function getVehicleMake() {
        $qry = "SELECT * FROM `make` where archive='0' order by make asc";
        $cArray = $this->Database->select_qry_array($qry);
        $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $return[$i] = array(
                'id' => $dft->id,
                'make' => $this->language == 'AR' ?!empty($dft->make_ar)?$dft->make_ar :$dft->make: $dft->make ,
                'make_icon'=>!empty($dft->make_icon) ? base_url() . "files/make_icon/" .$dft->make_icon: ''
                );
        
            }
        $result['make'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>  $this->lang->line("success")), 'result' => $result)));
    }
     public function getVehicleModel() {
          $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $make = !empty($json['make_id']) ? $json['make_id'] : '';
        $qry = "SELECT * FROM `model` where archive='0' and make='".$make."' order by model asc";
        $cArray = $this->Database->select_qry_array($qry);
         $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $return[$i] = array(
                'id' => $dft->id,
                'model' => $this->language == 'AR' ?!empty($dft->model_ar)?$dft->model_ar :$dft->model: $dft->model ,
                'make'=>$dft->make
                );
        
            }
        $result['model'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>  $this->lang->line("success")), 'result' => $result)));
    }
     public function getPaintMethod() {
        $qry = "SELECT * FROM `paint_method` where archive='0'";
        $cArray = $this->Database->select_qry_array($qry);
         $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $return[$i] = array(
                'id' => $dft->id,
                'paint_method' => $this->language == 'AR' ?!empty($dft->paint_method_ar)?$dft->paint_method_ar :$dft->paint_method: $dft->paint_method ,
                );
        
            }
        $result['paint_method'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>  $this->lang->line("success")), 'result' => $result)));
    }
     public function getPaints() {
        $qry = "SELECT * FROM `type_paints` where archive='0'";
        $cArray = $this->Database->select_qry_array($qry);
        $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $return[$i] = array(
                'id' => $dft->id,
                'type_paints' => $this->language == 'AR' ?!empty($dft->type_paints_ar)?$dft->type_paints_ar :$dft->type_paints: $dft->type_paints ,
                );
        
            }
        $result['type_paints'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>  $this->lang->line("success")), 'result' => $result)));
    }
     public function getPaintColor() {
        $qry = "SELECT * FROM `paint_color` where archive='0'";
        $cArray = $this->Database->select_qry_array($qry);
         $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $return[$i] = array(
                'id' => $dft->id,
                'paint_color_code'=>$dft->paint_color_code,
                'paint_color' => $this->language == 'AR' ?!empty($dft->paint_color_ar)?$dft->paint_color_ar :$dft->paint_color: $dft->paint_color ,
                );
        
            }
                    $result['paint_color'] = $return;

        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>  $this->lang->line("success")), 'result' => $result)));
    }
    
     public function getPlateCode() {
          $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $plate_source_id = !empty($json['plate_source_id']) ? $json['plate_source_id'] : '';
        $qry = "SELECT * FROM `plate_code` where archive='0' and plate_source='".$plate_source_id."'";
        $cArray = $this->Database->select_qry_array($qry);
         $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $return[$i] = array(
                'id' => $dft->id,
                'plate_code' =>  $dft->plate_code ,
                'plate_source'=>$dft->plate_source
                );
        
            }
        $result['plate_code'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>  $this->lang->line("success")), 'result' => $result)));
    }
function getAdvanceOption()
    {
          $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $category_id = !empty($json['category_id']) ? $json['category_id'] : '';
        $type = isset($json['type']) ? $json['type'] : '';
        if($type!='')
        {
        $qry = "SELECT * FROM `subcategory_details` where archive='0' and category_id='".$category_id."' and FIND_IN_SET (".$type.",type)";
        }
        else
        {
                   $qry = "SELECT * FROM `subcategory_details` where archive='0' and category_id='".$category_id."'  ";
 
        }
        $cArray = $this->Database->select_qry_array($qry);
         $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $return[$i] = array(
                'id' => $dft->id,
                'category_id' =>  $dft->category_id ,'type' =>  $type ,
                'subcategory' => $this->language == 'AR' ?!empty($dft->subcategory_ar)?$dft->subcategory_ar :$dft->subcategory: $dft->subcategory ,
                );
        
            }
        $result['subcategory'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>  $this->lang->line("success")), 'result' => $result)));
    }
    
       
       
     public function quotation_submit() {
        $db = LoadDB();
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $garage_id = !empty($json['garage_id']) ? $json['garage_id'] : '';
        $request_id = !empty($json['request_id']) ? $json['request_id'] : '';
        $payment_type = isset($json['payment_type']) ? $json['payment_type'] : '';
        $layout_type = isset($json['layout_type']) ? (int)$json['layout_type'] : '';
        
        $appointment_date = !empty($json['appointment_date']) ? $json['appointment_date'] : '';
        $user_De=GetuserdetailsBy($user_id);
      $request_garage_id = !empty($json['request_garage_id']) ? $json['request_garage_id'] : '';
        if ((empty($request_id))) {
            die(json_encode(array("code" => $this->error, "response" => array("status" => true, "message" => "Please Pass request_id"))));
        }

        //end
        
        // $Inst1['approve_status'] = '4';
        // $this->Database->update('request_garage', $Inst1, array('id' => $request_garage_id));
         $Inst['archive'] = '1';
        $this->Database->update('appointment', $Inst, array('request_id' => $request_id));
//request table
        $Json['user_id'] = $user_id;
        $Json['garage_id'] = $garage_id;
        $Json['request_id'] = $request_id;
        $Json['appoint_date'] = $appointment_date;

        $appointmentId = $db->Database->insert('appointment', $Json);

    
        if (!empty($appointmentId)) {
            //invoice
            
            $subject ='Invoice Recevied';
            $message ='Invoice Recevied for the Request';
            // $invoice = request_invoice_pdf1($request_id,$garage_id);
            // send_pushnotifaction([$user_De->device_id],$subject ,$message, $identifier = '3', $argument = array('request_id' => $requestId));

            //end
            $noti['appointment_id'] = $appointmentId;
             $noti['request_id'] = !empty($request_id) ? $request_id : '';
            $noti['receiver_id'] = '1';
            $noti['sender_id'] = $user_id;

            $noti['notification_type'] = '3';
            $noti['notification_message'] = 'New Appointment';
            $noti['notification_read'] = '0';
            $noti['redirect_url'] = "Admin/request_summary?requestId=".base64_encode($request_id);

            $notId = $db->Database->insert('notification', $noti);
            $noti['redirect_url'] = "Admin/vendor_request_summary?id=".base64_encode($request_garage_id);
            $reqeust_garag_det = GetgaragerequestdetailsBy($request_garage_id);
            $vendor_id = !empty($reqeust_garag_det->vendor_id)?$reqeust_garag_det->vendor_id:'';
            $noti['receiver_id'] = $vendor_id;
            $notId = $db->Database->insert('notification', $noti);

          // send_appointment_mail($appointmentId);
          
             $UpdateArray = array(
                'request_status' => 2,
                'payment_type'=>$payment_type

            );
            $CondArray = array('id' => $request_id);
            $db->Database->update('request_table', $UpdateArray, $CondArray);
            $invoice = request_invoice_pdf1($request_id,$garage_id);
             $UpdateArray1 = array(
                'approve_status' => 4,
            
            );
            $CondArray1 = array('request_id' => $request_id,'garage_id'=>$garage_id);
            $db->Database->update('request_garage', $UpdateArray1, $CondArray1);
            $result['request_id']=$request_id;
            $result['layout_type']=$layout_type;

           //mark noti as read
            $UpdateArray = array("notification_read" => 1);
            $Conducation = array('user_id' => $user_id, 'request_id' => $request_id);
            $db->Database->update('notification_user', $UpdateArray, $Conducation);
           
           //end
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("request_successfully")),"result"=>$result)));
        } else {
            die(json_encode(array("code" => $this->error, "response" => array("status" => true, "message" => $this->lang->line("Unsuccessfull")))));
        }
        //end request table
    }
    
    public function profile() {

        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $customer_id = !empty($json['customer_id']) ? $json['customer_id'] : '';

        $cArray = GetCustomerArrayById($customer_id);
        //  $result['customer'] = $cArray;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $cArray)));
    }
    public function update_profile() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        
        if (!is_array($json)) {
            $json = json_decode($json, true);
        }
        
        $today = date('Y-m-d H:i:s');
        $phone = !empty($json['mobile_number']) ? $json['mobile_number'] : '';
        $phonecode = !empty($json['mobile_code']) ? $json['mobile_code'] : '';
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $email = !empty($json['email']) ? $json['email'] : '';
        $isVerify = !empty($json['is_verify']) ? $json['is_verify'] : '';
        
       // $qry = "SELECT * FROM `users` WHERE (CONCAT(mobile_code,mobile) LIKE '$phonecode$phone') AND id='$user_id' AND is_verify='1'";
        $qry = "SELECT * FROM `users` WHERE id='$user_id' AND is_verify='1'";

        $Array = $this->Database->select_qry_array($qry);

        if (empty($Array)) {
            die(json_encode(array("code" => $this->error, "response" => array("status" => false, "message" =>$this->lang->line("user_not_exist")))));
        }
        if ($isVerify == false) {
          
            $otp = '0000';
            $result = array('OTP' => $otp);
            $otp = rand(1000, 9999);
            // $result = array('OTP' => (string) $otp);
            // $messagqwe = "Your verification code is " . $otp;

            // send_sms($phonecode . $phone, $messagqwe);
          
            $array['otp'] = $otp;
            $array['name'] = $json['full_name'];
            $array['email'] = $email;
            send_email_for_otp($array);
            die(json_encode(array("response" => array("status" => true, "message" =>  $this->lang->line("otp_send_successfully")), 'result' => $result)));
            // die(json_encode(array("response" => array("status" => true, "message" =>  $this->lang->line("user_not_exist")), 'result' => [])));
        } else {
            $full_name = !empty($json['full_name'])?$json['full_name']:'';
            
            $Inst['mobile_code'] = $phonecode;
            $Inst['mobile'] = $phone;
            $Inst['lastupdate'] = $today;

            $cond = array('id' => $user_id);
            $this->Database->update('users', $Inst, $cond);
            $cArray = GetuserdetailsBy($user_id);
            $result['customer'] = empty($cArray) ? new stdClass() : $cArray;
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_registered")), 'result' => $result)));
        }
    }


   
    public function forgot_password() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        
        $customerId = !empty($json['user_id']) ? $json['user_id'] : '';
        $email_id = !empty($json['email_id']) ? $json['email_id'] : '';
        $mobile_code = !empty($json['mobile_code']) ? $json['mobile_code'] : '';
        $mobile_number = !empty($json['mobile_number']) ? $json['mobile_number'] : '';

        $message = $this->lang->line("Invalid_email");
       
        $cArray = GetuserdetailsBy($customerId);
        if (empty($cArray)) {
            $mob =   substr($email_id, 1);
            $qry = "SELECT * FROM `users` WHERE (email LIKE '$email_id' or  mobile='$mob') ";
            
            $rArray = $this->Database->select_qry_array($qry);
            $rArray = !empty($rArray[0]) ? $rArray[0] : '';
            if (empty($rArray)) {
                $result['OTP'] = 0;
                $result['user_id'] = '';
                die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $message), 'result' => $result)));
            }
            $result['OTP'] = mt_rand(1000, 9999);
           // $result['OTP'] = '0000';
            $result['user_id'] = $rArray->id;
            send_email_forgotpassord($rArray->name, $result['OTP'], $rArray->email);

            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("Please_check_your_mail_otpcode")), 'result' => $result)));
        } else {
            if (empty($json['password'])) {
                die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("Please_enter_Password")))));
            }
            $Inst['password'] = md5($json['password']);
            $this->Database->update('users', $Inst, array('id' => $customerId));

            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("successfully_updated")), 'result' => new stdClass())));
        }
    }

    public function reset_password() {
        $json = file_get_contents('php://input');
       // send_mail('upendra@alwafaagroup.com', "power group - reset_password ", json_encode($json));
        $json = json_decode($json, TRUE);
        if(!is_array($json)){
          
               $json = json_decode($json, TRUE);
        }
       
       
        $customerId = !empty($json['user_id']) ? $json['user_id'] : '';
       
        // $email_id = !empty($json['email_id']) ? $json['email_id'] : '';
        if (empty($json['password'])) {
            die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => $this->lang->line("invalid_new_password")))));
        }
        if (empty($customerId)) {
            die(json_encode(array("code" => $this->success, "response" => array("status" => false, "message" => "Invalid customer id."))));
        }
        $Inst['password'] = md5($json['password']);
        $this->Database->update('users', $Inst, array('id' => $customerId));
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>$this->lang->line("successfully_updated")))));
    }
    public function request_submit() {
        $db = LoadDB();$vendor=[];$garage=[];
        // $json_re = file_get_contents('php://input');
        // $json = file_get_contents('php://input');
        $json = $_POST;
       //$json = json_decode($json);
    
        send_mail('deeshma@alwafaagroup.com', "fix ur car - add_requestNew", json_encode($json) . '--' . json_encode($_FILES));
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $category_id = !empty($json['category_id']) ? $json['category_id'] : '';
        $layout_type = isset($json['layout_type']) ? $json['layout_type'] : '';
        $additional_comment = !empty($json['additional_comment']) ? $json['additional_comment'] : '';
       
        //end
    
        $requestId='';$advance_option='';
        $emirate_id = !empty($json['emirate_id']) ? $json['emirate_id'] : '';
        $region_id = !empty($json['region_id']) ? $json['region_id'] : '';
        $garage_type = isset($json['garage_type']) ? $json['garage_type'] : '';
        $vehicle = !empty($json['vehicle']) ? $json['vehicle'] : '';
        
        $vehicle_info = !empty($vehicle) ? json_decode(stripslashes($vehicle), true) : '';
        $make = !empty($vehicle_info['make_id']) ? $vehicle_info['make_id'] : '';
        $model = !empty($vehicle_info['model_id']) ? $vehicle_info['model_id'] : '';
        $year= !empty($vehicle_info['year_id']) ? $vehicle_info['year_id'] : '';
        $plate_code= !empty($vehicle_info['plate_code_id']) ? $vehicle_info['plate_code_id'] : '';
        $plate_source= !empty($vehicle_info['plate_source_id']) ? $vehicle_info['plate_source_id'] : '';
        $plate_no= !empty($vehicle_info['plate_no']) ? $vehicle_info['plate_no'] : '';
        $gcc_type= isset($vehicle_info['gcc_type']) ? $vehicle_info['gcc_type'] : '';
        $chassis_no= isset($vehicle_info['chassis_no']) ? $vehicle_info['chassis_no'] : '';
 
        //add to favoirte
        $fav['user_id']=$user_id;
        $fav['make_id']=$make;
        $fav['model_id']=$model;
        $fav['year']=$year;
        $fav['plate_code']=$plate_code;
        $fav['plate_source']=$plate_source;
        $fav['plate_no']=$plate_no;
        $fav['gcc_type']=$gcc_type;
        $fav['chassis_no']=$chassis_no;
        $qry = "SELECT * FROM `favourite_vehicle` WHERE plate_code = '$plate_code' AND plate_no='$plate_no' AND plate_source='$plate_source' AND user_id='$user_id' and archive='0'";
        $Array = $this->Database->select_qry_array($qry);
        if (empty($Array)&&$plate_no!='') {
            $favId = $db->Database->insert('favourite_vehicle', $fav);
        }
        //end
        
        $Json['make'] = $make;
        $Json['model'] = $model;
        $Json['year'] = $year;
        $Json['category_id'] = $category_id;
        $Json['user_id'] = $user_id;
        $Json['additional_comment'] = $additional_comment;
        $Json['plate_code'] = $plate_code;
        $Json['plate_source'] = $plate_source;
        $Json['plate_no'] = $plate_no;
        $Json['gcc_type'] = $gcc_type;
        $Json['chassis_no'] = $chassis_no;
        $Json['region_id'] = $region_id;
        $Json['emirate_id'] = $emirate_id;
        $Json['garage_type'] = $garage_type;
        

        //Vehicle Paint
        if((int)$layout_type==2)
        {
            $paint_color_id = !empty($json['paint_color_id']) ? $json['paint_color_id'] : '';
            $paint_type_id = !empty($json['paint_type_id']) ? $json['paint_type_id'] : '';
            $paint_method_id = !empty($json['paint_method_id']) ? $json['paint_method_id'] : '';
            $Json['paint_color'] = $paint_color_id;
            $Json['paint_type'] = $paint_type_id;
            $Json['paint_method'] = $paint_method_id;
        }
        
        //Special Services or Vehicle check up and repair or recovery 

        if((int)$layout_type==5||(int)$layout_type==3||(int)$layout_type==7)
        {
            $location = !empty($json['location']) ? $json['location'] : '';
            $latitude = !empty($json['latitude']) ? $json['latitude'] : '';
            $longitude = !empty($json['longitude']) ? $json['longitude'] : '';
            $dest_location = !empty($json['garage_destination']) ? $json['garage_destination'] : '';
            $dest_latitude = !empty($json['garage_latitude']) ? $json['garage_latitude'] : '';
            $dest_longitude = !empty($json['garage_longitude']) ? $json['garage_longitude'] : '';
            $Json['location'] = $location;
            $Json['latitude'] = $latitude;
            $Json['longitude'] = $longitude;
            $Json['dest_location'] = $dest_location;
            $Json['dest_latitude'] = $dest_latitude;
            $Json['dest_longitude'] = $dest_longitude;
        }
        $requestId = $db->Database->insert('request_table', $Json);
        //ACCIDENT REPAIR
        if((int)$layout_type==0)
        {
            $sub_category_list = !empty($json['sub_category_list']) ? $json['sub_category_list'] : '';
            $quote_details = !empty($sub_category_list) ? json_decode(stripslashes($sub_category_list), true) : '';
            $advance=array();
            // foreach ($sub_info as $value) {
            if(isset($quote_details)&&$quote_details!='')
            {
                foreach ($quote_details as $value) {
                    $sub_cat['request_id']=$requestId;
                    $sub_cat['subcategory_id']=$value['id'];
                    $sub_cat['type']=$value['type'];
                    $sub_cat['vehicle_parts_type']=$value['vehicle_parts_type'];
                    $sub_cat['replace_type']=$value['replace_type'];
                    $advance[]=$value['id'];
                    $sub_catId = $db->Database->insert('request_advanced_option', $sub_cat);
                }
            }
              $advance_option = implode(',',$advance);
        }
        //Mechanical Repair or vehicle services or special services

        if((int)$layout_type==1||(int)$layout_type==4||(int)$layout_type==5)
        {
            $sub_category_list = !empty($json['sub_category_list']) ? $json['sub_category_list'] : '';
            $quote_details = !empty($sub_category_list) ? json_decode(stripslashes($sub_category_list), true) : '';
            $advance=array();
        // foreach ($sub_info as $value) {
            if(isset($quote_details)&&$quote_details!='')
            {
                foreach ($quote_details as $value) {
                    $advance[]=$value['id'];
                }
            }
            $advance_option = implode(',',$advance);
        }
//request table
//images and document
 //$image = isset($_FILES['image']) ? $_FILES['image'] : '';
  //$document = isset($_FILES['document']) ? $_FILES['document'] : '';

       // $image = $this->post("upload_image");
        $image_name='';$document_name='';
        // if (isset($image) || !empty($image)) {
        //     $FileName = uniqid() . '.' . pathinfo($image['name'], PATHINFO_EXTENSION);
        //     if (move_uploaded_file($image['tmp_name'], HOME_DIR . 'files/request_images/' . $FileName)) {
        //          $image_name = $FileName;
        //     }
        // }
        
        $filecount = !empty($_FILES['images']['name']) ? $_FILES['images']['name'] : [];
        for ($i = 0; $i < count($filecount); $i++) {
            $form = $_FILES['images'];
            $FileName = uniqid() . '.' . pathinfo($form['name'][$i], PATHINFO_EXTENSION);
            if (move_uploaded_file($form['tmp_name'][$i], HOME_DIR . 'files/request_images/' . $FileName)) {
                $inserttt = array(
                    'request_id' => $requestId,
                    'file_name' => $FileName,
                    'timestamp' => date('Y-m-d H:i:s'),
                );
                $this->Database->insert('request_images', $inserttt);
            }
        }
//doc
   $filecount1 = !empty($_FILES['document']['name']) ? $_FILES['document']['name'] : [];
        for ($i = 0; $i < count($filecount1); $i++) {
            $form = $_FILES['document'];
            $FileName = uniqid() . '.' . pathinfo($form['name'][$i], PATHINFO_EXTENSION);
            if (move_uploaded_file($form['tmp_name'][$i], HOME_DIR . 'files/request_images/' . $FileName)) {
                $inserttt1 = array(
                    'request_id' => $requestId,
                    'file_name' => $FileName,
                    'file_type' => 1,
                    'timestamp' => date('Y-m-d H:i:s'),
                );
                $this->Database->insert('request_images', $inserttt1);
            }
        }

//enmd
//  if (isset($document) || !empty($document)) {
//             $FileName = uniqid() . '.' . pathinfo($document['name'], PATHINFO_EXTENSION);
//             if (move_uploaded_file($document['tmp_name'], HOME_DIR . 'files/request_images/' . $FileName)) {
//                 $document_name = $FileName;
//             }
//         }

//end
        $request_no = '00' . $requestId . date('dmY');
        $invoice_request_no = '1000' . $requestId ;
           $UpdateArray = array(
               'advance_option'=>$advance_option,
                'request_no'=>$request_no,
                'invoice_no'=>$invoice_request_no
            );
            $CondArray = array('id' => $requestId);
            $db->Database->update('request_table', $UpdateArray, $CondArray);

        if (!empty($requestId)) {
            
            $noti['request_id'] = $requestId;
            $noti['sender_id'] = !empty($json['user_id']) ? $json['user_id'] : '';
            $noti['receiver_id'] = '1';
            $noti['notification_type'] = '1';
            $noti['notification_message'] = 'New Request';
            $noti['notification_read'] = '0';
            $noti['redirect_url'] = "Admin/request_summary?requestId=".base64_encode($requestId);
            $notId = $db->Database->insert('notification', $noti);
            send_request_mail($requestId,'1');
            $garage1['request_id'] = $requestId;
            
            $get_vendor = get_vendor_garage($category_id,$advance_option,$garage_type,$emirate_id);

            for ($z = 0; $z < count($get_vendor); $z++) {
                $z_mang = $get_vendor[$z];
                $noti['receiver_id'] = $z_mang->vendor;
                $noti['garage_id'] = $z_mang->id;
                $vendor[]=$z_mang->vendor;
                $garage[]=$z_mang->id;
           
                $garage1['garage_id'] = $z_mang->id;
                $garage1['vendor_id'] = $z_mang->vendor;
                
                $garageDetailsId = GetgaragedetailsBy($z_mang->id);
                // print_r($garageDetailsId);
                if((int)$layout_type==5||(int)$layout_type==3||(int)$layout_type==7)
                {
                    $originLat = !empty($json['latitude']) ? $json['latitude'] : '';
                    $originLong = !empty($json['longitude']) ? $json['longitude'] : '';
                    $destinationLat = $garageDetailsId->latitude;
                    $destinationLong = $garageDetailsId->longitude;
                    // $distance = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=imperial&origins=".urlencode($origin)."&destinations=".urlencode($destination)."&key=AIzaSyAQwW9qLomFYijAntw2-REYJEfyxJG1u4k");
                    $distance = file_get_contents("https://maps.googleapis.com/maps/api/distancematrix/json?units=metric&origins=".$originLat.",".$originLong."&destinations=".$destinationLat.",".$destinationLong."&key=".API_KEY."");
                    $data = json_decode($distance);
                    // print_r($data);
                    $distance = $data->rows[0]->elements[0]->distance->text;
                    $garage1['distance'] = $distance;
                }
                else
                {
                    $garage1['distance'] = '';
                }

                $garageId = $db->Database->insert('request_garage', $garage1);
                
                $noti['redirect_url'] = "Admin/vendor_request_summary?id=".base64_encode($garageId);
                $notId = $db->Database->insert('notification', $noti);
                send_request_mail($requestId,$z_mang->vendor);
                
                $garageArray[$z] = array(
                    'Latitude'=>$garageDetailsId->latitude,
                    'Longitude'=>$garageDetailsId->longitude,
                    'Location'=>$garageDetailsId->location,
                    'distance'=>$distance
                );

            }
            $vendor_ids = implode(',',$vendor);
            $garage_ids = implode(',',$garage);

           $UpdateArray = array(
                'receive_vendor_id' => $vendor_ids,
                'receive_garage_id' => $garage_ids,
            );
            $CondArray = array('id' => $requestId);
            $db->Database->update('request_table', $UpdateArray, $CondArray);
            if((int)$layout_type==5||(int)$layout_type==3||(int)$layout_type==7)
        {
            $result['garage_location']=$garageArray;
        }
        
            $result['request_id']=$requestId;
            $result['layout_type']=(int)$layout_type;

            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" =>$this->lang->line("request_successfully") ), 'result' => $result)));
        } else {
            die(json_encode(array("code" => $this->error, "response" => array("status" => false, "message" =>$this->lang->line("Unsuccessfull")))));
        }
        //end request table
    }
    public function  vehicle_favourite_list () {
          $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $qry = "SELECT * FROM `favourite_vehicle` where user_id='".$user_id."' and archive='0'";
        $cArray = $this->Database->select_qry_array($qry);
        $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $make_name = GetmakedetailsBy($dft->make_id);
            $model_name = GetmodeldetailsBy($dft->model_id);
            $year_name = GetyeardetailsBy($dft->year);
            $plate_code_name = GetplatecodedetailsBy($dft->plate_code);
            $plate_source_name = GetemiratedetailsBy($dft->plate_source);
            if($this->language == 'AR'){
                if(!empty($make_name->make_ar)){
                    $make = $make_name->make_ar;
                }
                else{
                    if(!empty($make_name->make)){
                        $make = $make_name->make;
                    }
                    else{
                        $make = "";
                    }
                }
            }
            else{
                if(!empty($make_name->make)){
                    $make = $make_name->make;
                }
                else{
                    $make = "";
                }
            }
            
            if($this->language == 'AR'){
                if(!empty($model_name->model_ar)){
                    $model = $model_name->model_ar;
                }
                else{
                    if(!empty($model_name->model)){
                        $model = $model_name->model;
                    }
                    else{
                        $model = "";
                    }
                }
            }
            else{
                if(!empty($model_name->model)){
                    $model = $model_name->model;
                }
                else{
                    $model = "";
                }
            }
            
            if($this->language == 'AR'){
                if(!empty($year_name->year_ar)){
                    $year = $year_name->year_ar;
                }
                else{
                    if(!empty($year_name->year)){
                        $year = $year_name->year;
                    }
                    else{
                        $year = "";
                    }
                }
            }
            else{
                if(!empty($year_name->year)){
                    $year = $year_name->year;
                }
                else{
                    $year = "";
                }
            }
            // 'make'=>$this->language == 'AR' ?(!empty($make_name->make_ar)?$make_name->make_ar :!empty($make_name->make)?$make_name->make:""): (!empty($make_name->make)?$make_name->make:""),
            // 'model'=>$this->language == 'AR' ?(!empty($model_name->model_ar)?$model_name->model_ar :!empty($model_name->model)?$model_name->model :""): (!empty($model_name->model)?$model_name->model :""),
            // 'year'=>$this->language == 'AR' ?(!empty($year_name->year_ar)?$year_name->year_ar :!empty($year_name->year)?$year_name->year :""): (!empty($year_name->year)?$year_name->year :""),
            $return[$i] = array(
                'id' => $dft->id,
                'user_id' =>  $dft->user_id ,
                'make_id'=>$dft->make_id,
                'model_id'=>$dft->model_id,
                'year_id'=>$dft->year,
                'make'=>$make,
                'model'=>$model,
                'year'=>$year,
                'plate_code_id' =>  $dft->plate_code ,
                'plate_source_id'=>$dft->plate_source,
                'plate_code' => !empty($plate_code_name->plate_code)?$plate_code_name->plate_code:'', 
                'plate_source'=>!empty($plate_source_name->emirate)?$plate_source_name->emirate:'',
                'plate_no'=>$dft->plate_no,
                'gcc_type'=>$dft->gcc_type,
                'chassis_no'=>$dft->chassis_no,
                'car_icon'=> !empty($make_name->make_icon) ? base_url() . "files/make_icon/" .$make_name->make_icon: ''
                );
        
            }
        $result['vehicle'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }
function delete_vehicle_favourite()
{
     $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $id = !empty($json['id']) ? $json['id'] : '';
     $UpdateArray = array(
               'archive'=>1,
              
            );
            $CondArray = array('id' => $id);
            $this->Database->update('favourite_vehicle', $UpdateArray, $CondArray);
                    die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")))));

}


    public function get_special_offer() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';

        $return_cat=array();
        $qry_cat = "SELECT * FROM `offer_category_details` where archive='0'  and status='0' ORDER BY category_id ASC";
        $cArray_cat = $this->Database->select_qry_array($qry_cat);
        for ($i = 0; $i < count($cArray_cat); $i++) {
            $dft = $cArray_cat[$i];
            $return_cat[$i] = array(
                'category_id' => $dft->category_id,
                'category_name' => $this->language == 'AR' ?!empty($dft->category_ar)?$dft->category_ar :$dft->category: $dft->category ,
                'category_icon' => base_url() . 'files/offer_image/' .$dft->category_icon,
                );
        }
            $result['category_list']=$return_cat;
            $qry1 = "SELECT * FROM `offer_banner`  where archive='0' and status='0' ORDER BY id DESC";
            $cArray1 = $this->Database->select_qry_array($qry1);
            $bannerimage = [];
            for ($k = 0; $k < count($cArray1); $k++) {
            $dryr = $cArray1[$k];
           
             $bannerimage[$k] = array('image_url'=>base_url() . 'uploads/offer_banner/' .$dryr->image,
            'url'=>!empty($dryr->url)?$dryr->url:""
            );
            }

        $result['banner_image'] = $bannerimage;
        $cur_date = date("Y-m-d");
        $qry = "SELECT * FROM `offers` where archive='0'  and status='0' and ('$cur_date' BETWEEN `valid_from` AND `valid_to`) group by category ORDER BY id DESC";
        $cArray = $this->Database->select_qry_array($qry);
        $offer_array =array();
        for ($i = 0; $i < count($cArray); $i++) {
            $dft = $cArray[$i];
            
          
            
            
            
            
            
            $category_id = !empty($dft->category) ?$dft->category:'';
             $category_De=GetOfferCategorydetailsBy($category_id);
            $category_name=!empty($category_De)?$category_De->category:"";
            $return=array();
            $qry_off = "SELECT * FROM `offers` where archive='0'  and status='0' and ('$cur_date' BETWEEN `valid_from` AND `valid_to`)  and category='$category_id' ORDER BY id DESC";
            $off_array = $this->Database->select_qry_array($qry_off);
            for($j=0;$j<count($off_array);$j++)
            {
                $off = $off_array[$j];
                
                    
         //check redeemed or not - by deeshma
        $qryr = "SELECT * FROM `offer_request_table` where archive='0'  and user_id='$user_id' and offer_id='$off->id' and is_freeCoupon='1' and archive='0' ORDER BY id DESC";
        $cArrayr = $this->Database->select_qry_array($qryr);
        if(!empty($cArrayr))
        {
            $free_coupon_redoom=true;
        }
        else
        {
            $free_coupon_redoom=false;
        }
        //end  
                //IMAGE
                $image = !empty($off->offer_image) ?$off->offer_image:'';
                $image_array=explode(',',$image); $image_url=[];
            for($m=0;$m<count($image_array);$m++)
            {
                $image_url[$m]=!empty($image_array[$m]) ? base_url() . "files/offer_image/" . $image_array[$m]: '';
            }
                //END
              $garage_name=  GetgaragedetailsBy($off->garage_id);
            //   'garage_name' => $this->language == 'AR' ?(!empty($garage_name->garage_name_ar)?$garage_name->garage_name_ar :!empty($garage_name->garage_name)?$garage_name->garage_name:""): (!empty($garage_name->garage_name)?$garage_name->garage_name:"") ,
            //  print_r($garage_name);
            if($this->language == 'AR'){
                if(!empty($garage_name->garage_name_ar)){
                    $garageName = $garage_name->garage_name_ar;
                }
                else{
                    if(!empty($garage_name->garage_name)){
                        $garageName = $garage_name->garage_name;
                    }
                    else{
                        $garageName = "";
                    }
                }
            }
            else{
                if(!empty($garage_name->garage_name)){
                        $garageName = $garage_name->garage_name;
                    }
                    else{
                        $garageName = "";
                    }
            }
            $return[$j] =
            array(
                'id' => $off->id,
                'title' => $this->language == 'AR' ?!empty($off->title_ar)?$off->title_ar :$off->title: $off->title ,
                'original_price' => $off->original_price,
                'discount_price' => $off->discount_price,
                'appointment_option' => (int)$off->appointment,
                'free_coupon' => $off->free_coupon,
                'is_redeemed'=>$free_coupon_redoom,
                'message' => $this->language == 'AR' ?!empty($off->message_ar)?$off->message_ar :$off->message: $off->message ,

                'description' => $this->language == 'AR' ?!empty($off->description_ar)?$off->description_ar :$off->description: $off->description ,
                'valid_from' => $off->valid_from,
                'valid_to' => $off->valid_to,
                'category_name' => $this->language == 'AR' ?!empty($category_De->category_ar)?$category_De->category_ar :$category_De->category: $category_De->category,
                'category_id' => $off->category,

                'garage_id' => $off->garage_id,
                'garage_name' => $garageName,
                'location' => !empty($garage_name->location)?$garage_name->location :'',
                'latitude' => !empty($garage_name->latitude)?$garage_name->latitude :'',
                'longitude' => !empty($garage_name->longitude)?$garage_name->longitude :'',

                'layout_type'=>6,
                'offer_image'=>$image_url,
                );

                
            
            }
           
            $offer_array[$i]=array('title'=>$this->lang->line("offer_for")." ".$category_name,  'offers'=>$return);
        }
             //   $result_merge = array_merge($return, $special_off);
             if((!empty($offer_array)) || ($offer_array!= null))
             {
                 $result['offers'] = $offer_array;
                 die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
             }
            else
            {
                $result['offers'] = [];
                die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("no_special_offer")), 'result' => $result)));
            }
        

        
    }
    public function special_offer_submit() {

        $db = LoadDB();$vendor=[];$garage=[];
        $json_re = file_get_contents('php://input');
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $offer_id = !empty($json['offer_id']) ? $json['offer_id'] : '';
        $payment_type = isset($json['payment_type']) ? $json['payment_type'] : '';
        $appointment_date = !empty($json['appointment_date']) ? $json['appointment_date'] : '';
       
        //bulding_details
        //validations
        if ((empty($offer_id))) {
            die(json_encode(array("code" => $this->error, "response" => array("status" => true, "message" => $this->lang->line("Please_Select_Offer")))));
        }

        //end
//request table
        $Json['user_id'] = $user_id;
        $Json['offer_id'] = $offer_id;
         

            $requestId = $db->Database->insert('offer_request_table', $Json);
            $request_no = '00' . $requestId . date('dmY');

            $fix_invoice_no = '1000' . $requestId ;
            $invoice_no = '100' . $requestId ;
            
            $is_freeCoupon = !empty($json['isFreeCoupon']) ? $json['isFreeCoupon'] : '';
            if($is_freeCoupon == true){
                $is_freeCoupon ='1';
                $free_coupon_timing=date('Y-m-d H:i:s');
                $request_status=3;
            }
            else{
                $is_freeCoupon='0';
                $free_coupon_timing='';
                $request_status=0;
            }

        if (!empty($requestId)) {
            $noti['request_id'] = $requestId;
            $noti['sender_id'] = !empty($json['user_id']) ? $json['user_id'] : '';
            $noti['receiver_id'] = '1';
            $noti['notification_type'] = '1';
                        $noti['offer'] = 1;

            $noti['notification_message'] = 'New Offer Request';
            $noti['notification_read'] = '0';
            $noti['redirect_url'] = "Admin/offer_request_summary?requestId=".base64_encode($requestId);
            $notId = $db->Database->insert('notification', $noti);
            send_offer_request_mail($requestId,'1');
            $garage1['request_id'] = $requestId;
            $get_vendor = get_vendor_garage_offer($offer_id);
            $garage_id='';$vendor_id='';
            if(count($get_vendor)>0)
            {
                $z_mang = $get_vendor[0];
                $noti['receiver_id'] = $z_mang->vendor;
                $noti['garage_id'] = $z_mang->id;
                $vendor[]=$z_mang->vendor;
                $garage[]=$z_mang->id;
                $noti['redirect_url'] =  "Admin/offer_request_summary?requestId=".base64_encode($requestId);
                $notId = $db->Database->insert('notification', $noti);
                send_offer_request_mail($requestId,$z_mang->vendor);

                $garage_id = $z_mang->id;
                $vendor_id = $z_mang->vendor;

            }

           $UpdateArray = array(
               'garage_id'=>$garage_id,
               'vendor_id'=>$vendor_id,
                'request_no'=>$request_no,                
                'payment_type'=>$payment_type,
                'invoice_no'=>$invoice_no,
                'fix_invoice_no'=>$fix_invoice_no,
                'is_freeCoupon'=>$is_freeCoupon,
                'free_coupon_timing'=>$free_coupon_timing,
                'request_status'=>$request_status

            );
            $CondArray = array('id' => $requestId);
            $db->Database->update('offer_request_table', $UpdateArray, $CondArray);
            
            //add to appointment table
            $Json1['user_id'] = $user_id;
            $Json1['offer_id'] = $offer_id;
            $Json1['garage_id'] = $garage_id;
            $Json1['offer_request_id'] = $requestId;

            $Json1['appointment_date'] = $appointment_date;

            $offer_appointmentid = $db->Database->insert('offer_appointment', $Json1);
            if(empty($offer_appointmentid)){
                $invoice = request_offer_invoice_pdf1($requestId);
            }
            if (!empty($offer_appointmentid)) {
                $invoice = request_offer_invoice_pdf($requestId);
                $noti['appointment_id'] = $offer_appointmentid;
                // $noti['sender_id'] = !empty($json['user_id']) ? $json['user_id'] : '';
                $noti['receiver_id'] = '1';
                $noti['notification_type'] = '3';
                $noti['notification_message'] = 'New Offer Appointment';
                $noti['offer'] = 1;
                $noti['notification_read'] = '0';
                $noti['redirect_url'] = "Admin/offer_request_summary?requestId=".base64_encode($requestId);
                $notId = $db->Database->insert('notification', $noti);
                $noti['receiver_id'] = $vendor_id;
                $notId = $db->Database->insert('notification', $noti);
            }
            //end
            $result['request_id']=$requestId;
            $result['layout_type']=6;

            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("request_successfully")), 'result' => $result)));
        } else {
            die(json_encode(array("code" => $this->error, "response" => array("status" => true, "message" => $this->lang->line("Unsuccessfull")))));
        }
    }
    public function new_request_list() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        
        $qry = "SELECT * FROM `request_table` where archive=0 and request_status=0 and user_id='$user_id' ORDER BY id desc";
        $cArray = $this->Database->select_qry_array($qry);
        $return = [];$special_off=[];$count_n=0;
        
        for ($i = 0; $i < count($cArray); $i++) {
            $dft = $cArray[$i];
            $category_De=GetCategorydetailsBy($dft->category_id);
            $category_name=!empty($category_De)?$category_De->category:"";
            $image = !empty($category_De->category_image) ?$category_De->category_image:'';
            $image_array=explode(',',$image); $image_url=[];
            for($j=0;$j<count($image_array);$j++)
            {
                $image_url[$j]=!empty($image_array[$j]) ? base_url() . "files/category_image/" . $image_array[$j]: '';
            }
            $em_De=GetemiratedetailsBy($dft->emirate_id);
            $emirate_name=!empty($em_De)?$em_De->emirate:"";
            $re_De=GetregiondetailsBy($dft->region_id);
            $region_name=!empty($re_De)?$re_De->region:"";
            $return[$i] = array(
                'id' => $dft->id,
                'request_no' => !empty($dft->request_no)?$dft->request_no :'',
                'category_name'=>$category_name,
                'category_id'=>!empty($dft->category_id)?$dft->category_id :'',
                'image_url'=>$image_url,
                'emirate_id'=>!empty($dft->emirate_id)?$dft->emirate_id :'',
                'emirate'=>!empty($emirate_name)?$emirate_name :'',

                'region_id'=>!empty($dft->region_id)?$dft->region_id :'',
                'region'=>!empty($region_name)?$region_name :'',

                'timestamp'=>!empty($dft->timestamp)?$dft->timestamp :'',

              'layout_type'=>isset($category_De->layout_type)?(int)$category_De->layout_type :'',
                );
            
        }
            
            $qry1 = "SELECT * FROM `offer_banner`  where archive='0' and status='0' ORDER BY id DESC";
            $cArray1 = $this->Database->select_qry_array($qry1);
            $offerimage = [];
            for ($k = 0; $k < count($cArray1); $k++) {
                $dryr = $cArray1[$k];
                $offerimage[$k] = base_url() . 'uploads/offer_banner/' .$dryr->image;
            }
        
            $qry2 = "SELECT offer_request_table.* FROM `offer_request_table` LEFT JOIN offers ON offer_request_table.offer_id=offers.id  where offer_request_table.archive='0' and offer_request_table.request_status='0'  and offer_request_table.user_id='$user_id' and offers.appointment=1 and offer_request_table.is_freeCoupon!=1 ORDER BY offer_request_table.id DESC";
            $cArray2 = $this->Database->select_qry_array($qry2);
            
            $return1 = [];
            for ($k = 0; $k < count($cArray2); $k++) {
                $dryr = $cArray2[$k];
                $offer_id=!empty($dryr->offer_id)?$dryr->offer_id :'';
                $offer_det = GetOfferdetailsBy($offer_id);
                $from=!empty($offer_det->valid_from)?$offer_det->valid_from:"";
                $to=!empty($offer_det->valid_to)?$offer_det->valid_to:"";
                $cur_date=date("Y-m-d");
                
                if($cur_date>$to || $cur_date<=$from)
                {
                    $is_exp=true;
                }
                else
                {
                    $is_exp=false;
    
                }
                $category = !empty($offer_det->category)?$offer_det->category :'';
                $category_De=GetOfferCategorydetailsBy($category);
                $category_name=!empty($category_De)?$category_De->category:"";
                $garage_id= !empty($offer_det->garage_id)?$offer_det->garage_id :'';
                $garage_de = GetgaragedetailsBy($garage_id);
                $garage_name=!empty($garage_de)?$garage_de->garage_name:"";
                $image = !empty($offer_det->offer_image) ?$offer_det->offer_image:'';
                $image_array=explode(',',$image); $image_url=[];
                for($m=0;$m<count($image_array);$m++)
                {
                    $image_url[$m]=!empty($image_array[$m]) ? base_url() . "files/offer_image/" . $image_array[$m]: '';
                }
                $return1[$k] = array(
                    'id' => $dryr->id,
                    'request_no' => !empty($dryr->request_no)?$dryr->request_no :'',
                    'offer_id' => !empty($dryr->offer_id)?$dryr->offer_id :'',
                    'offer_title' => !empty($offer_det->title)?$offer_det->title :'',
                    'offer_category' => !empty($category_name)?$category_name :'',
                    'garage_id' => !empty($garage_id)?$garage_id :'',
                    'garage_name'=>$garage_name,
                    'layout_type'=>6,
                    'timestamp'=>!empty($dryr->timestamp)?$dryr->timestamp :'',
                    'image_url'=>$image_url,
                    'request_status'=>0,
                    'total_price' => !empty($offer_det->total_price)?$offer_det->total_price :'',
                    'VAT' => !empty($offer_det->vat)?$offer_det->vat :'',
                    'appointment' => 1,
                    'is_freeCoupon'=>!empty($dryr->is_freeCoupon)?$dryr->is_freeCoupon :'',
                    'isExpiry'=>$is_exp,
                    'invoice_pdf' => !empty($dryr->invoice)?base_url() . 'uploads/invoice/'.$dryr->invoice :''
                    );
            }
            $allRequest = array_merge($return,$return1);
            usort($allRequest, 'date_compare');
    
            if((!empty($allRequest)) || ($allRequest!= null)){
                $result['request_list'] = $allRequest;
                die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
            }
            else{
                $result['request_list'] = [];
                die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("No_new_request")), 'result' => $result)));
            }
    }
    public function completed_request_list() {
        $json_re = file_get_contents('php://input');
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $qry = "SELECT * FROM `request_table` where archive='0'  and request_status='3' and user_id='$user_id' ORDER BY id desc";
        $cArray = $this->Database->select_qry_array($qry);
        $return = [];$special_off=[];$count_n=0;
        
        for ($i = 0; $i < count($cArray); $i++) {
            $dft = $cArray[$i];
            $category_De=GetCategorydetailsBy($dft->category_id);
            $category_name=!empty($category_De)?$category_De->category:"";
             $image = !empty($category_De->category_image) ?$category_De->category_image:'';
            $image_array=explode(',',$image); $image_url=[];
            for($j=0;$j<count($image_array);$j++)
            {
                $image_url[$j]=!empty($image_array[$j]) ? base_url() . "files/category_image/" . $image_array[$j]: '';
            }
            $em_De=GetemiratedetailsBy($dft->emirate_id);
            $emirate_name=!empty($em_De)?$em_De->emirate:"";
            $re_De=GetregiondetailsBy($dft->region_id);
            $region_name=!empty($re_De)?$re_De->region:"";
            $return[$i] = array(
                'id' => $dft->id,
                'request_no' => !empty($dft->request_no)?$dft->request_no :'',
                'category_name'=>$category_name,
                'category_id'=>!empty($dft->category_id)?$dft->category_id :'',
                'image_url'=>$image_url,
                'emirate_id'=>!empty($dft->emirate_id)?$dft->emirate_id :'',
                'emirate'=>!empty($emirate_name)?$emirate_name :'',

                'region_id'=>!empty($dft->region_id)?$dft->region_id :'',
                'region'=>!empty($region_name)?$region_name :'',

                'timestamp'=>!empty($dft->timestamp)?$dft->timestamp :'',

               'layout_type'=>isset($category_De->layout_type)?(int)$category_De->layout_type :'',
                );
            
            }
            
            $qry1 = "SELECT * FROM `offer_banner`  where archive='0' and status='0' ORDER BY id DESC";
            $cArray1 = $this->Database->select_qry_array($qry1);
            $offerimage = [];
            for ($k = 0; $k < count($cArray1); $k++) {
                $dryr = $cArray1[$k];
                $offerimage[$k] = base_url() . 'uploads/offer_banner/' .$dryr->image;
            }
        
            $qry1 = "SELECT offer_request_table.* FROM `offer_request_table` LEFT JOIN offers ON offer_request_table.offer_id=offers.id  where offer_request_table.archive='0' and offer_request_table.request_status='3'  and offer_request_table.user_id='$user_id' and offers.appointment=1 and offer_request_table.is_freeCoupon!=1 ORDER BY offer_request_table.id DESC";
            $cArray1 = $this->Database->select_qry_array($qry1);
            $return1 = [];
            for ($off = 0; $off < count($cArray1); $off++) {
                $dryr = $cArray1[$off];
                $offer_id=!empty($dryr->offer_id)?$dryr->offer_id :'';
                $offer_det = GetOfferdetailsBy($offer_id);
                $from=!empty($offer_det->valid_from)?$offer_det->valid_from:"";
                $to=!empty($offer_det->valid_to)?$offer_det->valid_to:"";
                $cur_date=date("Y-m-d");
                if($cur_date>$to || $cur_date<=$from)
                {
                    $is_exp=true;
                }
                else
                {
                    $is_exp=false;
    
                }
                $category = !empty($offer_det->category)?$offer_det->category :'';
                $vat = !empty($offer_det->vat)?$offer_det->vat :'';
                $category_De=GetOfferCategorydetailsBy($category);
                $category_name=!empty($category_De)?$category_De->category:"";
                $garage_id= !empty($offer_det->garage_id)?$offer_det->garage_id :'';
                $garage_de = GetgaragedetailsBy($garage_id);
                $garage_name=!empty($garage_de)?$garage_de->garage_name:"";
                $image = !empty($offer_det->offer_image) ?$offer_det->offer_image:'';
                $image_array=explode(',',$image); $image_url=[];
                for($m=0;$m<count($image_array);$m++)
                {
                    $image_url[$m]=!empty($image_array[$m]) ? base_url() . "files/offer_image/" . $image_array[$m]: '';
                }
                $return1[$off] = array(
                    'id' => $dryr->id,
                    'request_no' => !empty($dryr->request_no)?$dryr->request_no :'',
                    'offer_id' => !empty($dryr->offer_id)?$dryr->offer_id :'',
                    'offer_title' => !empty($offer_det->title)?$offer_det->title :'',
                    'offer_category' => !empty($category_name)?$category_name :'',
                    'garage_id' => !empty($garage_id)?$garage_id :'',
                    'garage_name'=>$garage_name,
                    'layout_type'=>6,
                    'timestamp'=>!empty($dryr->timestamp)?$dryr->timestamp :'',
                    'payment_method'=>!empty($dryr->payment_type)?$dryr->payment_type :'',
                    'VAT'=>$vat,
                    'total_price' => !empty($offer_det->total_price)?$offer_det->total_price :'',
                    'appointment' => 1,
                    'request_status'=>3,
                    'isExpiry'=>$is_exp,
                    'image_url'=>$image_url,
                );
            }
        $result_merge = array_merge($return, $return1);
        usort($result_merge, 'date_compare');
        if((!empty($result_merge)) || ($result_merge!= null)){
            $result['request_list'] = $result_merge;
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
        else{
            $result['request_list'] = [];
           die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("No_new_request")), 'result' => $result))); 
        }
    }
    public function ongoing_request_list() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $qry = "SELECT * FROM `request_table` where archive='0'  and request_status!='3' and request_status!='0' and request_status!='4'  and user_id='$user_id' ORDER BY id desc";
        $cArray = $this->Database->select_qry_array($qry);
        $return = [];$special_off=[];$count_n=0;
        
        for ($i = 0; $i < count($cArray); $i++) {
            $dft = $cArray[$i];
            $category_De=GetCategorydetailsBy($dft->category_id);
            $category_name=!empty($category_De)?$category_De->category:"";
             $image = !empty($category_De->category_image) ?$category_De->category_image:'';
            $image_array=explode(',',$image); $image_url=[];
            for($j=0;$j<count($image_array);$j++)
            {
                $image_url[$j]=!empty($image_array[$j]) ? base_url() . "files/category_image/" . $image_array[$j]: '';
            }
           $em_De=GetemiratedetailsBy($dft->emirate_id);
            $emirate_name=!empty($em_De)?$em_De->emirate:"";
            $re_De=GetregiondetailsBy($dft->region_id);
            $region_name=!empty($re_De)?$re_De->region:"";
            $return[$i] = array(
                'id' => $dft->id,
                'request_no' => !empty($dft->request_no)?$dft->request_no :'',
                'category_name'=>$category_name,
                'category_id'=>!empty($dft->category_id)?$dft->category_id :'',
                'image_url'=>$image_url,
                'emirate_id'=>!empty($dft->emirate_id)?$dft->emirate_id :'',
                'emirate'=>!empty($emirate_name)?$emirate_name :'',

                'region_id'=>!empty($dft->region_id)?$dft->region_id :'',
                'region'=>!empty($region_name)?$region_name :'',

                'timestamp'=>!empty($dft->timestamp)?$dft->timestamp :'',

               'layout_type'=>isset($category_De->layout_type)?(int)$category_De->layout_type :'',
                );
            
            }

             //   $result_merge = array_merge($return, $special_off);
        $qry1 = "SELECT * FROM `offer_banner`  where archive='0' and status='0' ORDER BY id DESC";
        $cArray1 = $this->Database->select_qry_array($qry1);
        $offerimage = [];
        for ($k = 0; $k < count($cArray1); $k++) {
            $dryr = $cArray1[$k];
           
            $offerimage[$k] = base_url() . 'uploads/offer_banner/' .$dryr->image;
        }
        $qry1 = "SELECT offer_request_table.* FROM `offer_request_table` LEFT JOIN offers ON offer_request_table.offer_id=offers.id  where offer_request_table.archive='0' and offer_request_table.request_status!='3' and offer_request_table.request_status!='4' and offer_request_table.request_status!='0'  and offer_request_table.user_id='$user_id' and offers.appointment=1 ORDER BY offer_request_table.id DESC";
        $cArray1 = $this->Database->select_qry_array($qry1);
        $return1 = [];
         for ($off = 0; $off < count($cArray1); $off++) {
            $dryr = $cArray1[$off];       
            $offer_id=!empty($dryr->offer_id)?$dryr->offer_id :'';
            $offer_det = GetOfferdetailsBy($offer_id);
            $from=!empty($offer_det->valid_from)?$offer_det->valid_from:"";
            $to=!empty($offer_det->valid_to)?$offer_det->valid_to:"";
            $cur_date=date("Y-m-d");
            if($cur_date>$to || $cur_date<=$from)
            {
                $is_exp=true;
            }
            else
            {
                $is_exp=false;

            }
            $category = !empty($offer_det->category)?$offer_det->category :'';
            $category_De=GetOfferCategorydetailsBy($category);
            $category_name=!empty($category_De)?$category_De->category:"";
            $garage_id= !empty($offer_det->garage_id)?$offer_det->garage_id :'';
            $garage_de = GetgaragedetailsBy($garage_id);
            $garage_name=!empty($garage_de)?$garage_de->garage_name:"";
            $image = !empty($offer_det->offer_image) ?$offer_det->offer_image:'';
            $image_array=explode(',',$image); $image_url=[];
            for($m=0;$m<count($image_array);$m++)
            {
                $image_url[$m]=!empty($image_array[$m]) ? base_url() . "files/offer_image/" . $image_array[$m]: '';
            }
            $return1[$off] = array(
                'id' => $dryr->id,
                'request_no' => !empty($dryr->request_no)?$dryr->request_no :'',
                'offer_id' => !empty($dryr->offer_id)?$dryr->offer_id :'',
                'offer_title' => !empty($offer_det->title)?$offer_det->title :'',
                'offer_category' => !empty($category_name)?$category_name :'',
                'garage_id' => !empty($garage_id)?$garage_id :'',
                'garage_name'=>$garage_name,
                'layout_type'=>6,
                'total_price' => !empty($offer_det->total_price)?$offer_det->total_price :'',
                'VAT' => !empty($offer_det->vat)?$offer_det->vat :'',
                'appointment' => 1,
                'is_freeCoupon'=>!empty($dryr->is_freeCoupon)?$dryr->is_freeCoupon :'',
                'isExpiry'=>$is_exp,
                'timestamp'=>!empty($dryr->timestamp)?$dryr->timestamp :'',
                'image_url'=>$image_url,
                );
                    }

        $result_merge = array_merge($return, $return1);
            // Sort the array 
        usort($result_merge, 'date_compare');
        if((!empty($result_merge)) || ($result_merge!= null)){
            $result['request_list'] = $result_merge;
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
        else{
            $result['request_list'] = [];
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("No_new_request")), 'result' => $result)));
        }
        
    }
    
    public function new_orders_list() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
       
        $qry1 = "SELECT * FROM `offer_banner`  where archive='0' and status='0' ORDER BY id DESC";
        $cArray1 = $this->Database->select_qry_array($qry1);
        $offerimage = [];
        for ($k = 0; $k < count($cArray1); $k++) {
            $dryr = $cArray1[$k];
            $offerimage[$k] = base_url() . 'uploads/offer_banner/' .$dryr->image;
        }
        
        $qry1 = "SELECT offer_request_table.* FROM `offer_request_table` LEFT JOIN offers ON offer_request_table.offer_id=offers.id  where offer_request_table.archive='0' and offer_request_table.request_status='0'  and offer_request_table.user_id='$user_id' and offers.appointment=0 and offer_request_table.is_freeCoupon!=1 ORDER BY offer_request_table.id DESC";
        $cArray1 = $this->Database->select_qry_array($qry1);
        $return1 = [];
        for ($k = 0; $k < count($cArray1); $k++) {
            $dryr = $cArray1[$k];
            $offer_id=!empty($dryr->offer_id)?$dryr->offer_id :'';
            $offer_det = GetOfferdetailsBy($offer_id);
            $from=!empty($offer_det->valid_from)?$offer_det->valid_from:"";
            $to=!empty($offer_det->valid_to)?$offer_det->valid_to:"";
            $cur_date=date("Y-m-d");
            if($cur_date>$to || $cur_date<=$from)
            {
                $is_exp=true;
            }
            else
            {
                $is_exp=false;

            }
            $category = !empty($offer_det->category)?$offer_det->category :'';
            $category_De=GetOfferCategorydetailsBy($category);
            $category_name=!empty($category_De)?$category_De->category:"";
            $garage_id= !empty($offer_det->garage_id)?$offer_det->garage_id :'';
            $garage_de = GetgaragedetailsBy($garage_id);
            $garage_name=!empty($garage_de)?$garage_de->garage_name:"";
            $image = !empty($offer_det->offer_image) ?$offer_det->offer_image:'';
            $image_array=explode(',',$image); $image_url=[];
            for($m=0;$m<count($image_array);$m++)
            {
                $image_url[$m]=!empty($image_array[$m]) ? base_url() . "files/offer_image/" . $image_array[$m]: '';
            }
            
            $return1[$k] = array(
                'id' => $dryr->id,
                'request_no' => !empty($dryr->request_no)?$dryr->request_no :'',
                'offer_id' => !empty($dryr->offer_id)?$dryr->offer_id :'',
                'offer_title' => !empty($offer_det->title)?$offer_det->title :'',
                'offer_category' => !empty($category_name)?$category_name :'',
                'garage_id' => !empty($garage_id)?$garage_id :'',
                'garage_name'=>$garage_name,
                'layout_type'=>6,
                'timestamp'=>!empty($dryr->timestamp)?$dryr->timestamp :'',
                'image_url'=>$image_url,
                'request_status'=>0,
                'total_price' => !empty($offer_det->total_price)?$offer_det->total_price :'',
                'VAT' => !empty($offer_det->vat)?$offer_det->vat :'',
                'appointment' => 0,
                'is_freeCoupon'=>!empty($dryr->is_freeCoupon)?$dryr->is_freeCoupon :'',
                'isExpiry'=>$is_exp,
                'invoice_pdf' => !empty($dryr->invoice)?base_url() . 'uploads/invoice/'.$dryr->invoice :''
                );
            }
        
        // usort($result_merge, 'date_compare');
        if((!empty($return1)) || ($return1!= null)){
            $result['orders_list'] = $return1;
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
        else{
            $result['orders_list'] = [];
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("No_new_order")), 'result' => $result)));
        }
    }
    
    public function completed_orders_list() {
        $json_re = file_get_contents('php://input');
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        
        $qry1 = "SELECT * FROM `offer_banner`  where archive='0' and status='0' ORDER BY id DESC";
        $cArray1 = $this->Database->select_qry_array($qry1);
        $offerimage = [];
        for ($k = 0; $k < count($cArray1); $k++) {
            $dryr = $cArray1[$k];
            $offerimage[$k] = base_url() . 'uploads/offer_banner/' .$dryr->image;
        }
        
        $qry1 = "SELECT offer_request_table.* FROM `offer_request_table` LEFT JOIN offers ON offer_request_table.offer_id=offers.id  where offer_request_table.archive='0' and offer_request_table.request_status='3' and offer_request_table.is_freeCoupon!=1  and offer_request_table.user_id='$user_id' and offers.appointment=0 ORDER BY offer_request_table.id DESC";
        $cArray1 = $this->Database->select_qry_array($qry1);
        $return1 = [];
        for ($off = 0; $off < count($cArray1); $off++) {
            $dryr = $cArray1[$off];
            $offer_id=!empty($dryr->offer_id)?$dryr->offer_id :'';
            $offer_det = GetOfferdetailsBy($offer_id);
            $from=!empty($offer_det->valid_from)?$offer_det->valid_from:"";
            $to=!empty($offer_det->valid_to)?$offer_det->valid_to:"";
            $cur_date=date("Y-m-d");
            if($cur_date>=$to || $cur_date<=$from)
            {
                $is_exp=true;
            }
            else
            {
            $is_exp=false;

            }
            $category = !empty($offer_det->category)?$offer_det->category :'';
            $vat = !empty($offer_det->vat)?$offer_det->vat :'';
            $category_De=GetOfferCategorydetailsBy($category);
            $category_name=!empty($category_De)?$category_De->category:"";
            $garage_id= !empty($offer_det->garage_id)?$offer_det->garage_id :'';
            $garage_de = GetgaragedetailsBy($garage_id);
            $garage_name=!empty($garage_de)?$garage_de->garage_name:"";
            $image = !empty($offer_det->offer_image) ?$offer_det->offer_image:'';
            $image_array=explode(',',$image); $image_url=[];
            for($m=0;$m<count($image_array);$m++)
            {
                $image_url[$m]=!empty($image_array[$m]) ? base_url() . "files/offer_image/" . $image_array[$m]: '';
            }
            $return1[$off] = array(
                'id' => $dryr->id,
                'request_no' => !empty($dryr->request_no)?$dryr->request_no :'',
                'offer_id' => !empty($dryr->offer_id)?$dryr->offer_id :'',
                'offer_title' => !empty($offer_det->title)?$offer_det->title :'',
                'offer_category' => !empty($category_name)?$category_name :'',
                'garage_id' => !empty($garage_id)?$garage_id :'',
                'garage_name'=>$garage_name,
                'layout_type'=>6,
                'isExpiry'=>$is_exp,
                'timestamp'=>!empty($dryr->timestamp)?$dryr->timestamp :'',
                'payment_method'=>!empty($dryr->payment_type)?$dryr->payment_type :'',
                'total_price' => !empty($offer_det->total_price)?$offer_det->total_price :'',
                'VAT'=>$vat,
                'request_status'=>3,
                'image_url'=>$image_url,
            );
        }
        // $result_merge = array_merge($return, $return1);
        // usort($result_merge, 'date_compare');
        if((!empty($return1)) || ($return1!= null)){
            $result['orders_list'] = $return1;
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
        else{
            $result['orders_list'] = [];
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("No_new_order")), 'result' => $result)));
        }
    }
    
    public function quotation_request_list() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $request_id = !empty($json['request_id']) ? $json['request_id'] : '';
        $quotations = get_garage_request($request_id);
        $request = GetrequestdetailsBy($request_id);
        $category_id=!empty($request->category_id)?$request->category_id:"";
        $request_date=!empty($request->timestamp)?$request->timestamp:"";
        $catname = GetCategorydetailsBy($category_id);
        
        $layout_type=isset($catname->layout_type)?$catname->layout_type:'';
        //no app in sepcials ervcie,checkup,recovery
        if($category_id==9||$category_id==5||$category_id==8)
        {
          $is_approve =2;   
        }
        else
        {
           $is_approve =1;   
 
        }
        $return=array();$quite_re_id='';
        for ($i = 0; $i < count($quotations); $i++) {
            $return_main=array();$return_sub=array();
            $dft = $quotations[$i];
            $garage_id = !empty($dft->garage_id) ? $dft->garage_id : '';
            $quote_id=!empty($dft->id) ? $dft->id : '';
                if($dft->quote=='1'&&$dft->approve_status == 4)
                {
                    $quite_re_id=$quote_id;
                    $quote_charges=get_quote_charges($quote_id,$request_id);
                    if(count($quote_charges)>0){
                                                   for($p=0;$p<count($quote_charges);$p++)
                                                    {
                                                        $des=!empty($quote_charges[$p]->description)?$quote_charges[$p]->description:"";
                                                        $price=!empty($quote_charges[$p]->price)?$quote_charges[$p]->price ." AED":"";
                                                        $return_sub[$p] = array('title'=>$des,'value'=>$price);
                                                    }
                    }
               
                    $extra_charge = !empty($dft->extra_charge) ? $dft->extra_charge ." AED" : '';
                    $labour_charge = !empty($dft->labour_charge) ? $dft->labour_charge." AED" : '';
                    $total_price = !empty($dft->total_price) ? $dft->total_price." AED" : '';
                    $vat = !empty($dft->vat) ? $dft->vat." AED" : '';
                    $grand_total = !empty($dft->grand_total) ? $dft->grand_total." AED" : '';
                    $spare_parts = !empty($dft->spare_parts) ? $dft->spare_parts." AED" : '';
                    $parts_open= !empty($dft->parts_open) ? $dft->parts_open : '';
                    $checkup_charges= !empty($dft->checkup_charges) ? $dft->checkup_charges." AED" : '';
                    $crane_service= !empty($dft->crane_service) ? $dft->crane_service." AED" : '';
                    $service_charges= !empty($dft->service_charge) ? $dft->service_charge." AED" : '';
                    $lump_sum= !empty($dft->lump_sum) ? $dft->lump_sum." AED" : '';
                    $working_days = !empty($dft->working_days) ? $dft->working_days." Days" : '';
                    $quote_receive_date = !empty($dft->quote_receive_date) ? $dft->quote_receive_date: '';
                    $comments = !empty($dft->comments) ? $dft->comments : '';
                    $is_recommended = !empty($dft->is_recommended) ? $dft->is_recommended : '';
                    $distance = !empty($dft->distance) ? $dft->distance : '';

                    if($labour_charge!='')
                    {
                    $return_main[] = array('title'=>$this->lang->line("labour_Changes"),'value'=>$labour_charge);
                    }
                     if($extra_charge!='')
                    {
                    $return_main[] = array('title'=>$this->lang->line("Extra_Changes"),'value'=>$extra_charge);
                    }
                    if($spare_parts!='')
                    {
                    $return_main[] = array('title'=>$this->lang->line("Spare_Parts"),'value'=>$spare_parts);
                    }
                    
                     if($crane_service!='')
                    {
                    $return_main[] = array('title'=>$this->lang->line("Crane_Service"),'value'=>$crane_service);
                    }
                    if($checkup_charges!='')
                    {
                    $return_main[] = array('title'=>$this->lang->line("Checkup_Charges"),'value'=>$checkup_charges);
                    }
                     if($service_charges!='')
                    {
                    $return_main[] = array('title'=>$this->lang->line("Service_charge"),'value'=>$service_charges);
                    }
                    if($lump_sum!='')
                    {
                    $return_main[] = array('title'=>$this->lang->line("LUMP_SUM"),'value'=>$lump_sum);
                    }
                    
                    if($comments!='')
                    {
                    $return_main[] = array('title'=>$this->lang->line("Comments"),'value'=>$comments);
                    }
                     if($parts_open!='')
                    {
                    $return_main[] = array('title'=>$this->lang->line("Parts_Open"),'value'=>'✓');
                    }
                    $return_main1=array(array('title'=>$this->lang->line("Working_Days"),'value'=>$working_days),array('title'=>$this->lang->line("vat"),'value'=>$vat),array('title'=>$this->lang->line("Total_Price"),'value'=>$total_price),array('title'=>$this->lang->line("grand_total"),'value'=>$grand_total));
                    
                    $result_price=array_merge( $return_sub,$return_main,$return_main1);
                    $garage_name=  GetgaragedetailsBy($garage_id);
                    
                    $garage=!empty($garage_name->garage_name)?$garage_name->garage_name:"";
                    $garage_latitude=!empty($garage_name->latitude)?$garage_name->latitude:"";
                    $garage_longitude=!empty($garage_name->longitude)?$garage_name->longitude:"";
                    $garage_location=!empty($garage_name->location)?$garage_name->location:"";
                    $return[]=array(
                        'request_garage_id'=>$quote_id,
                        'garage_id'=>$garage_id,
                        'garage_name'=>$garage,
                        'garage_location'=>$garage_location,
                        'garage_latitude'=>$garage_latitude,
                        'garage_longitude'=>$garage_longitude,
                        'request_id'=>$request_id,
                        'request_date'=>$request_date,
                        'quote_receive_date'=>$quote_receive_date,
                        'layout_type'=>(int)$layout_type,
                        'price_list'=>$result_price,
                        'appointment_option'=>(int)$is_approve,
                        'is_recommended'=>$is_recommended,
                        'distance'=>$distance
                    );
                }
        }

        for ($i = 0; $i < count($quotations); $i++) {
            $return_main=array();$return_sub=array();
            $dft = $quotations[$i];
            $garage_id = !empty($dft->garage_id) ? $dft->garage_id : '';
            $quote_id=!empty($dft->id) ? $dft->id : '';
            if($dft->quote=='1'&&$dft->approve_status == 1)
            {
                $quote_charges=get_quote_charges($quote_id,$request_id);
                if(count($quote_charges)>0){
                                               for($p=0;$p<count($quote_charges);$p++)
                                                {
                                                    $des=!empty($quote_charges[$p]->description)?$quote_charges[$p]->description:"";
                                                    $price=!empty($quote_charges[$p]->price)?$quote_charges[$p]->price." AED":"";
                                                    $return_sub[$p] = array('title'=>$des,'value'=>$price);
                                                }
                }
              
                $extra_charge = !empty($dft->extra_charge) ? $dft->extra_charge ." AED" : '';
                $labour_charge = !empty($dft->labour_charge) ? $dft->labour_charge." AED" : '';
                $total_price = !empty($dft->total_price) ? $dft->total_price." AED" : '';
                $vat = !empty($dft->vat) ? $dft->vat." AED" : '';
                $grand_total = !empty($dft->grand_total) ? $dft->grand_total." AED" : '';
                $spare_parts = !empty($dft->spare_parts) ? $dft->spare_parts." AED" : '';
                $parts_open= !empty($dft->parts_open) ? $dft->parts_open : '';
                $checkup_charges= !empty($dft->checkup_charges) ? $dft->checkup_charges." AED" : '';
                $crane_service= !empty($dft->crane_service) ? $dft->crane_service." AED" : '';
                $service_charges= !empty($dft->service_charge) ? $dft->service_charge." AED" : '';
                $lump_sum= !empty($dft->lump_sum) ? $dft->lump_sum." AED" : '';
                $working_days = !empty($dft->working_days) ? $dft->working_days." Days" : '';
                $quote_receive_date = !empty($dft->quote_receive_date) ? $dft->quote_receive_date: '';
                $comments = !empty($dft->comments) ? $dft->comments : '';
                $is_recommended = !empty($dft->is_recommended) ? $dft->is_recommended : '';
                $distance = !empty($dft->distance) ? $dft->distance : '';
                if($labour_charge!='')
                {
                $return_main[] = array('title'=>$this->lang->line("labour_Changes"),'value'=>$labour_charge);
                }
                 if($extra_charge!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Extra_Changes"),'value'=>$extra_charge);
                }
                if($spare_parts!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Spare_Parts"),'value'=>$spare_parts);
                }
                
                 if($crane_service!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Crane_Service"),'value'=>$crane_service);
                }
                if($checkup_charges!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Checkup_Charges"),'value'=>$checkup_charges);
                }
                 if($service_charges!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Service_charge"),'value'=>$service_charges);
                }
                if($lump_sum!='')
                {
                $return_main[] = array('title'=>$this->lang->line("LUMP_SUM"),'value'=>$lump_sum);
                }
                
                if($comments!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Comments"),'value'=>$comments);
                }
                 if($parts_open!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Parts_Open"),'value'=>'✓');
                }
                $return_main1=array(array('title'=>$this->lang->line("Working_Days"),'value'=>$working_days),array('title'=>$this->lang->line("vat"),'value'=>$vat),array('title'=>$this->lang->line("Total_Price"),'value'=>$total_price),array('title'=>$this->lang->line("grand_total"),'value'=>$grand_total));
                
                $result_price=array_merge( $return_sub,$return_main,$return_main1);
                $garage_name=  GetgaragedetailsBy($garage_id);
                $garage_name=!empty($garage_name->garage_name)?$garage_name->garage_name:"";
                $return[]=array(
                    'request_garage_id'=>$quote_id,
                    'garage_id'=>$garage_id,
                    'garage_name'=>$garage_name,
                    'request_id'=>$request_id,
                    'request_date'=>$request_date,
                    'quote_receive_date'=>$quote_receive_date,
                    'layout_type'=>(int)$layout_type,
                    'price_list'=>$result_price,
                    'appointment_option'=>(int)$is_approve,
                    'is_recommended'=>$is_recommended,
                    'distance'=>$distance
                );
            }
        }
        $result['quotation_list'] = $return;
        $result['accepted_quotation_id']=$quite_re_id;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

      public function request_details_list() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $request_id = !empty($json['request_id']) ? $json['request_id'] : '';
        $layout_type1 = !empty($json['layout_type']) ? (int)$json['layout_type'] : '';
        if($layout_type1!=6)
        {
        $quotations = get_garage_request($request_id);
        $request = GetrequestdetailsBy($request_id);
        $category_id=!empty($request->category_id)?$request->category_id:"";
        $request_date=!empty($request->timestamp)?$request->timestamp:"";
        $catname = GetCategorydetailsBy($category_id);

        $layout_type=isset($catname->layout_type)?$catname->layout_type:'';
        
        if($this->language == 'AR'){
            if(!empty($catname->category_ar)){
                $catname = $catname->category_ar;
            }
            else{
                if(!empty($catname->category)){
                    $catname = $catname->category;
                }
                else{
                    $catname = "";
                }
            }
        }
        else{
            if(!empty($catname->category)){
                    $catname = $catname->category;
                }
                else{
                    $catname = "";
                }
        }
        $category_name=$catname;
        // $category_name=$this->language == 'AR' ?(!empty($catname->category_ar)?$catname->category_ar :!empty($catname->category)?$catname->category:""): (!empty($catname->category)?$catname->category:"");
        
        if($this->language == 'AR'){
            if(!empty($em_De->emirate_ar)){
                $emirate = $em_De->emirate_ar;
            }
            else{
                if(!empty($em_De->emirate)){
                    $emirate = $em_De->emirate;
                }
                else{
                    $emirate = "";
                }
            }
        }
        else{
            if(!empty($em_De->emirate)){
                    $emirate = $em_De->emirate;
                }
                else{
                    $emirate = "";
                }
        }
        // $emirate_name=$this->language == 'AR' ?(!empty($em_De->emirate_ar)?$em_De->emirate_ar :!empty($em_De->emirate)?$em_De->emirate:""): (!empty($em_De->emirate)?$em_De->emirate:"") ;
        
        if($this->language == 'AR'){
            if(!empty($re_De->region_ar)){
                $region = $re_De->region_ar;
            }
            else{
                if(!empty($re_De->region)){
                    $region = $re_De->region;
                }
                else{
                    $region = "";
                }
            }
        }
        else{
            if(!empty($re_De->region)){
                    $region = $re_De->region;
                }
                else{
                    $region = "";
                }
        }
        // $region_name=$this->language == 'AR' ?(!empty($re_De->region_ar)?$re_De->region_ar :!empty($re_De->region)?$re_De->region:""): (!empty($re_De->region)?$re_De->region:"") ;
        $emirate_id=!empty($request->emirate_id)?$request->emirate_id:"";
                $region_id=!empty($request->region_id)?$request->region_id:"";

        $catname = GetCategorydetailsBy($category_id);
        $em_De=GetemiratedetailsBy($emirate_id);
        $emirate_name=$emirate;
        $re_De=GetregiondetailsBy($region_id);
        $region_name=$region;
        
        $return=array();$return_his=array();
        for ($i = 0; $i < count($quotations); $i++) {
            $return_main=array();$return_sub=array();
        $dft = $quotations[$i];
        $garage_id = !empty($dft->garage_id) ? $dft->garage_id : '';
        
        $quote_id=!empty($dft->id) ? $dft->id : '';
                if($dft->quote=='1'&&$dft->approve_status == 4)
                {
                    $quote_charges=get_quote_charges($quote_id,$request_id);
                    if(count($quote_charges)>0){
                                                   for($p=0;$p<count($quote_charges);$p++)
                                                    {
                                                        $des=!empty($quote_charges[$p]->description)?$quote_charges[$p]->description:"";
                                                        $price=!empty($quote_charges[$p]->price)?$quote_charges[$p]->price." AED":"";
                                                        $return_sub[$p] = array('title'=>$des,'value'=>$price);
                                                    }
                    }
              
                $extra_charge = !empty($dft->extra_charge) ? $dft->extra_charge ." AED" : '';
                $labour_charge = !empty($dft->labour_charge) ? $dft->labour_charge." AED" : '';
                $total_price = !empty($dft->total_price) ? $dft->total_price." AED" : '';
                   
                    $vat = !empty($dft->vat) ? $dft->vat." AED" : '';
                                   $grand_total = !empty($dft->grand_total) ? $dft->grand_total." AED" : '';
  
                $spare_parts = !empty($dft->spare_parts) ? $dft->spare_parts." AED" : '';

                $parts_open= !empty($dft->parts_open) ? $dft->parts_open: '';

                $checkup_charges= !empty($dft->checkup_charges) ? $dft->checkup_charges." AED" : '';

                $crane_service= !empty($dft->crane_service) ? $dft->crane_service." AED" : '';
                $service_charges= !empty($dft->service_charge) ? $dft->service_charge." AED" : '';

                $lump_sum= !empty($dft->lump_sum) ? $dft->lump_sum." AED" : '';

                $working_days = !empty($dft->working_days) ? $dft->working_days." Days" : '';
                $quote_receive_date = !empty($dft->quote_receive_date) ? $dft->quote_receive_date: '';
                                $comments = !empty($dft->comments) ? $dft->comments : '';

             if($labour_charge!='')
                {
                $return_main[] = array('title'=>$this->lang->line("labour_Changes"),'value'=>$labour_charge);
                }
                 if($extra_charge!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Extra_Changes"),'value'=>$extra_charge);
                }
                if($spare_parts!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Spare_Parts"),'value'=>$spare_parts);
                }
                
                 if($crane_service!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Crane_Service"),'value'=>$crane_service);
                }
                if($checkup_charges!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Checkup_Charges"),'value'=>$checkup_charges);
                }
                 if($service_charges!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Service_charge"),'value'=>$service_charges);
                }
                if($lump_sum!='')
                {
                $return_main[] = array('title'=>$this->lang->line("LUMP_SUM"),'value'=>$lump_sum);
                }
                
                if($comments!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Comments"),'value'=>$comments);
                }
                 if($parts_open!='')
                {
                $return_main[] = array('title'=>$this->lang->line("Parts_Open"),'value'=>'✓');
                }
                $return_main1=array(array('title'=>$this->lang->line("Working_Days"),'value'=>$working_days),array('title'=>$this->lang->line("vat"),'value'=>$vat),array('title'=>$this->lang->line("Total_Price"),'value'=>$total_price),array('title'=>$this->lang->line("grand_total"),'value'=>$grand_total));
                
                $result_price=array_merge( $return_sub,$return_main,$return_main1);
                //get job history
               
                $return_his=array();
                
                
                $app_st =[0,4,3,5,1];$appo_date='';
                 $select_jon="select * from appointment where request_id='$request_id' and archive='0' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $appo_date=$Array[0]->appoint_date;
                        }
                        
                for($a=0;$a<count($app_st);$a++)
                {
                    $his=$app_st[$a];
                    $is_completed=false;$status_name='';$date ='';$appointment_date='';
                   
                    if($his==0)
                    {
                        $status_name = $this->lang->line("Appointment_Received");
                         $select_jon="select * from appointment where request_id='$request_id' and archive='0' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $date=$Array[0]->timestamp;
                        }
                        $is_completed=true;
                        $appointment_date='';
                    }

                 
                     else if($his==4)
                    {
                     $status_name = $this->lang->line("Appointment_Approved");
                      $select_jon="select * from appointment_history where request_id='$request_id' and appointment_status='$his' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $is_completed=true;
                            $date=$Array[0]->timestamp;
                        }
                    $appointment_date=$appo_date;

                    }
                       else if($his==3)
                    {
                         $status_name = $this->lang->line("Vehicle_Received");
                           $select_jon="select * from appointment_history where request_id='$request_id' and appointment_status='$his' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $is_completed=true;
                            $date=$Array[0]->timestamp;
                        }
                        $appointment_date='';
                    }
                    else if($his==5)
                    {
                         $status_name =  $this->lang->line("Job_Started");
                           $select_jon="select * from appointment_history where request_id='$request_id' and appointment_status='$his' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $is_completed=true;
                            $date=$Array[0]->timestamp;
                        }
                        $appointment_date='';
                    }
                    else if($his==1)
                    {
                         $status_name = $this->lang->line("Job_Completed");
                           $select_jon="select * from appointment_history where request_id='$request_id' and appointment_status='$his' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $is_completed=true;
                            $date=$Array[0]->timestamp;
                        }
                        $appointment_date='';
                    }
                    $return_his[$a]=array('title'=>$status_name,'date'=>$date,'is_completed'=>$is_completed,'appointment_date'=>$appointment_date);
                    
                }

                //end
            $garage_name=  GetgaragedetailsBy($garage_id);
            $garage_name1=!empty($garage_name->garage_name)?$garage_name->garage_name:"";
            $location_image=!empty($garage_name->location_image)?$garage_name->location_image:"";
            $location=!empty($garage_name->location) ? $garage_name->location : '';
            $latitude=!empty($garage_name->latitude) ? $garage_name->latitude : '';
            $longitude=!empty($garage_name->longitude) ? $garage_name->longitude : '';
            $request_status=!empty($request->request_status) ? $request->request_status : '';
                    if($request_status == 0){
                        $status = 'Received';
                    }
                    else if($request_status == 1){
                        $status = 'Confirmed';
                    }
                    else if($request_status == 2){
                        $status = 'Scheduled';
                    }
                    else if($request_status == 3){
                        $status = 'Completed';
                    }
                    else{
                        $status = 'Cancelled';
                    }
                    $rquest_com ['request_status']=$status;
                $return=array(
                    'request_garage_id'=>$quote_id,
                    'garage_id'=>$garage_id,
                    'garage_name'=>$garage_name1,
                    'location_image'=>!empty($location_image) ? base_url() . "files/garage_image/" . $location_image: '',
                    'location'=>$location,
                    'latitude'=>$latitude,
                    'longitude'=>$longitude,
                    'request_id'=>$request_id,
                    'request_no' => !empty($request->request_no)?$request->request_no :'',
                    'category_name'=>$category_name,
                    'category_id'=>!empty($category_id)?$category_id :'',
                    'emirate_id'=>!empty($request->emirate_id)?$request->emirate_id :'',
                    'emirate'=>!empty($emirate_name)?$emirate_name :'',
                    'region_id'=>!empty($request->region_id)?$request->region_id :'',
                    'request_status'=> $status,
                    'payment_status'=>isset($request->payment_status)?$request->payment_status :'',
                    'payment_type'=>isset($request->payment_type)?$request->payment_type :'',
                    'region_id'=>!empty($request->region_id)?$request->region_id :'',
                    'region'=>!empty($region_name)?$region_name :'',
                    'request_date'=>$request_date,
                    'quote_receive_date'=>$quote_receive_date,
                    'price_list'=>$result_price,
                    );
                }
        }
        
        
        //add details
         $make_name = GetmakedetailsBy($request->make);
            $model_name = GetmodeldetailsBy($request->model);
            $year_name = GetyeardetailsBy($request->year);
            $plate_code_name = GetplatecodedetailsBy($request->plate_code);
            $plate_source_name = GetemiratedetailsBy($request->plate_source);
            
            // 'make'=>$this->language == 'AR' ?(!empty($make_name->make_ar)?$make_name->make_ar :!empty($make_name->make)?$make_name->make:""): (!empty($make_name->make)?$make_name->make:""),
            // 'model'=>$this->language == 'AR' ?(!empty($model_name->model_ar)?$model_name->model_ar :!empty($model_name->model)?$model_name->model :""): (!empty($model_name->model)?$model_name->model :""),
            // 'year'=>$this->language == 'AR' ?(!empty($year_name->year_ar)?$year_name->year_ar :!empty($year_name->year)?$year_name->year :""): (!empty($year_name->year)?$year_name->year :""),
                
            if($this->language == 'AR') {
                if(!empty($make_name->make_ar)){
                    $make = $make_name->make_ar;
                }
                else{
                    if(!empty($make_name->make)){
                        $make = $make_name->make;
                    }
                    else{
                        $make = "";
                    }
                }
            }
            else{
                if(!empty($make_name->make)){
                    $make = $make_name->make;
                }
                else{
                    $make = "";
                }
            }
            
            if($this->language == 'AR') {
                if(!empty($model_name->model_ar)){
                    $model = $model_name->model_ar;
                }
                else{
                    if(!empty($model_name->model)){
                        $model = $model_name->model;
                    }
                    else{
                        $model = "";
                    }
                }
            }
            else{
                if(!empty($model_name->model)){
                    $model = $model_name->model;
                }
                else{
                    $model = "";
                }
            }
            
            if($this->language == 'AR') {
                if(!empty($year_name->year_ar)){
                    $year = $year_name->year_ar;
                }
                else{
                    if(!empty($year_name->year)){
                        $year = $year_name->year;
                    }
                    else{
                        $year = "";
                    }
                }
            }
            else{
                if(!empty($year_name->year)){
                    $year = $year_name->year;
                }
                else{
                    $year = "";
                }
            }
            
        if($layout_type1!='7'&&$layout_type1!='3')
        {
            $result['vehicle'] = array(
                
                'make_id'=>$request->make,
                'model_id'=>$request->model,
                'year_id'=>$request->year,
                'make'=>$make,
                'model'=>$model,
                'year'=>$year,
             
                'plate_code_id' =>  $request->plate_code ,
                'plate_source_id'=>$request->plate_source,
                'plate_code' => !empty($plate_code_name->plate_code)?$plate_code_name->plate_code:'', 
                'plate_source'=>!empty($plate_source_name->emirate)?$plate_source_name->emirate:'',
                'plate_no'=>$request->plate_no,
                'gcc_type'=>$request->gcc_type,
                'chassis_no'=>$request->chassis_no,
                'car_icon' => !empty($make_name->make_icon) ? base_url() . "files/make_icon/" . $make_name->make_icon: '',
                );
}
        
        //end
        if(!empty($return))
        {
           $result['quotation_list'] = $return;
        }
        if(!empty($return_his))
        {
           $result['job_history'] = $return_his;
        }
            $result['invoice_pdf'] = !empty($request->invoice)?base_url() . 'uploads/invoice/'.$request->invoice :'';
            $select_jon="select * from appointment_history where request_id='$request_id' and appointment_status='1' limit 1";
            $Array = $this->Database->select_qry_array($select_jon);
            $select_ra="select * from rating where request_id='$request_id'  limit 1";
            $Array_ra = $this->Database->select_qry_array($select_ra);
                        if(count($Array)>0&&count($Array_ra)<1&&$request->payment_type!='')
                        {
                            $is_rating =true;
                        }
                        else
                        {
                        $is_rating =false;

                        }
                         if(count($Array)>0&&$request->payment_type=='')
                        {
                            $is_make_payment =true;
                        }
                        else
                        {
                        $is_make_payment =false;

                        }
                                   $result['is_rating'] = $is_rating;
                                   $result['is_make_payment'] = $is_make_payment;



        //add details
        //sub_category_list
        //accient repair
        if($layout_type1==0)
        {
            $return_sub=array();
            $qry = "SELECT * FROM `request_advanced_option` where  request_id='$request_id'   ORDER BY id desc";
            $cArray = $this->Database->select_qry_array($qry);
            for($i=0;$i<count($cArray);$i++)
            {
                $sub=$cArray[$i];
                $sub_cat_id = !empty($sub->subcategory_id)?$sub->subcategory_id:'';
                $subcategory = Getsubcategory_detailsBy($sub_cat_id);
                $return_sub[$i]= array(
                "id"=>!empty($sub->subcategory_id)?$sub->subcategory_id:'',
                "subcategory"=>  $this->language == 'AR' ?!empty($subcategory->subcategory_ar)?$subcategory->subcategory_ar :$subcategory->subcategory: $subcategory->subcategory ,
                "vehicle_parts_type"=>isset($sub->vehicle_parts_type)?$sub->vehicle_parts_type:'',
                "replace_type" => isset($sub->replace_type)?$sub->replace_type:'',
                "type"=> isset($sub->type)?$sub->type:'',
                );

            }
             $rquest_com['subcategory_list']   = $return_sub;
            
        }
        //vehicle paint
        else if($layout_type1==2)
        {
            if($this->language == 'AR') {
                if(!empty($paint_color->paint_color_ar)){
                    $paint_colour = $paint_color->paint_color_ar;
                }
                else{
                    if(!empty($paint_color->paint_color)){
                        $paint_colour = $paint_color->paint_color;
                    }
                    else{
                        $paint_colour = "";
                    }
                }
            }
            else{
                if(!empty($paint_color->paint_color)){
                    $paint_colour = $paint_color->paint_color;
                }
                else{
                    $paint_colour = "";
                }
            }
            
            if($this->language == 'AR') {
                if(!empty($paint_type->type_paints_ar)){
                    $paint_type = $paint_type->type_paints_ar;
                }
                else{
                    if(!empty($paint_type->type_paints)){
                        $paint_type = $paint_type->type_paints;
                    }
                    else{
                        $paint_type = "";
                    }
                }
            }
            else{
                if(!empty($paint_type->type_paints)){
                    $paint_type = $paint_type->type_paints;
                }
                else{
                    $paint_type = "";
                }
            }
            
            if($this->language == 'AR') {
                if(!empty($paint_method->paint_method_ar)){
                    $paint_method = $paint_method->paint_method_ar;
                }
                else{
                    if(!empty($paint_method->paint_method)){
                        $paint_method = $paint_method->paint_method;
                    }
                    else{
                        $paint_method = "";
                    }
                }
            }
            else{
                if(!empty($paint_method->paint_method)){
                    $paint_method = $paint_method->paint_method;
                }
                else{
                    $paint_method = "";
                }
            }
            
            // $rquest_com['paint_color']=$this->language == 'AR' ?(!empty($paint_color->paint_color_ar)?$paint_color->paint_color_ar :!empty($paint_color->paint_color)?$paint_color->paint_color:""): (!empty($paint_color->paint_color)?$paint_color->paint_color:"");
            // $rquest_com['paint_type']=$this->language == 'AR' ?(!empty($paint_type->type_paints_ar)?$paint_type->type_paints_ar :!empty($paint_type->type_paints)?$paint_type->type_paints:""): (!empty($paint_type->type_paints)?$paint_type->type_paints:"") ;
            // $rquest_com['paint_method']=$this->language == 'AR' ?(!empty($paint_method->paint_method_ar)?$paint_method->paint_method_ar :!empty($paint_method->paint_method)?$paint_method->paint_method:""): (!empty($paint_method->paint_method)?$paint_method->paint_method:"") ;
            
            $rquest_com['paint_color_id']=!empty($request->paint_color)?$request->paint_color:"";
            $rquest_com['paint_type_id']=!empty($request->paint_type)?$request->paint_type:"";
            $rquest_com['paint_method_id']=!empty($request->paint_method)?$request->paint_method:"";

            $paint_color = GetpaintcolordetailsBy($request->paint_color);
            $rquest_com['paint_color']=$paint_colour;
            $rquest_com['paint_color_code']=!empty($paint_color->paint_color_code)?$paint_color->paint_color_code:"";

            $paint_type=GettypepaintdetailsBy($request->paint_type);
            $rquest_com['paint_type']=$paint_type;
            $paint_method=GetpaintmethoddetailsBy($request->paint_method);
            $rquest_com['paint_method']=$paint_method;

        }
        //Mechanical repair 
        else if($layout_type1==1||$layout_type1==4||$layout_type1==5)
        {
         $advance_options_mach = !empty($request->advance_option)?$request->advance_option:"";
        $advance_options_mach=explode(',',$advance_options_mach);
        for($i=0;$i<count($advance_options_mach);$i++)
            {
                $sub=$advance_options_mach[$i];
                $sub_cat_id = !empty($sub)?$sub:'';
                $subcategory = Getsubcategory_detailsBy($sub_cat_id);
                $return_sub[$i]= array(
                "id"=>!empty($sub_cat_id)?$sub_cat_id:'',
                "subcategory"=>  $this->language == 'AR' ?!empty($subcategory->subcategory_ar)?$subcategory->subcategory_ar :$subcategory->subcategory: $subcategory->subcategory ,
              
                );

            }
            $rquest_com['subcategory_list']   = $return_sub;
        }
        if($layout_type1=='3'||$layout_type1=='5'||$layout_type1=='7')
        {
            $rquest_com ['location']=!empty($request->location) ? $request->location : '';
            $rquest_com ['latitude']=!empty($request->latitude) ? $request->latitude : '';
            $rquest_com ['longitude']=!empty($request->longitude) ? $request->longitude : '';
        }
        $request_status=!empty($request->request_status) ? $request->request_status : '';
        if($request_status == 0){
            $status = 'Received';
        }
        else if($request_status == 1){
            $status = 'Confirmed';
        }
        else if($request_status == 2){
            $status = 'Scheduled';
        }
        else if($request_status == 3){
            $status = 'Completed';
        }
        else{
            $status = 'Cancelled';
        }
        
        if($this->language == 'AR') {
            if(!empty($reg_name->region_ar)){
                $reg_name = $reg_name->region_ar;
            }
            else{
                if(!empty($reg_name->region)){
                    $reg_name = $reg_name->region;
                }
                else{
                    $reg_name = "";
                }
            }
        }
        else{
            if(!empty($reg_name->region)){
                $reg_name = $reg_name->region;
            }
            else{
                $reg_name = "";
            }
        }
        
        if($this->language == 'AR') {
            if(!empty($emir->emirate_ar)){
                $em_name = $emir->emirate_ar;
            }
            else{
                if(!empty($emir->emirate)){
                    $em_name = $emir->emirate;
                }
                else{
                    $em_name = "";
                }
            }
        }
        else{
            if(!empty($emir->emirate)){
                $em_name = $emir->emirate;
            }
            else{
                $em_name = "";
            }
        }
        // $rquest_com['region_name']= $this->language == 'AR' ?(!empty($reg_name->region_ar)?$reg_name->region_ar :!empty($reg_name->region)?$reg_name->region:""):(!empty($reg_name->region)? $reg_name->region :"");
        // $rquest_com['emirate_name']= $this->language == 'AR' ?(!empty($emir->emirate_ar)?$emir->emirate_ar :!empty($emir->emirate)? $emir->emirate:""): (!empty($emir->emirate)?  $emir->emirate:"");
        
        $rquest_com ['request_status']=$status;
        $rquest_com ['category_name']=$category_name;
        $rquest_com ['layout_type']=$layout_type1;
        $emirate_id=$request->emirate_id?$request->emirate_id:"";
        $region_id=$request->region_id?$request->region_id:"";
        $emir=GetemiratedetailsBy($emirate_id);
        $reg_name = GetregiondetailsBy($region_id);
        $rquest_com['region_name']= $reg_name;
        $rquest_com['emirate_name']= $em_name;

        //additional_comment
        $rquest_com ['additional_comment']=!empty($request->additional_comment)?$request->additional_comment:"";
         $cArray = get_request_images($request_id,'0');$return_img=array();
             for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $return_img[$i] = array(
                'image_url' =>!empty($dft->file_name) ? base_url() . "files/request_images/" . $dft->file_name: '',
                );
            }
             $cArray = get_request_images($request_id,'1');$return_doc=array();
             for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $return_doc[$i] = array(
                'doc_url' =>!empty($dft->file_name) ? base_url() . "files/request_images/" . $dft->file_name: '',
                );
            }
            $rquest_com ['document']=$return_doc;
            $rquest_com ['images']=$return_img;

        //end
        //end
        //end
        $result['request_details']=$rquest_com;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
        else
        {
             //get job history
               
                $return_his=array();
                
                
                $app_st =[0,4,3,5,1];
                $app_date='';
                 $select_jon="select * from offer_appointment where offer_request_id='$request_id' and archive='0' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $date=$Array[0]->timestamp;
                        }
                for($a=0;$a<count($app_st);$a++)
                {
                    $his=$app_st[$a];
                    $is_completed=false;$status_name='';$date ='';$appointment_date='';
                    if($his==0)
                    {
                        $status_name = $this->lang->line("Appointment_Received");
                         $select_jon="select * from offer_appointment where offer_request_id='$request_id' and archive='0' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $date=$Array[0]->timestamp;
                        }
                        $is_completed=true;
                        $appointment_date='';
                    }
                   
                      else if($his==4)
                    {
                     $status_name =$this->lang->line("Appointment_Approved");
                      $select_jon="select * from offer_appointment_history where offer_request_id='$request_id' and appointment_status='$his' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $is_completed=true;
                            $date=$Array[0]->timestamp;
                        }
                        $appointment_date=$app_date;
                    }
                    else if($his==3)
                    {
                         $status_name = $this->lang->line("Vehicle_Received");
                           $select_jon="select * from offer_appointment_history where offer_request_id='$request_id' and appointment_status='$his' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $is_completed=true;
                            $date=$Array[0]->timestamp;
                        }
                        $appointment_date='';
                    }
                   
                    else if($his==5)
                    {
                        $status_name = $this->lang->line("Job_Started");
                        $select_jon="select * from offer_appointment_history where offer_request_id='$request_id' and appointment_status='$his' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $is_completed=true;
                            $date=$Array[0]->timestamp;
                        }
                        $appointment_date='';
                    }
                    else if($his==1)
                    {
                        $status_name = $this->lang->line("Job_Completed");
                        $select_jon="select * from offer_appointment_history where offer_request_id='$request_id' and appointment_status='$his' limit 1";
                        $Array = $this->Database->select_qry_array($select_jon);
                        if(!empty($Array))
                        {
                            $is_completed=true;
                            $date=$Array[0]->timestamp;
                        }
                        $appointment_date='';
                    }
                    $return_his[$a]=array('title'=>$status_name,'date'=>$date,'is_completed'=>$is_completed,'appointment_date'=>$appointment_date);
                    
                }

                //end
                
                //OFFER DETAILS
                $request = GetofferrequestdetailsBy($request_id);
                $off_de = GetOfferdetailsBy($request->offer_id);
                $category_id=!empty($off_de->category)?$off_de->category:"";
                $catname = GetOfferCategorydetailsBy($category_id);
                $request_date=!empty($request->timestamp)?$request->timestamp:"";
                $garage_name=  GetgaragedetailsBy($request->garage_id);
                $location_image = !empty($garage_name->location_image)?$garage_name->location_image:"";
                $location=!empty($garage_name->location) ? $garage_name->location : '';
                $latitude=!empty($garage_name->latitude) ? $garage_name->latitude : '';
                $longitude=!empty($garage_name->longitude) ? $garage_name->longitude : '';
                $request_status = !empty($request->request_status) ? $request->request_status : '';
                if($request_status == 0){
                    $status = 'Received';
                }
                else if($request_status == 2)
                {
                    $status = 'Scheduled';
                }
                else if($request_status == 3)
                {
                    $status = 'Completed';
                }
                else{
                    $status = 'Cancelled';
                }
           //   $garage_name=!empty($garage_name->garage_name)?$garage_name->garage_name:"";
           
                if($this->language == 'AR') {
                    if(!empty($off_de->title_ar)){
                        $title = $off_de->title_ar;
                    }
                    else{
                        if(!empty($off_de->title)){
                            $title = $off_de->title;
                        }
                        else{
                            $title = "";
                        }
                    }
                }
                else{
                    if(!empty($off_de->title)){
                        $title = $off_de->title;
                    }
                    else{
                        $title = "";
                    }
                }
                
                if($this->language == 'AR') {
                    if(!empty($catname->category_ar)){
                        $cat_name = $catname->category_ar;
                    }
                    else{
                        if(!empty($catname->category)){
                            $cat_name = $catname->category;
                        }
                        else{
                            $cat_name = "";
                        }
                    }
                }
                else{
                    if(!empty($catname->category)){
                        $cat_name = $catname->category;
                    }
                    else{
                        $cat_name = "";
                    }
                }
                
                if($this->language == 'AR') {
                    if(!empty($garage_name->garage_name_ar)){
                        $gara_name = $garage_name->garage_name_ar;
                    }
                    else{
                        if(!empty($garage_name->garage_name)){
                            $gara_name = $garage_name->garage_name;
                        }
                        else{
                            $gara_name = "";
                        }
                    }
                }
                else{
                    if(!empty($garage_name->garage_name)){
                        $gara_name = $garage_name->garage_name;
                    }
                    else{
                        $gara_name = "";
                    }
                }
                // 'title' => $this->language == 'AR' ?(!empty($off_de->title_ar)?$off_de->title_ar :!empty($off_de->title)?$off_de->title:""):(!empty($off_de->title)?$off_de->title:"") ,
                // 'category_name' => $this->language == 'AR' ?(!empty($catname->category_ar)?$catname->category_ar :!empty($catname->category)?$catname->category:""): (!empty($catname->category)?$catname->category:""),
                // 'garage_name' => $this->language == 'AR' ?(!empty($garage_name->garage_name_ar)?$garage_name->garage_name_ar :!empty($garage_name->garage_name)?$garage_name->garage_name:""): (!empty($garage_name->garage_name)?$garage_name->garage_name:""),
                
               $result['special_offer_details']=        array(
                'id' => $off_de->id,
                'title' => $title,
                'original_price' => $off_de->original_price,
                'discount_price' => $off_de->discount_price,
                'vat'=>$off_de->vat,
                'total_price' => !empty($off_de->total_price)?$off_de->total_price :'',
                'grand_total' => !empty($off_de->grand_total)?$off_de->grand_total :'',
                'appointment' => $off_de->appointment,
                'free_coupon' => $off_de->free_coupon,
                'description' => $this->language == 'AR' ?!empty($off_de->description_ar)?$off_de->description_ar :$off_de->description: $off_de->description ,
                'valid_from' => $off_de->valid_from,
                'valid_to' => $off_de->valid_to,
                'category_name' => $cat_name,
                'category_id' => $off_de->category,
                'location_image'=>!empty($location_image) ? base_url() . "files/garage_image/" . $location_image: '',
                'location'=>$location,
                'latitude'=>$latitude,
                'longitude'=>$longitude,
                'garage_id' => $off_de->garage_id,
                'garage_name' => $gara_name,
                'location' => !empty($garage_name->location)?$garage_name->location :'',
                'latitude' => !empty($garage_name->latitude)?$garage_name->latitude :'',
                'longitude' => !empty($garage_name->longitude)?$garage_name->longitude :'',
                'layout_type'=>6
                );

                $return=array(
                    'garage_id'=>!empty($request->garage_id)?$request->garage_id:"",
                'garage_name' => $this->language == 'AR' ?!empty($garage_name->garage_name_ar)?$garage_name->garage_name_ar :$garage_name->garage_name: $garage_name->garage_name ,
                    'offer_title' =>  $this->language == 'AR' ?!empty($off_de->title_ar)?$off_de->title_ar :$off_de->title: $off_de->title ,
                    'valid_from' => !empty($off_de->valid_from)?$off_de->valid_from:"",
                    'valid_to' => !empty($off_de->valid_to)?$off_de->valid_to:"",
                    'request_id'=>$request_id,
                     'request_no' => !empty($request->request_no)?$request->request_no :'',
                    'category_name'=>!empty($catname->category)?$catname->category :'',
                    'category_id'=>!empty($category_id)?$category_id :'',
                    'request_date'=>$request_date,
                    'price_list'=>array(array('title'=>'Original Price','value'=>!empty($off_de->original_price)?$off_de->original_price." AED":""),array('title'=>'After Discount Price','value'=>!empty($off_de->total_price)?$off_de->total_price." AED":""),array('title'=>'Vat(% 5)','value'=>!empty($off_de->vat)?$off_de->vat." AED":""),array('title'=>'Total Price','value'=>!empty($off_de->grand_total)?$off_de->grand_total." AED":""))
                    );
                
                //END
                         $result['quotation_list'] = $return;
                         $result['job_history'] = $return_his;
            $result['invoice_pdf'] = !empty($request->invoice)?base_url() . 'uploads/invoice/'.$request->invoice :'';
            $select_jon="select * from offer_appointment_history where offer_request_id='$request_id' and appointment_status='1' limit 1";
            $Array = $this->Database->select_qry_array($select_jon);
            $select_ra="select * from rating where offer_request_id='$request_id'  limit 1";
            $Array_ra = $this->Database->select_qry_array($select_ra);
                       if(count($Array)>0&&count($Array_ra)<1&&$request->payment_type!='')
                        {
                            $is_rating =true;
                        }
                        else
                        {
                        $is_rating =false;

                        }
                         if($request->payment_type!='')
                        {
                            if($request->payment_status=='1')
                            {
                            $is_make_payment =true;
                            $is_paid=true;
                            }
                            else
                            {
                                $is_make_payment =false;
                                $is_paid=false;


                            }
                        }
                        else
                        {
                        $is_make_payment =false;
                            $is_paid=false;


                        }
                                   $result['is_rating'] = $is_rating;
                                   $result['is_make_payment'] = $is_make_payment;
                                   $result['is_paid'] = $is_paid;
                                   $result['request_status'] = $status;

                             die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
       
        }
     }
        //end request table
        
        
        function about_us_content()
        {
            $sql = "select * FROM cms where identify='ABOUT'";
            $Array = $this->Database->select_qry_array($sql);
            $data =  $this->language == 'AR' ?!empty($Array[0]->col1_ar)?$Array[0]->col1_ar :$Array[0]->col1: $Array[0]->col1 ;
            $result['content'] = $data;

            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
       
        }
         function app_data()
         {
            $sql = "select * FROM cms where identify='CALL_US'";
            $Array = $this->Database->select_qry_array($sql);
            $data =   $Array[0]->col1 ;
            $result['call_us'] = $data;
            
            $sql = "select * FROM company_info";
            $Array = $this->Database->select_qry_array($sql);
            $data =   $Array[0]->tollfree_no ;
            $result['tollfree_number'] = $data;
            
            $sql = "select * FROM cms where identify='CUSTOMER_SERVICE'";
            $Array = $this->Database->select_qry_array($sql);
            
            $data =  $this->language == 'AR' ?!empty($Array[0]->col1_ar)?$Array[0]->col1_ar :$Array[0]->col1: $Array[0]->col1 ;
            $result['customer_service_msg'] = $data;
            $sql = "select * FROM cms where identify='FEEDBACK'";
            $Array = $this->Database->select_qry_array($sql);
            
            $data =  $this->language == 'AR' ?!empty($Array[0]->col1_ar)?$Array[0]->col1_ar :$Array[0]->col1: $Array[0]->col1 ;
            $result['feedback_msg'] = $data;
            
            //img
            $cArray = Getintrodetails();
             for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $return[$i] = array(
                'image_url' =>!empty($dft->file_name) ? base_url() . "files/category_image/" . $dft->file_name: '',
                'title' =>  $dft->text
                );
        
            }
        $result['intro_screens'] = $return;
        
        
            $Array = Getlangdetails(2);
            
            $data = !empty($Array->file_name) ? base_url() . "files/images/" . $Array->file_name: '';
            $result['register_image'] = $data;
                        $Array = Getlangdetails(1);

            $data = !empty($Array->file_name) ? base_url() . "files/images/" . $Array->file_name: '';
            $result['language_image'] = $data;
            
                    die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));

         }
        function call_us()
        {
            $sql = "select * FROM cms where identify='CALL_US'";
            $Array = $this->Database->select_qry_array($sql);
            $data =   $Array[0]->col1 ;
            $result['content'] = $data;

            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
       
        }
         function customer_service()
        {
            $sql = "select * FROM cms where identify='CUSTOMER_SERVICE'";
            $Array = $this->Database->select_qry_array($sql);
            $data =  $this->language == 'AR' ?!empty($Array[0]->col1_ar)?$Array[0]->col1_ar :$Array[0]->col1: $Array[0]->col1 ;
            $result['content'] = $data;

            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
       
        }
          function privacy_policy()
        {
            $sql = "select * FROM cms where identify='PRIVACY_POLICY'";
            $Array = $this->Database->select_qry_array($sql);
            $data =  $this->language == 'AR' ?!empty($Array[0]->col1_ar)?$Array[0]->col1_ar :$Array[0]->col1: $Array[0]->col1 ;
            $result['content'] = $data;

            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
       
        }
        function terms_conditions()
        {
            $sql = "select * FROM cms where identify='TERMS_CONDITION'";
            $Array = $this->Database->select_qry_array($sql);
            $data =  $this->language == 'AR' ?!empty($Array[0]->col1_ar)?$Array[0]->col1_ar :$Array[0]->col1: $Array[0]->col1 ;
            $result['content'] = $data;

        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
       
        }
        
        function frequent_ask_questions()
        {
            $sql = "select * FROM faqs where archive='0'";
            $Array = $this->Database->select_qry_array($sql);
            $data =  $this->language == 'AR' ?!empty($Array[0]->question_ar)?$Array[0]->question_ar :$Array[0]->question_en: $Array[0]->question_en ;
            // for ($faq = 0; $faq < count($Array); $faq++) {
            //     $dryr = $Array[$faq];
            //     $content = array(
            //      'faqs' => $this->language == 'AR' ?!empty($dryr->question_ar)?$dryr->question_ar :$dryr->question_en: $dryr->question_en ,  
            //     );
            // }
            $result['content'] = $data;
            
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
        
        function how_it_works()
        {
            $sql = "select * FROM cms where identify='HOW_IT_WORK'";
            $Array = $this->Database->select_qry_array($sql);
            $data =  $this->language == 'AR' ?!empty($Array[0]->col1_ar)?$Array[0]->col1_ar :$Array[0]->col1: $Array[0]->col1 ;
            $result['content'] = $data;

            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
    function contact_us()
    {
        $json_re = file_get_contents('php://input');
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $name = !empty($json['name']) ? $json['name'] : '';
        $email = !empty($json['email']) ? $json['email'] : '';

        $phone_number = !empty($json['phone_number']) ? $json['phone_number'] : '';
        $message = !empty($json['message']) ? $json['message'] : '';

        $Json['user_id'] = $user_id;
        $Json['name'] = $name;
        $Json['email'] = $email;
        $Json['phone_number'] = $phone_number;
        $Json['message'] = $message;

        $contactId = $this->Database->insert('contact_us', $Json);
         $case_id = 'Customer Service Case '.$contactId;
        // mail to admin
        send_mail_to_admin($case_id,$json);
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")))));

    }
      
    
        function notification_count ()
        {
              $json_re = file_get_contents('php://input');
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $return=[];
          $qry = "SELECT count(*) as total FROM `notification_user` WHERE  user_id='$user_id' and notification_read='0' order by id desc";
        $Array = $this->Database->select_qry_array($qry);
        $return = !empty($Array[0]->total)?$Array[0]->total:"";
        $result['notification_count']=$return;
            
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
   
        }
        function notification_list()
        {
            $json_re = file_get_contents('php://input');
            $json = file_get_contents('php://input');
            $json = json_decode($json, TRUE);
            $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
            $return=[];
            $qry = "SELECT * FROM `notification_user` WHERE  user_id='$user_id' and notification_read='0' order by id desc";
            $Array = $this->Database->select_qry_array($qry);
           
            for($i=0;$i<count($Array);$i++)
            {
                $dft=$Array[$i];
                $return1=[];
              
                if($dft->notification_type == 4)
                {
                    $return[$i] =array(
                        "id"=> $dft->id,
                        "user_id"=> $dft->user_id,
                        "message"=> $dft->message,
                        "subject"=> $dft->subject,
                        "timestamp"=> $dft->timestamp,
                        "notification_read"=> $dft->notification_read,
                        "notification_type"=> $dft->notification_type,
                        "url"=>$dft->url
                    );  
                }
                //request_details
                else{
                    $requestId=!empty($dft->request_id)?$dft->request_id:'';
                    $request=GetrequestdetailsBy($requestId);
                    
                    $category_id=!empty($request->category_id) ? $request->category_id : '';
                    $categor_d =GetCategorydetailsBy($category_id);
                    $image = !empty($categor_d->category_image) ?$categor_d->category_image:'';
                    $image_array=explode(',',$image); $image_url=[];
                    for($j=0;$j<count($image_array);$j++)
                    {
                        $image_url[$j]=!empty($image_array[$j]) ? base_url() . "files/category_image/" . $image_array[$j]: '';
                    }
                    $em_De=GetemiratedetailsBy($request->emirate_id);
                    $emirate_name=!empty($em_De)?$em_De->emirate:"";
                    $re_De=GetregiondetailsBy($request->region_id);
                    $region_name=!empty($re_De)?$re_De->region:"";
                    $return1= array(
                        'id' => $request->id,
                        'request_no' => !empty($request->request_no)?$request->request_no :'',
                        'category_name'=>!empty($categor_d->category)?$categor_d->category :'',
                        'category_id'=>!empty($request->category_id)?$request->category_id :'',
                        'image_url'=>$image_url,
                        'emirate_id'=>!empty($request->emirate_id)?$request->emirate_id :'',
                        'emirate'=>!empty($emirate_name)?$emirate_name :'',
                        'region_id'=>!empty($request->region_id)?$request->region_id :'',
                        'region'=>!empty($region_name)?$region_name :'',
                        'timestamp'=>!empty($request->timestamp)?$request->timestamp :'',
                        'layout_type'=>isset($categor_d)?(int)$categor_d->layout_type :'',
                    );
                //end
                    $return[$i] =array(
                        "id"=> $dft->id,
                        "user_id"=> $dft->user_id,
                        "request_id"=> $dft->request_id,
                        "request_no"=> $dft->request_no,
                        "message"=> $dft->message,
                        "subject"=> $dft->subject,
                        "timestamp"=> $dft->timestamp,
                        "notification_read"=> $dft->notification_read,
                        "notification_type"=> $dft->notification_type,
                        "layout_type"=> (int)$dft->layout_type,
                        "request_details"=>$return1
                        );
                }
            }
            $result['notification_list']=$return;
            $qry = "SELECT count(*) as total FROM `notification_user` WHERE  user_id='$user_id' and notification_read='0' order by id desc";
            $Array = $this->Database->select_qry_array($qry);
            $return_not = !empty($Array[0]->total)?$Array[0]->total:"";
            $result['notification_count']=$return_not;
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
        
        function read_notification()
        {
            $json = file_get_contents('php://input');
            $json = json_decode($json, TRUE);
            $id = !empty($json['id']) ? $json['id'] : '';
            $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
            $UpdateArray = array("notification_read" => 1);
            $Conducation = array('user_id' => $user_id, 'id' => $id);
            $this->Database->update('notification_user', $UpdateArray, $Conducation);
             $qry = "SELECT count(*) as total FROM `notification_user` WHERE  user_id='$user_id' and notification_read='0' order by id desc";
            $Array = $this->Database->select_qry_array($qry);
            $return = !empty($Array[0]->total)?$Array[0]->total:"";
            $result['notification_count']=$return;
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")),'result' => $result)));
        }
        
        function all_notification_read()
        {
            $json = file_get_contents('php://input');
            $json = json_decode($json, TRUE);
            // $id = !empty($json['id']) ? $json['id'] : '';
            $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
            
            $UpdateArray = array("notification_read" => 1);
            $Conducation = array('user_id' => $user_id);
            $this->Database->update('notification_user', $UpdateArray, $Conducation);
            
            // $qry = "SELECT count(*) as total FROM `notification_user` WHERE  user_id='$user_id' and notification_read='0' order by id desc";
            // $Array = $this->Database->select_qry_array($qry);
            // $return = !empty($Array[0]->total)?$Array[0]->total:"";
            // $result['notification_count']=$return;
                die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")))));

        }
        
        public function getgarages() {
          $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
       
        $qry = "SELECT * FROM `garage_details` where archive='0' and special_offer!='1' ";
        $cArray = $this->Database->select_qry_array($qry);
        $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $get_rating = get_rating_average($dft->id);
            $return[$i] = array(
                'id' => $dft->id,
                'garage_name' => $this->language == 'AR' ?!empty($dft->garage_name_ar)?$dft->garage_name_ar :$dft->garage_name: $dft->garage_name ,
                'description'=>$this->language == 'AR' ?!empty($dft->description_ar)?$dft->description_ar :$dft->description: $dft->description ,
                'emirate' => $dft->emirate,
                'region' => $dft->region,
                'location' => $dft->location,
                'latitude' => $dft->latitude,
                'longitude' => $dft->longitude,
                'rating'=>number_format($get_rating,1),
                );
        
            }
        $result['garges'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => "successfull"), 'result' => $result)));
    }

        public function getoffergarages() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
       
        $qry = "SELECT * FROM `garage_details` where archive='0' and special_offer='1' ";
        $cArray = $this->Database->select_qry_array($qry);
        $return = [];
            for($i=0;$i<count($cArray);$i++)
            {
            $dft=$cArray[$i];
            $get_rating = get_rating_average($dft->id);
            $return[$i] = array(
                'id' => $dft->id,
                'garage_name' => $this->language == 'AR' ?!empty($dft->garage_name_ar)?$dft->garage_name_ar :$dft->garage_name: $dft->garage_name ,
                'description'=>$this->language == 'AR' ?!empty($dft->description_ar)?$dft->description_ar :$dft->description: $dft->description ,
                'emirate' => $dft->emirate,
                'region' => $dft->region,
                'location' => $dft->location,
                'latitude' => $dft->latitude,
                'longitude' => $dft->longitude,
                'rating'=>number_format($get_rating,1),
                );
        
            }
        $result['garges'] = $return;
        die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => "successfull"), 'result' => $result)));
    }

public function add_vehicle_favourite() {
          $json = file_get_contents('php://input');
        $vehicle_info = json_decode($json, TRUE);
       
 $make = !empty($vehicle_info['make_id']) ? $vehicle_info['make_id'] : '';
       $user_id = !empty($vehicle_info['user_id']) ? $vehicle_info['user_id'] : '';
        $model = !empty($vehicle_info['model_id']) ? $vehicle_info['model_id'] : '';
        $year= !empty($vehicle_info['year_id']) ? $vehicle_info['year_id'] : '';
        $plate_code= !empty($vehicle_info['plate_code_id']) ? $vehicle_info['plate_code_id'] : '';
        $plate_source= !empty($vehicle_info['plate_source_id']) ? $vehicle_info['plate_source_id'] : '';
        $plate_no= !empty($vehicle_info['plate_no']) ? $vehicle_info['plate_no'] : '';
        $gcc_type= isset($vehicle_info['gcc_type']) ? $vehicle_info['gcc_type'] : '';
        $chassis_no= isset($vehicle_info['chassis_no']) ? $vehicle_info['chassis_no'] : '';

        
        //add to favoirte
        $fav['user_id']=$user_id;

        $fav['make_id']=$make;
        $fav['model_id']=$model;
        $fav['year']=$year;
        $fav['plate_code']=$plate_code;
        $fav['plate_source']=$plate_source;
        $fav['plate_no']=$plate_no;
        $fav['gcc_type']=$gcc_type;
        $fav['chassis_no']=$chassis_no;
        $qry = "SELECT * FROM `favourite_vehicle` WHERE plate_code LIKE '$plate_code' AND plate_source='$plate_source' AND user_id='$user_id' and archive='0'";
        $Array = $this->Database->select_qry_array($qry);
        if (empty($Array)&&$plate_no!='') {

        $favId = $this->Database->insert('favourite_vehicle', $fav);
        }

die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")))));

        //end
}

function get_nearest_garages()
{
    $json = file_get_contents('php://input');
    $json = json_decode($json, TRUE);
    $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
    // $location = !empty($json['location']) ? $json['location'] : '';
    $latitude = !empty($json['latitude']) ? $json['latitude'] : '';
    $longitude = !empty($json['longitude']) ? $json['longitude'] : '';
    // $Json['location'] = $location;
    $Json['latitude'] = $latitude;
    $Json['longitude'] = $longitude;
    $qry = "SELECT *, (6371*acos(cos(radians($latitude))*cos(radians(latitude))*cos(radians(longitude)-radians($longitude))+ sin(radians($latitude))*sin(radians(latitude)))) AS distance FROM garage_details HAVING distance <= 10 ORDER BY distance";
    $Array = $this->Database->select_qry_array($qry);
    
    $return1 = [];
        for ($k = 0; $k < count($Array); $k++) {
            $dryr = $Array[$k];
            $get_rating = get_rating_average($dryr->id);
            $return1[$k] = array(
                'id' => $dryr->id,
                'garage_name' => !empty($dryr->garage_name)?$dryr->garage_name :'',
                'location' => !empty($dryr->location)?$dryr->location :'',
                'latitude' => !empty($dryr->latitude)?$dryr->latitude :'',
                'longitude' => !empty($dryr->longitude)?$dryr->longitude :'',
                'rating' => number_format($get_rating,1),
                );
        }
    
        if((!empty($return1)) || ($return1!= null)){
            $result['garage_list'] = $return1;
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
        }
        else{
            $result['garage_list'] = [];
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("No_garages")), 'result' => $result)));
        }
}
    public function special_offer_details() {
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $offer_id = !empty($json['offer_id']) ? $json['offer_id'] : '';
        $qry = "SELECT * FROM `offers` where archive='0'  and status='0' and id='$offer_id' ORDER BY id DESC";
        $cArray = $this->Database->select_qry_array($qry);
        
        
        //check redeen button is click or not - by deeshma
        $qryr = "SELECT * FROM `offer_request_table` where archive='0'  and user_id='$user_id' and offer_id='$offer_id' and is_freeCoupon='1' ORDER BY id DESC";
        $cArrayr = $this->Database->select_qry_array($qryr);
        if(!empty($cArrayr))
        {
            $free_coupon_timing=!empty($cArrayr[0]->free_coupon_timing)?$cArrayr[0]->free_coupon_timing:"";
        $is_redeemed=true;
            
        }
        else
        {
            $free_coupon_timing="";
                    $is_redeemed=false;

        }
        //end
        $offer_array =array();
        for ($i = 0; $i < count($cArray); $i++) {
            $dft = $cArray[$i];
            $category_id = !empty($dft->category) ?$dft->category:'';
            $category_De=GetOfferCategorydetailsBy($category_id);
            $category_name=!empty($category_De)?$category_De->category:"";
            $return=array();
            $image = !empty($dft->offer_image) ?$dft->offer_image:'';
            $image_array=explode(',',$image); $image_url=[];
            for($m=0;$m<count($image_array);$m++)
            {
                $image_url[$m]=!empty($image_array[$m]) ? base_url() . "files/offer_image/" . $image_array[$m]: '';
            }
            $garage_name=  GetgaragedetailsBy($dft->garage_id);
            
            if($this->language == 'AR') {
                if(!empty($garage_name->garage_name_ar)){
                    $gara_name = $garage_name->garage_name_ar;
                }
                else{
                    if(!empty($garage_name->garage_name)){
                        $gara_name = $garage_name->garage_name;
                    }
                    else{
                        $gara_name = "";
                    }
                }
            }
            else{
                if(!empty($garage_name->garage_name)){
                    $gara_name = $garage_name->garage_name;
                }
                else{
                    $gara_name = "";
                }
            }
                
                // 'garage_name' => $this->language == 'AR' ?(!empty($garage_name->garage_name_ar)?$garage_name->garage_name_ar :!empty($garage_name->garage_name)?$garage_name->garage_name:""): (!empty($garage_name->garage_name)?$garage_name->garage_name:""),
                
            $return = array(
                'id' => $dft->id,
                'title' => $this->language == 'AR' ?!empty($dft->title_ar)?$dft->title_ar :$dft->title: $dft->title ,
                'original_price' => $dft->original_price,
                'discount_price' => $dft->discount_price,
                'appointment_option' => (int)$dft->appointment,
                'free_coupon' => $dft->free_coupon,
                'message' => $this->language == 'AR' ?!empty($dft->message_ar)?$dft->message_ar :$dft->message: $dft->message ,
                'description' => $this->language == 'AR' ?!empty($dft->description_ar)?$dft->description_ar :$dft->description: $dft->description ,
                'valid_from' => $dft->valid_from,
                'valid_to' => $dft->valid_to,
                'category_name' => $this->language == 'AR' ?!empty($category_De->category_ar)?$category_De->category_ar :$category_De->category: $category_De->category,
                'category_id' => $dft->category,
                'garage_id' => $dft->garage_id,
                'garage_name' => $gara_name,
                'location' => !empty($garage_name->location)?$garage_name->location :'',
                'latitude' => !empty($garage_name->latitude)?$garage_name->latitude :'',
                'longitude' => !empty($garage_name->longitude)?$garage_name->longitude :'',
                'layout_type'=>6,
                'offer_image'=>$image_url,
                'is_redeemed'=>$is_redeemed,
                'redeem_at' => $free_coupon_timing
                );
        }
       
                 $result['offers'] = $return;
                 die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")), 'result' => $result)));
    }

 function rate_order()
        {
        $json_re = file_get_contents('php://input');
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $garage_id = !empty($json['garage_id']) ? $json['garage_id'] : '';
                $request_id = !empty($json['request_id']) ? $json['request_id'] : '';
                $layout_type = isset($json['layout_type']) ? $json['layout_type'] : '';

        $rating = !empty($json['rating']) ? $json['rating'] : '';
        $comments = !empty($json['comments']) ? $json['comments'] : '';

        
        $Json['user_id'] = $user_id;
        $Json['garage_id'] = $garage_id;
        if($layout_type==6)
        {
        $Json['offer_request_id'] = $request_id;

        }
        else
        {
                    $Json['request_id'] = $request_id;

        }

        $Json['rating'] = $rating;
        $Json['comments'] = $comments;

              $contactId = $this->Database->insert('rating', $Json);
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success_rate")))));

        }
          function feedback()
        {
        $json_re = file_get_contents('php://input');
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $name = !empty($json['name']) ? $json['name'] : '';
        $email = !empty($json['email']) ? $json['email'] : '';

        $phone_number = !empty($json['phone_number']) ? $json['phone_number'] : '';
        $message = !empty($json['message']) ? $json['message'] : '';

        $Json['user_id'] = $user_id;
        $Json['name'] = $name;
        $Json['email'] = $email;
        $Json['phone_number'] = $phone_number;
        $Json['message'] = $message;

              $contactId = $this->Database->insert('feedback', $Json);
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")))));

        }
        
    public function update_payment() {
        $db = LoadDB();
        $json = file_get_contents('php://input');
        $json = json_decode($json, TRUE);
        $user_id = !empty($json['user_id']) ? $json['user_id'] : '';
        $request_id = !empty($json['request_id']) ? $json['request_id'] : '';
        $payment_type = isset($json['payment_type']) ? $json['payment_type'] : '';
        $payment_status = isset($json['payment_status']) ? $json['payment_status'] : '';
        $layout_type = !empty($json['layout_type']) ? (int)$json['layout_type'] : '';
        
        $user_De=GetuserdetailsBy($user_id);
        if ((empty($request_id))) {
            die(json_encode(array("code" => $this->error, "response" => array("status" => true, "message" => "Please Pass request_id"))));
        }

        //end
        // $payment_status='0';
        // if($payment_type=='1'){
        //     $payment_status='1';
        // }
             $UpdateArray = array(
                'payment_type'=>$payment_type,
                'payment_status'=>$payment_status

            );
            $CondArray = array('id' => $request_id);
            if($layout_type!=6)
            {
            $db->Database->update('request_table', $UpdateArray, $CondArray);
            }
            else
            {
            $db->Database->update('offer_request_table', $UpdateArray, $CondArray);

            }
           //end
            die(json_encode(array("code" => $this->success, "response" => array("status" => true, "message" => $this->lang->line("success")))));
        
        //end request table
    }

    /* Not In use down side  */
}
