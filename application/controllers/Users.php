<?php

defined('BASEPATH') OR exit('No direct script access allowed');

class Users extends CI_Controller {

    private $segment2;
    private $segment3;
    private $segment4;

    public function __construct() {
        parent::__construct();
        // IsLogedVendor();
        error_reporting(0);
        date_default_timezone_set('Asia/Dubai');
        $this->load->Model('Database');
        $this->load->language("common_lang", "app_englist");
        $this->segment2 = $this->uri->segment(2);
        $this->segment3 = $this->uri->segment(3);
        $this->segment4 = $this->uri->segment(4);
    }

    public function helper() {
        $function = !empty($_REQUEST['function']) ? $_REQUEST['function'] : 'Invalid';
        if (function_exists($function)) {
            $function();
        } else {
            die(json_encode(array('status' => false, 'message' => 'function not exists', 'data' => '')));
        }
    }

    public function index() {
        $this->load->view(USER_DIR . 'includes/header');
        $this->load->view(USER_DIR . 'index');
        $this->load->view(USER_DIR . 'includes/footer');
    }
    public function userLoginout() {
        $this->session->unset_userdata('Vendor');
        session_destroy();
    }
    
    public function manage_account() {
        $this->load->view(USER_DIR . 'includes/header');
        $this->load->view(USER_DIR . 'manage_account');
        $this->load->view(USER_DIR . 'includes/footer');
    }
    
    public function updateUser() {
        $ci =& get_instance();
        if (isset($_POST)) {
            $db = LoadDB();
            $id = $_POST['id'];
            $Qry = "SELECT license,VAT FROM `register` WHERE id='" . $id . "'";
            $doc = $db->Database->select_qry_array($Qry);
           
            $password = $_POST['password'];
            $upload_vat_data = $ci->uploadDoc('VAT');
            $upload_license_data = $ci->uploadDoc('license');
           
            $filename_vat = '';
            $filename_license = '';
           
            if(!empty($upload_vat_data)){
                if($upload_vat_data['status'] == 1){
                    $filename_vat = $upload_vat_data['upload_data']['file_name'];
                }else{
                    $filename_vat = $doc[0]->VAT;
                   
                }
            }else{
                $filename_vat = '';
            }
            
            if(!empty($upload_license_data)){
                if($upload_license_data['status'] == 1){
                    $filename_license = $upload_license_data['upload_data']['file_name'];
                }else{
                    $filename_license = $doc[0]->license;
                    
                }
            }else{
                $filename_license = '';
            }
            
          
            if(!empty($password)) {
                $user = array(
                    'first_name' => $_POST['fname'],
                    'last_name' => $_POST['lname'],
                    'email' => $_POST['email'],
                    'mobile' => $_POST['phone'],
                    'license' => $filename_license,
                    'VAT' => $filename_vat,
                    'password' => md5($password),
                    'lastupdate' => date('Y-m-d H:i:s')
                );
            }
            else{
                $user = array(
                    'first_name' => $_POST['fname'],
                    'last_name' => $_POST['lname'],
                    'email' => $_POST['email'],
                    'mobile' => $_POST['phone'],
                    'license' => $filename_license,
                    'VAT' => $filename_vat,
                    'lastupdate' => date('Y-m-d H:i:s')
                );
            }
            $condition = array('id' => $id);
            $db->Database->update('register', $user,$condition);
            $this->data['message'] =  'Success';
            redirect('/Users/manage_account');
        }
        $this->load->view(USER_DIR . 'includes/header');
        $this->load->view(USER_DIR . 'manage_account');
        $this->load->view(USER_DIR . 'includes/footer');
        }
    
    function uploadDoc($file)
    {
        $ci =& get_instance();
        $config['upload_path']          = './uploads/Register';
        $config['allowed_types']        = 'gif|jpg|png|pdf|jpeg';
        $config['max_size']             = 2048;
       

        $ci->load->library('upload', $config);

        if ( ! $ci->upload->do_upload($file))
        {
            $data= array('status'=>0, 'error' => $ci->upload->display_errors());
            return $data;
        }
        else
        {
            $data = array('status'=>1, 'upload_data' => $ci->upload->data());
            return $data;
        }
    }
}