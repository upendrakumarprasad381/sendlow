<?php

function LoadDB() {
    $ci = & get_instance();
    $ci->load->model('Database');
    return $ci;
}

function includePHPExcel() {
    require_once HOME_DIR . 'application/third_party/PHPExcel/Classes/PHPExcel.php';
}

function GetSessionArrayShippingAddress() {
    $ci = & get_instance();
    $SessionArray = $ci->session->userdata('ShippingAddress');
    return $SessionArray;
}

function UserDelete($Array) {
    $db = LoadDB();
    $CondArray = array('id' => $Array['UserId']);
    $json['archive'] = 1;
    $db->Database->update('admin_login', $json, $CondArray);
    echo json_encode(array('status' => 'success'));
}

function autoupdate() {
    $updatejson = !empty($_POST['updatejson']) ? json_decode($_POST['updatejson'], true) : [];
    $condjson = !empty($_POST['condjson']) ? json_decode($_POST['condjson'], true) : [];
    $dbtable = !empty($_POST['dbtable']) ? $_POST['dbtable'] : '';
    if (!empty($dbtable) && is_array($updatejson) && is_array($condjson)) {
        $db = LoadDB();
        $db->Database->update($dbtable, $updatejson, $condjson);
        die(json_encode(array('status' => true, 'message' => 'Successfully')));
    }
}

function autodelete() {
    $condjson = !empty($_POST['condjson']) ? json_decode($_POST['condjson'], true) : [];
    $dbtable = !empty($_POST['dbtable']) ? $_POST['dbtable'] : '';
    $removefile = !empty($_POST['removefile']) ? $_POST['removefile'] : '';
    if (is_file($removefile)) {
        unlink($removefile);
    }
    if (!empty($dbtable) && is_array($condjson)) {
        $db = LoadDB();
        $db->Database->delete($dbtable, $condjson);
        die(json_encode(array('status' => true, 'message' => 'Successfully')));
    }
}

function DecimalAmount($Amount = 0) {
    return sprintf('%0.2f', $Amount);
}

function getCitiesByCountry($countryId = '') {
    $db = LoadDB();
    $countryId = !empty($_POST['countryId']) ? $_POST['countryId'] : $countryId;
    $qry1 = "SELECT country FROM `country` WHERE id='$countryId' AND archive=0";
    $countryArray = $db->Database->select_qry_array($qry1);
    $qry = "SELECT * FROM `city` WHERE country_id='$countryId' AND archive=0";
    $dArray = $db->Database->select_qry_array($qry);
    if (isset($_POST['encode'])) {
        die(json_encode(array('status' => true, 'message' => 'success', 'data' => $dArray, 'country' => $countryArray[0]->country)));
    }
    return $dArray;
}

function send_sms($mobile_no, $messagqwe = '') {
    header('Content-Type: text/html; charset=utf-8');
    $UserName = SMS_USERNAME;
    $Password = SMS_PASSWORD;
    $senderid = SMS_SENDERID;
    $messagqwe1 = $messagqwe;
    $mobile_no = $mobile_no;
    $string = "https://www.smartsmsgateway.com/api/api_http.php?username=$UserName&password=$Password&senderid=$senderid&to=$mobile_no&text=$messagqwe1&type=text";
    $url = preg_replace("/ /", "%20", $string);
    $sendSms = file_get_contents($url);
    return $sendSms;
}

function send_mail($email_id, $subject, $messages, $cc = '', $file_path = '') {
    $ci = & get_instance();
    $config = Array(
        'protocol' => 'sendmail',
        'smtp_host' => 'ssl://smtp.googlemail.com',
        'smtp_port' => 465,
        'smtp_user' => 'shamzammail@gmail.com', // change it to yours
        'smtp_pass' => 'shamzam123', // change it to yours
        'mailtype' => 'html',
        'crlf' => '\r\n',
        'charset' => 'utf-8',
        'wordwrap' => TRUE
    );

    $ci->load->library('email');
    $ci->email->clear(TRUE);
    $ci->email->initialize($config);

    $ci->email->set_newline("\r\n");
    $ci->email->from('shamzammail@gmail.com', "Sendlow");
    $ci->email->to($email_id);
    $ci->email->subject($subject);
    $ci->email->message($messages);

    $ci->email->send();
}

function send_pushnotifaction($device_id = array(), $subject = '', $message = '', $identifier = '', $argument = array()) {
    $notification['title'] = $subject;
    $notification['body'] = $message;
    $notification['sound'] = 'default';
    $argument['identifier'] = $identifier;
    $argument['sound'] = 'default';
    $argument['click_action'] = 'FLUTTER_NOTIFICATION_CLICK';
    $notification['image'] = 'https://static.pexels.com/photos/4825/red-love-romantic-flowers.jpg';

    $dArray = $device_id;
    $ch = curl_init("https://fcm.googleapis.com/fcm/send");
    $arrayToSend = array('registration_ids' => $dArray, 'notification' => $notification, 'priority' => 'high', 'sound' => 'default', 'data' => $argument);

    $json = json_encode($arrayToSend);

    $headers = array();
    $headers[] = 'Content-Type: application/json';
    $headers[] = 'Authorization: key=AAAAM1BR-w4:APA91bEumgmkqLRg5zo97kN9LELprr1IZP5pIOy0NLN6qoAHyCvhpMyewIq3TNd5ykm8kqlbfYcchIiuInFK1nKqqqfQ2UOpSBo5NMt1KVe7xZiZI_34AH7-czbfhIqkOWpzdijLJsht'; // key here
    curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "POST");
    curl_setopt($ch, CURLOPT_POSTFIELDS, $json);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
    curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
    curl_setopt($ch, CURLOPT_HTTPHEADER, $headers);
    $result = curl_exec($ch);
    curl_close($ch);
    $myfile = fopen("newfile.txt", "a") or die("Unable to open file!");
    $date = new DateTime();
    $date = $date->format("y:m:d h:i:s");
    $newline = '#######' . PHP_EOL;
    fwrite($myfile, $newline);
    fwrite($myfile, $date);
    fwrite($myfile, '--------------');
    fwrite($myfile, $json);
    fclose($myfile);
}

function GetMenus() {
    $db = LoadDB();
    $Qry = "SELECT * FROM `sidebar_menu` WHERE archive='0' and parent='0' order by order_view asc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetUserMenu($menuid, $userid) {
    $db = LoadDB();
    $Qrys = "SELECT * FROM `user_permission` WHERE user_id='" . $userid . "' and menu_id='" . $menuid . "'";
    $Array = $db->Database->select_qry_array($Qrys);
    return $Array;
}

function GetSubMenu($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM `sidebar_menu` WHERE parent='" . $id . "' and archive='0' order by order_view asc";
    $Array = $db->Database->select_qry_array($Qry);
    return $Array;
}

function GetMenuPermission($MenuId = '', $full = false) {
    $db = LoadDB();
    $controller = $db->router->fetch_class();
    $method = $db->router->fetch_method();
    $Session = $db->session->userdata('Admin');
    $Search = $controller . '/' . $method;
    $condSearch = "";
    if (!in_array($Session->id, ADMIN_PERMISSION_ARRAY)) {
        $condSearch = $condSearch . " AND UP.user_id='$Session->id' ";
    }
    $MenuIdCond = !empty($MenuId) ? " SM.menu_id='$MenuId' " : " SM.menu_url='$Search' ";
    $Select = ",UP.additional_json";
    $join = " LEFT JOIN user_permission UP ON UP.menu_id=SM.menu_id ";
    $qry = "SELECT SM.* $Select FROM `sidebar_menu` SM $join WHERE $MenuIdCond $condSearch ORDER BY SM.order_view ASC";
    $MenuArray = $db->Database->select_qry_array($qry);
    $additional_json = [];
    if (!empty($MenuArray)) {
        $MenuArray = $MenuArray[0];
        if (in_array($Session->id, ADMIN_PERMISSION_ARRAY)) {
            $menu_json = json_decode($MenuArray->menu_json, true);
            $menu_json = !empty($menu_json) ? $menu_json : [];
            for ($i = 0; $i < count($menu_json); $i++) {
                $additional_json[] = $menu_json[$i]['actionId'];
            }
        } else {
            $additional_json = !empty($MenuArray->additional_json) ? json_decode($MenuArray->additional_json) : [];
            $additional_json = !empty($additional_json) && is_array($additional_json) ? $additional_json : [];
        }
        $MenuArray->additional_json = $additional_json;
    }
    if (!empty($full)) {
        return $MenuArray;
    } else {
        return $additional_json;
    }
}

function removeUserDoc() {
    $db = LoadDB();
    $valArray = array(
        $_POST['field'] => ''
    );
    $condition = array('id' => $_POST['id']);
    $result = $db->Database->update('register', $valArray, $condition);
    $destination = HOME_DIR . $_POST['path'] . "/" . $_POST['name'];
    unlink($destination);
    echo $json = '{"status":"success","message":"Deleted successfully"}';
}

function common_remove_image($array) {
    $db = LoadDB();
    if ($array['type'] == 'multiple') {
        $img_nam = '';
        if ($array['table'] == 'home_banner') {
            $qry = "SELECT * FROM " . $array['table'] . " where id='" . $array['id'] . "'";
            $arrays = $db->Database->select_qry_array($qry);
            $img_nam = $arrays[0]->image;
        } else {
            $qry = "SELECT * FROM " . $array['table'] . " where id='" . $array['id'] . "'";
            $arrays = $db->Database->select_qry_array($qry);
            $img_nam = $arrays[0]->flag;
        }

        if ($img_nam != '') {
            $arr = explode(",", $img_nam);
            foreach ($arr as $k => $v) {
                $destination = HOME_DIR . $array['path'] . $v;
                if ($array['name'] == $v) {
                    unset($arr[$k]);
                    if (is_file($destination)) {
                        unlink($destination);
                    }
                }
            }
            if (!empty($arr)) {
                $newarray = implode(",", $arr);
            } else {
                $newarray = '';
            }
            $update[$array['field']] = $newarray;
            $CondArray = array('id' => $array['id']);
            $result = $db->Database->update($array['table'], $update, $CondArray);
        }
    } else {
        $delete_path = HOME_DIR . $array['path'] . $array['name'];
        if (is_file($delete_path)) {
            unlink($delete_path);
        }
        $CondArray = array('id' => $array['id']);
        if ($array['type'] != '') {
            $json[$array['type']] = '';
        } else {
            $json[$array['field']] = '';
        }
        $result = $db->Database->update($array['table'], $json, $CondArray);
    }
    if (isset($result)) {
        echo $json = '{"status":"success","message":"Deleted successfully"}';
        exit;
    } else {
        echo $json = '{"status":"error","message":"Something went wrong."}';
        exit;
    }
}

function addNotifaction($primaryId = '', $notificationType = '') {
    $db = LoadDB();
    $add['primary_id'] = $primaryId;
    $add['notification_type'] = $notificationType;
    $add['notification_title'] = 'Unknown notification';
    $add['notification_url'] = '';
    $add['timestamp'] = date('Y-m-d H:i:s');
    if ($notificationType == NEW_USER_NOTIFICATION) {
        $add['notification_title'] = "New customer registered.";
        $add['notification_url'] = 'Admin/user_info/' . base64_encode($primaryId);
    } else if ($notificationType == NEW_RECHARGE_REQUEST) {
        $add['notification_title'] = "New recharge request. ";
        $add['notification_url'] = 'admin/request_info/' . base64_encode($primaryId);
    }
    // else if ($notificationType == NEW_ORDER_NOTIFICATION) {
    //     $Order = GetordersByordersId($primaryId);
    //     $add['notification_title'] = "New order received. #" . $Order->order_no;
    //     $add['notification_url'] = 'admin/OrderDetails?orderId=' . base64_encode($primaryId);
    //     updateorderVendornotifaction($primaryId, $add);
    // } else if ($notificationType == STOCK_QUANTITY_NOTIFICATION) {
    //     $pArray = GetproductsBy($primaryId);
    //     $add['notification_title'] = "$pArray->product_name. Current stock " . $pArray->quantity;
    //     $add['notification_url'] = 'admin/addproducts?productId=' . base64_encode($primaryId);
    //     $venorNot = $add;
    //     $venorNot['vendor_id'] = $pArray->vendor_id;
    //     $venorNot['notification_url'] = 'vendor/addproducts?productId=' . base64_encode($primaryId);
    //     $db->Database->insert('notification', $venorNot);
    // }
    $notifactionId = $db->Database->insert('notification', $add);
    return $notifactionId;
}

function notificationsOnclick() {
    $db = LoadDB();
    $notificationId = !empty($_POST['notificationId']) ? $_POST['notificationId'] : '';
    $update['admin_read'] = '1';
    $cond = array('notification_id' => $notificationId);
    $db->Database->update('notification', $update, $cond);
    die(json_encode(array('status' => true, 'message' => 'Successfully.')));
}

function notificationsAdminReadAll() {
    $db = LoadDB();
    $Sql = "UPDATE `notification` SET `admin_read`=1";
    $db->Database->PrepareQuery($Sql, [], true);
    die(json_encode(array('status' => true, 'message' => 'Successfully.')));
}

function time_elapsed_string($datetime, $full = false) {
    $now = new DateTime;
    $ago = new DateTime($datetime);
    $diff = $now->diff($ago);

    $diff->w = floor($diff->d / 7);
    $diff->d -= $diff->w * 7;

    $string = array(
        'y' => 'year',
        'm' => 'month',
        'w' => 'week',
        'd' => 'day',
        'h' => 'hour',
        'i' => 'minute',
        's' => 'second',
    );
    foreach ($string as $k => &$v) {
        if ($diff->$k) {
            $v = $diff->$k . ' ' . $v . ($diff->$k > 1 ? 's' : '');
        } else {
            unset($string[$k]);
        }
    }

    if (!$full)
        $string = array_slice($string, 0, 1);
    return $string ? implode(', ', $string) . ' ago' : 'just now';
}

function common_approve() {
    $db = LoadDB();
    $id = !empty($_POST['id']) ? $_POST['id'] : '';
    $user_id = !empty($_POST['user']) ? $_POST['user'] : '';
    $cond_array = array('id' => $id);
    $json['is_approved'] = 1;
    $db->Database->update($_POST['table'], $json, $cond_array);

    $Qry = "SELECT first_name,email FROM  register WHERE id= $user_id";
    $userArray = $db->Database->select_qry_array($Qry);

    // send_email_to_vendor_after_approve($userArray);
    die(json_encode(array('status' => true, 'message' => 'Approved Successfully')));
}

function common_reject() {
    $db = LoadDB();
    $id = !empty($_POST['id']) ? $_POST['id'] : '';
    $cond_array = array('id' => $id);
    $json['is_approved'] = 2;
    $db->Database->update($_POST['table'], $json, $cond_array);
    die(json_encode(array('status' => true, 'message' => 'Rejected Successfully')));
}

function updateMyWallet($user_id = '', $amount = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM  my_wallet WHERE user_id= $user_id";
    $walletArray = $db->Database->select_qry_array($Qry);
    $balance = '';
    if (!empty($walletArray)) {
        $balance = $walletArray[0]->account_balance;
        $balance = $balance + $amount;
        $condition = array('user_id' => $user_id);
        $insert['account_balance'] = $balance;
        $insert['lastupdate'] = date('Y-m-d H:i:s');
        $resp = $db->Database->update('my_wallet', $insert, $condition);
        send_email_to_customer_after_addToWallet($user_id, $amount);
        return $resp;
    } else {
        return 0;
    }
}

function getUserWallet($id = '') {
    $db = LoadDB();
    $Qry = "SELECT * FROM  my_wallet WHERE user_id= $id";
    $walletArray = $db->Database->select_qry_array($Qry);
    return $walletArray;
}

function encryptIt($q) {
    $cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
    $qEncoded = base64_encode(mcrypt_encrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), $q, MCRYPT_MODE_CBC, md5(md5($cryptKey))));
    return($qEncoded);
}

function decryptIt($q) {
    $cryptKey = 'qJB0rGtIn5UB1xG03efyCp';
    $qDecoded = rtrim(mcrypt_decrypt(MCRYPT_RIJNDAEL_256, md5($cryptKey), base64_decode($q), MCRYPT_MODE_CBC, md5(md5($cryptKey))), "\0");
    return($qDecoded);
}

function insertTemp($json = []) {
    $db = LoadDB();
    $db->Database->insert('city', $json);
}
