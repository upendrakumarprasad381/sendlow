<?php

function getCommonSearchFormateAPI() {
    $searchData = getSessionSearchData();

    $fromcountry = GetcountryById($searchData['packageFrom']);
    $cityFrom = GetcityById($searchData['packageCityFrom']);
    $tocountry = GetcountryById($searchData['packageTo']);
    $cityTo = GetcityById($searchData['packageCityTo']);

    $myAr = [
        'origin_country_code' => $fromcountry->code,
        'origin_postal_code' => $searchData['packageFromPincode'],
        'origin_city_name' => $cityFrom->city,
        'destination_country_code' => $tocountry->code,
        'destination_postal_code' => $searchData['packageToPincode'],
        'destination_city_name' => $cityTo->city,
        'weight' => !empty($searchData['packageWeight']) ? $searchData['packageWeight'] : '',
        'shipDate' => date('Y-m-d', strtotime('+20 days')),
        'parcel_type'=>$searchData['search_type'],
    ];
    return $myAr;
}

function loadMycouriercompany() {
    $db = LoadDB();

    $searchData = getSessionSearchData();
    $packageWeight = !empty($searchData['packageWeight']) ? $searchData['packageWeight'] : '';

    $companyAr = GetcouriercompanymasterAll();
    for ($i = 0; $i < count($companyAr); $i++) {
        $d = $companyAr[$i];
        $price = 0;
        if ($d->courier_id == '1') {
            $db->load->library("aramex");
            $price = $db->aramex->calculaterate();
        } else if ($d->courier_id == '2') {
            $db->load->library("dhl");
            $price = $db->dhl->calculaterate();
        } else if ($d->courier_id == '3') {
            $db->load->library("fedex");
            $price = $db->fedex->calculaterate();
        }
        ?>
        <div class="row g-0 text-lg-center text-left shadowL py-3 ">
            <div class="col-lg-2 col-sm-6 noWrap">
                <img src="<?= base_url("files/images/$d->logo") ?>" class="img-fluid" alt="...">
            </div>
            <div class="col-lg-1 col-sm-6 noWrap">
                <p class="card-text">Weight <br><small class="text-muted lH4"><?= $packageWeight ?> KG</small></p>
            </div>
            <div class="col-lg-2 col-sm-6 noWrap">
                <p class="card-text">Choose Location
                    <small class="text-muted">
                        <form>
                            <label class="radio-inline">
                                <input type="radio" onclick="myBooking.pickupdropoffOnchange();" name="pickup_dropoff<?= $d->courier_id ?>" value="0" checked>&nbsp;Pickup&nbsp;
                            </label>
                            <label class="radio-inline">
                                <input type="radio" onclick="myBooking.pickupdropoffOnchange();" value="1" name="pickup_dropoff<?= $d->courier_id ?>">&nbsp;Drop Off
                            </label>
                            <i class="fa fa-map-marker absGps" aria-hidden="true"></i>
                        </form>
                    </small>
                </p>
            </div>
            <div class="col-lg-2 col-sm-6 noWrap">
                <p class="card-text">Schedule date <br>
                    <input type="text" id="schedule_date_<?= $d->courier_id ?>"  class="form-control datepicker" readonly pbase="<?= base64_encode($price) ?>" style="max-width: 150px;">
                </p>
            </div>
            <div class="col-lg-2 col-sm-6 noWrap">
                <?php
                $estimatedDelivery = date("Y-m-d", strtotime("+20 month"));
                ?>
                <p class="card-text">Estimated Delivery <br><small class="text-muted lH4" value="<?= $estimatedDelivery ?>" id="estimated_delivery"><?= date('d M Y', strtotime($estimatedDelivery)) ?></small>
                </p>
            </div>
            <div class="col-lg-1 col-sm-6 noWrap">
                <p class="card-text">Price</p>
                <h5 class="card-title text-danger fnt14">AED <?= DecimalAmount($price) ?></h5>
            </div>
            <div class="col-lg-2 col-sm-6 noWrap mdlCont">
                <a courierId="<?= $d->courier_id ?>" href="javascript:void(0)" onclick="myBooking.chooseYourCourier(this);"><button class="btn btnSec bdr25 py-0 ">Book Now</button></a>
            </div>
        </div>
    <?php } ?>

    <!-- -----------------------------dummy content ----------------------------------- -->

    <!--    <div class="row mt-4 float-right">
            <div class="form-check-inline px-2">
                <label class="form-check-label">
                    <input type="checkbox" class="form-check-input" value="">Save as Draft
                </label>
            </div>
            <button class="btn btnPrm py-0 rounded ">Continue</button>
        </div>-->

    <?php
}

function loadMySenderReciverDetails() {
    ob_start();
    $sessionAr = GetSessionArrayLogin();
    $db = LoadDB();
    $searchData = getSessionSearchData();
    $prefix = !empty($_POST['prefix']) ? $_POST['prefix'] : '';
    $selectedId = !empty($searchData[$prefix]) ? $searchData[$prefix] : '';
    $type = $prefix == '_sen' ? '0' : '1';
    $Sql = "SELECT SA.*,C.country AS country_name FROM `sender_address` SA LEFT JOIN country C ON C.id=SA.country WHERE SA.user_id='$sessionAr->id' AND SA.archive=0 AND SA.type='$type' LIMIT 10";
    $dArray = $db->Database->select_qry_array($Sql);
    for ($i = 0; $i < count($dArray); $i++) {
        $d = $dArray[$i];
        ?>
        <div class="form-check form-check-inline mr-0 wdFull mt-1">
            <input prefix="<?= $prefix ?>" <?= $selectedId == $d->id ? 'checked' : '' ?> id="addressIdr<?= $d->id ?>"  addressid="<?= $d->id ?>" onclick="myBooking.loadMyFormsenderReceiver(this);" class="form-check-input" type="radio" name="address_id<?= $prefix ?>"
                   value="<?= $d->id ?>">
            <label class="form-check-label pl-1 text-left" for="senderId"><?= $d->full_name ?><br>
                <small class="text-secondary"><?= $d->country_name ?></small>
            </label>
        </div>
        <?php
    }
    $html = ob_get_clean();
    die(json_encode(array('status' => true, 'message' => 'success', 'HTML' => $html)));
}

function senderReciverFroms($prefix = '') {
    $sessionAr = GetSessionArrayLogin();
    $searchData = getSessionSearchData();
    $prefix = !empty($_POST['prefix']) ? $_POST['prefix'] : $prefix;

    $city = $searchData['packageCityTo'];
    $pinCode = $searchData['packageToPincode'];
    $countryId = $searchData['packageTo'];
    if ($prefix == '_sen') {
        $city = $searchData['packageCityFrom'];
        $pinCode = $searchData['packageFromPincode'];
        $countryId = $searchData['packageFrom'];
    }
    $country = GetcountryById($countryId);
    $cityAr = GetcityById($city);
    ?>

    <div class="form-row mt-5 text-left" style="width: 100%;">
        <div class="form-group col-sm-3">
            <label for="" class="text-secondary">Customer ID</label>
        </div>
        <div class="form-group col-sm-3">
            <label for=""><?= strtoupper($sessionAr->customerId) ?></label>
        </div>
        <div class="form-group col-sm-3">
            <label for="" class="text-secondary">&nbsp;</label>
        </div>  
        <div class="form-group col-sm-3">
            <label for="">&nbsp;</label>
        </div>                      
    </div>
    <div class="form-row text-left" style="width: 100%;">
        <div class="form-group col-sm-4">
            <label for="">Full Name</label>
            <input type="text" class="form-control brdOrg" id="full_name<?= $prefix ?>" placeholder="">
        </div>
        <div class="form-group col-sm-4">
            <label for="">Father Name</label>
            <input type="text" class="form-control brdOrg" id="father_name<?= $prefix ?>" placeholder="">
        </div>
        <div class="form-group col-sm-4">
            <label for="">Grand Father Name</label>
            <input type="text" class="form-control brdOrg" id="grand_father_name<?= $prefix ?>" placeholder="">
        </div>                       
    </div>
    <div class="form-row text-left" style="width: 100%;">
        <div class="form-group col-sm-4">
            <label for="">Family Name</label>
            <input type="text" class="form-control brdOrg" id="family_name<?= $prefix ?>" placeholder="">
        </div>
        <div class="form-group col-sm-4">
            <label for="">Mobile Number</label>
            <input type="number" class="form-control brdOrg" id="mobile<?= $prefix ?>" placeholder="">
        </div>
        <div class="form-group col-sm-4">
            <label for="">E-mail</label>
            <input type="email" class="form-control brdOrg" id="email<?= $prefix ?>" placeholder="">
        </div>                       
    </div>
    <div class="form-row text-left" style="width: 100%;">
        <div class="form-group col-sm-12">
            <label for="">Address</label>
            <input type="text" class="form-control brdOrg" id="address<?= $prefix ?>" placeholder="">
        </div>

    </div>
    <div class="form-row text-left" style="width: 100%;">
        <div class="form-group col-sm-4">
            <label for="">Company Name</label>
            <input type="text" class="form-control brdOrg" id="company_name<?= $prefix ?>" placeholder="">
        </div>
        <div class="form-group col-sm-4">
            <label for="">Country</label>
            <input type="text" value="<?= $country->country ?>" countryId="<?= $country->code ?>" readonly class="form-control brdOrg" id="country<?= $prefix ?>">

        </div>
        <div class="form-group col-sm-4">
            <label for="">City</label>
            <input type="text" class="form-control brdOrg" value="<?= $cityAr->city ?>" readonly id="city<?= $prefix ?>">

        </div>                       
    </div>
    <div class="form-row text-left" style="width: 100%;">
        <div class="form-group col-sm-4">
            <label for="">Post / Zip Code</label>
            <input type="text" class="form-control brdOrg" value="<?= $pinCode ?>" readonly id="zip_code<?= $prefix ?>" placeholder="">
        </div>
        <div class="form-group col-sm-4 dGrdEnd">
            <div class="form-check">
                <input class="form-check-input " type="checkbox" value="" id="is_new<?= $prefix ?>">
                <label class="form-check-label" for="is_new<?= $prefix ?>">
                    <?= $prefix == '_sen' ? 'Save New Sender Details' : 'Save New Recipient Details'; ?>  
                </label>
            </div>
            <?php if ($prefix == '_sen') { ?>
                <div class="form-check ">
                    <input class="form-check-input" type="radio" name="pickup_type" checked value="0" id="pickup_type_same">
                    <label class="form-check-label" for="pickup_type_same">
                        Pickup Address same as sender
                    </label>
                </div>
            <?php } ?>
        </div>
        <?php if ($prefix == '_sen') { ?>
            <div class="form-group col-sm-4 dGrdEnd">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="pickup_type" value="1" id="pickup_type_new">
                    <label class="form-check-label" for="pickup_type_new">
                        Enter New Pickup Address  <i class="fa fa-map-marker absGps" aria-hidden="true"></i>
                    </label>
                </div>

            </div>
        <?php } ?>
    </div>


    <?php
}

function setSessionSearchData() {
    $db = LoadDB();
    $data = $_POST;

    $db->session->set_userdata('searchData', $data);
    $url = base_url('choose-your-courier');
    die(json_encode(array('status' => true, 'message' => 'success', 'url' => $url)));
}

function getSessionSearchData() {
    $db = LoadDB();
    $searchData = $db->session->userdata('searchData');
    return $searchData;
}

function chooseYourCourier() {
    $data = getSessionSearchData();
    $data['courierId'] = $_POST['courierId'];
    $data['schedule_date'] = !empty($_POST['schedule_date']) ? $_POST['schedule_date'] : '';
    $data['price'] = !empty($_POST['price']) ? base64_decode($_POST['price']) : '0';

    $data['pickup_dropoff'] = !empty($_POST['pickup_dropoff']) ? $_POST['pickup_dropoff'] : '';
    $data['estimated_delivery'] = !empty($_POST['estimated_delivery']) ? $_POST['estimated_delivery'] : '';

    $_POST = $data;
    setSessionSearchData();
}

function chooseshipmentDetails() {
    $data = getSessionSearchData();
    $data['shipmentDetails'] = $_POST;
    $_POST = $data;
    setSessionSearchData();
}

function saveSessionForsenderReceiver() {
    $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
    $prefix = !empty($_POST['prefix']) ? $_POST['prefix'] : '';
    $sessionAr = GetSessionArrayLogin();
    $db = LoadDB();
    if (!empty($json)) {
        $json['lastupdate'] = date('Y-m-d H:i:s');
        $is_new = !empty($json['is_new']) ? $json['is_new'] : '0';
        unset($json['is_new']);
        $addressId = $json['id'];
        if (!empty($is_new) || empty($json['id'])) {
            unset($json['id']);
            $json['timestamp'] = date('Y-m-d H:i:s');
            $json['user_id'] = $sessionAr->id;

            $addressId = $db->Database->insert('sender_address', $json);
        } else if (!empty($json['id'])) {
            $db->Database->update('sender_address', $json, ['id' => $json['id']]);
        }

        $data = getSessionSearchData();
        $data["$prefix"] = $addressId;
        $_POST = $data;
        setSessionSearchData();
    }
}

function savemyAddress() {
    $json = !empty($_POST['json']) && is_array($_POST['json']) ? $_POST['json'] : [];
    $sessionAr = GetSessionArrayLogin();
    $json['lastupdate'] = date('Y-m-d H:i:s');
    $db = LoadDB();
    if (!empty($json['id'])) {
        $db->Database->update('sender_address', $json, ['id' => $json['id']]);
    } else {
        $json['timestamp'] = date('Y-m-d H:i:s');
        $json['user_id'] = $sessionAr->id;

        $addressId = $db->Database->insert('sender_address', $json);
    }
    die(json_encode(array('status' => true, 'message' => 'Updated successfully', 'url' => false)));
}

function loadMyBookingSummary() {
    $searchData = getSessionSearchData();
    $sessionAr = GetSessionArrayLogin();
    $packageWeight = !empty($searchData['packageWeight']) ? $searchData['packageWeight'] : '0';
    $searchType = !empty($searchData['search_type']) ? $searchData['search_type'] : '';

    $fromcountry = GetcountryById($searchData['packageFrom']);
    $tocountry = GetcountryById($searchData['packageTo']);

    $schedule_date = !empty($searchData['schedule_date']) ? date('d-m-Y', strtotime($searchData['schedule_date'])) : '';
    $courierId = !empty($searchData['courierId']) ? $searchData['courierId'] : '';
    $price = !empty($searchData['price']) ? $searchData['price'] : '0';
    ob_start();
    $defaultLogo = base_url('css/images/LogoClr.svg');

    $courier = Getcouriercompanymaster($courierId);
    //  echo '<pre>';
    //  print_r($searchData);
//    print_r($courier);
    //  echo '</pre>';
    if (!empty($courier)) {
        $defaultLogo = $courier->logo_url;
    }
    ?>
    <div class="row g-0  text-left shadowL py-3 fontSmall">
        <div class="col-lg-1 col-sm-6 noWrap p-0">
            <img src="<?= $defaultLogo ?>" class="img-fluid" alt="...">
        </div>

        <div class="col-lg-2 col-sm-6 noWrap">
            <label for="">Weight</label>
            <input type="text" class="form-control brdOrg" readonly value="<?= $packageWeight ?> KG">
        </div>
        <div class="col-lg-2 col-sm-6 noWrap">
            <label for="">From</label>
            <input type="text" class="form-control brdOrg" readonly value="<?= $fromcountry->country ?>">
        </div>
        <div class="col-lg-2 col-sm-6 noWrap">
            <label for="">To</label>
            <input type="text" class="form-control brdOrg" readonly value="<?= $tocountry->country ?>">
        </div>

        <div class="col-lg-2 col-sm-6 noWrap">
            <label for="">Schedule date</label>
            <input type="text" class="form-control brdOrg" readonly value="<?= $schedule_date ?>"  style="max-width: 130px;">
        </div>                   

        <div class="col-lg-2 col-sm-6 text-center ">
            <label for="">Type</label>
            <input type="button" class="form-control btn btnSec bdr25 py-0" placeholder="" value="<?= $searchType ?>">

        </div>
    </div>

    <div class="row mt-4 float-left">
        <div class="form-check-inline px-2">
            <label class="form-check-label">
                <input id="TermsAndConditions" type="checkbox" class="form-check-input" value="">Terms & Conditions
            </label>
        </div>
    </div>
    <?php
    $bookingSummery = ob_get_clean();

    ob_start();

    $payment['base_price'] = $price;
    $payment['vat'] = ($payment['base_price'] * 5 / 100);
    $payment['total'] = $payment['base_price'] + $payment['vat'];

    $isBook = false;

    if (!empty($courier) && !empty($searchData['shipmentDetails']) && !empty($searchData['_sen']) && !empty($searchData['_rec'])) {

        $isBook = true;
    }
    $payment['isBook'] = $isBook;
    ?>
    <div class="card shadowL border-0 ">
        <div class="card-body p-2">
            <table class="table mb-0">
                <tbody>
                    <tr>
                        <td class="border-0">Base Charge</td>
                        <td class="border-0">AED <?= DecimalAmount($payment['base_price']) ?></td>

                    </tr>
                    <tr>
                        <td>Fee & Surcharges</td>
                        <td>AED 0.00</td>

                    </tr>
                    <tr>
                        <td>VAT</td>
                        <td><?= DecimalAmount($payment['vat']) ?></td>

                    </tr>
                    <tr>
                        <td>Total Charge</td>
                        <td> AED <?= DecimalAmount($payment['total']) ?></td>
                    </tr>  
                </tbody>          
            </table>
        </div>
    </div>
    <div class="card shadowL border-0 mt-3">
        <div class="card-body p-2">
            <form class="row">
                <div class="form-group col-sm-12">
                    <label for="">Promo Code</label>
                    <input type="Text" class="form-control" id="" aria-describedby="" placeholder="">
                </div>
                <div class="form-group col-sm-12 text-center">
                    <button <?= empty($isBook) ? 'disabled' : '' ?> class="btn btnPrm rounded">Apply Code</button>
                </div>
            </form>
        </div>
    </div>
    <?php if (!empty($isBook)) { ?>
        <div class="row justify-content-center mt-4">
            <div class="col-auto">
                <button onclick="myBooking.submitMyorder();" class="btn btnPrm rounded">PAY NOW</button>             
            </div>
            <div class="col-auto">
                <button onclick="myBooking.submitMyorder();" class="btn btnSec rounded">PAY FROM WALLET</button>
            </div>
        </div>
        <?php
    } else {
        ?>
        <div class="row justify-content-center mt-4">
            <div class="col-auto">
                <button disabled disabled class="btn btnPrm rounded">PAY NOW</button>             
            </div>
            <div class="col-auto">
                <button disabled class="btn btnSec rounded">PAY FROM WALLET</button>
            </div>
        </div> 
        <?php
    }
    $paymentDetails = ob_get_clean();
    $return = array('status' => true, 'message' => 'success', 'bookingSummery' => $bookingSummery, 'paymentDetails' => $paymentDetails, 'payment' => $payment);
    if (isset($_POST['encode'])) {
        die(json_encode($return));
    }
    return $return;
}

function getNewBookingno() {
    $db = LoadDB();
    $qry = "SELECT MAX(booking_id) AS max_num FROM `booking`";
    $dArray = $db->Database->select_qry_array($qry);
    $max_num = !empty($dArray[0]->max_num) ? ($dArray[0]->max_num + 1) : '1';
    $max_num = 'B-' . str_pad($max_num, 5, "0", STR_PAD_LEFT);
    return $max_num;
}

function submitMyorder() {
    $db = LoadDB();
    $sessionAr = GetSessionArrayLogin();
    $searchData = getSessionSearchData();
    $summery = loadMyBookingSummary();
    if (empty($summery['payment']['isBook'])) {
        die(json_encode(array('status' => false, 'message' => 'something went wrong', 'url' => false)));
    }
    $payment = $summery['payment'];
    $shipmentlist = !empty($searchData['shipmentDetails']['list']) && is_array($searchData['shipmentDetails']['list']) ? $searchData['shipmentDetails']['list'] : [];

   
    $myBooking['booking_no'] = getNewBookingno();
    $myBooking['user_id'] = $sessionAr->id;
    $myBooking['courier_id'] = $searchData['courierId'];
    $myBooking['booking_type'] = $searchData['search_type'];
    $myBooking['sender_id'] = $searchData['_sen'];
    $myBooking['receiver_id'] = $searchData['_rec'];
    $myBooking['weight'] = $searchData['packageWeight'];
    $myBooking['pickup_dropoff '] = $searchData['pickup_dropoff'];
    $myBooking['schedule_date'] = !empty($searchData['schedule_date']) ? date('Y-m-d', strtotime($searchData['schedule_date'])) : '';
    $myBooking['estimated_delivery'] = !empty($searchData['estimated_delivery']) ? date('Y-m-d', strtotime($searchData['estimated_delivery'])) : '';
    $myBooking['base_price'] = $payment['base_price'];
    $myBooking['vat'] = $payment['vat'];
    $myBooking['total'] = $payment['total'];
    $myBooking['fill_dimensions'] = !empty($searchData['shipmentDetails']['fill_dimensions']) ? $searchData['shipmentDetails']['fill_dimensions'] : '1';
    $myBooking['lastupdate'] = date('Y-m-d H:i:s');
    $myBooking['timestamp'] = date('Y-m-d H:i:s');

    $bookingId = $db->Database->insert('booking', $myBooking);

    for ($i = 0; $i < count($shipmentlist); $i++) {
        $d = $shipmentlist[$i];
        $d['booking_id'] = $bookingId;
        $d['lastupdate'] = $myBooking['lastupdate'];
        $db->Database->insert('booking_package_details', $d);
    } 
   
    $db->session->set_userdata('searchData', '');

   
    $url = base_url('thank-you?bookingId=' . base64_encode($bookingId));
    die(json_encode(array('status' => true, 'message' => 'Thank you for your order', 'url' => $url)));
}
