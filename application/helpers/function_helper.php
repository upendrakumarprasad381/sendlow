<?php

function company_info() {
    $db = LoadDB();
    $qry = "SELECT * FROM `company_info` ";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : [];
    return $dArray;
}

function Gethelp($Id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `faqs` WHERE id='$Id'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : [];
    return $dArray;
}

function GetcouriercompanymasterAll($Id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `courier_company_master` WHERE archive='0'";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetemiratesAll() {
    $db = LoadDB();
    $qry = "SELECT * FROM `emirates` WHERE 1";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetemiratesById($emiratesId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `emirates` WHERE emirates_id='$emiratesId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : [];
    return $dArray;
}

function GetbookingById($bookingId = '') {
    $db = LoadDB();
    $select = ",R.first_name";
    $join = " LEFT JOIN register R ON R.id=B.user_id";
    $qry = "SELECT B.* $select FROM `booking` B $join WHERE B.booking_id='$bookingId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : [];
    return $dArray;
}
function GetbookingpackagedetailsById($bookingId = '') {
    $db = LoadDB();
    $select = "";
    $join = " ";
    $qry = "SELECT BPD.* $select FROM `booking_package_details` BPD $join WHERE BPD.booking_id='$bookingId'";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}
function Getcouriercompanymaster($courierId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `courier_company_master` WHERE courier_id='$courierId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : [];
    if (!empty($dArray)) {
        $dArray->logo_url = base_url("files/images/$dArray->logo");
    }
    return $dArray;
}

function pickup_dropoff($pickup_dropoff = '0') {
    $return = 'PICKUP';
    if (!empty($pickup_dropoff)) {
        $return = 'DROP OFF';
    }
    return $return;
}

function GetAccountType($type = '') {
    $return = '';
    if ($type == '1') {
        $return = 'Individual';
    } else if ($type == '2') {
        $return = 'Company';
    } else if ($type == '3') {
        $return = 'Business';
    } else if ($type == '4') {
        $return = 'Broker';
    } else if ($type == '5') {
        $return = 'Normal User';
    }
    return $return;
}

function GetpricelistBypriceId($priceId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `price_list` WHERE price_id='$priceId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : [];

    return $dArray;
}

function Getweightmaster() {
    $db = LoadDB();
    $qry = "SELECT * FROM `weight_master` ORDER BY weight weight_from";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetweightmasterByType($type = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `weight_master` WHERE type='$type' ORDER BY weight_from ASC";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetbookingBy($bookingId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `booking` WHERE booking_id='$bookingId'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : [];
    return $dArray;
}

function GetUsers($type = '') {
    $db = LoadDB();
    $cond = '';

    $Session = $db->session->userdata('Admin');
    $ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;

    if (!in_array($Session->id, $ADMIN_PERMISSION_ARRAY)) {
        $cond = "AND id='$Session->id'";
    }
    $qry = "SELECT * FROM `admin_login` WHERE  FIND_IN_SET($type, `user_type`) > 0 and archive='0' $cond";

    $dArray = $db->Database->select_qry_array($qry);

    return $dArray;
}

function GetUsersById($id) {
    $db = LoadDB();
    $qry = "SELECT * FROM `admin_login` WHERE id='$id' ";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : [];
    return $dArray;
}

function GetlogindetailsBy($loginid = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `admin_login` WHERE id='$loginid' and archive='0'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : [];
    // if (!empty($dArray)) {
    //     $dArray->fullImg = DEFAULT_IMG_USER;
    // }
    return $dArray;
}

function GetAllSenderAddress($id) {
    $db = LoadDB();
    $qry = "SELECT * FROM `sender_address` WHERE user_id=$id AND archive=0";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function GetAllRecipientAddress($id) {
    $db = LoadDB();
    $qry = "SELECT * FROM `recipient_address` WHERE user_id=$id AND archive=0";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function getcountrylist($id) {
    $db = LoadDB();
    $qry = "SELECT * FROM `country` WHERE id=$id AND archive=0";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function getcitylist($id) {
    $db = LoadDB();
    $qry = "SELECT * FROM `city` WHERE id=$id AND archive=0";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function getcountry() {
    $db = LoadDB();
    $qry = "SELECT * FROM `country` WHERE archive=0";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function getcountryForImport() {
    $db = LoadDB();
    $qry = "SELECT * FROM `country` WHERE archive=0 AND id NOT IN (SELECT country_id FROM `city` GROUP BY country_id) LIMIT 100";
    $dArray = $db->Database->select_qry_array($qry);
    return $dArray;
}

function getMyDestinationList($origincountryId = '') {
    $db = LoadDB();
    $origincountryId = !empty($_POST['origincountryId']) ? $_POST['origincountryId'] : $origincountryId;
    $cond = "";
    if ($origincountryId != '229') {
        $cond = $cond . " AND id IN (229)";
    }
    $qry = "SELECT * FROM `country` WHERE archive=0 $cond";
    $dArray = $db->Database->select_qry_array($qry);
    if (isset($_POST['encode'])) {
        die(json_encode(array('status' => true, 'message' => 'success', 'data' => $dArray)));
    }
    return $dArray;
}

function GetcountryById($Id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `country` WHERE id='$Id'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    return $dArray;
}

function GetcityById($Id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `city` WHERE id='$Id'";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    return $dArray;
}

function Getcountnotification($receiver_id) {
    $db = LoadDB();
    $Conducation = array('receiver_id' => $receiver_id);

    $Qry = "SELECT count(*) as count FROM `notification` WHERE receiver_id=:receiver_id and notification_read='0'";

    $Array = $db->Database->select_qry_array($Qry, $Conducation);
    return $Array;
}

function GetNotification($receiver_id) {
    $db = LoadDB();
    $Conducation = array('receiver_id' => $receiver_id);

    $Qry = "SELECT * FROM `notification` WHERE receiver_id=:receiver_id and notification_read='0' order by id desc";

    $Array = $db->Database->select_qry_array($Qry, $Conducation);
    return $Array;
}

function change_notification_status($array) {
    $db = LoadDB();
    $noti_id = $array['noti_id'];
    $receiver_id = $array['receiver_id'];
    $redirect_url = $array['redirect_url'];
    $UpdateArray = array("notification_read" => 1);
    $Conducation = array('receiver_id' => $receiver_id, 'id' => $noti_id);
    $db->Database->update('notification', $UpdateArray, $Conducation);
    die(json_encode(array('status' => 'success', 'message' => 'Successfully', 'redirect_url' => $redirect_url)));
}

function LobiboxNotifyOrder($Array) {
    $db = LoadDB();
    $Session = $db->session->userdata('Admin');
    $session_id = !empty($Session->id) ? $Session->id : '';
    $Select = 'UD.name as user_name,RD.request_no as request_no,CD.category as category';
    $Join = 'LEFT JOIN users UD ON UD.id=notification.sender_id LEFT JOIN request_table RD ON RD.id=notification.request_id LEFT JOIN category_details CD ON CD.category_id=RD.category_id';
    $qry = "SELECT notification.*,$Select FROM `notification` $Join WHERE  notification.notification_type=1 and notification.notification_read=0 and notification.receiver_id='$session_id' ORDER BY notification.id DESC LIMIT 1";
    $CondutaionArray = array('notification_read' => 0);
    $OrderArray = $db->Database->select_qry_array($qry, $CondutaionArray);

    for ($i = 0; $i < count($OrderArray); $i++) {
        $d = $OrderArray[$i];
    }

    $Select1 = 'UD.name as user_name,RD.request_no as request_no  ';
    $Join1 = 'LEFT JOIN users UD ON UD.id=notification.sender_id LEFT JOIN offer_request_table RD ON RD.id=notification.request_id';
    $qry1 = "SELECT notification.*,$Select FROM `notification` $Join WHERE  notification.notification_type=1 and notification.notification_read=0 and notification.receiver_id='$session_id' ORDER BY notification.id DESC LIMIT 1";
    $CondutaionArray1 = array('notification_read' => 0);
    $OrderArray1 = $db->Database->select_qry_array($qry, $CondutaionArray1);
    for ($i = 0; $i < count($OrderArray1); $i++) {
        $d = $OrderArray1[$i];
    }
    $OrderArray2 = array_merge($OrderArray, $OrderArray1);
    $ReturnArray = array(
        'NewOrder' => $OrderArray2,
    );
    echo json_encode($ReturnArray);
}

function GetcmsBycmsId($cmsid) {
    $db = LoadDB();
    $qry = "SELECT * FROM `cms` WHERE identify='$cmsid'";
    $Array = $db->Database->select_qry_array($qry);
    $Array = !empty($Array) ? $Array[0] : '';
    return $Array;
}

function date_compare($element1, $element2) {
    $datetime1 = strtotime($element1['timestamp']);
    $datetime2 = strtotime($element2['timestamp']);
    return $datetime2 - $datetime1;
}

function GetSenderAddressByAddressId($addressId = '') {
    $db = LoadDB();
    $id = !empty($_POST['addressId']) ? $_POST['addressId'] : $addressId;
    $qry = "SELECT * FROM sender_address WHERE id='$id' ";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    if (isset($_POST['encode'])) {
        die(json_encode(array('status' => true, 'message' => 'success', 'data' => $dArray)));
    }
    return $dArray;
}

function GetcitylistBycountryId($countryId = '') {
    $db = LoadDB();
    $countryId = !empty($_POST['countryId']) ? $_POST['countryId'] : $countryId;
    $qry = "SELECT * FROM city WHERE country_id='$countryId' ";
    $dArray = $db->Database->select_qry_array($qry);
    if (isset($_POST['encode'])) {
        die(json_encode(array('status' => true, 'message' => 'success', 'data' => $dArray)));
    }
    return $dArray;
}

function GetRecipientAddressByAddressId($addressId = '') {
    $db = LoadDB();
    $id = !empty($_POST['addressId']) ? $_POST['addressId'] : $addressId;
    $qry = "SELECT * FROM recipient_address WHERE id='$id' ";
    $dArray = $db->Database->select_qry_array($qry);
    $dArray = !empty($dArray) ? $dArray[0] : '';
    if (isset($_POST['encode'])) {
        die(json_encode(array('status' => true, 'message' => 'success', 'data' => $dArray)));
    }
    return $dArray;
}

function GetOffersByOfferId($couponId = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `offers` WHERE id='$couponId' AND archive=0";
    $Array = $db->Database->select_qry_array($qry);
    $Array = !empty($Array) ? $Array[0] : [];
    return $Array;
}

function GetAllOffers() {
    $db = LoadDB();
    $qry = "SELECT * FROM `offers` WHERE archive=0 AND CURDATE() BETWEEN start_date AND expiry_date";
    $Array = $db->Database->select_qry_array($qry);
    return $Array;
}

function GetOfferByOfferId($id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `offers` WHERE id=$id AND archive=0";
    $Array = $db->Database->select_qry_array($qry);
    return $Array;
}

function GetUserById($id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `register` WHERE id=$id AND archive=0";
    $Array = $db->Database->select_qry_array($qry);
    return $Array;
}

function GetRequestById($id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `wallet_request` WHERE id=$id AND archive=0";
    $Array = $db->Database->select_qry_array($qry);
    return $Array;
}

function GetCardRequestById($id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM `wallet_card_request` WHERE id=$id AND archive=0";
    $Array = $db->Database->select_qry_array($qry);
    return $Array;
}

function getHomeRightBanner() {
    $db = LoadDB();
    $qry = "SELECT image FROM `home_right_banner` WHERE archive=0";
    $Array = $db->Database->select_qry_array($qry);
    return $Array;
}

function getHomeLeftBanner() {
    $db = LoadDB();
    $qry = "SELECT image FROM `home_left_banner` WHERE archive=0";
    $Array = $db->Database->select_qry_array($qry);
    return $Array;
}

function getHomeExportBanner() {
    $db = LoadDB();
    $qry = "SELECT image FROM `export_banner` WHERE archive=0";
    $Array = $db->Database->select_qry_array($qry);
    return $Array;
}

function getHomeImportBanner() {
    $db = LoadDB();
    $qry = "SELECT image FROM `import_banner` WHERE archive=0";
    $Array = $db->Database->select_qry_array($qry);
    return $Array;
}

function getTestimonials() {
    $db = LoadDB();
    $qry = "SELECT * FROM `testimonials` WHERE archive=0";
    $Array = $db->Database->select_qry_array($qry);
    return $Array;
}
