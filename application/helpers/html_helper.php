<?php

function pdf_Footer() {
    ob_start();
    $company_info = company_info();

    $name = !empty($company_info->name) ? $company_info->name : "";

    $email = !empty($company_info->email) ? $company_info->email : "";
    $po_box = !empty($company_info->po_box) ? $company_info->po_box : "";
    $telephone = !empty($company_info->telephone) ? $company_info->telephone : "";
    $fax = !empty($company_info->fax) ? $company_info->fax : "";
    $ofc_address = !empty($company_info->office_address) ? $company_info->office_address : "";
    ?>


    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0"style="font-size:13px; color:#000; background:#fff; padding:15px 0; line-height:20px; ">
        <tbody>

            <tr>
                <td align="center" valign="middle" style="text-align: center; padding-top: 7rem;">
                    Invoice was created on a computer and is valid without the signature and seal. <br/>

                </td>

            </tr>   
        </tbody></table>

    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-size:13px; color:#000; background:#fff; padding:15px 0; line-height:20px; border-top: 1px solid #848181;">
        <tbody>
            <tr>
                <td align="center" valign="middle" style="text-align: center; padding-top: 7rem;">
                    <?= $name ?>|Email:<?= $email ?> ,P.O BOX:<?= $po_box ?>,Telephone:<?= $telephone ?>,Office Address:<?= $ofc_address ?> <br/>

                </td>
        </tbody></table>

    <?php
    $html = ob_get_clean();
    return $html;
}

function request_offer_invoice_pdf1($requestId) {
    ob_start();
    error_reporting(0);
    $db = LoadDB();
    $db->load->library('MPdf');
    $mpdf = new \Mpdf\Mpdf();
    $date = date("d-m-Y");
    $calib = 'Arial';
    $datet = date("jFY");
    $total = 0;
    $cnt = 1;
    $request = GetofferrequestdetailsBy($requestId);
    if (!empty($request)) {
        $userdetails = GetuserdetailsBy($request->user_id);
    }
    $offer = GetOfferdetailsBy($request->offer_id);
    $garage_id = !empty($offer->garage_id) ? $offer->garage_id : "";
    $gaarge = GetgaragedetailsBy($garage_id);
    $garage_name = !empty($gaarge->garage_name) ? $gaarge->garage_name : "";
    $mpdf->mirrorMargins = 0;
    $html = '   <style>
       .clearfix:after {
  content: "";
  display: table;
  clear: both;
}

a {
  color: #5D6975;
  text-decoration: underline;
}

body {
  position: relative;
  width: 21cm;  
  height: 29.7cm; 
  margin: 0 auto; 
  color: #001028;
  background: #FFFFFF; 
  font-family: Arial, sans-serif; 
  font-size: 12px; 
  font-family: Arial;
}

header {
  padding: 10px 0;
  margin-bottom: 30px;
}

#logo {
  text-align: center;
  margin-bottom: 10px;
}

#logo img {
  width: 130px;
}

h1 {
  border-top: 1px solid  #5D6975;
  border-bottom: 1px solid  #5D6975;
  color: #fff;
  font-size: 2.4em;
  line-height: 1.4em;
  font-weight: normal;
  text-align: center;
  margin: 0 0 20px 0;
  background-color: #000;
}

#project {
  float: left;
}


#project span {
  color: #000;
  text-align: left;
  width: 60px;
  margin-right: 5em;
  display: inline-block;
  font-size: 14px;
  font-weight: 600;

}

#company {
  text-align: right;
  font-weight: 400;
  color: #000;
  font-size: 14px;
 
}

#project div,
#company div {
  white-space: nowrap;  
  font-size: 16px;      
  color: #313131;

}

table {
  width: 100%;
  border-collapse: collapse;
  border-spacing: 0;
  margin-bottom: 10px;
}

table tr:nth-child(2n-1) td {
  background: #F5F5F5;
}

table th,
table td {
  text-align: center;
  border: 1px solid #dddddd;
}

table th {
  padding: 10px 10px;
  color: #000;
  border-bottom: 1px solid #C1CED9;
  white-space: nowrap;        
  font-size:14px;
}
 table .request{
  font-size: 20px;
  text-align: center;
  background-color: #d3b850;
  padding: 13px;
  color: #fff;
 }

table .service,
table .desc {
  text-align: left;
}

table td {
  padding: 10px;
  font-size: 16px;
}
table td.category{
  font-size: 16px !important;
    color: #000;
    font-weight: 500
}
table td.accident{
  font-size: 16px;
    color: #000;
    font-weight: 500
}
table td.service,
table td.desc {
  vertical-align: top;
}

table td.unit,
table td.qty,
table td.total {
  font-size: 1.2em;
}

table td.grand {
  border-top: 1px solid #5D6975;;
}

#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
   </style>
';
    $html = $html . '  <body>
    <header class="clearfix">
      <div id="logo">
        <img src="' . base_url() . 'assets/layouts/layout/img/logo_fix.png" style=" width: 130px;">
      </div>
      <h1>INVOICE</h1>
     
      <table width="100%" border="0" cellspacing="0" cellpadding="10" style="font-family:Arial, Helvetica, sans-serif;font-size:14px;
    color:#000; background: #ffffff;">
    <tr style="background: #ffffff;  padding-bottom: 15px;" >
        <td width="50%" align="left" valign="top" style="background: #ffffff; border: none;">
            <h5 style="<?= $style ?>">Offer No: <span style="font-weight: normal;">#' . $request->request_no . '</span></h5>
            <h5 style="<?= $style ?>">Purchase Date: <span style="font-weight: normal;">' . date('d/m/Y', strtotime($request->timestamp)) . '</span></h5>
            <h5 style="<?= $style ?>">Payment Method: <span style="font-weight: normal;"> Card</span></h5>';
    /* if($request->payment_type=='0')
      {
      $html=$html.'<h5 style="<?= $style ?>">Payment Method: <span style="font-weight: normal;"> Cash</span></h5>';
      }
      else if($request->payment_type=='1'){
      $html=$html.'<h5 style="<?= $style ?>">Payment Method: <span style="font-weight: normal;"> Card</span></h5>';
      }
      if($request->payment_status=='0')
      {
      $html=$html.'<h5 style="<?= $style ?>">Payment Status: <span style="font-weight: normal;"> Pending</span></h5>';
      }
      else if($request->payment_status=='1'){
      $html=$html.'<h5 style="<?= $style ?>">Payment Status: <span style="font-weight: normal;"> Paid</span></h5>';
      } */
    $html = $html . '
        </td>
        </td>
        <td width="50%" align="right" valign="top" style="background: #ffffff; border: none;">
            <h5 style="<?= $style ?>">Customer Name: <span style="font-weight: normal;">' . $userdetails->name . '</span></h5>
            <h5 style="<?= $style ?>">E-mail:<span style="font-weight: normal;"><a href="mailto:' . $userdetails->email . '">' . $userdetails->email . '</a></span></h5>
            <h5 style="<?= $style ?>">Phone:<span style="font-weight: normal;">' . $userdetails->mobile_code . $userdetails->mobile . '</span></h5>
        </td> 
    </tr>
</table>
   
     
    </header>
    <main>
      <table>
        <thead>
          
          <tr>
            <th class="request"> Request Details</th>
           
          </tr>
        </thead>
        </table>
      <table>
        <thead>

          <tr>
            <td colspan="4" style="
            font-size: 16px; text-align: right;">Offer Name </td>
            <td style="
            font-size: 16px;">' . $garage_name . '<br/>' . $offer->title . '</td>
</tr>';

    $original_value = !empty($offer->original_price) ? $offer->original_price : "";
    $discount_value = !empty($offer->discount_price) ? $offer->discount_price : "";

    if ($original_value != '') {
        $html = $html . '
          <tr>
            <td colspan="4" style="
            text-align: right;">Original Price</td>
            <td class="total">AED ' . $original_value . '</td>
          </tr>';
    }
    if ($discount_value != '') {
        $html = $html . '
          <tr>
            <td colspan="4" style="
            text-align: right;">Discount Price</td>
            <td class="total">AED ' . $discount_value . '</td>
          </tr>';
    }
    $total_price = $original_value - $discount_value;
    if ($total_price != '') {
        $html = $html . '  <tr>
            <td colspan="4" class="grand total" style="
            text-align: right;">Grand Total</td>
            <td class="grand total">AED ' . $total_price . '</td>
          </tr>';
    }
    $html = $html . '   </tbody>
      </table>
      
     




    </main>
    <footer>
      Invoice was created on a computer and is valid without the signature and seal.
    </footer>
  </body>
';
    $mpdf->defaultfooterline = 0;
    $a = $mpdf->WriteHTML($html);

    $filename = HOME_DIR . "uploads/invoice/" . $request->request_no . "-" . $datet . ".pdf";
    $mpdf->Output($filename, 'f');
    $CondArray = array('id' => $requestId);
    $json['invoice'] = $request->request_no . "-" . $datet . ".pdf";
    $db->Database->update('offer_request_table', $json, $CondArray);
}

function request_invoice_pdf1($request_id = '', $garage_id = '') {
    ob_start();
    error_reporting(0);
    $db = LoadDB();
    $db->load->library('MPdf');
    // $mpdf = new \Mpdf\Mpdf();
    $date = date("d-m-Y");
    $calib = 'Arial';
    $datet = date("jFY");
    $total = 0;
    $cnt = 1;
    $request = GetrequestdetailsBy($request_id);
    $fix_invoice = $request->invoice_no;
    $request_no = $request->request_no;

    $company_info = company_info();
    $fix_trn_no = !empty($company_info->trn_no) ? $company_info->trn_no : "";
    if (!empty($request)) {
        $userdetails = GetuserdetailsBy($request->user_id);
    }
    //$mpdf->mirrorMargins = 0;
    $html = "<style>
       body {
            font-family: 'Poppins', sans-serif;
            font-size: 14px;
            position: relative;
  width: 21cm;  
  height: 29.7cm; 
  margin: 0 auto; 
        }

        table {
            font-family: 'Poppins', sans-serif;
            border-collapse: collapse;
            /* width: 100%; */
        }

        td, th {
           
            text-align: left;
            padding: 8px;
            font-weight: 400;
            vertical-align: top;
        }
        th{
            font-weight: 400;
            font-size: 14px;
        }
        tr:nth-child(even) {
  background-color: #dddddd;
}
        tbody{
            vertical-align: top ;
        }
        
#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
</style>";
    $category_id = !empty($request->category_id) ? $request->category_id : "";
    $catname = GetCategorydetailsBy($category_id);
    $get_garage_quot = get_garage_request_ad($request_id, $garage_id);
    $request_garage_id = !empty($get_garage_quot->id) ? $get_garage_quot->id : "";
    $garage_invoice_no = !empty($get_garage_quot->invoice_no) ? $get_garage_quot->invoice_no : "";

    $garage_Det = GetgaragerequestdetailsBy($request_garage_id);
    $garage_Details = GetgaragedetailsBy($garage_id);
    $gara_trn_no = !empty($garage_Details->TRN_NO) ? $garage_Details->TRN_NO : "";
    $category = !empty($catname->category) ? $catname->category : '';

    $garage_img = !empty($garage_Details->garage_image) ? $garage_Details->garage_image : "";
    $layout_type = isset($catname->layout_type) ? $catname->layout_type : '';
    $html = '
 <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
        <td width="50%" align="left" style=" padding:20px 0; width:130px;"> 
          <img  src="' . base_url() . 'assets/layouts/layout/img/logo_fix.png"  style="height: 120px;"> </td>
          <td width="50%" align="right" valign="left" style=" padding:20px 0; width:130px; padding-left: 30rem;"> ';
    if ($garage_img != '') {
        $html = $html . ' <img src="' . base_url() . 'files/garage_image/' . $garage_img . '" style="height: 120px; argin-bottom:10px;"> <br/>';
    } else {
        $html = $html . '      <img src="' . base_url() . '/uploads/garage_image.png" style="height: 120px; argin-bottom:10px;"> <br/>';
    }
    $html = $html . '   </td>
      </tr>
    </tbody></table>';

//      $html =$html.'<table width="100%">
//      <tbody>
//       <tr>
//          <td width="100%" style="    font-family: Arial, Helvetica, sans-serif; background-color: #d0cfcf; color: #000; font-size: 18px; text-transform: uppercase; font-weight:bold; text-align: center; letter-spacing: 1px;          padding-top:6px;
//         padding-bottom:6px;   ">
//           Tax invoice 
//          </td>
//       </tr>
//      </tbody>
//     </table>';
//     $html =$html.'<table width="100%" border="0" align="center" cellpadding="0" cellspacing="0">
//       <tbody><tr>
//         <td><table width="100%" border="0" cellspacing="0" cellpadding="0" style="font-family: Arial, Helvetica, sans-serif;  font-size:14px; color:#323232;">
//       <tbody><tr>
//         <td width="70%" align="left" valign="top" style=" line-height:23px;">
//         <h5 style="color:#444343; font-size:15px; font-weight:600; line-height: 27px; 
//         padding:4px 00 6px 0; 
//         margin:0;">Invoice No: <strong style="padding-left: 1rem; color:#e6be44;">'.$fix_invoice.'</strong><br>
//       TRN No: <strong style="padding-left: 1rem; color:#e6be44;">'.$fix_trn_no.'</strong><br>
//     Request No: <strong style="padding-left: 1rem; color:#e6be44;">'.$request_no.'</strong><br>
//     Request Date: <strong style="padding-left: 1rem; color:#e6be44;">' . date('d/m/Y', strtotime($request->timestamp)) . '</strong><br>';
//   if($request->payment_status=='1')
// {
//   $html =$html. '    Payment Status: <strong style="padding-left: 1rem; color:#e6be44;">Paid</strong>';
// }
// else
// {
//   $html =$html. '    Payment Status: <strong style="padding-left: 1rem; color:#e6be44;">Pending</strong>';
// }
//   $html =$html. '  </h5> 
//         </td>
//         <td width="70%" align="right" valign="top" style=" line-height:23px;">
//         <h5 style="color:#444343; font-size:15px; font-weight:600; line-height: 27px; 
//         padding:4px 00 6px 0; 
//         margin:0;">Invoice No: <strong style="padding-left: 1rem; color:#e6be44;">'.$garage_invoice_no.'</strong><br>
//       TRN No: <strong style="padding-left: 1rem; color:#e6be44;">'.$gara_trn_no.'</strong><br>
//   </h5>
//         </td>
//       </tr>
//     </tbody></table></tbody></table>';
    $html = $html . '
    
    
    
     <table width="100%" border="0" cellspacing="0" cellpadding="7" style="font-family: Arial, Helvetica, sans-serif;  border: 1px solid #d6d5d5; border-top:none;
    font-size:14px; color:#323232; margin-top:15px;">
      <tbody>
        <tr>
          <td colspan="2" align="center" valign="top" style="  line-height:23px; 
   
          background-color: #d0cfcf; color: #000; font-size: 18px; text-transform: uppercase; font-weight:bold; text-align: center;  
         ">           Tax invoice 
        </td>
        </tr>
        <tr>
        <td width="50%" align="left" valign="top" style=" font-family: Arial, Helvetica, sans-serif;  color:#444343; font-size:14px; font-weight:600; line-height: 27px; 
         ">
              
     <h5 style="color:#444343; font-size:15px; font-weight:600; line-height: 27px; 
        padding:4px 00 6px 0; 
        margin:0;">Invoice No: <strong style="padding-left: 1rem; color:#e6be44;">' . $fix_invoice . '</strong><br>
       TRN No: <strong style="padding-left: 1rem; color:#e6be44;">' . $fix_trn_no . '</strong><br>
    Request No: <strong style="padding-left: 1rem; color:#e6be44;">' . $request_no . '</strong><br>
    Request Date: <strong style="padding-left: 1rem; color:#e6be44;">' . date('d/m/Y', strtotime($request->timestamp)) . '</strong><br>';

    $html = $html . '</h5> 
        </td>
        <td width="50%" align="right" valign="top" style="font-family: Arial, Helvetica, sans-serif;  line-height:27px;
         ">';

    $html = $html . '   <h5 style="color:#444343; font-size:15px; font-weight:600; line-height: 27px; 
        padding:4px 00 6px 0; 
        margin:0;">Invoice No: <strong style="padding-left: 1rem; color:#e6be44;">' . $garage_invoice_no . '</strong><br>
       TRN No: <strong style="padding-left: 1rem; color:#e6be44;">' . $gara_trn_no . '</strong><br>';
    if ($request->payment_type == '0') {
        $html = $html . '    Payment Method: <strong style="padding-left: 1rem; color:#e6be44;">Cash</strong><br>';
    } else if ($request->payment_type == '1') {
        $html = $html . '    Payment Method: <strong style="padding-left: 1rem; color:#e6be44;">Card</strong><br>';
    }
    if ($request->payment_status == '0') {
        $html = $html . '    Payment Status: <strong style="padding-left: 1rem; color:#e6be44;">Pending</strong>';
    } else if ($request->payment_status == '1') {
        $html = $html . '    Payment Status: <strong style="padding-left: 1rem; color:#e6be44;">Paid</strong>';
    }
    $html = $html . '
  </h5>
        </td>
      </tr>
    </tbody></table>


    
    
    
    
    
    <table width="100%" border="0" cellspacing="0" cellpadding="7" style="font-family: Arial, Helvetica, sans-serif;  border: 1px solid #d6d5d5; border-top:none;
    font-size:14px; color:#323232; margin-top:15px;">
      <tbody>
        <tr>
          <td colspan="2" align="center" valign="top" style="  line-height:23px; 
   
          background-color: #d0cfcf; color: #000; font-size: 14px; text-transform: uppercase; font-weight:bold; text-align: center;  
         ">Customer Details </td>
        </tr>
        <tr>
        <td width="50%" align="left" valign="top" style=" font-family: Arial, Helvetica, sans-serif;  color:#444343; font-size:14px; font-weight:600; line-height: 27px; 
        
      
         ">
              
        
        Customer Name : <strong style="padding-left: 1rem; color:#e6be44;">' . $userdetails->name . ' </strong><br>
        
      E-mail: <strong style="padding-left: 1rem; color:#e6be44;">' . $userdetails->email . '</strong><br>
      
    Phone Number: <strong style="padding-left: 1rem; color:#e6be44;">' . $userdetails->mobile_code . $userdetails->mobile . '</strong><br>
    
    
 
        </td>
        
        <td width="50%" align="right" valign="top" style="font-family: Arial, Helvetica, sans-serif;  line-height:23px;
         ">';
    $yearname = GetyeardetailsBy($request->year);
    $year = !empty($yearname) ? $yearname->year : '';
    $makename = GetmakedetailsBy($request->make);
    $make = !empty($makename->make) ? $makename->make : '';
    $modelname = GetmodeldetailsBy($request->model);
    $model = !empty($modelname->model) ? $modelname->model : '';
    $souname = GetemiratedetailsBy($request->plate_source);
    $plate_source = !empty($souname->emirate) ? $souname->emirate : '';
    $codename = GetplatecodedetailsBy($request->plate_code);
    $plate_code = !empty($codename->plate_code) ? $codename->plate_code : '';
    $plate_no = !empty($request->plate_no) ? $request->plate_no : '';

    $chassis_no = !empty($request->chassis_no) ? $request->chassis_no : '';

    $html = $html . '  <h5 style="font-family: Arial, Helvetica, sans-serif; color:#444343; font-size:14px; font-weight:600; line-height: 27px; 
        padding:4px 00 6px 0; 
        margin:0;">Plate Information : <strong style="padding-left: 1rem; color:#e6be44;">' . $plate_source . $plate_code . $plate_no . '</strong>  <br>
       Make: <strong style="padding-left: 1rem; color:#e6be44;">' . $make . '</strong><br>
      Model: <strong style="padding-left: 1rem; color:#e6be44;">' . $model . '</strong><br>
      Year: <strong style="padding-left: 1rem; color:#e6be44;">' . $year . '</strong><br>
     Chassis Number: <strong style="padding-left: 1rem; color:#e6be44;">' . $chassis_no . '</strong>
  </h5>
        </td>
      </tr>
    </tbody></table>


    
    
    
    
    
    
    
    
    
    
    
    
    
    

    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top:25px;  margin-bottom:7px;">
      <tbody><tr>
                <td style="  padding: 10px;
       background-color: #e4b62b; text-align: center;
        font-size: 17px;
        color: #000;
        font-weight: 600;">Quotation Details</td>
      </tr>
    </tbody></table>';

    $html = $html . '  <table width="100%" border="0" align="center" cellpadding="5" cellspacing="0" style="border: 1px solid #a09e9e; font-family:Arial, Helvetica, sans-serif; font-size:14px;">
      <tbody>
      <tr>

        <th width="225"  style=" border-right: 1px solid #a09e9e;    background-color: #dedede; font-size: 15px; color: #000; font-weight: 600; text-align:left  ">Description</th>
        <th width="225"  style="    background-color: #dedede; font-size: 15px; color: #000; font-weight: 600; text-align: right;">Price (AED)</th>
      </tr>
      ';

    $comments = !empty($get_garage_quot->comments) ? $get_garage_quot->comments : "";

    $quote_charges = get_quote_charges($request_garage_id, $request_id);
    if (count($quote_charges) > 0) {


        for ($i = 0; $i < count($quote_charges); $i++) {
            $h = count($quote_charges);
            $description = !empty($quote_charges[$i]->description) ? $quote_charges[$i]->description : "";
            $get_price = !empty($quote_charges[$i]->price) ? $quote_charges[$i]->price : "";

            if ($i == 0) {
                if (!empty($description) && !empty($get_price)) {
                    $html = $html . '<tr>
        <td  style="border-right: 1px solid #a09e9e; text-align: left;">' . $description . '</td>
        <td style="  text-align: right;">' . $get_price . ' AED</td>
      </tr>';
                }
            } else {
                if ($i % 2 == 1) {
                    if (!empty($description) && !empty($get_price)) {
                        $html = $html . '
      <tr>
        <td  style="    background-color: #dedede; border-right: 1px solid #a09e9e; text-align: left;">' . $description . '</td>
        <td style="    background-color: #dedede; text-align: right;">' . $get_price . ' AED</td>
      </tr>';
                    }
                } else {
                    if (!empty($description) && !empty($get_price)) {
                        $html = $html . '
      <tr>
        <td  style="border-right: 1px solid #a09e9e; text-align: left;">' . $description . '</td>
        <td style="  text-align: right;">' . $get_price . ' AED</td>
      </tr>';
                    }
                }
            }
        }
    }
    $labour_charge = !empty($get_garage_quot->labour_charge) ? $get_garage_quot->labour_charge : '';
    $extra_charge = !empty($get_garage_quot->extra_charge) ? $get_garage_quot->extra_charge : '';
    $working_days = !empty($get_garage_quot->working_days) ? $get_garage_quot->working_days : '';
    $spare_parts = !empty($get_garage_quot->spare_parts) ? $get_garage_quot->spare_parts : '';

    $parts_open = !empty($get_garage_quot->parts_open) ? $get_garage_quot->parts_open : '';
    $crane_service = !empty($get_garage_quot->crane_service) ? $get_garage_quot->crane_service : '';

    $checkup_charges = !empty($get_garage_quot->checkup_charges) ? $get_garage_quot->checkup_charges : '';
    $lump_sum = !empty($get_garage_quot->lump_sum) ? $get_garage_quot->lump_sum : '';

    $service_charge = !empty($get_garage_quot->service_charge) ? $get_garage_quot->service_charge : '';
    $total_price = !empty($get_garage_quot->total_price) ? $get_garage_quot->total_price : '';
    $parts_open = !empty($get_garage_quot->parts_open) ? $get_garage_quot->parts_open : "";
    $vat = !empty($get_garage_quot->vat) ? $get_garage_quot->vat : "";

    $grand_total = !empty($get_garage_quot->grand_total) ? $get_garage_quot->grand_total : "";
    if ($labour_charge != '') {
        $html = $html . '   <tr>
        <td  style="background: #fff; border-right: 1px solid #a09e9e; border-top: 1px solid #a09e9e;">Labour Charge</td>
        <td style=" background: #fff;  border-top: 1px solid #a09e9e; text-align: right;">' . $labour_charge . ' AED</td>
      </tr>';
    }

    if ($service_charge != '') {
        $html = $html . '   <tr>
        <td  style="border-right: 1px solid #a09e9e; border-top: 1px solid #a09e9e;">Service Charge</td>
        <td style="  border-top: 1px solid #a09e9e; text-align: right;">' . $service_charge . ' AED</td>
      </tr>';
    }
    if ($extra_charge != '') {
        $html = $html . '   <tr>
         <td style="background: #fff; border-right: 1px solid #a09e9e; border-top: 1px solid #a09e9e;">Extra Charge</td>
        <td style=" background: #fff; border-top: 1px solid #a09e9e; text-align: right;">' . $extra_charge . ' AED</td>
      </tr>';
    }
    if ($spare_parts != '') {
        $html = $html . '   <tr>
        <td style="border-right: 1px solid #a09e9e; border-top: 1px solid #a09e9e;">Spare Parts</td>
       <td style="  border-top: 1px solid #a09e9e; text-align: right;">' . $spare_parts . ' AED</td>
      </tr>';
    }
    if ($crane_service != '') {
        $html = $html . '   <tr>
        <td  style=" background: #fff; border-right: 1px solid #a09e9e; border-top: 1px solid #a09e9e;">Crane Service</td>
         <td style="  background: #fff; border-top: 1px solid #a09e9e; text-align: right;">' . $crane_service . ' AED</td>
      </tr>';
    }
    if ($checkup_charges != '') {
        $html = $html . '  <tr>
        <td  style="border-right: 1px solid #a09e9e; border-top: 1px solid #a09e9e;">Checkup Charge</td>
          <td style="  border-top: 1px solid #a09e9e; text-align: right;">' . $checkup_charges . ' AED</td>
      </tr>';
    }
    if ($lump_sum != '') {
        $html = $html . '   <tr>
        <td style="background: #fff; border-right: 1px solid #a09e9e; border-top: 1px solid #a09e9e;">LUMP SUM</td>
         <td style=" background: #fff;  border-top: 1px solid #a09e9e; text-align: right;">' . $lump_sum . ' AED</td>
      </tr>';
    }
    if ($parts_open == '1') {
        $html .= '<tr>
                    <td style="border-right: 1px solid #a09e9e; border-top: 1px solid #a09e9e;">Parts Open</td>
                    <td style="  border-top: 1px solid #a09e9e; text-align: right;">&#10004;</td>
                </tr>';
    }
    $html = $html . '
      <tr>
        <td  style="background: #ffffff;border-right: 1px solid #a09e9e; border-top: 2px solid #000;">SUBTOTAL</td>
        <td style="  background: #fff; border-top: 2px solid #000; text-align: right;">' . $total_price . ' AED</td>
      </tr>
     
      <tr>
        <td  style="border-right: 1px solid #a09e9e; border-top: 1px solid #a09e9e;">VAT 5%</td>
        <td style=" border-top: 1px solid #a09e9e; text-align: right;">' . $vat . ' AED</td>
      </tr>
      <tr>
        <td style="background: #fff; border-right: 1px solid #a09e9e;border-top: 1px solid #a09e9e;"><strong>Grand Total</strong></td>
        <td style="background: #fff;  text-align: right;border-top: 1px solid #a09e9e;"><strong>' . $grand_total . ' AED</strong></td>
      </tr>
      
    </tbody>
    </table><br/>
    
    
    
    
    <table width="100%" border="0" cellspacing="0" cellpadding="4" style="font-family: Arial, Helvetica, sans-serif;  border: 1px solid #d6d5d5; border-top:none;
    font-size:14px; color:#323232;">
      <tbody>
        <tr>
          <td colspan="2" align="center" valign="top" style="  line-height:23px; 
   
          background-color: #d0cfcf; color: #000; font-size: 14px; text-transform: uppercase; font-weight:bold; text-align: center;  
         ">COMMENTS</td>
        </tr>
        <tr>
        <td width="60%" align="left" valign="top" style="font-family: Arial, Helvetica, sans-serif;  color:#444343; font-size:13px;  ;  line-height:23px;
         ">
     
    ' . $comments . ' 
     
 
        </td>
        
</tr>
    </tbody></table>
    
    
    

  </body>
  
  
  
</html>';
//   $mpdf->defaultfooterline = 0;
//     $a = $mpdf->WriteHTML($html);
    $mpdfConfig = array(
        'mode' => 'utf-8',
        'format' => 'A4',
        'default_font_size' => 0,
        'default_font' => '',
        'margin_left' => 10,
        'margin_right' => 10,
        'margin_top' => '10',
        'margin_bottom' => '30',
        'margin_header' => 13,
        'margin_footer' => 2,
        'orientation' => 'P'
    );
    $mpdf = new \Mpdf\Mpdf($mpdfConfig);
    $mpdf->SetHTMLFooter(pdf_Footer());
    $mpdf->WriteHTML($html);
    $filename = HOME_DIR . "uploads/invoice/" . $request->request_no . "-" . $datet . ".pdf";
    $mpdf->Output($filename, 'f');
    $CondArray = array('id' => $request_id);
    $json['invoice'] = $request->request_no . "-" . $datet . ".pdf";
    $db->Database->update('request_table', $json, $CondArray);
}

function request_offer_invoice_pdf($requestId) {
    ob_start();
    error_reporting(0);
    $db = LoadDB();
    $db->load->library('MPdf');
    // $mpdf = new \Mpdf\Mpdf();
    $date = date("d-m-Y");
    $calib = 'Arial';
    $datet = date("jFY");
    $total = 0;
    $cnt = 1;
    $request = GetofferrequestdetailsBy($requestId);
    $fix_invoice = $request->invoice_no;
    if (!empty($request)) {
        $userdetails = GetuserdetailsBy($request->user_id);
    }
    $offer = GetOfferdetailsBy($request->offer_id);
    $garage_id = !empty($offer->garage_id) ? $offer->garage_id : "";
    $gaarge = GetgaragedetailsBy($garage_id);

    $gara_trn_no = !empty($gaarge->TRN_NO) ? $gaarge->TRN_NO : "";
    $garage_name = !empty($gaarge->garage_name) ? $gaarge->garage_name : "";
    $garage_img = !empty($gaarge->garage_image) ? $gaarge->garage_image : "";

    $category_id = !empty($request->category_id) ? $request->category_id : "";
    $catname = GetCategorydetailsBy($category_id);
    $get_garage_quot = get_garage_request_ad($request_id, $garage_id);

    // if(!empty($get_garage_quot->invoice_no)){
    $garage_invoice_no = !empty($get_garage_quot->invoice_no) ? $get_garage_quot->invoice_no : "";
    // }
    // else{
    //     $garage_invoice_no = mt_rand(10000000, 99999999);
    // }


    $company_info = company_info();
    $fix_trn_no = !empty($company_info->trn_no) ? $company_info->trn_no : "";

    //$mpdf->mirrorMargins = 0;
    $html = "<style>
       body {
            font-family: 'Poppins', sans-serif;
            font-size: 14px;
            position: relative;
  width: 21cm;  
  height: 29.7cm; 
  margin: 0 auto; 
        }

        table {
            font-family: 'Poppins', sans-serif;
            border-collapse: collapse;
            /* width: 100%; */
        }

        td, th {
           
            text-align: left;
            padding: 8px;
            font-weight: 400;
            vertical-align: top;
        }
        th{
            font-weight: 400;
            font-size: 14px;
        }
        tr:nth-child(even) {
  background-color: #dddddd;
}
        tbody{
            vertical-align: top ;
        }
        
#notices .notice {
  color: #5D6975;
  font-size: 1.2em;
}

footer {
  color: #5D6975;
  width: 100%;
  height: 30px;
  position: absolute;
  bottom: 0;
  border-top: 1px solid #C1CED9;
  padding: 8px 0;
  text-align: center;
}
</style>";

    $html = '
 <table width="800" border="0" align="center" cellpadding="0" cellspacing="0">
      <tbody>
        <tr>
         <td width="50%" align="left" valign="left" style=" padding:20px 0; width:130px; padding-left: 30rem;"> ';
    if ($garage_img != '') {
        $html = $html . ' <img src="' . base_url() . 'files/garage_image/' . $garage_img . '" style="height: 120px; argin-bottom:10px;"> <br/>';
    } else {
        $html = $html . '      <img src="' . base_url() . '/uploads/garage_image.png" style="height: 120px; argin-bottom:10px;"> <br/>';
    }
    $html = $html . '   
        </td>
        <td width="50%" align="right" style=" padding:20px 0; width:130px;"> 
          <img  src="' . base_url() . 'assets/layouts/layout/img/logo_fix.png"  style="height: 120px;"> 
        </td>
        
      </tr>
    </tbody></table>
    
   
 <table width="100%" border="0" cellspacing="0" cellpadding="7" style="font-family: Arial, Helvetica, sans-serif;  border: 1px solid #d6d5d5; border-top:none;
    font-size:14px; color:#323232; margin-top:15px;">
      <tbody>
        <tr>
          	<td colspan="2" align="center" valign="top" style="  line-height:23px; 
          	background-color: #d0cfcf; color: #000; font-size: 18px; text-transform: uppercase; font-weight:bold; text-align: center;  
         	">           Tax invoice 
 		</td>
        </tr>
        <tr>
        	<td width="50%" align="left" valign="top" style=" font-family: Arial, Helvetica, sans-serif;  color:#444343; font-size:14px; font-weight:600; line-height: 27px; ">   
     			<h5 style="color:#444343; font-size:15px; font-weight:600; line-height: 27px; 
        		padding:4px 00 6px 0; 
        		margin:0;">
       			Invoice No: <strong style="padding-left: 1rem; color:#e6be44;">' . $garage_invoice_no . '</strong><br>
            	TRN No: <strong style="padding-left: 1rem; color:#e6be44;">' . $gara_trn_no . '</strong><br>
                Order No: <strong style="padding-left: 1rem; color:#e6be44;">#' . $request->request_no . '</strong><br>
                Order Date: <strong style="padding-left: 1rem; color:#e6be44;">' . date('d/m/Y', strtotime($request->timestamp)) . '</strong><br>
                Valid From: <strong style="padding-left: 1rem; color:#e6be44;">' . date('d/m/Y', strtotime($offer->valid_from)) . '</strong><br>';
    $html = $html . '</h5> 
        	</td>
        	<td width="50%" align="right" valign="top" style="font-family: Arial, Helvetica, sans-serif;  line-height:27px; ">';
    $html = $html . '   
			<h5 style="color:#444343; font-size:15px; font-weight:600; line-height: 27px; 
        		padding:4px 00 6px 0; 
        		margin:0;">
        		Invoice No: <strong style="padding-left: 1rem; color:#e6be44;">' . $request->fix_invoice_no . '</strong><br>
           		TRN No: <strong style="padding-left: 1rem; color:#e6be44;">' . $fix_trn_no . '</strong><br>';
    // 		Valid From: <strong style="padding-left: 1rem; color:#e6be44;">'.date('d/m/Y', strtotime($offer->valid_from)).'</strong><br>';
    // 	Valid Until: <strong style="padding-left: 1rem; color:#e6be44;">'.date('d/m/Y', strtotime($offer->valid_to)).'</strong><br>';
    if ($request->payment_type == '0') {
        $html = $html . 'Payment Method: <strong style="padding-left: 1rem; color:#e6be44;"> Cash</strong><br>';
    } else if ($request->payment_type == '1') {
        $html = $html . 'Payment Method: <strong style="padding-left: 1rem; color:#e6be44;"> Card</strong><br>';
    }
    if ($request->payment_status == '0') {
        $html = $html . '    Payment Status: <strong style="padding-left: 1rem; color:#e6be44;">Pending</strong><br>';
    } else if ($request->payment_status == '1') {
        $html = $html . '    Payment Status: <strong style="padding-left: 1rem; color:#e6be44;">Paid</strong><br>';
    }

    $html = $html . '
        			Valid Until: <strong style="padding-left: 1rem; color:#e6be44;">' . date('d/m/Y', strtotime($offer->valid_to)) . '</strong><br>
  			</h5>
        	</td>
      	</tr>
    </tbody></table>

    
     <table width="100%" border="0" cellspacing="0" cellpadding="7" style="font-family: Arial, Helvetica, sans-serif;  border: 1px solid #d6d5d5; border-top:none;
    font-size:14px; color:#323232; margin-top:15px;">
      <tbody>
        <tr>
          <td colspan="2" align="center" valign="top" style="  line-height:23px; 
   
          background-color: #d0cfcf; color: #000; font-size: 14px; text-transform: uppercase; font-weight:bold; text-align: center;  
         ">Customer Details </td>
        </tr>
        <tr>
        <td width="100%" align="left" valign="top" style=" font-family: Arial, Helvetica, sans-serif;  color:#444343; font-size:14px; font-weight:600; line-height: 27px; 
        
      
         ">
              
        
        Customer Name : <strong style="padding-left: 1rem; color:#e6be44;">' . $userdetails->name . ' </strong><br>
        
      E-mail: <strong style="padding-left: 1rem; color:#e6be44;">' . $userdetails->email . '</strong><br>
      
    Phone Number: <strong style="padding-left: 1rem; color:#e6be44;">' . $userdetails->mobile_code . $userdetails->mobile . '</strong><br>
    
    
 
        </td>
        
       
    
    
    
        
</tr>
    </tbody></table>
    </td>
    </tr>
    </tbody>
    </table>

    <table width="100%" border="0" align="center" cellpadding="0" cellspacing="0" style="margin-top:25px;  margin-bottom:7px;">
      <tbody><tr>
                <td style="  padding: 10px;
       background-color: #e4b62b; text-align: center;
        font-size: 17px;
        color: #000;
        font-weight: 600;">ORDER DETAILS</td>pr
      </tr>
    </tbody></table>';

    $html = $html . '  <table width="100%" border="0" align="center" cellpadding="8" cellspacing="0" style="border: 1px solid #a09e9e; font-family:Arial, Helvetica, sans-serif; font-size:15px;">
      <tbody>
     
      ';

    $original_value = !empty($offer->original_price) ? $offer->original_price : "";
    $discount_value = !empty($offer->discount_price) ? $offer->discount_price : "";
    $total_price = !empty($offer->total_price) ? $offer->total_price : "";
    $vat = !empty($offer->vat) ? $offer->vat : "";
    $grand_total = !empty($offer->grand_total) ? $offer->grand_total : "";
// <td width="225" style="  background: #fff; border-top: 1px solid #a09e9e; text-align: right;"> '.$garage_name .'<br/>'.$offer->title .'</td>

    $html = $html . '
      <tr>
        <td width="225" style="background: #fff;border-right: 1px solid #a09e9e; border-top: 1px solid #a09e9e;">Offer Name</td>
        <td width="225" style="  background: #fff; border-top: 1px solid #a09e9e; text-align: left;"> ' . $garage_name . '<br/>' . $offer->title . '</td>
      </tr>
     
      <tr>
        <td style="border-right: 1px solid #a09e9e;border-top: 1px solid #a09e9e;">Original Price</td>
        <td style="  border-top: 1px solid #a09e9e; text-align: right;"> ' . $original_value . ' AED</td>
      </tr>
       <tr>
        <td style="border-right: 1px solid #a09e9e;border-top: 1px solid #a09e9e;">After Discount Price</td>
        <td style="  border-top: 1px solid #a09e9e; text-align: right;"> ' . $total_price . ' AED</td>
      </tr>
       <tr>
        <td  style="background: #fff;border-right: 1px solid #a09e9e; border-top: 1px solid #a09e9e;">Vat(5%)</td>
        <td style="  background: #fff; border-top: 1px solid #a09e9e; text-align: right;"> ' . $vat . ' AED</td>
      </tr>
      <tr>
        <td  style="background: #fff; border-right: 1px solid #a09e9e;border-top: 1px solid #a09e9e;"><strong>Grand Total</strong></td>
        <td style=" background: #fff;  text-align: right;border-top: 1px solid #a09e9e;"><strong> ' . $grand_total . ' AED</strong></td>
      </tr>
      
    </tbody>
    </table>

  </body>
</html>';
//   $mpdf->defaultfooterline = 0;
//     $a = $mpdf->WriteHTML($html);
    $mpdfConfig = array(
        'mode' => 'utf-8',
        'format' => 'A4',
        'default_font_size' => 0,
        'default_font' => '',
        'margin_left' => 10,
        'margin_right' => 10,
        'margin_top' => '10',
        'margin_bottom' => '30',
        'margin_header' => 13,
        'margin_footer' => 2,
        'orientation' => 'P'
    );
    $mpdf = new \Mpdf\Mpdf($mpdfConfig);
    $mpdf->SetHTMLFooter(pdf_Footer());
    $mpdf->WriteHTML($html);
    $filename = HOME_DIR . "uploads/invoice/" . $request->request_no . "-" . $datet . ".pdf";
    $mpdf->Output($filename, 'f');
    $CondArray = array('id' => $requestId);
    $json['invoice'] = $request->request_no . "-" . $datet . ".pdf";
    $db->Database->update('offer_request_table', $json, $CondArray);
}

function searchMymarkup() {
    ob_start();
    $db = LoadDB();
    $type = !empty($_POST['type']) ? $_POST['type'] : 'NA';
    $from_country = !empty($_POST['from_country']) ? $_POST['from_country'] : '';
    $to_country = !empty($_POST['to_country']) ? $_POST['to_country'] : '';

    $fromc = GetcountryById($from_country);
    $toc = GetcountryById($to_country);
    if ($from_country == '229' && $to_country == '229') {
        $from_city = !empty($_POST['from_city']) ? $_POST['from_city'] : '';
        $to_city = !empty($_POST['to_city']) ? $_POST['to_city'] : '';

        $frmcity = GetemiratesById($from_city);
        $tocity = GetemiratesById($to_city);
    }


    $companyAr = GetcouriercompanymasterAll();
    $weight = GetweightmasterByType($type);
    for ($i = 0; $i < count($companyAr); $i++) {
        $d = $companyAr[$i];
        ?>
        <tr>
            <th courierId="<?= $d->courier_id ?>" colspan="7"><?= $d->courier_name ?> 
                <?php
                if (!empty($frmcity) && !empty($tocity)) {
                    ?><span>(<?= $frmcity->emirates_name . ' To ' . $tocity->emirates_name ?>)</span>
                    <?php
                } else {
                    ?><span>(<?= $fromc->code.' - '.$fromc->capital . ' To ' . $toc->code .' - '.$toc->capital ?>)</span>
                <?php } ?>
            </th>
        </tr>
        <?php
        for ($j = 0; $j < count($weight); $j++) {
            $rd = $weight[$j];
            $uniqid = uniqid();
            $cond = "";
            if (!empty($from_city) && !empty($to_city)) {
                $cond = $cond . " AND city_from='$from_city' AND city_to='$to_city'";
            }
            $Sql = "SELECT * FROM `price_list` WHERE type='$type' AND courier_id='$d->courier_id' AND country_from='$fromc->code' AND country_to='$toc->code' AND weight_from='$rd->weight_from' AND weight_to='$rd->weight_to' $cond";

            $priceAr = $db->Database->select_qry_array($Sql);
            $priceAr = !empty($priceAr) ? $priceAr[0] : '';

            $api_price = '100';
            $markup = !empty($priceAr->markup) ? $priceAr->markup : '0';
            $markupInper = ($api_price * $markup / 100);
            $final_price = ($api_price * 5 / 100) + $api_price + $markupInper;
            ?>
            <tr class="rowlist" id="rowId-<?= $uniqid ?>" uniqid="<?= $uniqid ?>"  courier_id="<?= $d->courier_id ?>" country_from="<?= $fromc->code ?>" country_to="<?= $toc->code ?>">
                <td weight_from="<?= $rd->weight_from ?>" id="weight_from-<?= $uniqid ?>"><?= !empty($rd->weight_from) ? $rd->weight_from . ' Kg' : '' ?></td>
                <td weight_to="<?= $rd->weight_to ?>" id="weight_to-<?= $uniqid ?>"><?= !empty($rd->weight_to) ? $rd->weight_to . ' Kg' : '' ?></td>
                <td api_price="<?= $api_price ?>" id="api_price-<?= $uniqid ?>"><?= DecimalAmount($api_price) ?></td>
                <td><input id="markup-<?= $uniqid ?>" uniqid="<?= $uniqid ?>" onkeyup="markupfn.markupAutocalc(this)" value="<?= !empty($priceAr->markup) ? $priceAr->markup : '0' ?>" type="number"></td>
                <td from_city="<?= $from_city ?>" to_city="<?= $to_city ?>" price_id="<?= !empty($priceAr->price_id) ? $priceAr->price_id : '' ?>" id="vat-<?= $uniqid ?>">5%</td>
                <td><input id="final_price-<?= $uniqid ?>" value="<?= DecimalAmount($final_price) ?>" disabled type="text"></td>
            </tr>
            <?php
        }
    }
    $html = ob_get_clean();
    die(json_encode(array('status' => true, 'message' => 'success', 'HTML' => $html)));
}

function onSavemarkup() {
    $rowlist = !empty($_POST['rowlist']) && is_array($_POST['rowlist']) ? $_POST['rowlist'] : [];
    $db = LoadDB();
    for ($i = 0; $i < count($rowlist); $i++) {
        $d = $rowlist[$i];
        $d['lastupdate'] = date('Y-m-d H:i:s');
        if (empty($d['price_id'])) {
            $d['timestamp'] = $d['lastupdate'];
            $db->Database->insert('price_list', $d);
        } else {
            $db->Database->update('price_list', $d, ['price_id' => $d['price_id']]);
        }
    }
    die(json_encode(array('status' => true, 'message' => 'Updated successfully', 'HTML' => false)));
}
?>