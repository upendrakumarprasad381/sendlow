<?php

function IsLogedAdmin() {
    $Ci = & get_instance();
    $AdminSession = $Ci->session->userdata('Admin');
    if (empty($AdminSession)) {
        redirect(base_url('Login/Admin'));
    }
}

function GetSessionArrayLogin() {
    $ci = & get_instance();
    $SessionArray = $ci->session->userdata('UserLogin');
    if (!empty($SessionArray->id)) {
        $SessionArray = GetUserArrayBy($SessionArray->id);
    }
    return $SessionArray;
}

function GetSessionArrayAdmin() {
    $ci = & get_instance();
    $SessionArray = $ci->session->userdata('Admin');
    if (!empty($SessionArray)) {
        $SessionArray = GetlogindetailsBy($SessionArray->id);
    }
    return $SessionArray;
}

function GetSessionArrayUser() {
    $Ci = & get_instance();
    $UserSession = $Ci->session->userdata('Users');
    if (!empty($UserSession)) {
        $UserSession = GetUserArrayBy($UserSession->id);
        return $UserSession;
    }
}

function GetUserArrayBy($Id = '') {
    $db = LoadDB();
    $qry = "SELECT * FROM  register  WHERE id = '$Id'";
    $array_list = $db->Database->select_qry_array($qry);
    if (!empty($array_list)) {
        $array_list = $array_list[0];
        // $ImageFullPath = 'uploads/Register/' . $array_list->image;
        // if (!is_file(HOME_DIR . $ImageFullPath)) {
        $ImageFullPath = 'assets/layouts/layout/img/avatar3_small.jpg';
        // }
        $array_list->LogoFullURL = PROJECT_PATH . $ImageFullPath;
        return $array_list;
    }
}

function Getcourierserviceconfiguration($courierId, $parcelType) {
    $returnAr = [];
    if ($courierId == '1') {
        if ($parcelType == 'DOCUMENTS') {
            //Priority Document Express ;
            $returnAr['parcel_type'] = 'PDX';
        } elseif ($parcelType == 'PACKAGE') {
            // Priority Parcel Express 
            $returnAr['parcel_type'] = 'PPX';
        }
    } else if ($courierId == '2') {
        if ($parcelType == 'DOCUMENTS') {
            $returnAr['parcel_type'] = 'false';
        } elseif ($parcelType == 'PACKAGE') {
            $returnAr['parcel_type'] = 'true';
        }
    }
    return $returnAr;
}

?>
