<?php

    function send_activation_mail($Insert){
       
        $email = base64_encode($Insert['email']);
        $activation_code = base64_encode($Insert['activation_code']);
        $newHtml = '';
        $newHtml = $newHtml . '<p>Dear ' . ucfirst($Insert['first_name']) . ',</p>';
        $newHtml = $newHtml . '<p>Click on the below link to activate your account.</p>';
        $newHtml = $newHtml . '<p><a href="'.base_url().'activateUserAccount?a='.$email.'_'.$activation_code.'.">Activate Your Account</a></p>';
       
        $newHtml = emailTemplate($newHtml);
        
        $html464 = "";
        $html464 = $html464 . "<p>Dear Team,</p>";
        $html464 = $html464 . "<p>New user registered.</p>";
        $html464 = $html464 . "<p>Name: " . (!empty($Insert['first_name']) ? $Insert['first_name'] : '') . "</p>";
        $html464 = $html464 . '<p>Email: ' . (!empty($Insert['email']) ? $Insert['email'] : '') . '</p>';
        $html464 = emailTemplate($html464);
    
    
        $subject = 'Activation';
        send_mail($Insert['email'], $subject, $newHtml, 'anusha@alwafaagroup.com');
        send_mail(ADMIN_EMAIL_ID, $subject, $html464, 'anusha@alwafaagroup.com');
    }
    
    function send_email_for_registration($array,$code) {
        $password = $array->encr_password;
        $decr_password = base64_decode($password);
        $newHtml = '';
        $newHtml = $newHtml . '<p>Dear ' . ucfirst($array->first_name . "" . $array->last_name) . ',</p>';
        $newHtml = $newHtml . '<p>Your registration is successful.</p>';
        $newHtml = $newHtml . '<p>Name: ' . (!empty($array->first_name) ? $array->first_name : '') . '</p>';
        $newHtml = $newHtml . '<p>Email: ' . (!empty($array->email) ? $array->email : '') . '</p>';
        $newHtml = $newHtml . '<p>Phone: ' . (!empty($array->mobile) ? $array->mobile : '') . '</p>';
        $newHtml = $newHtml . '<p>Password: ' . $decr_password . '</p>';
        $newHtml = $newHtml . '<p>Your coupon code is ' . $code->discount . '. Use it to get discount.</p>';
        // $newHtml = $newHtml . '<p><a type="button" href="' . base_url('login_signup') . '" style="text-decoration:none;">Click Here To Login</a></p>';
        $newHtml = emailTemplate($newHtml);
    
        $html464 = "";
        $html464 = $html464 . "<p>Dear Team,</p>";
        $html464 = $html464 . "<p>New user registered.</p>";
        $html464 = $html464 . "<p>Name: " . (!empty($array->name) ? $array->name : '') . "</p>";
        $html464 = $html464 . '<p>Email: ' . (!empty($array->email) ? $array->email : '') . '</p>';
        $html464 = emailTemplate($html464);
    
    
        $subject = 'Registration';
        send_mail($array->email, $subject, $newHtml, 'anusha@alwafaagroup.com');
        // send_mail(ADMIN_EMAIL_ID, $subject, $html464, 'anusha@alwafaagroup.com');
    }
    
    function send_email_for_forgot_password($id = '', $email = '') {
        $name = explode("@", $email);
    
        $newHtml = '';
        $newHtml = $newHtml . '<p>Dear ' . ucfirst($name[0]) . ', </p>';
        $newHtml = $newHtml . '<p><a href="' . base_url() . 'resetpassword/' . base64_encode($id) . '">Click here to reset your password </a></p>';
    
        $newHtml = emailTemplate($newHtml);
    
        $subject = 'Reset Password';
        send_mail($email, $subject, $newHtml);
    }
    
    function send_email_to_customer_after_addToWallet($user_id = '',$amount = '') {
        $userArray = GetUserArrayBy($user_id);
        $userWallet = getUserWallet($user_id);
        $newHtml = '';
        $newHtml = $newHtml . '<p>Dear ' . ucfirst($userArray->first_name) . ',</p>';
        $newHtml = $newHtml . '<p>Amount AED '.$amount.' is added to your wallet .</p>';
        $newHtml = $newHtml . '<p>Your balance is AED ' . $userWallet[0]->account_balance .' </p>';
        $newHtml = emailTemplate($newHtml);
    
    
        $subject = 'Wallet Recharged';
        send_mail($userArray->email, $subject, $newHtml);
    }

    function emailTemplate($message = '') {
    
        $html = '<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
        <html xmlns="http://www.w3.org/1999/xhtml">
        <head>
        <title></title>
        </head> 
        <body>
        <table width="600" border="0" align="center" cellpadding="0" cellspacing="0" style="border-right:solid 1px #ccc;border-left:solid 1px #ccc;border-top:solid 1px #2d2c7f;border-bottom:solid 1px #2d2c7f;font-size:15px;">
        <tbody><tr>
        <td style="border-top:solid 5px #2d2c7f"><table width="94%" border="0" align="center" cellpadding="0" cellspacing="0" style="font-family:Arial,Helvetica,sans-serif;font-size:15px;color:#333;line-height:18px">
        <tbody><tr>
        <td  align="left" valign="top" style="padding:15px 0px;">
            <img src="' . base_url('css/images/logo.png') . '" alt="" style="width:150px;padding-top:10px" class="CToWUd">
        </td>
        </tr>
        <tr>
        <td style="border-bottom: solid 3px #eee;"><strong><br/>
        </strong></td>
        </tr>
        <tr><td>&nbsp;</td></tr>
        <tr><td>
      ' . $message . '
         </td></tr>
       
        <tr>
        <td>&nbsp;</td>
        </tr>
        <tr><td ><strong>Sendlow Team</strong></td></tr>
        <tr>
        <td>&nbsp;</td>
        </tr>
        </tbody></table>
        </td>
        </tr>       
        <tr>
        <td style="padding:5px 0;text-align:center;background: #2d2c7f;color: #000;font-size:11px;font-family:Arial,Helvetica,sans-serif;">Copyright &copy; ' . date("Y", time()) . ' Sendlow . All rights reserved.</td>
        </tr>
        </tbody></table></body>
        </html>';
        return $html;
    }

?>