<?php

class Aramex {

    private $courier_id;

    public function __construct() {
        $this->courier_id = '1';
    }

    public function calculaterate() {

        $mySearch = getCommonSearchFormateAPI();
        $config = Getcourierserviceconfiguration($this->courier_id, $mySearch['parcel_type']);

        $curl = curl_init();
        $jsonBody = '{

    "OriginAddress": {

        "Line1": null,

        "Line2": null,

        "Line3": null,

       "City": "' . $mySearch['origin_city_name'] . '",

        "StateOrProvinceCode": null,

        "PostCode": "' . $mySearch['origin_postal_code'] . '", 

        "CountryCode": "' . $mySearch['origin_country_code'] . '",

        "Longitude": 0,

        "Latitude": 0,

        "BuildingNumber": null,

        "BuildingName": null,

        "Floor": null,

        "Apartment": null,

        "POBox": null,

        "Description": null

    },

    "DestinationAddress": {

        "Line1": null,

        "Line2": null,

        "Line3": null,

        "City": "' . $mySearch['destination_city_name'] . '",

        "StateOrProvinceCode": null,

        "PostCode": "' . $mySearch['destination_postal_code'] . '",

        "CountryCode": "' . $mySearch['destination_country_code'] . '",

        "Longitude": 0,

        "Latitude": 0,

        "BuildingNumber": null,

        "BuildingName": null,

        "Floor": null,

        "Apartment": null,

        "POBox": null,

        "Description": null

    },

    "ShipmentDetails": {

        "Dimensions": null,

        "ActualWeight": {

            "Unit": "KG",

            "Value": ' . $mySearch['weight'] . '

        },

        "ChargeableWeight": {

            "Unit": "KG",

            "Value": ' . $mySearch['weight'] . '

        },

        "DescriptionOfGoods": null,

        "GoodsOriginCountry": null,

        "NumberOfPieces": 1,

        "ProductGroup": "EXP",

        "ProductType": "' . $config['parcel_type'] . '",

        "PaymentType": "P",

        "PaymentOptions": null,

        "CustomsValueAmount": null,

        "CashOnDeliveryAmount": null,

        "InsuranceAmount": null,

        "CashAdditionalAmount": null,

        "CashAdditionalAmountDescription": null,

        "CollectAmount": null,

        "Services": "",

        "Items": null,

        "DeliveryInstructions": null,

        "AdditionalProperties": null,

        "ContainsDangerousGoods": false

    },

    "PreferredCurrencyCode": "AED",

    "ClientInfo": {

        "UserName": "The-Pulsar@outlook.com",

        "Password": "Alwafaa@123",

        "Version": "v1.0",

        "AccountNumber": "60525521",

        "AccountPin": "321321",

        "AccountEntity": "DXB",

        "AccountCountryCode": "AE",

        "Source": 0,

        "PreferredLanguageCode": null

    },

    "Transaction": null

}';

        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://ws.aramex.net/ShippingAPI.V2/RateCalculator/Service_1_0.svc/json/CalculateRate",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $jsonBody,
            CURLOPT_HTTPHEADER => array(
                "accept: application/json",
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 57a4c1cf-a8c3-2f85-fe96-1838664b761c"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
            return 0;
            //  echo "cURL Error #:" . $err;
        } else {
            $response = json_decode($response, true);

            $TotalAmount = !empty($response['TotalAmount']['Value']) ? $response['TotalAmount']['Value'] : '0';
            return $TotalAmount;
        }
    }

    public function createShipment($bookingId) {
        $booking = GetbookingById($bookingId);
        if (empty($booking)) {
            return false;
        }
        echo '<pre>';
        print_r($booking);
        echo $bookingId . 'AAA';
    }

    public function citylist() {
        $country = getcountryForImport();

        for ($i = 0; $i < count($country); $i++) {
            $d = $country[$i];
            $soapClient = new SoapClient('https://ws.aramex.net/ShippingAPI.V2/Location/Service_1_0.svc?singleWsdl');
            $params = array(
                'ClientInfo' => array(
                    'AccountCountryCode' => 'AE',
                    'AccountEntity' => 'DXB',
                    'AccountNumber' => '60525521',
                    'AccountPin' => '321321',
                    'UserName' => 'The-Pulsar@outlook.com',
                    'Password' => 'Alwafaa@123',
                    'Version' => 'v1.0',
                    'Source' => 0
                ),
                'Transaction' => array(
                ),
                'CountryCode' => $d->code,
                'State' => NULL,
                'NameStartsWith' => ''
            );
            try {
                $auth_call = $soapClient->FetchCities($params);
                $Cities = !empty($auth_call->Cities->string) ? $auth_call->Cities->string : [];
                $Cities = !empty($Cities) && is_array($Cities) ? $Cities : [];
                for ($j = 0; $j < count($Cities); $j++) {
                    $inset = array(
                        'country_id' => $d->id,
                        'city' => $Cities[$j],
                        'lastupdate' => date('Y-m-d H:i:s'),
                        'timestamp' => date('Y-m-d H:i:s'),
                    );
                    insertTemp($inset);
                }
                echo '<pre>';
                print_r($Cities);
                echo '<pre>';
            } catch (SoapFault $fault) {
                echo 'Error : ' . $d->code . '-----' . $fault->faultstring;
            }

            echo '----------------------------------------------------<br>';
        }
    }

}
