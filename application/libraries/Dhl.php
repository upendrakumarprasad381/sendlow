<?php

class Dhl {

    private $courier_id;

    public function __construct() {
        $this->courier_id = '2';
    }

    public function calculaterate() {

        $mySearch = getCommonSearchFormateAPI();
        $config = Getcourierserviceconfiguration($this->courier_id, $mySearch['parcel_type']);
        
        $urlSeg = "https://express.api.dhl.com/mydhlapi/test/rates?accountNumber=969277615&";
        $urlSeg = $urlSeg . "originCountryCode=" . $mySearch['origin_country_code'] . "&originPostalCode=" . $mySearch['origin_postal_code'] . "&originCityName=" . urlencode($mySearch['origin_city_name']) . "&destinationCountryCode=" . $mySearch['destination_country_code'] . "&destinationPostalCode=" . $mySearch['destination_postal_code'] . "&destinationCityName=" . urlencode($mySearch['destination_city_name']) . "&weight=" . $mySearch['weight'] . "&length=15&width=10&height=5&plannedShippingDate=" . $mySearch['shipDate'] . "&isCustomsDeclarable=" . $config['parcel_type'] . "&unitOfMeasurement=metric&nextBusinessDay=false";

        $curl = curl_init();
        curl_setopt_array($curl, array(
            CURLOPT_URL => $urlSeg,
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "GET",
            CURLOPT_HTTPHEADER => array(
                "authorization: Basic YXBYMHhGOGhaNXpTNGQ6QyE2cEojMGtOQDhz",
                "cache-control: no-cache",
                "postman-token: f3e63d35-eeb2-c244-60a6-1e2c3efe43e5"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);

        curl_close($curl);

        if ($err) {
//            echo "cURL Error #:" . $err;

            return 0;
        } else {


            $response = json_decode($response, true);

            $price = !empty($response['products'][0]['totalPrice'][0]['price']) ? $response['products'][0]['totalPrice'][0]['price'] : '0';
            return $price;
        }
    }

}
