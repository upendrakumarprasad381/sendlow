<?php

class Fedex {

    public function __construct() {
        
    }

    public function calculaterate() {
        $mySearch = getCommonSearchFormateAPI();
        $dateFater = date('Y-m-d', strtotime('+20 days'));
        $curl = curl_init();
        $params = '{
  "accountNumber": {
    "value": "740561073"
  },
  "requestedShipment": {
    "shipper": {
      "address": {
        "postalCode": '.$mySearch['origin_postal_code'].',
        "countryCode": "'.$mySearch['origin_country_code'].'"
      }
    },
    "recipient": {
      "address": {
        "postalCode": '.$mySearch['destination_postal_code'].',
        "countryCode": "'.$mySearch['destination_country_code'].'"
      }
    },
    "shipDateStamp": "'.$mySearch['shipDate'].'",
    "pickupType": "DROPOFF_AT_FEDEX_LOCATION",
    "serviceType": "INTERNATIONAL_PRIORITY",
    "shipmentSpecialServices": {
      "specialServiceTypes": [
        "RETURN_SHIPMENT"
      ],
      "returnShipmentDetail": {
        "returnType": "PRINT_RETURN_LABEL"
      }
    },
    "rateRequestType": [
      "LIST",
      "ACCOUNT"
    ],
    "customsClearanceDetail": {
      "dutiesPayment": {
        "paymentType": "SENDER",
        "payor": {}
      },
      "commodities": [
        {
          "description": "Camera",
          "quantity": 1,
          "quantityUnits": "PCS",
          "weight": {
            "units": "KG",
            "value": '.$mySearch['weight'].'
          },
          "customsValue": {
            "amount": 0,
            "currency": "USD"
          }
        }
      ]
    },
    "requestedPackageLineItems": [
      {
        "weight": {
          "units": "KG",
          "value": '.$mySearch['weight'].'
        }
      }
    ]
  }
}';
        curl_setopt_array($curl, array(
            CURLOPT_URL => "https://apis-sandbox.fedex.com/rate/v1/rates/quotes",
            CURLOPT_RETURNTRANSFER => true,
            CURLOPT_ENCODING => "",
            CURLOPT_MAXREDIRS => 10,
            CURLOPT_TIMEOUT => 30,
            CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
            CURLOPT_CUSTOMREQUEST => "POST",
            CURLOPT_POSTFIELDS => $params,
            CURLOPT_HTTPHEADER => array(
                "authorization: Bearer " . $this->token(),
                "cache-control: no-cache",
                "content-type: application/json",
                "postman-token: 26ece168-66d4-6360-9470-aa9cffd34bf2",
                "x-locale: en_US"
            ),
        ));

        $response = curl_exec($curl);
        $err = curl_error($curl);
        curl_close($curl);
        //echo "cURL Error #:" . $err;
        $response = json_decode($response, true);
        $price = !empty($response['output']['rateReplyDetails'][0]['ratedShipmentDetails'][0]['totalNetFedExCharge']) ? $response['output']['rateReplyDetails'][0]['ratedShipmentDetails'][0]['totalNetFedExCharge'] : '0';
        return ($price);
    }

    public function token() {
        $data = array("grant_type" => 'client_credentials', "client_id" => "l72465cfab683d4990a9238a00556d5773", 'client_secret' => '5c6fc48755034b87848829970eb27f98');
        $curl = curl_init("https://apis-sandbox.fedex.com/oauth/token");
        curl_setopt($curl, CURLOPT_POST, true);
        curl_setopt($curl, CURLOPT_POSTFIELDS, http_build_query($data));
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($curl, CURLOPT_HTTPHEADER, array('Content-Type: application/x-www-form-urlencoded'));
        $response = curl_exec($curl);
        curl_close($curl);
        $response = json_decode($response, true);
        $access_token = !empty($response['access_token']) ? $response['access_token'] : '';
        return $access_token;
    }

}
