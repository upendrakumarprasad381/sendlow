<?php

class Pdf {

    public function __construct() {
        $includePath = HOME_DIR . 'application/third_party/mpdf/';
        require_once $includePath . '/vendor/autoload.php';
    }

}
