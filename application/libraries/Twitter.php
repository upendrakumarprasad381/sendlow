<?php

use Abraham\TwitterOAuth\TwitterOAuth;

class Twitter {

    private $CONSUMER_KEY = '1t4ssqmwHEfH9EcpSgD2jk2AE';
    private $CONSUMER_SECRET = 'rxJAfSfT2FmqaIdhpgMGEbnCRrq8KyYcHPAc7ibMnNwaaC35Ym';
    private $OAUTH_CALLBACK = PROJECT_PATH . 'Login/TwitterDetails';
    private $Connection;

    public function __construct() {
        $dir = HOME_DIR . 'application/third_party/Twitter/';
        require_once $dir . 'vendor/autoload.php';
        $this->Connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET);
    }

    public function Login() {
        $ci = & get_instance();
        $request_token = $this->Connection->oauth('oauth/request_token', array('oauth_callback' => $this->OAUTH_CALLBACK));
        $LoginOuthTocken = array(
            'oauth_token' => $request_token['oauth_token'],
            'oauth_token_secret' => $request_token['oauth_token']
        );
        $ci->session->set_userdata('TwitterOuthTocken', $LoginOuthTocken);
        $url = $this->Connection->url('oauth/authorize', array('oauth_token' => $request_token['oauth_token']));
        header('Location: ' . filter_var($url, FILTER_SANITIZE_URL));
    }

    public function UserInfo($oauth_token, $oauth_verifier) {
        $ci = & get_instance();
        $TwitterOuthTocken = $ci->session->userdata('TwitterOuthTocken');
        $connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET, $TwitterOuthTocken['oauth_token'], $TwitterOuthTocken['oauth_token_secret']);
        $token_credentials = $connection->oauth("oauth/access_token", ["oauth_verifier" => $oauth_verifier]);
        $connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET, $token_credentials['oauth_token'], $token_credentials['oauth_token_secret']);
        $params = array('include_email' => 'true');
        $account = $connection->get('account/verify_credentials', $params);
        if ($account) {
            $Return = array(
                'Live' => true,
                'SocialType' => 3,
                'AccountId' => $account->id,
                'AccountName' => $account->name,
                'UserProfilePic' => $account->profile_image_url_https,
                'Email' => $account->email,
                'Gender' => '',
                'Birthday' => '',
            );
            return $Return;
        }
    }

    public function ProductPost() {
        $ci = & get_instance();
        $this->OAUTH_CALLBACK = base_url('Share/Twitter');
        $TwitterOuthTocken = $ci->session->userdata('TwitterOuthTocken');
        if (isset($_GET['oauth_verifier']) && $_GET['oauth_token']) {
            $ArraySocial = GetSessionArraySocialShare();
            $TwitterPost = $ArraySocial['Twitter'];
            $oauth_verifier = $_GET['oauth_verifier'];
            $TwitterOuthTocken = $ci->session->userdata('TwitterOuthTocken');
            $connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET, $TwitterOuthTocken['oauth_token'], $TwitterOuthTocken['oauth_token_secret']);
            $token_credentials = $connection->oauth("oauth/access_token", ["oauth_verifier" => $oauth_verifier]);
            $connection = new TwitterOAuth($this->CONSUMER_KEY, $this->CONSUMER_SECRET, $token_credentials['oauth_token'], $token_credentials['oauth_token_secret']);
            $media_id_array = [];
            for ($i = 0; $i < count($TwitterPost['Media']); $i++) {
                $d = $TwitterPost['Media'][$i];
                if (is_file($d['path'])) {
                    $TempStriing = $connection->upload('media/upload', ['media' => $d['path']]);
                    $media_id_array[] = $TempStriing->media_id_string;
                }
            }
            $parameters = [
                'status' => $TwitterPost['ProductSummery']->product_name,
                'media_ids' => implode(',', $media_id_array)
            ];
            $result = $connection->post('statuses/update', $parameters);
            if ($connection->getLastHttpCode() == 200) {
                $Base64Encode = base64_encode($TwitterPost['ProductSummery']->id) . '/' . base64_encode($TwitterPost['RedirectMediaId']);
                $RedirectUrl = base_url() . 'Product/' . $Base64Encode;
                header('Location: ' . filter_var($RedirectUrl, FILTER_SANITIZE_URL));
            } else {
                
            }
        } else {
            $this->Login();
        }
    }

}
