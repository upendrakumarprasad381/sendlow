<?php

class Wing {

    private $api_base_url = 'https://prodapi.safe-arrival.com/api/v1/';
    private $SetHeader = array('Content-Type: application/json', 'Accept: application/json');
    private $PostArray = array();
    private $url = 'customer/authenticate';
    private $RequestType = 'POST';

    public function __construct() {
        $ci = & get_instance();
        $val = $ci->input->request_headers();
        date_default_timezone_set('Asia/Dubai');
    }

    public function SendNewOrder() {
        $OrderId = isset($_POST['OrderId']) ? $_POST['OrderId'] : '';
        $OrderArray = GetOrderArray($OrderId);
        if (!empty($OrderArray) && empty($OrderArray->isshippedApi)) {
            $UserArray = GetUserArrayByUserId($OrderArray->user_id);
            $AddressArray = GetAddressArrayByAddressId($OrderArray->shipping_id);
            $PickFrom = array(
                'city' => 'Dubai',
                'pickup' => true,
                'address' => 'AL KABAYEL OASIS TRADING LLC, AL QUOZ MALL, AL QUOZ INDUSTRIAL AREA NO.3, NEAR AL QUOZ MEDICAL FITNESS CENTER, PO BOX 283943, DUBAI',
                'contact_name' => 'Firezapp',
                'email' => 'marketingteam@firezapp.com',
                'phone' => '8009277',
                'company_name' => 'Firezapp',
                'address_type' => 'Business',
            );
            $Drop = array(
                'city' => $AddressArray->city,
                'pickup' => false,
                'address' => $AddressArray->building . ' ' . $AddressArray->street . ' ' . $AddressArray->area,
                'contact_name' => $AddressArray->receiver_name,
                'email' => $UserArray->email,
                'phone' => $AddressArray->mobile_code . $AddressArray->mobile_no,
                'company_name' => '',
                'address_type' => $AddressArray->location_type == 1 ? 'Business' : 'Home',
            );

            if($OrderArray->payment_type == 0) {
                // for cod payments 
                $charge_type = 'cod'; 
                $charge = $OrderArray->total_price; // total price including the shipping charge 

                $charge_items = array(
                    array('charge_type' => $charge_type, 'charge' => $charge, 'payer' => 'recipient'), array('charge_type' => 'service_custom', 'charge' => 0, 'payer' => 'recipient')
                );                                  

            } else {
                // for credit card payments 
                $charge_type = 'cod';
                $charge = 0;  
                $charge_items = array(
                    array('charge_type' => $charge_type, 'charge' => 0, 'payer' => 'sender'), array('charge_type' => 'service_custom', 'charge' => 0, 'payer' => 'sender')
                );                               

            }

            $ShippingArray = array(
                'locations'         => array($PickFrom, $Drop),
                'package'           => array('courier_type' => 'next_day','menu' => array('id' => 3)),
                'payment_type'      => 'credit_balance',
                'payer'             => '',
                'recipient_not_available' => '',
                'charge_items'      => $charge_items,
                'note'              => $AddressArray->note,
                'force_create'      => true,
                'pickup_time_now'   => true,
                'fragile'           => true,
                'parcel_value'      => 0,
                'reference_id'      => 'string'
            );       
            
            $Result = $this->authentication();
            print_r($Result);exit;
            $AuthArray = json_decode($Result, true);
            if ($AuthArray['status'] == 'error') {
                die($Result);
            }
            $id_token = 'Authorization:' . 'Bearer ' . $AuthArray['data']['id_token'];
            array_push($this->SetHeader, $id_token);
            $this->PostArray = json_encode($ShippingArray);
            $this->RequestType = 'POST';
            $this->url = 'customer/order';
            $ReturnString = $this->CurlPost();
            $db = LoadDB();
            $res = json_decode($ReturnString, true);
            if ($res['status'] == 'success') {
                $CondArray = array('order_id' => $OrderArray->order_id);
                $Json22['isshippedApi'] = 1;
                $Json22['order_status'] = 4;
                $Json22['apiJson'] = $ReturnString;
                $db->Database->update('order_details', $Json22, $CondArray);
                SendOrderNotifaction($OrderArray->order_id);
                die($ReturnString);
            }
            die($ReturnString);
        } else {
            die(json_encode(array('status' => 'error', 'message' => 'Invalid input please contact to admin.')));
        }
    }

    public function GetOrderStatus($OrderId) {
        $Order = GetOrderArray($OrderId);
        if (!empty($Order)) {
            $apiJson = json_decode($Order->apiJson, true);
            $order_number = !empty($apiJson['data']['order_number']) ? $apiJson['data']['order_number'] : '';
            if (!empty($order_number)) {
                $AuthArray = $this->authentication();
                $AuthArray = json_decode($AuthArray, true);
                $this->SetHeader= array('Content-Type: application/json', 'Accept: application/json');
                $id_token = 'Authorization:' . ' Bearer ' . $AuthArray['data']['id_token'];
                array_push($this->SetHeader, $id_token);
                $this->RequestType = 'GET';
                $this->url = "customer/order/$order_number/history_items";
                $Result = $this->CurlPost();
                $Result = json_decode($Result, true);
                return $Result;
            }
        }
    }

    public function GetOrderInvoice($OrderId) {
        $Order = GetOrderArray($OrderId);

        $apiJson = json_decode($Order->apiJson, true);
        $WingOrderId = !empty($apiJson['data']['id']) ? $apiJson['data']['id'] : '';
        if (!empty($WingOrderId)) {
            $AuthArray = $this->authentication();
            $AuthArray = json_decode($AuthArray, true);
            $id_token = 'Authorization:' . ' Bearer ' . $AuthArray['data']['id_token'];
            array_push($this->SetHeader, $id_token);
            $this->RequestType = 'GET';
            $this->url = "customer/order/$WingOrderId/airwaybill";
            $Result = $this->CurlPost();
            $Result = json_decode($Result, true);
            if ($Result['status'] == 'success') {
                return $Result['data'];
            }
        }
    }

    public function GetOrderminifests($OrderId) {
        $Order = GetOrderArray($OrderId);

        $apiJson = json_decode($Order->apiJson, true);
        $WingOrderId = !empty($apiJson['data']['id']) ? $apiJson['data']['id'] : '';
        if (!empty($WingOrderId)) {

            $AuthArray = $this->authentication();
            $AuthArray = json_decode($AuthArray, true);
            $id_token = 'Authorization:' . ' Bearer ' . $AuthArray['data']['id_token'];
            array_push($this->SetHeader, $id_token);
            $this->RequestType = 'POST';
            $this->url = "customer/order/download_manifests";
            $PostArray=array(
                'compact'=>true,
                'customer'=>true,
                'ids'=>array(0),
                'order_numbers'=>array($apiJson['data']['order_number']),
                'summary'=>false,
            );
            $this->PostArray= json_encode($PostArray);
            $Result = $this->CurlPost();
            $Result = json_decode($Result, true);
            if ($Result['status'] == 'success') {
                return $Result['data'];
            }
        }
    }

    public function authentication() {
        $LoginArray = array(
            'username' => 'kabayloa@eim.ae',
            'password' => 'Gate@2015',
            'remember_me' => true,
        );
        $this->PostArray = json_encode($LoginArray);
        $this->RequestType = 'POST';
        $this->url = 'customer/authenticate';
        $ReturnString = $this->CurlPost();
        return $ReturnString;
    }

    public function CurlPost() {
        $FullURL = $this->api_base_url . $this->url;
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $FullURL);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_CUSTOMREQUEST, $this->RequestType);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $this->PostArray);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $this->SetHeader);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

}
