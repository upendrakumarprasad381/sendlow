<?php

// autoload_static.php @generated by Composer

namespace Composer\Autoload;

class ComposerStaticInit8bd5556251f0252a2612a620948afdb8
{
    public static $prefixLengthsPsr4 = array (
        'A' => 
        array (
            'Abraham\\TwitterOAuth\\' => 21,
        ),
    );

    public static $prefixDirsPsr4 = array (
        'Abraham\\TwitterOAuth\\' => 
        array (
            0 => __DIR__ . '/..' . '/abraham/twitteroauth/src',
        ),
    );

    public static function getInitializer(ClassLoader $loader)
    {
        return \Closure::bind(function () use ($loader) {
            $loader->prefixLengthsPsr4 = ComposerStaticInit8bd5556251f0252a2612a620948afdb8::$prefixLengthsPsr4;
            $loader->prefixDirsPsr4 = ComposerStaticInit8bd5556251f0252a2612a620948afdb8::$prefixDirsPsr4;

        }, null, ClassLoader::class);
    }
}
