<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">

                            <span class="caption-subject bold uppercase"><?= empty($Id) ? "Add New" : "Update"; ?> city</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <fff  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Country </label>
                                    <div class="col-md-4">
                                        <select class="form-control selectpicker" data-live-search="true" id="country_id" name="country_id" >
                                            <option value="">Choose Country</option>
                                            <?php
                                            $country = getcountry();
                                            foreach ($country as $con) {
                                                if ($city->country_id == $con->id) {
                                                    $select = 'selected = selected';
                                                } else {
                                                    $select = '';
                                                }
                                                ?>
                                                <option class="" <?= $select ?> value="<?= $con->id ?>"><?= $con->country ?></option>
                                                <?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">city</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($city->city) ? $city->city : '' ?>" id="Name" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Postal Code</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($city->post_code) ? $city->post_code : '' ?>" id="post_code" type="number" class="form-control" autocomplete="off">
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="Submitcity">
                                                <i class="fa fa-check"></i>Submit</button>
                                            <button type="button" onclick="window.location = '<?= base_url('Admin/city'); ?>';" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </fff>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
