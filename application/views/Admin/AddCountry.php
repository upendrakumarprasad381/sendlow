<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">

                            <span class="caption-subject bold uppercase"><?= empty($Id) ? "Add New" : "Update"; ?> country</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <fff  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Country</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($country->country) ? $country->country : '' ?>" id="Name" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Country(in Arabic)</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($country->country_ar) ? $country->country_ar : '' ?>" id="Name_ar" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Country Code</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($country->code) ? $country->code : '' ?>" id="code" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group" style="display: none;">
                                    <label class="control-label col-md-3">Flag</label>
                                    <div class="col-md-4">
                                        <input type="file" class="custom-file-input CommonImages"  src="" page="add_country">
                                    </div>
                                    <div class="col-md-3">
                                        <a target="_blank" href="<?php
                                        if (isset($country->flag) && $country->flag != "") {
                                            echo base_url() . 'uploads/Flags/' . $country->flag;
                                        }
                                        ?>">
                                            <img src=" <?php
                                            if (isset($country->flag) && $country->flag != "") {
                                                echo base_url() . 'uploads/Flags/' . $country->flag;
                                            }
                                            ?>" width="20%" id="ImagesEncode" OldImage='<?= !empty($country->flag) ? 'uploads/Flags/' . $country->flag : '' ?>'>
                                        </a>
                                        <?php if (isset($country->flag) && $country->flag != "") { ?>
                                            <br><a class="remove_image_common" path="uploads/Flags/" name="<?= isset($country->flag) ? $country->flag : '' ?>" table="country" id="<?= $Id ?>" type="" field="flag">Remove</a>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">From/To</label>
                                    <div class="col-md-4">
                                        <input class="form-check-input" <?= isset($country->country_from) ? ($country->country_from == '1' ? 'checked' : '') : 'checked' ?> type="checkbox" value="" id="from" >
                                        <label class="form-check-label" for="from">
                                            From
                                        </label>
                                        <input class="form-check-input" <?= isset($country->country_to) ? ($country->country_to == '1' ? 'checked' : '') : 'checked' ?> type="checkbox" value="" id="to" >
                                        <label class="form-check-label" for="to">
                                            To
                                        </label>
                                    </div>
                                </div>



                                <div class="form-group">
                                    <label class="control-label col-md-3">Courier</label>
                                    <div class="col-md-4">
                                        <?php
                                        $companyAr = GetcouriercompanymasterAll();
                                        $courierId = !empty($country->courier_id) ? json_decode($country->courier_id) : [];
                                        $courierId = !empty($courierId) && is_array($courierId) ? $courierId : [];
                                        
                                        for ($i = 0; $i < count($companyAr); $i++) {
                                            $d = $companyAr[$i];
                                            ?>
                                            <input class="form-check-input courier_id" <?= in_array($d->courier_id, $courierId) ? 'checked' : '' ?> type="checkbox" value="<?= $d->courier_id ?>" >
                                            <label class="form-check-label" >
                                                <?= $d->courier_name ?>
                                            </label>
                                        <?php } ?>
                                    </div>
                                </div>

                                <div class="form-group">
                                    <label class="control-label col-md-3">Status</label>
                                    <div class="col-md-4">
                                        <?php
                                        $archive = !empty($country->archive) ? $country->archive : '';
                                        ?>
                                        <select id="archive"  class="form-control" autocomplete="off">
                                            <option value="0">Active</option>
                                            <option <?= $archive == '1' ? 'selected' : '' ?> value="1">Inactive</option>
                                        </select>
                                    </div>
                                </div>

                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="Submitcountry">
                                                <i class="fa fa-check"></i>Submit</button>
                                            <button type="button" onclick="window.location = '<?= base_url('Admin/country'); ?>';" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </fff>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
    <style>
        .form-check-input{
            margin-left: 17px !important;
        }
    </style>