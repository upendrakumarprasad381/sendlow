<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                        
                            <span class="caption-subject bold uppercase"><?= empty($Id) ? "Add New":"Update"; ?> Couriers</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <fff  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Courier</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($courier->courier	) ? $courier->courier	 : '' ?>" id="Name" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <!--<div class="form-group">-->
                                <!--    <label class="control-label col-md-3">Country Code</label>-->
                                <!--    <div class="col-md-4">-->
                                <!--        <input value="<?= !empty($country->code	) ? $country->code	 : '' ?>" id="code" type="text" class="form-control" autocomplete="off">-->
                                <!--    </div>-->
                                <!--</div>-->
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="SubmitCourier">
                                                <i class="fa fa-check"></i>Submit</button>
                                            <button type="button" onclick="window.location = '<?= base_url('Admin/couriers'); ?>';" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </fff>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
