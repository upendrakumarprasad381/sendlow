<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                        
                            <span class="caption-subject bold uppercase"><?= empty($Id) ? "Add New":"Update"; ?> Discount</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <fff  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Discount </label>
                                    <div class="col-md-4">
                                        <select class="form-control selectpicker" data-live-search="true" id="type" name="type" >
            							    <option value="">Choose Users</option>
            							    <?php
            							    $ind = $com=$busi=$bro=$eco='';
            							    if($discount->register_type == 1)
            							    {
            							        $ind = "selected = selected";
            							    }else if($discount->register_type == 2){
            							        $com = "selected = selected";
            							    }else if($discount->register_type == 3){
            							        $busi = "selected = selected";
            							    }else if($discount->register_type == 4){
            							        $bro = "selected = selected";
            							    }else if($discount->register_type == 5){
            							        $eco = "selected = selected";
            							    }else{
            							        $ind=$com=$busi=$bro=$eco='';
            							    }
            							    
            							    
            							    ?>
                                            <option <?=$ind?> value="1">Individual</option>
                                            <option <?=$com?> value="2">Company</option>
                                            <option <?=$busi?> value="3">Small Business</option>
                                            <option <?=$bro?> value="4">Broker</option>
                                            <option <?=$eco?> value="5">Ecommerce</option>
        							</select>
        							</div>
        						</div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Discount Code</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($discount->discount) ? $discount->discount : '' ?>" id="discount" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Percentage</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($discount->amount) ? $discount->amount : '' ?>" id="amount" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="SubmitDiscount">
                                                <i class="fa fa-check"></i>Submit</button>
                                            <button type="button" onclick="window.location = '<?= base_url('Admin/autoDiscount'); ?>';" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </fff>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
