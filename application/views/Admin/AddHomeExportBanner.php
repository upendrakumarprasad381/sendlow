<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= $Id == '' ? 'Add New' :'Update'; ?> Banner </span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <fff  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Banner Image<br>
                                    <span class="img-size-common"></span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="file" class="custom-file-input CommonImages"  src="" page="home_export_banner">
                                    </div>
                                    <div class="col-md-3">
                                        <a target="_blank" href="<?php if(isset($bannerArray->image) && $bannerArray->image!=""){ echo base_url().'uploads/home_banner/'.$bannerArray->image;} ?>">
                                            <img src=" <?php if(isset($bannerArray->image) && $bannerArray->image!=""){ echo base_url().'uploads/home_banner/'.$bannerArray->image;} ?>" width="20%" id="ImagesEncode" OldImage='<?= !empty($bannerArray->image) ? 'uploads/home_banner/' . $bannerArray->image : '' ?>'>
                                        </a>
                                        <?php if(isset($bannerArray->image) && $bannerArray->image!=""){ ?>
                                            <br><a class="remove_image_common" path="uploads/home_banner/" name="<?=isset($bannerArray->image)?$bannerArray->image:''?>" table="export_banner" id="<?=$Id?>" type="" field="image">Remove</a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Url</label>
                                    <div class="col-md-4">
                                        <input id="url" value="<?= isset($bannerArray->url) ? $bannerArray->url : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Status</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="HomeExportStatus">
                                            <option <?= isset($bannerArray->status) ? $bannerArray->status == 0 ? 'selected="selected"' : '' : '' ?> value="0">Active</option>
                                            <option <?= isset($bannerArray->status) ? $bannerArray->status == 1 ? 'selected="selected"' : '' : '' ?> value="1">Inactive</option>
                                        </select>
                                    </div>
                                </div>                                

                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="SubmitHomeExportBanner" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location =' <?= base_url('Admin/export_banner'); ?>';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->