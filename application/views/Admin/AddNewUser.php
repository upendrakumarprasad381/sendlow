<?php 
$Session =  $this->session->userdata('Admin');
$ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;
?>
<style>
    
.modal-title {
    margin: 0;
    line-height: 1.42857143;
    font-size: 16px;
    text-transform: uppercase;
    padding: 3px 0;
}

   .pac-container, .pac-item{ z-index: 2147483647 !important; }

</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= $UserId == '' ? 'Add New' : 'Update'; ?> User</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="UserId" value="<?= $UserId ?>">
                            <input type="hidden" id="baseurl" value="<?= base_url(); ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">User Name</label>
                                    <div class="col-md-4">
                                        <input id="UserName" value="<?= isset($UserArray->name) ? $UserArray->name : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">User Email</label>
                                    <div class="col-md-4">
                                        <input id="UserEmail" value="<?= isset($UserArray->email) ? $UserArray->email : '' ?>" type="text" class="form-control">
                                    </div><span class='email_error' id='email_error' style='color:red'></span>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Phone Number</label>
                                    <div class="col-md-4">
                                        <input id="mobile" value="<?= isset($UserArray->mobile) ? $UserArray->mobile : '' ?>" type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">User Password</label>
                                    <div class="col-md-4">
                                        <input id="UserPassword"  type="password" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Status</label>
                                    <div class="col-md-4">
                                        <select class="form-control" id="UserStatus">
                                            <option <?= isset($UserArray->status) ? $UserArray->status == 0 ? 'selected="selected"' : '' : '' ?> value="0">Active</option>
                                            <option <?= isset($UserArray->status) ? $UserArray->status == 1 ? 'selected="selected"' : '' : '' ?> value="1">Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">User Type</label>
                                    <div class="col-md-4">


                                        <select class="form-control  UserType" id="UserType"  >
                                            <option value=''>Select User Type</option>
                                            <option <?= isset($UserArray->user_type) ? $UserArray->user_type == 0 ? 'selected="selected"' : '' : '' ?>  value="0"> Admin</option>
                                            <option  <?= isset($UserArray->user_type) ? $UserArray->user_type == 1 ? 'selected="selected"' : '' : '' ?> value="1">Sub Admin</option>
                                        </select>
                                       
                                    </div>
                                </div>
                                 
                                
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="SubmitNewUser" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '<?= base_url('Admin/UserManagement'); ?>';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>


<div id="party_venue_map" class="modal fade" role="dialog">
            <div class="modal-dialog modal-lg">
                <!-- Modal content-->
                <div class="modal-content">
                    <input type="hidden" value="" id="isEditing">
                    <div class="modal-header modal-alt-header">
                        <h5 class="modal-title" id="exampleModalLongTitle"> Location</h5>
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"> <span aria-hidden="true">×</span> </button>
                    </div>
                    <div class="modal-body">
                        <div class="srch-by-txt">
                            <input type="text"  class="form-control" id='store-location' value="" placeholder="Search Road or Landmark" autocomplete="off">
                        </div>
                        <div class="map-wrapper-inner" id="map-page" >
                            <div id="google-maps-box">
                                <div id="map" style="width:100%; height:300px;"></div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <div class="button-wrap">
                            <button type="button" class="btn btn-success confirm_location" data-dismiss="modal">CONFIRM</button>
                        </div>
                    </div>
                </div>
            </div>
         </div>


    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
