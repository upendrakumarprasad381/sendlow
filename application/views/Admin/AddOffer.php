<?php

$offerId = !empty($_REQUEST['offerId']) ? base64_decode($_REQUEST['offerId']) : '';
$offerArray = GetOffersByOfferId($offerId);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= empty($offerArray->id) ? 'Add New' : 'Update'; ?> Coupon</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="offerId" value="<?= !empty($offerArray->id) ? $offerArray->id : '' ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Paid Amount</label>
                                    <div class="col-md-4">
                                        <input id="paidAmount" value="<?= isset($offerArray->paid_amount) ? $offerArray->paid_amount : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Discount Amount</label>
                                    <div class="col-md-4">
                                        <input id="discountAmount" value="<?= !empty($offerArray->get_amount) ? $offerArray->get_amount : '' ?>" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Start Date</label>
                                    <div class="col-md-4">
                                        <input id="start_date" value="<?= isset($offerArray->start_date) ? date('d-m-Y',  strtotime($offerArray->start_date)) : '' ?>" type="text" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Expiry Date</label>
                                    <div class="col-md-4">
                                        <input id="expiry_date" value="<?= isset($offerArray->expiry_date) ? date('d-m-Y',  strtotime($offerArray->expiry_date)) : '' ?>" type="text" class="form-control datepicker" autocomplete="off">
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="add_offer" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location = '<?= base_url('Admin/offers') ?>';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
