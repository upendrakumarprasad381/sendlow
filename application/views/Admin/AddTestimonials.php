<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= $Id == '' ? 'Add New' :'Update'; ?> Testimonials </span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <fff  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">client Image<br>
                                    <span class="img-size-common"></span>
                                    </label>
                                    <div class="col-md-4">
                                        <input type="file" class="custom-file-input CommonImages" id="profile_image" src="" page="testimonials">
                                    </div>
                                    <div class="col-md-3">
                                        <a target="_blank" href="<?php if(isset($testimonialArray->image) && $testimonialArray->image!=""){ echo base_url().'uploads/testimonials/'.$testimonialArray->image;} ?>">
                                            <img src=" <?php if(isset($testimonialArray->image) && $testimonialArray->image!=""){ echo base_url().'uploads/testimonials/'.$testimonialArray->image;} ?>" width="20%" id="ImagesEncode" OldImage='<?= !empty($testimonialArray->image) ? 'uploads/testimonials/' . $testimonialArray->image : '' ?>'>
                                        </a>
                                        <?php if(isset($testimonialArray->image) && $testimonialArray->image!=""){ ?>
                                            <br><a class="remove_image_common" path="uploads/testimonials/" name="<?=isset($testimonialArray->image)?$testimonialArray->image:''?>" table="testimonials" id="<?=$Id?>" type="" field="image">Remove</a>
                                        <?php } ?>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name</label>
                                    <div class="col-md-4">
                                        <input id="name" value="<?= isset($testimonialArray->name) ? $testimonialArray->name : '' ?>" type="text" class="form-control" >
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Name(in Arabic)</label>
                                    <div class="col-md-4">
                                        <input id="name_ar" value="<?= isset($testimonialArray->name_ar) ? $testimonialArray->name_ar : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Comment</label>
                                    <div class="col-md-4">
                                        <textarea id="comment" type="text" class="form-control summernote" row="5"><?= isset($testimonialArray->comment) ? $testimonialArray->comment : '' ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Comment (in Arabic)</label>
                                    <div class="col-md-4">
                                        <textarea id="comment_ar" type="text" class="form-control summernote" row="5"><?= isset($testimonialArray->comment_ar) ? $testimonialArray->comment_ar : '' ?> </textarea>
                                    </div>
                                </div>
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="SubmitTestimonial" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                        <button type="button" onclick="window.location =' <?= base_url('Admin/testimonials'); ?>';" class="btn default">Cancel</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>

    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->