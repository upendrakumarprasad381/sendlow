<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                        
                            <span class="caption-subject bold uppercase"><?= empty($Id) ? "Add New":"Update"; ?> Ticket</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <fff  class="form-horizontal form-bordered">
                            <input type="hidden" id="Id" value="<?= $Id ?>">
                            <div class="form-body">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Ticket Number</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($ticket->ticket_no) ? $ticket->ticket_no : '' ?>" id="ticket_no" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Customer Name</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($ticket->customer_name) ? $ticket->customer_name : '' ?>" id="customer_name" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Customer Email</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($ticket->customer_email) ? $ticket->customer_email : '' ?>" id="customer_email" type="email" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Order Number</label>
                                    <div class="col-md-4">
                                        <input value="<?= !empty($ticket->order_no) ? $ticket->order_no : '' ?>" id="order_no" type="text" class="form-control" autocomplete="off">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Issue</label>
                                    <div class="col-md-4">
                                        <textarea id="issue" class="form-control" autocomplete="off" row="5"><?= !empty($ticket->Issue) ? $ticket->Issue : '' ?></textarea>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Status </label>
                                    <div class="col-md-4">
                                        <select class="form-control selectpicker" data-live-search="true" id="status" name="status" >
        							     <option value="">Select Status</option>
        							      <?php
        							      $qry = "SELECT * FROM status wHERE archive=0";
        							      $result = $this->Database->select_qry_array($qry);
                                            foreach($result as $status){
                                                if($status->id == $ticket->status_id)
                                                {
                                                    $select = 'selected = selected';
                                                }
                                                else{
                                                    $select = '';
                                                }
                                          ?>
                                          <option class="" <?=$select?> value="<?=$status->id?>"><?=$status->status?></option>
                                          <?php
                                            }
                                          ?>
        							</select>
        							</div>
        						</div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" id="SubmitTickets">
                                                <i class="fa fa-check"></i>Submit</button>
                                            <button type="button" onclick="window.location = '<?= base_url('Admin/ticketSettings'); ?>';" class="btn default">Cancel</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </fff>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
