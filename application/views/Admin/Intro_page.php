<?php

$cArray = Getintrodetails();
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Intro Page Contents</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered" method="post" enctype="multipart/form-data">
                            <div class="form-body">
                                 
                           
                                <?php  for($i=0;$i<count($cArray);$i++){ 
                                    $image=$cArray[$i];
                                    ?>
                                <div class="form-group">
                                    <label class="control-label col-md-3"></label>
                                    <div class="col-md-8">
                                    
                                     <div class="up_tm_box">
                                    <a target="_blank" href="<?php if(isset($image) && $image->file_name!=""){ echo base_url().'files/category_image/'.$image->file_name;} ?>">
                                        <img src=" <?php if(isset($image) && $image->file_name!=""){ echo base_url().'files/category_image/'.$image->file_name;} ?>" id="ImagesEncode_<?=$i?>" OldImage='<?= !empty($image) ?  $image->file_name : '' ?>' class="more_images_cls img-responsive" newimage=''>
                                    </a> 
                                   
                                 </div>   
                             
                                <?php if(isset($image) && $image->file_name!=""){ ?>
                                         <br/>
                                           <a updatejson='{"archive":"<?= empty($image->archive) ? '1' : '0' ?>"}'  title='Delete' condjson='{"id":"<?= $image->id ?>"}' dbtable="intro_images" class="autoupdate"><span class="label label-sm label-danger"><i class="fas fa-trash-alt"></i></span></a>
                                    <?php } ?>
                               </div>
                                </div>
                                  <!--<div class="form-group">-->
                                  <!--  <label class="control-label col-md-3">Title-->
                                    
                                  <!--  </label>-->
                                  <!--  <div class="col-md-6">-->
                                  <!--      <input type='text' name='content' class="form-control" value='<?= !empty($image->text)?$image->text:""?>'>-->
                                  <!--      </div>-->
                                        
                                    <!--</div>-->
                                 <?php } ?>
                                <div id="more_upload_section">
                                <div class="form-group">
                                    <label class="control-label col-md-3">Upload Category Images<br>Image dimension should be within 600-650*400-450
                                    
                                    </label>
                                    <div class="col-md-6">
                                     <?php $rand=rand();?>
                                     <input type="file" class="custom-file-input MultipleImages" id="" refval="<?=$rand?>" src="" page="intro_page">
                                    </div>
                                    <div class="col-md-3">
                                      
                                      <a target="_blank" href="">
                                        <img src="" width="20%" id="ImagesEncode_<?=$rand?>" OldImage='' class="more_images_cls" newimage=''>
                                    </a> 
                                    <button type="button" class="btn btn-info" id="add_menu_images1" inputclassname="more_images_cls1">
                                                     <span class="glyphicon glyphicon-plus"></span> Add More
                                      </button>
                                    </div>
                                 </div>
                                <!--<div class="form-group">-->
                                <!--    <label class="control-label col-md-3">Title-->
                                    
                                <!--    </label>-->
                                <!--    <div class="col-md-6">-->
                                <!--        <input type='text' name='content' class="form-control">-->
                                <!--        </div>-->
                                <!--    </div>-->
                               </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="button" class="btn blue" name="Submit_intropage" id='Submit_intropage'>
                                                <i class="fa fa-check"></i> Submit</button>
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
