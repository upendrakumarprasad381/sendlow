<html lang="en">
    <head>
        <title>Login</title>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/bootstrap/css/'); ?>bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/fonts/font-awesome-4.7.0/css/'); ?>font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/fonts/Linearicons-Free-v1.0.0/'); ?>icon-font.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/fonts/iconic/css/'); ?>material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/animate/'); ?>animate.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/css-hamburgers/'); ?>hamburgers.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/animsition/css/'); ?>animsition.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/select2/'); ?>select2.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/daterangepicker/'); ?>daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/css/'); ?>util.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/css/'); ?>main.css">
    </head>
    <body style="background-color: #999999;">

        <div class="limiter">
            <div class="container-login100">
                
                <div class="login100-more" style="background-image: url();"></div>

                <div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
                    <form class="login100-form validate-form">
                        <span class="login100-form-title p-b-59">
                           <img src="<?= base_url() ?>css/images/logo.png" alt="logo" class="logo-default" style="width: 170px" /> </a>
                        </span>
                        <div style="width: 100%;text-align: center;" id="MessageDiv">
                           
                        </div>
                        <br>
                        <div class="wrap-input100 validate-input" data-validate="Username is required">
                            <span class="label-input100">Username</span>
                            <input id="UserName" class="input100" type="text" name="username" placeholder="Username...">
                            <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate = "Password is required">
                            <span class="label-input100">Password</span>
                            <input class="input100" type="password" id="Password" name="pass" placeholder="*************">
                            <span class="focus-input100"></span>
                        </div>


                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button class="login100-form-btn" style='background-color:#FFD700;'>
                                    Login   
                                </button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
        <script>var base_url = '<?= base_url() ?>';</script>
        <script src="<?= base_url('assets/AdminLogin/vendor/jquery/') ?>jquery-3.2.1.min.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/animsition/js/') ?>animsition.min.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/bootstrap/js/') ?>popper.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/bootstrap/js/') ?>bootstrap.min.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/select2/') ?>select2.min.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/daterangepicker/') ?>moment.min.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/daterangepicker/') ?>daterangepicker.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/countdowntime/') ?>countdowntime.js"></script>
        <script src="<?= base_url('assets/AdminLogin/js/') ?>main.js"></script>
    </body>
</html>