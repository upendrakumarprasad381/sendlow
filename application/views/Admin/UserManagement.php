<?php 
$Session =  $this->session->userdata('Admin');
$ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;
$Permission = GetMenuPermission();
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">User Management</span>
                        <a href="<?= base_url('Admin/AddNewUser') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a> 
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>User Name</th>
                                    <th>Email</th>
                                    <th>User Type</th>
                                    <th>Phone Number</th>
                                    <th>Date</th>
                                    <th>Archive</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($UserArray); $i++) {
                                    $d = $UserArray[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->name ?></td>
                                        <td><?= $d->email ?></td>
                                        <td>
                                         <?php     $user_type=isset($d->user_type) ? $d->user_type : '';
                                                $user_type1=explode (",", $user_type); 
                                                
                                        ?>
                                        
                                        <?php  
                                        if(in_array("0",$user_type1)){ echo ' Admin';?> &nbsp;<?php }
                                       if(in_array("1",$user_type1)){ echo 'Subadmin';?> &nbsp;<?php }
                                        if(in_array("2",$user_type1)){ echo 'Vendor';?> &nbsp;<?php } ?>
                                        </td>
                                        <td><?= $d->mobile ?></td>
                                        <td><?= date('H:i-d/m/Y', strtotime($d->timestamp)); ?></td>
                                        <td><?= $d->status == 0 ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Inactive</span>' ?></td>
                                        <td style="width: 100px;">
                                       
                                        <a title="Edit" href="<?= base_url('Admin/AddNewUser/' . base64_encode($d->id)); ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>
                                       
                                       <?php  if (in_array($Session->id, $ADMIN_PERMISSION_ARRAY)) {
                                        ?>  <a href="<?= base_url('Admin/userpermission?userId=' . base64_encode($d->id)) ?>" class="m-portlet__nav-link btn m-btn m-btn--hover-success m-btn--icon m-btn--icon-only m-btn--pill" title="Set Permission"><i class="fas fa-lock"></i></a><?php
                                        }
                                        ?>
                                        <a UserId="<?= $d->id ?>" UserName="<?= $d->name ?>" baseurl="<?php echo base_url();?>" class="UserDelete"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>

                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
