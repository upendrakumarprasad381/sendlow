
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Request List</span>
                            
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontable">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Full Name</th>
                                    <th>Customer Type</th>
                                    <th>Recharge Amount</th>
                                    <th>Approve Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($request); $i++) {
                                    $d = $request[$i];
                                    if($d->type == 1){
                                        $type = "Individual";
                                    }else if($d->type == 2){
                                        $type = "Company";
                                    }else if($d->type == 3){
                                        $type = "Small Business";
                                    }else if($d->type == 4){
                                        $type = "Broker";
                                    }else{
                                        $type = "Ecommerce";
                                    }
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->first_name . " " . $d->last_name ?></td>
                                        <td><?= $type ?></td>
                                        <td>AED <?= $d->recharge_amount ?></td>
                                        <td>
                                            <?php echo ($d->is_approved == 1) ? 'Approved' : 'Rejected'; ?>
                                        </td>  
                                        <td><a href="<?= base_url('Admin/request_info/' . base64_encode($d->id)) ?>" Id="<?= $d->id ?>"><span class="label label-sm label-success"><i class="fa fa-eye" aria-hidden="true" title="View" ></i></span></a></td>
                                        <!--<td>-->
                                        <!--    <a redrurl="vendor" class="commonApprove" table="users" Id="<?= $d->id ?>"><span class="label label-sm label-success"><i class="fa fa-check" aria-hidden="true" title="Approve" ></i></span></a> -->
                                        <!--    <a redrurl="vendor" class="commonReject" table="users" Id="<?= $d->id ?>"><span class="label label-sm label-danger"><i class="fas fa-ban" title="Reject"></i></span></a>-->
                                        <!--</td>-->
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
