
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Booking Management</span>

                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover XXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>TRACKING ID</th>
                                    <th>COURIER</th>
                                    <th>STATUS</th>
                                    <th>RECIPIENT NAME </th>
                                  
                                    <th>SENDER NAME </th>
                                 
                                    <th>WEIGHT</th>
                                    <th>AMOUNT</th>
                                    <th>ACTION</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $join = " LEFT JOIN sender_address SEN ON SEN.id=B.sender_id LEFT JOIN sender_address REC ON REC.id=B.receiver_id LEFT JOIN courier_company_master CCM ON CCM.courier_id=B.courier_id";
                                $select = ",SEN.full_name AS sen_name,REC.full_name AS rec_name,CCM.logo";
                                $Sql = "SELECT B.* $select FROM `booking` B $join WHERE  B.archive=0 ORDER BY B.booking_id DESC LIMIT 3000";
                                $dArray = $this->Database->select_qry_array($Sql);
                                for ($i = 0; $i < count($dArray); $i++) {
                                    $d = $dArray[$i];
                                    ?>
                                    <tr>
                                        <td class="pl-0"><?= $i + 1 ?></td>
                                        <td class="pl-0"><?= $d->booking_no ?></td>
                                        <td class="pl-0"><img style='width: 100%;' src="<?= base_url("files/images/$d->logo") ?>"></td>
                                        <td>In-process</td>
                                        <td><?= $d->sen_name ?></td>
                                      
                                        <td><?= $d->rec_name ?></td>
                                      
                                        <td><?= $d->weight ?> Kg <?= $d->booking_type ?></td>
                                        <td><?= DecimalAmount($d->total) ?></td>
                                        <td>
                                            <a href="<?= base_url("admin/bookingdetails?bookingId=". base64_encode($d->booking_id)) ?>"><span class="label label-sm label-success"><i class="fas fa-external-link-alt"></i></span></a>
                                            <a ref="javascript:void(0)" updatejson='{"archive":"1"}'  condjson='{"booking_id":"<?= $d->booking_id ?>"}' dbtable="booking" class="autoupdate" ><span class="label label-sm label-danger"><i class="fas fa-trash-alt"></i></span></a>


                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
