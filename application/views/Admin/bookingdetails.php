<?php
$bookingId = !empty($_REQUEST['bookingId']) ? base64_decode($_REQUEST['bookingId']) : '';
$cArray = GetbookingById($bookingId);
if (empty($cArray)) {
    die('Invalid Booking no');
}
$courier = Getcouriercompanymaster($cArray->courier_id);
$user = GetUserArrayBy($cArray->user_id);
$sender = GetSenderAddressByAddressId($cArray->sender_id);
$receiver = GetSenderAddressByAddressId($cArray->receiver_id);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">BOOKING DETAILS - <?= $cArray->booking_no ?> - <?= $cArray->first_name ?></span>

                        </div>
                    </div>

                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12 col-sm-12">
                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fab fa-first-order"></i>  Booking Details 
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Booking No: </div>
                                                    <div class="col-md-7 value"><?= $cArray->booking_no ?></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Courier Company: </div>
                                                    <div class="col-md-7 value"><img style="width: 20%;" src="<?= $courier->logo_url ?>"></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Booking Type:</div>
                                                    <div class="col-md-7 value"><?= $cArray->booking_type ?></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Total Weight:</div>
                                                    <div class="col-md-7 value"><?= $cArray->weight ?> Kg</div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Choose Location: </div>
                                                    <div class="col-md-7 value"><?= pickup_dropoff($cArray->pickup_dropoff) ?></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Schedule Date: </div>
                                                    <div class="col-md-7 value"><?= date('d M, Y', strtotime($cArray->schedule_date)) ?></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Estimated Delivery: </div>
                                                    <div class="col-md-7 value"><?= date('d M, Y', strtotime($cArray->estimated_delivery)) ?></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Dimensions available: </div>
                                                    <div class="col-md-7 value">   <?= !empty($cArray->fill_dimensions) ? 'YES' : 'NO' ?>   </div>
                                                </div>

                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Date of creation: </div>
                                                    <div class="col-md-7 value"><?= date('h:i A d M, Y', strtotime($cArray->timestamp)) ?></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Last Update: </div>
                                                    <div class="col-md-7 value"><?= date('h:i A d M, Y', strtotime($cArray->lastupdate)) ?> </div>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Customer Id: </div>
                                                    <div class="col-md-7 value"><?= $user->customerId ?></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Full Name: </div>
                                                    <div class="col-md-7 value"><?= $user->first_name . ' ' . $user->last_name ?> </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Email : </div>
                                                    <div class="col-md-7 value"><?= $user->email ?></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Mobile No: </div>
                                                    <div class="col-md-7 value"><?= $user->mobile ?></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name">Account Type: </div>
                                                    <div class="col-md-7 value"><?= GetAccountType($user->type) ?></div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Base Price: </div>
                                                    <div class="col-md-7 value"><?= DecimalAmount($cArray->base_price) ?> AED</div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Markup: </div>
                                                    <div class="col-md-7 value"><?= DecimalAmount($cArray->markup) ?> AED</div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> VAT: </div>
                                                    <div class="col-md-7 value"><?= DecimalAmount($cArray->vat) ?> AED</div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Total: </div>
                                                    <div class="col-md-7 value"><?= DecimalAmount($cArray->total) ?> AED</div>
                                                </div>
                                            </div>






                                            <div class="col-sm-12">
                                                <div class="table-scrollable">
                                                    <?php
                                                    $summery = GetbookingpackagedetailsById($bookingId);
                                                    ?>
                                                    <table class="table table-bordered table-hover">
                                                        <tr><th colspan="9">Shipment Details</th></tr>
                                                        <tr>
                                                            <th>Parcel Title</th>
                                                            <th>Gross Weight</th>
                                                            <th>Chargeable Weight</th>
                                                            <th>Unit Declared Value</th>
                                                            <th>Length</th>
                                                            <th>Width</th>
                                                            <th>Height</th>
                                                            <th>Weight</th>
                                                            <th>Last Update</th>
                                                        </tr>
                                                        <?php
                                                        for ($i = 0; $i < count($summery); $i++) {
                                                            $d = $summery[$i];
                                                            ?>
                                                            <tr>
                                                                <td><?= $d->parcel_title ?></td>
                                                                  <td><?= $d->gross_weight ?></td>
                                                                  <td><?= $d->chargeable_weight ?></td>
                                                                  <td><?= $d->unit_declared_value ?></td>
                                                                  <td><?= $d->length ?></td>
                                                                  <td><?= $d->width ?></td>
                                                                  <td><?= $d->height ?></td>
                                                                  <td><?= $d->weight ?></td>
                                                                  <td><?= date('h:i A d M, Y', strtotime($d->lastupdate)) ?></td>
                                                                
                                                                  
                                                            </tr> 
                                                            <?php
                                                        }
                                                        ?>
                                                    </table>
                                                </div>
                                            </div>






                                        </div>

                                    </div>
                                </div>
                            </div>
                            <div class="col-md-12 col-sm-12">


                                <div class="portlet blue-hoki box">
                                    <div class="portlet-title">
                                        <div class="caption">
                                            <i class="fab fa-first-order"></i> Sender/Reciver Details 
                                        </div>
                                    </div>
                                    <div class="portlet-body">
                                        <div class="row">
                                            <div class="col-sm-6">
                                                <div class="table-scrollable">

                                                    <table class="table table-bordered table-hover">
                                                        <tr><th colspan="2">Sender Details</th></tr>
                                                        <tr>
                                                            <th>Full Name</th>
                                                            <td><?= $sender->full_name ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Mobile</th>
                                                            <td><?= $sender->mobile ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Country</th>
                                                            <td><?= $sender->mobile ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>City</th>
                                                            <td><?= $sender->city ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Zip Code</th>
                                                            <td><?= $sender->zip_code ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Father Name</th>
                                                            <td><?= $sender->father_name ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Grand Father Name</th>
                                                            <td><?= $sender->grand_father_name ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Family Name</th>
                                                            <td><?= $sender->family_name ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Email</th>
                                                            <td><?= $sender->email ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Address</th>
                                                            <td><?= $sender->address ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Company Name</th>
                                                            <td><?= $sender->company_name ?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                            <div class="col-sm-6">
                                                <div class="table-scrollable">

                                                    <table class="table table-bordered table-hover">
                                                        <tr><th colspan="2">Reciver Details</th></tr>
                                                        <tr>
                                                            <th>Full Name</th>
                                                            <td><?= $receiver->full_name ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Mobile</th>
                                                            <td><?= $receiver->mobile ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Country</th>
                                                            <td><?= $receiver->mobile ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>City</th>
                                                            <td><?= $receiver->city ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Zip Code</th>
                                                            <td><?= $receiver->zip_code ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Father Name</th>
                                                            <td><?= $receiver->father_name ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Grand Father Name</th>
                                                            <td><?= $receiver->grand_father_name ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Family Name</th>
                                                            <td><?= $receiver->family_name ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Email</th>
                                                            <td><?= $receiver->email ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Address</th>
                                                            <td><?= $receiver->address ?></td>
                                                        </tr>
                                                        <tr>
                                                            <th>Company Name</th>
                                                            <td><?= $receiver->company_name ?></td>
                                                        </tr>
                                                    </table>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>   



                            </div>

                        </div>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
<style>
    .table th{
            font-weight: 600 !important;
    }
</style>