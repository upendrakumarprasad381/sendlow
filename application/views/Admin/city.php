
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">city</span>
                            <a href="<?= base_url('admin/city/add-new'); ?>" class="btn btn-sm green small"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                          <!--   &nbsp;&nbsp;-->
                          <!--<a href="<?= base_url('admin/city_history'); ?>" target="_blank" class="btn btn-sm green small">History-->
                          <!--  </a>  -->
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover XXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?></th>
                                    <th>city</th>
                                    <th>country</th>
                                     <th>Postal Code</th>
                                    <th><?php echo $this->lang->line("action")?></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($city); $i++) {
                                    $d = $city[$i];
                                    $country = getcountrylist($d->country_id);
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->city ?></td>
                                        <td><?=$country[0]->country?></td>
                                           <td><?= $d->post_code ?></td>
                                        <td>
                                            <a href="<?= base_url('Admin/city/add-new/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a> 
                                            <a ref="javascript:void(0)" updatejson='{"archive":"<?= empty($d->archive) ? '1' : '0' ?>"}'  condjson='{"id":"<?= $d->id ?>"}' dbtable="city" class="autoupdate" id="autoupdate" redrurl="city"><span class="label label-sm label-<?= empty($d->archive) ? 'info' : 'danger' ?>"><?= empty($d->archive) ? '<i class="far fa-check-circle"></i>' : '<i class="fas fa-ban"></i>' ?></span></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
