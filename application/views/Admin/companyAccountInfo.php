<?php 
$Session =  $this->session->userdata('Admin');
$ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;
?>
<style>
    
.modal-title {
    margin: 0;
    line-height: 1.42857143;
    font-size: 16px;
    text-transform: uppercase;
    padding: 3px 0;
}

   .pac-container, .pac-item{ z-index: 2147483647 !important; }

</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= $accountId == '' ? 'Add New' : 'Update'; ?> company account information</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered">
                            <input type="hidden" id="UserId" value="<?= isset($accountArray->id) ? $accountArray->id : ''  ?>">
                            <div class="form-body">
                                  <div class="form-group">
                                    <label class="control-label col-md-3">Bank Name</label>
                                    <div class="col-md-4">
                                        <input id="bank_name" value="<?= isset($accountArray->bank_name) ? $accountArray->bank_name : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Account Name</label>
                                    <div class="col-md-4">
                                        <input id="account_name" value="<?= isset($accountArray->account_name) ? $accountArray->account_name : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email</label>
                                    <div class="col-md-4">
                                        <input id="email" value="<?= isset($accountArray->email) ? $accountArray->email : '' ?>" type="text" class="form-control">
                                    </div><span class='email_error' id='email_error' style='color:red'></span>
                                </div>
                                 <div class="form-group">
                                    <label class="control-label col-md-3">Whatsapp Number</label>
                                    <div class="col-md-4">
                                        <input id="mobile" value="<?= isset($accountArray->mobile) ? $accountArray->mobile : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Account Number</label>
                                    <div class="col-md-4">
                                        <input id="account_number" value="<?= isset($accountArray->account_number) ? $accountArray->account_number : '' ?>" type="number" class="form-control">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Swift Code</label>
                                    <div class="col-md-4">
                                        <input id="swift_code" value="<?= isset($accountArray->swift_code) ? $accountArray->swift_code : '' ?>" type="text" class="form-control">
                                    </div>
                                </div>                
	                            <div class="form-group">
                                    <label class="control-label col-md-3">IBAN Number</label>
                                    <div class="col-md-4">
                                    <input id="iban_number" value="<?= isset($accountArray->iban_number) ? $accountArray->iban_number : '' ?>" type="text" class="form-control">
                                    </div>
                                </div> 
                            </div>
                            <div class="form-actions">
                                <div class="row">
                                    <div class="col-md-offset-3 col-md-9">
                                        <button type="button" id="SubmitNewinfo" class="btn blue">
                                            <i class="fa fa-check"></i> Submit</button>
                                    </div>
                                </div>
                            </div>
                        </form>
                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
