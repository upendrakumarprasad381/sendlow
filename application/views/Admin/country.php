
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Country</span>
                            <a href="<?= base_url('Admin/country/add-new'); ?>" class="btn btn-sm green small"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover XXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th>sl_no</th>
                                    <th>Name</th>
                                    <th>Code</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($country); $i++) {
                                    $d = $country[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->country ?></td>
                                        <td><?= $d->code ?></td>
                                        <td>
                                            <a href="<?= base_url('Admin/country/add-new/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a> 
                                            <!--<a ref="javascript:void(0)" updatejson='{"archive":"<= empty($d->archive) ? '1' : '0' ?>"}'  condjson='{"id":"<= $d->id ?>"}' dbtable="country" class="autoupdate" id="autoupdate" redrurl="country"><span class="label label-sm label-<= empty($d->archive) ? 'info' : 'danger' ?>"><= empty($d->archive) ? '<i class="far fa-check-circle"></i>' : '<i class="fas fa-ban"></i>' ?></span></a>-->
                                            <!--<a ref="javascript:void(0)" class="common_delete" Id="<?= $d->id ?>" table="type_of_car" class="autoupdate" redrurl="country"><span class="label label-sm label-<?= empty($d->archive) ? 'danger' : 'info' ?>"><?= empty($d->archive) ? '<i class="fas fa-ban"></i>' : '<i class="far fa-check-circle"></i>' ?></span></a>-->
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
