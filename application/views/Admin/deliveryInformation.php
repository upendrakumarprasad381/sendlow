<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">

                    <div class="portlet-body form"> 

                        <div class="row">
                            <div class="col-md-12 ">
                                <div class=" " style="padding:15px; padding-bottom:0px;" > 
                                    <form class="form" role="form" method="post">
                                        <?php
                                        if (isset($_POST['updateDeliveryInfo'])) {
                                            $update = array(
                                                'col1' => $_POST['deliveryInfo'],
                                                'col1_ar' => $_POST['deliveryInfo_ar']
                                            );
                                            $cond = array('identify' => 'DELIVERY_INFO');
                                            $this->Database->update('cms', $update, $cond);
                                        }
                                        $data = GetcmsBycmsId('DELIVERY_INFO');
                                        ?>
                                        <div class="portlet box grey-cascade" style="margin-bottom: 0px;">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                DELIVERY INFORMATION                                                
                                                </div>
                                                <div class="tools">
                                                    <a href="" class="collapse" data-original-title="" title=""> </a>
                                                    <button name="updateDeliveryInfo" style="float: left;padding: 1px 6px 2px 6px;margin-right: 7px;" type="submit" class="btn btn-primary">Submit</button>
                                                </div> 
                                            </div>
                                            <div class="portlet-body " > 
                                                <div class="row"> 
                                                    <div class="col-sm-12">
                                                        <div class="form-group">
                                                            <label>DELIVERY INFORMATION (English)<span style="color:red">*</span></label>
                                                            <textarea class="form-control summernote" rows="3" name="deliveryInfo" placeholder="Delivery Information!"><?=(isset($data->col1) && $data->col1 ? $data->col1:'')?></textarea>
                                                        </div>
                                                        <div class="form-group">
                                                            <label>DELIVERY INFORMATION (Arabic)<span style="color:red">*</span></label>
                                                            <textarea class="form-control summernote" rows="3" name="deliveryInfo_ar" placeholder="Delivery Information!"><?=(isset($data->col1_ar) && $data->col1_ar ? $data->col1_ar:'')?></textarea>
                                                        </div>
                                                    </div>

                                                </div>      
                                            </div>
                                        </div>
                                    </form>
                                </div>
                            </div>                            
                           
                        </div>


                        <!-- END FORM-->
                    </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
