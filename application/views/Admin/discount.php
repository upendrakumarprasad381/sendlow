
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Discount</span>
                            <a href="<?= base_url('Admin/autoDiscount/add-new'); ?>" class="btn btn-sm green small"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover XXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl No</th>
                                    <th>Register As</th>
                                    <th>Discount Code</th>
                                    <th>Amount</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($discount); $i++) {
                                    $d = $discount[$i];
                                    if($d->register_type == 1){
                                        $type = "Individual";
                                    }else if($d->register_type == 2){
                                        $type = "Company";
                                    }else if($d->register_type == 3){
                                        $type = "Small business";
                                    }else if($d->register_type == 4){
                                        $type = "broker";
                                    }else{
                                        $type = "Ecommerce";
                                    }
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $type ?></td>
                                        <td><?= $d->discount ?></td>
                                        <td><?= $d->amount ?>%</td>
                                        <td>
                                            <a href="<?= base_url('Admin/autoDiscount/add-new/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a> 
                                            <a ref="javascript:void(0)" updatejson='{"archive":"<?= empty($d->archive) ? '1' : '0' ?>"}'  condjson='{"id":"<?= $d->id ?>"}' dbtable="auto_discount" class="autoupdate" id="autoupdate" redrurl="autoDiscount"><span class="label label-sm label-<?= empty($d->archive) ? 'info' : 'danger' ?>"><?= empty($d->archive) ? '<i class="far fa-check-circle"></i>' : '<i class="fas fa-ban"></i>' ?></span></a>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
