<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"> Banner Management</span>
                            <a href="<?= base_url('Admin/leftBanner/add-new'); ?>" class="btn btn-sm green small"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>sl_no</th>
                                    <th>Banner Image</th>
                                    <th>Status</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                for ($i = 0; $i < count($bannerArray); $i++) {
                                    $d = $bannerArray[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><img src="<?=base_url('uploads/home_banner/'.$d->image)?>" width="100px"/></td>
                                        <td><?= $d->status == 0 ? '<span class="label label-sm label-success">Active</span>' : '<span class="label label-sm label-danger">Inactive</span>' ?></td>
                                        <td>
                                            <a href="<?= base_url('Admin/leftBanner/add-new/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a>
                                           <a updatejson='{"archive":"<?= empty($d->archive) ? '1' : '0' ?>"}'  title='Delete' condjson='{"id":"<?= $d->id ?>"}' dbtable="home_left_banner" class="autoupdate"><span class="label label-sm label-danger"><i class="fas fa-trash-alt"></i></span></a>
                                            <!--<a HomeId="<?= $d->id ?>"  class="Homeurl"><span class="label label-sm label-danger"><i class="far fa-trash-alt"></i></span></a>-->
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
