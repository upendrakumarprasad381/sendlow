</div>
<!-- END CONTAINER -->


<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> 

        © <?= date('Y') ?> Fix Your Car. All Rights Reserved.
        <!--<a href="https://www.alwafaagroup.com" target="_blank">Web Design  &nbsp;|&nbsp; Alwafaa Group, Dubai</a>-->
    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
</div>
<!-- BEGIN QUICK NAV -->
<div class="quick-nav-overlay"></div>
<!-- END QUICK NAV -->


<?php
$Controller = $this->router->fetch_class();
$Method = $this->router->fetch_method();
$segment2 = $this->uri->segment(2);

$segment3 = $this->uri->segment(3);
$segment4 = $this->uri->segment(4);
$datatable = array('Products', 'Categories', 'UserManagement', 'order', 'customer', 'user_info');
$SummeryNote = array('AddNewProduct');
?>
<!-- BEGIN CORE PLUGINS -->
<script>
    var base_url = '<?= base_url(); ?>';
    var Controller = '<?= $Controller ?>';
    var Method = '<?= $Method ?>';
    var segment3 = '<?= $segment3 ?>';
    var segment4 = '<?= $segment4 ?>';
</script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>

<script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/pages/scripts/DatatableClass.js" type="text/javascript"></script> 
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
<!--<script src="<?= base_url(); ?>assets/pages/scripts/components-bootstrap-multiselect.min.js" type="text/javascript"></script>-->
<script src="https://www.ontime24.ae/assets/users/js/ekko-lightbox.min.js"></script>
<?php if ($Method == 'MediaUpload') { ?>
    <script src="<?= base_url('js/' . ADMIN_DIR); ?>dropzone.min.js" type="text/javascript"></script>
    <script src="<?= base_url('js/' . ADMIN_DIR); ?>form-dropzone.min.js" type="text/javascript"></script>
    <script src="<?= base_url('js/' . ADMIN_DIR); ?>fileupload.js" type="text/javascript"></script>
<?php } ?>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>  
<script src="<?= base_url(); ?>assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>

<script src="<?= base_url(); ?>assets/global/plugins/horizontal-timeline/horizontal-timeline.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>js/jquery-confirm.min.js"></script>
<script src="<?= base_url(); ?>assets/lobibox-master/js/lobibox.js"></script>

<script src="<?= base_url(); ?>assets/js/bootstrap-select.min.js"></script>
<script src="<?= base_url(); ?>js/jquery-ui.js"></script>
<script src="<?= base_url('js/' . ADMIN_DIR); ?>common.js" type="text/javascript"></script>
<script src="<?= base_url('js/' . ADMIN_DIR); ?>common_new.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>js/notification.js"></script>

<?php
if ($segment2 == 'addnewgarage') {
    ?>  
    <script src="<?= base_url() ?>js/update_location.js"></script>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= API_KEY ?>&callback=initMap&libraries=geometry,places&language=en"></script><?php
}
?>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
<script src="<?= base_url('js/' . ADMIN_DIR); ?>permission.js" type="text/javascript"></script>

<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-datetimepicker/js/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/pages/scripts/components-editors.min.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
<script>
    $(document).ready(function () {

        $('li').find('.newclass').parent().parent().addClass('open');
        $('li').find('.newclass').parent().parent().addClass('activeclass');
        $('li').find('.newclass').parent().parent().addClass('active');
        $('li').find('.active ').parent().parent().addClass('open');
        $('li').find('.active').parent().parent().addClass('activeclass');
        $('li').find('.active').parent().parent().addClass('active');


        $(".summernoteOffer").summernote({
            height: 100,
            callbacks: {
                onImageUpload: function (files) {
                    var $files = $(files);
                    var $this = $(this);
                    $files.each(function () {
                        var image = this;
                        var data = new FormData();
                        data.append("image", image);
                        $.ajax({
                            data: data,
                            type: "POST",
                            url: base_url + "Admin/ImageUploadServer",
                            cache: false,
                            contentType: false,
                            processData: false,
                            success: function (response) {
                                $this.summernote('insertImage', response, function ($image) {
                                });

                            },
                            error: function (xhr, ajaxOptions, thrownError) {
                                if (xhr.responseText) {
                                    toastr.error(xhr.responseText, 'Inconceivable!')
                                } else {
                                    console.error("<div>Http status: " + xhr.status + " " + xhr.statusText + "</div>" + "<div>ajaxOptions: " + ajaxOptions + "</div>"
                                            + "<div>thrownError: " + thrownError + "</div>");
                                }
                            }
                        });
                    });
                }
            },
            toolbar: [
                ['style', ['style']],
                ['font', ['bold', 'underline', 'clear']],
                ['fontname', ['fontname']],
                ['color', ['color']],
                ['fontsize', ['fontsize']],
                ['para', ['ul', 'ol', 'paragraph']],
                ['table', ['table']],
                ['insert', ['picture', 'link', 'video', 'table']],
                ['view', ['fullscreen', 'codeview', 'help']],
            ],
            link: [
                ['link', ['linkDialogShow', 'unlink']]
            ],
            //fontSizes: ['8', '9', '10', '11', '12', '14', '18', '24', '36', '48' , '64', '82', '150']
        });

    });
</script>








<script>
    $(document).ready(function () {
        if (Method == 'markup') {
            setTimeout(function () {
                markupfn.getMyDestinationList();
            }, 500);
            setTimeout(function () {
                $('#searchReport').click();
            }, 1500);

        }
    });
    var markupfn = (function () {
        var fn = {};
        fn.searchMymarkup = function () {
            var data = {function: 'searchMymarkup'};
            data['from_country'] = $('#from_country').val();
            data['to_country'] = $('#to_country').val();

            data['from_city'] = $('#from_city').val();
            data['to_city'] = $('#to_city').val();

            data['type'] = $('#type').val();
            if ($('#from_country').val() == '') {
                alertsimple('From country is required');
                return false;
            }
            if ($('#to_country').val() == '') {
                alertsimple('to country is required');
                return false;
            }
            if (data['from_country'] == '229' && data['to_country'] == '229') {
                if ($('#from_city').val() == '' || $('#to_city').val() == '') {
                    alertsimple('From and To city is required');
                    return false;
                }
            }
            $.ajax({
                url: base_url + "Frontend/Helper",
                type: 'POST',
                data: data,
                async: true,
                success: function (data) {
                    try {
                        var json = jQuery.parseJSON(data);
                        if (json.status == true) {
                            $('#mytbody').html(json.HTML);
                        }
                    } catch (e) {

                    }
                }
            });
        }
        fn.getmycityListForprice = function (e) {
            var myId = $(e).attr('id');
            var countryId = $('#' + myId).val();
            var append = $(e).attr('append');
            var data = {function: 'GetcitylistBycountryId', countryId: countryId, encode: true};
            $.ajax({
                url: base_url + "Frontend/Helper",
                type: 'POST',
                data: data,
                async: true,
                success: function (data) {
                    try {
                        var json = jQuery.parseJSON(data);

                        if (json.status == true) {
                            var html = '<option value="">--select--</option>';
                            var data = json.data;
                            for (var i = 0; i < data.length; i++) {
                                var d = data[i];
                                html = html + '<option pincode="' + d.post_code + '" value="' + d.id + '">' + d.city + '</option>';
                            }
                            $('#' + append).html(html);
                            //$('.selectpicker').selectpicker('refresh');
                        }
                    } catch (e) {

                    }
                }
            });
        }
        fn.getMyDestinationList = function (e) {
            var myid = $(e).attr('id');
            var selectedId = myid == undefined ? 'EG' : '';
            var countryId = $('#from_country').val();
            var data = {function: 'getMyDestinationList', origincountryId: countryId, encode: true};
            $.ajax({
                url: base_url + "Frontend/Helper",
                type: 'POST',
                data: data,
                async: true,
                success: function (data) {
                    try {
                        var json = jQuery.parseJSON(data);
                        $('#mytbody').html('');
                        markupfn.onchnageTocountry();
                        if (json.status == true) {
                            var html = '<option value="">--select--</option>';
                            var data = json.data;
                            for (var i = 0; i < data.length; i++) {
                                var d = data[i];
                                html = html + '<option ' + (selectedId == d.code ? 'selected' : '') + '  value="' + d.id + '">' + d.country + '</option>';
                            }
                            $('#to_country').html(html);
                            //$('.selectpicker').selectpicker('refresh');
                        }
                    } catch (e) {

                    }
                }
            });
        }
        fn.onchnageTocountry = function () {
            $('#mytbody').html('');
            var from_country = $('#from_country').val();
            var to_country = $('#to_country').val();
            if (from_country == '229' && to_country == '229') {
                $('#stateListdiv').show();
            } else {
                $('#stateListdiv').hide();
            }
        }
        fn.onSavemarkup = function () {
            var rowlist = [];
            $('.rowlist').each(function () {
                var uniqid = $(this).attr('uniqid');
                var d = {};

                d['type'] = $('#type').val();
                d['courier_id'] = $('#rowId-' + uniqid).attr('courier_id');
                d['country_from'] = $('#rowId-' + uniqid).attr('country_from');
                d['country_to'] = $('#rowId-' + uniqid).attr('country_to');

                d['city_from'] = $('#vat-' + uniqid).attr('from_city');
                d['city_to'] = $('#vat-' + uniqid).attr('to_city');

                d['weight_from'] = $('#weight_from-' + uniqid).attr('weight_from');
                d['weight_to'] = $('#weight_to-' + uniqid).attr('weight_to');
                d['markup'] = $('#markup-' + uniqid).val();
                d['price_id'] = $('#vat-' + uniqid).attr('price_id');

                rowlist.push(d);
            });
            if (rowlist.length == '0') {
                alertsimple('No data found');
                return false;
            }
            console.log(rowlist);


            var data = {function: 'onSavemarkup', rowlist: rowlist};
            $.ajax({
                url: base_url + "Frontend/Helper",
                type: 'POST',
                data: data,
                async: true,
                success: function (data) {
                    try {
                        var json = jQuery.parseJSON(data);
                        if (json.status == true) {
                            alertsimple(json.message);
                        }
                    } catch (e) {

                    }
                }
            });
        }
        fn.markupAutocalc = function (e) {
            var uniqid = $(e).attr('uniqid');
            var markup = $('#markup-' + uniqid).val();
            var api_price = $('#api_price-' + uniqid).attr('api_price');
            var myMarkup = parseFloat(api_price) * parseFloat(markup) / 100;
            var vatAdd = parseFloat(api_price) * 5 / 100;
            var total = parseFloat(api_price) + parseFloat(vatAdd) + parseFloat(myMarkup);
            $('#final_price-' + uniqid).val(total);
        }
        fn.onchnageFromcity = function () {
            $('#mytbody').html('');
        }
        fn.onchnageTocity = function () {
            markupfn.onchnageFromcity();
        }
        return fn;
    })();
</script>
</body>
</html>