<?php
$Controller = $this->router->fetch_class();
$Method = $this->router->fetch_method();
$SessionArray = GetSessionArrayAdmin();
?>
<?php
$admin_session = $this->session->userdata('Admin');
$ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;

$segment2 = $this->uri->segment(2);

?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>SENDLOW</title>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" href="https://www.ontime24.ae/assets/users/css/ekko-lightbox.css">
            <link href="<?= base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <?php if ($Method == 'MediaUpload') { ?>
            <link href="<?= base_url('css/' . ADMIN_DIR); ?>dropzone.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url('css/' . ADMIN_DIR); ?>basic.min.css" rel="stylesheet" type="text/css" />   
        <?php } ?>
        <link href="<?= base_url() ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
       
       <link href="<?= base_url() ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/jquery-nestable/jquery.nestable.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?= base_url() ?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?= base_url() ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
                <link rel="stylesheet" href="<?= base_url() ?>assets/lobibox-master/dist/css/lobibox.min.css"/>

        <!-- END THEME LAYOUT STYLES -->
        <link rel="stylesheet" href="<?= base_url() ?>css/jquery-ui.css">
        <link rel="stylesheet" href="<?= base_url('css/jquery-confirm.min.css') ?>">
        <link href="<?= base_url(); ?>assets/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="<?= base_url('css/' . ADMIN_DIR); ?>main.css" rel="stylesheet" type="text/css" /> 
         <link rel="stylesheet" href="<?= base_url() ?>css/main.css"/>
        <?= $Method == 'AddNewProduct' ? '<link rel="stylesheet" href="' . base_url('css/') . 'star-rating.css">' : '' ?>
        <!--<link rel="shortcut icon" href="<?= base_url('files/images') ?>/favicon.ico" type="image/x-icon">-->
                <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

        <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="<?= base_url('Admin') ?>">
                            <img src="<?= base_url() ?>css/images/logo.png" alt="logo" class="logo-default" style="width: 138px" /> </a>
                            <!--<img src="<?= base_url() ?>assets/layouts/layout/img/fix.png" alt="logo" class="logo-default" /> </a>-->

                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li class="dropdown dropdown-extended dropdown-notification" id="header_notification_bar">
                                <?php
                                $qry = "SELECT SQL_CALC_FOUND_ROWS * FROM `notification` WHERE admin_read=0 ORDER BY timestamp DESC LIMIT 50";
                                $nArray = $this->Database->select_qry_array($qry);
                                
                                $calRow = "SELECT FOUND_ROWS() AS TotalRows";
                                $calRow = $this->Database->select_qry_array($calRow);
                                $calRow = !empty($calRow) ? $calRow[0]->TotalRows : 0;
                                $calRowIcon=$calRow>50 ? '+' : ''; 
                                ?>
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <span class="badge badge-default"> <?= count($nArray).$calRowIcon ?>  </span>
                                </a>
                                 <ul class="dropdown-menu" style="    max-width: 400px;width: 400px;">

                                    <li class="external">
                                        <h3><span class="bold"><?= count($nArray).$calRowIcon ?> pending</span> notifications</h3>
                                        <a onclick="ADMIN.notificationsAdminReadAll(this);" href="javascript:void(0)">clear all</a>
                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283" id="HeaderNotificationHtml">
                                            <?php
                                            $addIcon = [NEW_USER_NOTIFICATION];
                                            for ($i = 0; $i < count($nArray); $i++) {
                                                $d = $nArray[$i];
                                                
                                                $icon = '<i class="fa fa-bell-o"></i>';
                                                if (in_array($d->notification_type, $addIcon)) {
                                                    $icon = '<i class="fa fa-plus"></i>';
                                                }
                                                ?>
                                            <li>
                                                <a href="javascript:void(0)" onclick="ADMIN.notificationsOnclick(this);" notificationId="<?= $d->notification_id ?>" fhref="<?= $d->notification_url ?>">
                                                    <span style="max-width: 88px;" class="time"><?= time_elapsed_string($d->timestamp) ?></span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success"><?= $icon ?></span>
                                                        <?= $d->notification_title ?>
                                                    </span>
                                                </a>
                                            </li>
                                            <?php } ?>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <li class="dropdown dropdown-user">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i style="    font-size: 18px;" class="fas fa-user"></i>
                                    <span class="username username-hide-on-mobile"> <?= $SessionArray->name; ?> </span>
                                    <i class="fa fa-angle-down"></i>
                                </a>
                                <ul class="dropdown-menu dropdown-menu-default">
                                    <!--<li>-->
                                    <!--    <a href="<?= base_url('Admin/order'); ?>">-->
                                    <!--        <i class="icon-user"></i>Order</a>-->
                                    <!--</li>-->
                                    <li>
                                        <a href="<?= base_url('Login/AdminLoginout'); ?>">
                                            <i class="icon-key"></i> Log Out </a>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->




            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <div class="page-sidebar-wrapper">
                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">




                            <?php 
                            
                            
                            $menu=GetMenus(); 
                            if(!empty($menu)){
                            for($i=0;$i<count($menu);$i++){
                            $d=$menu[$i];
                            if (!in_array($admin_session->id, $ADMIN_PERMISSION_ARRAY)) {
                            $usermenu=GetUserMenu($d->menu_id,$admin_session->id);
                             }
                             else
                             {
                               $usermenu='Admin';  
                             }
                            // $usermenu=GetUserMenu($d->menu_id,$admin_session->id);
                            // print_r($admin_session->id);
                            if (!empty($usermenu)) {
                            ?>
                            <li class="nav-item start <?php if('Admin/'.$segment2==$d->menu_url || ($d->menu_url=="Admin" && $segment2=="")){echo "active";echo " activeclass";} ?> liclass<?=$i?>">
                               <?php
                            //   if($d->menu_id=='29')
                            //   {
                            //       if($admin_session->user_type=='2')
                            //       $men_u = "Admin/user_request";
                            //       else
                            //       {
                            //         $men_u = "Admin/new_request";

                            //       }
                                   
                            //   }
                            //   else if($d->menu_id=='28')
                            //   {
                            //         if($admin_session->user_type=='2')
                            //       $men_u = "Admin/all_user_request";
                            //       else
                            //       {
                            //         $men_u = "Admin/request";

                            //       }
                            //   }
                            //   else
                            //   {
                              $men_u=    $d->menu_url;
                            //   }
                               ?>
                                <a href="<?= base_url($men_u); ?>" class="nav-link nav-toggle">
                                    <?php
                                    if($d->menu_image!='')
                                    {?>
                                       <img src="<?= base_url().'uploads/'.$d->menu_image?>" alt="logo" class="logo-default" />
 
                                <?php    }
                                    else
                                    {
                                    echo $d->menu_icon;
                                    }
                                    ?>
                                    <span class="title"><?=$d->menu_name?></span>
                                    <?php if($d->toggle=='1'){?>
                                    <span class="arrow arrowclass<?=$i?>"></span>
                                    <?php }?>
                                </a>
                            <?php
                            $submenu=GetSubMenu($d->menu_id);
                            if(!empty($submenu)){
                            ?>
                            <ul class="sub-menu ulclass<?=$i?>">
                                <?php for($j=0;$j<count($submenu);$j++){
                                       $dn=$submenu[$j];
                                        if (!in_array($admin_session->id, $ADMIN_PERMISSION_ARRAY)) {
                                            $usermenusub=GetUserMenu($dn->menu_id,$admin_session->id);
                                        }
                             else
                             {
                               $usermenusub='Admin';  
                             }
                                    //   $usermenusub=GetUserMenu($dn->menu_id,$admin_session->id);
                                     // print_r($usermenusub);
                                if (!empty($usermenusub)) {
                                    if(($dn->menu_id == 81)||($dn->menu_id == 83))
                                    {
                                ?>
                                
                                <li class="nav-item <?php if(('Admin/'.$segment2==$dn->menu_url)){echo "active";echo " activeclass";echo " newclass";}  ?>" id="<?=$i?>">
                                    <a class="nav-link">
                                       </i> <span class="title"><?=$dn->menu_name?></span>
                                       <?php if($dn->toggle=='1'){?>
                                        <span class="arrow arrowclass<?=$i?>"></span>
                                        <?php }?>
                                    </a>
                                    <?php
                                    $childMenu = GetSubMenu($dn->menu_id);
                                    if(!empty($childMenu)){
                                    ?>
                                    <ul class="sub-menu ulclass<?=$j?>">
                                    <?php
                                    for($k=0;$k<count($childMenu);$k++){
                                       $dnc=$childMenu[$k];
                                        if (!in_array($admin_session->id, $ADMIN_PERMISSION_ARRAY)) {
                                            $usermenuchild=GetUserMenu($dnc->menu_id,$admin_session->id);
                                        }
                                        else
                                        {
                                           $usermenuchild='Admin';  
                                        }
                                        if (!empty($usermenuchild)) {
                                    ?>
                                        <li class="nav-item <?php if(('Admin/'.$segment2==$dnc->menu_url)){echo "active";echo " activeclass";echo " newclass";}  ?>" id="<?=$i?>">
                                            <a href="<?= base_url($dnc->menu_url); ?>" class="nav-link">
                                               <i class="fas fa-angle-double-left"></i> <span class="title"><?=$dnc->menu_name?></span>
                                            </a>
                                        </li>
                                    <?php
                                            }
                                        }
                                    ?>
                                    </ul>
                                    <?php
                                        }
                                    ?>
                                </li>
                                <?php
                                    }else{
                                ?>
                                
                                <!--<li class="nav-item <?php if('Admin/'.$segment2==$dn->menu_url){echo "active";echo " activeclass";}  ?>" id="<?=$i?>">-->
                                <!--        <a href="<?= base_url($dn->menu_url); ?>" class="nav-link">-->
                                <!--            <span class="title"><?=$dn->menu_name?></span>-->
                                <!--        </a>-->
                                <!--</li>-->
                                    
                                
                                <li class="nav-item <?php if(('Admin/'.$segment2==$dn->menu_url)){echo "active";echo " activeclass";echo " newclass";}  ?>" id="<?=$i?>">
                                    <a href="<?= base_url($dn->menu_url); ?>" class="nav-link">
                                       <i class="fas fa-angle-double-left"></i> <span class="title"><?=$dn->menu_name?></span>
                                    </a>
                                </li>
                                
                            <?php }} } ?>
                            </ul>    
                            <?php } ?>    
                            </li>
                            <?php }  
                              }
                            }
                            ?>     
                           
                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->