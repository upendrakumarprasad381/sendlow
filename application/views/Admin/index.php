<?php
$session_admin = GetSessionArrayAdmin();
$todays = date("Y-m-d");
$oneWeek = date("Y-m-d", strtotime("-1 week"));
$before7days = date('Y-m-d', strtotime('-7 days'));

$total_customer = ",(SELECT COUNT(id) FROM `register` WHERE archive=0) AS total_customer";
// $total_order = ",(SELECT COUNT(order_id) FROM `orders` WHERE order_status NOT IN (2) AND archive=0) AS total_order";
// $total_order_cncl = ",(SELECT COUNT(order_id) FROM `orders` WHERE order_status IN (2) AND archive=0) AS total_order_cncl";

// $todaysorder = ",(SELECT (SUM(total))   FROM `orders` WHERE archive=0 AND order_status NOT IN (2)  AND DATE(timestamp)='$todays') AS todays_order";
// $oneweek = ",(SELECT (SUM(total)) FROM `orders` WHERE archive=0 AND order_status NOT IN (2)  AND DATE(timestamp) BETWEEN '$before7days' AND '$todays') AS one_week";
// $this_month = ",(SELECT (SUM(total)) FROM `orders` WHERE archive=0 AND order_status NOT IN (2)  AND MONTH(timestamp) = '" . date('m') . "' AND YEAR(timestamp) = '" . date('Y') . "') AS this_month";
// $this_year = ",(SELECT (SUM(total)) FROM `orders` WHERE archive=0 AND order_status NOT IN (2)   AND YEAR(timestamp) = '" . date('Y') . "') AS this_year";

$sql = "SELECT COUNT(id) AS totalEmp $total_customer FROM `admin_login` WHERE archive=0 AND id NOT IN (1)";
$dArray = $this->Database->select_qry_array($sql);
$dArray = !empty($dArray) ? $dArray[0] : '';
?>
<style>
    .fasCd {
        opacity: 1 !important;
        line-height: 50px !important;
        margin-left: -5px !important;
        font-size: 50px !important;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 blue" href="<?= base_url('admin/customers') ?>">
                    <div class="visual">
                        <i class="fasCd fas fa-user"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value="1349"><?= $dArray->total_customer ?></span>
                        </div>
                        <div class="desc"> Total Customers </div>
                    </div>
                </a>
            </div>
            <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">-->
            <!--    <a class="dashboard-stat dashboard-stat-v2 red" href="<?= base_url('admin/approved_vendor') ?>">-->
            <!--        <div class="visual">-->
            <!--            <i class="fasCd fas fa-users"></i>-->
            <!--        </div>-->
            <!--        <div class="details">-->
            <!--            <div class="number">-->
            <!--                <span data-counter="counterup" data-value=""><?= $dArray->total_vendor ?></div>-->
            <!--            <div class="desc"> Total Vendor </div>-->
            <!--        </div>-->
            <!--    </a>-->
            <!--</div>-->
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 green" href="<?= base_url('admin/orders') ?>">
                    <div class="visual">
                        <i class="fasCd fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""></span>
                        </div>
                        <div class="desc"> total Orders </div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 purple" href="<?= base_url('admin/orders') ?>">
                    <div class="visual">
                        <i class="fasCd fas fa-power-off"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""></span>
                        </div>
                        <div class="desc"> Cancelled Orders </div>
                    </div>
                </a>
            </div>


            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="portlet blue-hoki box">
                    <a style="background-color: #9C27B0;" class="dashboard-stat dashboard-stat-v2 green" >
                        <div class="visual">
                            <i class="fasCd fas fa-check-circle"></i>
                        </div>
                        <div class="details">
                            <div class="number">

                                <span data-counter="counterup" data-value=""></span>
                            </div>
                            <div class="desc">Todays Orders </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="portlet blue-hoki box">
                    <a style="    background-color: #009688;" class="dashboard-stat dashboard-stat-v2 green" >
                        <div class="visual">
                            <i class="fasCd fas fa-check-circle"></i>
                        </div>
                        <div class="details">
                            <div class="number">

                                <span data-counter="counterup" data-value=""></span>
                            </div>
                            <div class="desc">Weekly Orders </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="portlet blue-hoki box">
                    <a style="    background-color: #4CAF50;" class="dashboard-stat dashboard-stat-v2 green" >
                        <div class="visual">
                            <i class="fasCd fas fa-check-circle"></i>
                        </div>
                        <div class="details">
                            <div class="number">

                                <span data-counter="counterup" data-value=""></span>
                            </div>
                            <div class="desc">Monthly Orders </div>
                        </div>
                    </a>
                </div>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <div class="portlet blue-hoki box">
                    <a style="background-color: #6523da;" class="dashboard-stat dashboard-stat-v2 green" >
                        <div class="visual">
                            <i class="fasCd fas fa-check-circle"></i>
                        </div>
                        <div class="details">
                            <div class="number">

                                <span data-counter="counterup" data-value=""></span>
                            </div>
                            <div class="desc">Yearly  Orders </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Todays Order</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th>Sl No</th> <!--<?php echo $this->lang->line("sl_no") ?>-->
                                    <th>Order No</th>
                                    <th>Customer Name</th>
                                    <th>Price</th>
                                    <th>Payment Status</th>
                                    <th>Order Status</th>
                                    <th>Order Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            
                                            <!--<span class="label label-sm label-success" style="    background-color: <?= $d->status_color ?>;"><?= !empty($d->status_name) ? $d->status_name : 'Received' ?></span>-->
                                            <!--<span class="label label-sm label-success <?= $OrserStatusName ?>"><?= $OrserStatusName ?></span>-->
                                        </td>
                                        <td></td>
                                        <td>
                                            <!--<a href="<?= base_url('Admin/user_info/' . base64_encode($d->user_id)) ?>" title="User dashboard" ><span class="label label-sm label-success"><i class="fas fa-street-view" aria-hidden="true"></i></span></a>-->
                                            <!--<a href="<?= base_url('Admin/orderDetails/' . base64_encode($d->order_id)) ?>" title="Order summary"><span class="label label-sm label-danger"><i class="fa fa-eye"></i></span></a>-->
                                        </td>
                                    </tr>
                                <?php if (empty($order)) {
                                    ?><tr>
                                        <td style="text-align: center;" colspan="9">no order found.</td>
                                    </tr><?php }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->

<style>
    #chartdiv,#newchartdiv {
        width: 100%;
        height: 500px;
    }
</style>

<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>

