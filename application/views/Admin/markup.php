<?php
$id = !empty($_REQUEST['id']) ? $_REQUEST['id'] : '';
$allowedUpdate = ['DOCUMENTS', 'PACKAGE'];
if (!in_array($id, $allowedUpdate)) {
    redirect(base_url());
}
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"><?= $id ?> Markup</span>


                        </div>
                    </div>

                    <div class="portlet-body">



                        <input type="hidden" id="type" value="<?= $id ?>">
                        <div class="row"> 
                            <div class="col-sm-3">
                                <label>FROM Country</label>
                                <select class="form-control selectpickerXX" onchange="markupfn.getMyDestinationList();" data-live-search="true" id="from_country">
                                    <option  value="">--select--</option>
                                    <?php
                                    $country = getcountry();
                                    for ($i = 0; $i < count($country); $i++) {
                                        $d = $country[$i];
                                        ?> <option <?= $d->code == 'AE' ? 'selected' : '' ?> value="<?= $d->id ?>"><?= $d->country ?></option><?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="col-sm-3">
                                <label>To Country</label>
                                <select class="form-control selectpickerx" onchange="markupfn.onchnageTocountry();" data-live-search="true"  id="to_country">
                                    <option  value="">--select--</option>

                                </select>
                            </div>
                            <div class="col-sm-6">
                                <?php
                                $emirates = GetemiratesAll();
                                ?>
                                <div class="row" id="stateListdiv" style="display: none;">
                                    <div class="col-sm-6">
                                        <label>From City</label>
                                        <select class="form-control " onchange="markupfn.onchnageFromcity();" id="from_city">
                                            <option  value="">--select--</option>
                                            <?php
                                            for ($i = 0; $i < count($emirates); $i++) {
                                                $df = $emirates[$i];
                                                ?><option  value="<?= $df->emirates_id ?>"><?= $df->emirates_name ?></option><?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                    <div class="col-sm-6">
                                        <label>To City</label>
                                        <select class="form-control " onchange="markupfn.onchnageTocity();" id="to_city">
                                            <option  value="">--select--</option>
                                            <?php
                                            for ($i = 0; $i < count($emirates); $i++) {
                                                $df = $emirates[$i];
                                                ?><option  value="<?= $df->emirates_id ?>"><?= $df->emirates_name ?></option><?php
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                            </div>

                            <div class="col-sm-3">
                                <label>&nbsp;</label>
                                <br>
                                <button onclick="markupfn.searchMymarkup();" type="button" name="searchReport" id="searchReport" class="btn btn-danger">Search</button>

                            </div>


                        </div>
                        <br>




                        <div class="row"> 
                            <table class="table table-striped table-bordered table-hover ">
                                <thead>
                                    <tr>
                                        <th>Weight <small>(Min)</small></th>
                                        <th>Weight <small>(Max)</small></th>
                                        <th>API Price</th>
                                        <th>Markup</th>
                                        <th>VAT (%)</th>
                                        <th>Final</th>
                                    </tr>

                                </thead>
                                <tbody id="mytbody">

                                </tbody>
                            </table>
                            <div class="col-sm-12">
                                <button type="button" onclick="markupfn.onSavemarkup();" class="btn btn-danger pull-right">Update</button>
                            </div>
                        </div>


                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
