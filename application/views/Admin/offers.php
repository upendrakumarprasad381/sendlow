
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Promotional Offers</span>
                            <a href="<?= base_url('Admin/add_offers') ?>" class="btn btn-sm green small"> Add-New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                        <div class="tools"> </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Paid Amount</th>
                                    <th>Discount Amount</th>
                                    <th>Start date</th>
                                    <th>Expiry date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $cond = "";
                                $selct = "";
                                $join = " ";
                                $qry = "SELECT * $selct FROM `offers` $join WHERE archive=0 ORDER BY id DESC";
                                $array = $this->Database->select_qry_array($qry);
                                for ($i = 0; $i < count($array); $i++) {
                                    $d = $array[$i];
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->paid_amount ?></td>
                                        <td><?= $d->get_amount ?></td>
                                        <td><?= date('d/m/Y', strtotime($d->start_date)); ?></td>
                                        <td><?= date('d/m/Y', strtotime($d->expiry_date)); ?></td>
                                        <td> 
                                            <a data-toggle="tooltip" href="<?= base_url('Admin/add_offers?offerId=' . base64_encode($d->id)); ?>" title="Edit" ><span class="label label-sm label-info"><i class="fas fa-edit"></i></span></a>
                                            <a  href="javascript:void(0)" updatejson='{"archive":"1"}'  condjson='{"id":"<?= $d->id ?>"}' dbtable="offers" class="autoupdate" href="javascript:void(0)" data-toggle="tooltip" ><span class="label label-sm label-danger"><i class="fas fa-trash-alt"></i></span></a>
                                        </td>
                                    </tr>
                                <?php } ?> 
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
