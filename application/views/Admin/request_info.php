
<style>
    .fasCd {
        opacity: 1 !important;
        line-height: 50px !important;
        margin-left: -5px !important;
        font-size: 50px !important;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">
                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i style="font-size: 24px;margin-top: 2px;" class="fas fa-user"></i>Recharge Request Details</div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Customer Name: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $UserInfo->first_name . " " . $UserInfo->last_name ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("email") ?>Email : </div>
                                                    <div class="col-md-3 value"> 
                                                        <?= $UserInfo->email ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("mobile_number") ?>Mobile Number : </div>
                                                    <div class="col-md-3 value"> 
                                                         <?= $UserInfo->mobile ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Register As : </div>
                                                    <div class="col-md-3 value"> 
                                                        <?php
                                                        if($UserInfo->type == 1){
                                                            $type = "Individual";
                                                        }else if($UserInfo->type == 2){
                                                            $type = "Company";
                                                        }else if($UserInfo->type == 3){
                                                            $type = "Small Business";
                                                        }else if($UserInfo->type == 4){
                                                            $type = "Broker";
                                                        }else{
                                                            $type = "Ecommerce";
                                                        }
                                                        echo $type;
                                                        ?>
                                                        
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> <?php echo $this->lang->line("account_status") ?>Approval Status: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?php
                                                        if ($UserInfo->is_approved == 1) {
                                                            ?>
                                                            <span style="cursor: pointer;" >Approved</span>
                                                        <?php } elseif ($UserInfo->is_approved == 0) {
                                                            ?>
                                                            <span style="cursor: pointer;" >Waiting for approval</span>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <span style="cursor: pointer;" >Rejected by Admin</span>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Recharge Amount : </div>
                                                    <div class="col-md-3 value"> 
                                                        AED <?= $UserInfo->recharge_amount ?>
                                                    </div>
                                                </div>
                                                <?php
                                                if($UserInfo->request_type == 2){
                                                    $offerArray = GetOfferByOfferId($UserInfo->offer_id);
                                                    if(isset($offerArray)){
                                                ?>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> offer Amount : </div>
                                                    <div class="col-md-3 value"> 
                                                        AED <?= $offerArray[0]->get_amount ?>
                                                    </div>
                                                </div>
                                                <?php
                                                    }
                                                }
                                                ?>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Transaction ID : </div>
                                                    <div class="col-md-3 value"> 
                                                        <?= $UserInfo->transaction_num ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Reciept : </div>
                                                    <div class="col-md-3 value"> 
                                                        <a target="_blank" href="<?php if(isset($UserInfo->reciept) && $UserInfo->reciept!=""){ echo base_url().'uploads/Reciept/'.$UserInfo->reciept;} ?>">
                                                            <img src=" <?php if(isset($UserInfo->reciept) && $UserInfo->reciept!=""){ echo base_url().'uploads/Reciept/'.$UserInfo->reciept;} ?>" width="100%" id="ImagesEncode" OldImage='<?= !empty($UserInfo->reciept) ? 'uploads/Reciept/' . $UserInfo->reciept : '' ?>'>
                                                        </a>
                                                        <?php 
                                                        // if(isset($UserInfo->reciept) && $UserInfo->reciept!=""){ 
                                                        ?>
                                                            <!--<br><a class="remove_image_common" path="uploads/Reciept/" name="<?=isset($UserInfo->reciept)?$UserInfo->reciept:''?>" table="wallet_request" id="<?=$UserInfo->id?>" type="" field="reciept">Remove</a>-->
                                                        <?php 
                                                        // } 
                                                        ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i style="font-size: 24px;margin-top: 2px;" class="fas fa-info-circle"></i> Recharge details updates
                                                </div>
                                            </div>
                                            <div class="portlet-body">
                                                <input type="hidden" value="<?= $UserInfo->id ?>" id="request_id">
                                                <input type="hidden" value="<?= $UserInfo->user_id ?>" id="user_id">
                                                <input type="hidden" value="<?= $UserInfo->offer_id ?>" id="offer_id">
                                                <input type="hidden" value="<?= $UserInfo->request_type ?>" id="request_type">
                                                <input type="hidden" value="<?= $UserInfo->transaction_num ?>" id="transaction_num">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Amount : </div>
                                                    <div class="col-md-3 value"> 
                                                        <input type="text" id="recharge_amount" class="form-control" value="<?= $UserInfo->recharge_amount ?>">
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-7 col-md-offset-5"> 
                                                        <button type="submit" id="update_details" class="btn green-meadow">Add</button>
                                                        <span class="" id="common_message"></span>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>



                            </div>

                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
