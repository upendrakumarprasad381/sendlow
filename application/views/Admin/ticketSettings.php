
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Ticket Management</span>
                            <a href="<?= base_url('Admin/ticketSettings/add-new'); ?>" class="btn btn-sm green small"> Add New
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover XXXX DataTableClass">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no")?>Sl No</th>
                                    <th>Ticket Number</th>
                                    <th>Customer Name</th>
                                    <th>Customer Email</th>
                                    <th>Order Number</th>
                                    <th>Issue</th>
                                    <th>Status</th>
                                    <th><?php echo $this->lang->line("action")?>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                if(!empty($ticket)){
                                    for ($i = 0; $i < count($ticket); $i++) {
                                        $d = $ticket[$i];
                                        $qry = "SELECT * FROM status wHERE archive=0 AND id=$d->status_id";
            							$status = $this->Database->select_qry_array($qry);
                                        ?>
                                        <tr>
                                            <td><?=$i+1?></td>
                                            <td><?=$d->ticket_no?></td>
                                            <td><?=$d->customer_name?></td>
                                            <td><?=$d->customer_email?></td>
                                            <td><?=$d->order_no?></td>
                                            <td><?=$d->Issue?></td>
                                            <td><?=$status[0]->status?></td>
                                            <td>
                                                <a href="<?= base_url('Admin/ticketSettings/add-new/' . base64_encode($d->id)) ?>"><span class="label label-sm label-success"><i class="fa fa-pencil" aria-hidden="true"></i></span></a> 
                                                <a ref="javascript:void(0)" updatejson='{"archive":"<?= empty($d->archive) ? '1' : '0' ?>"}'  condjson='{"id":"<?= $d->id ?>"}' dbtable="ticket_settings" class="autoupdate" id="autoupdate" redrurl="ticketSettings"><span class="label label-sm label-<?= empty($d->archive) ? 'info' : 'danger' ?>"><?= empty($d->archive) ? '<i class="far fa-check-circle"></i>' : '<i class="fas fa-ban"></i>' ?></span></a>
                                            </td>
                                        </tr>
                                    <?php 
                                    } 
                                }else{
                                ?>
                                <tr>
                                    <td style="align: centre" colspan="8">No tickets found</td>
                                </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
