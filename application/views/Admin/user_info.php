
<style>
    .fasCd {
        opacity: 1 !important;
        line-height: 50px !important;
        margin-left: -5px !important;
        font-size: 50px !important;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- Begin: life time stats -->
                <div class="portlet light portlet-fit portlet-datatable bordered">
                    <div class="portlet-body">
                        <div class="tab-content">
                            <div class="tab-pane active" id="tab_1">
                                <div class="row">

                                    <div class="col-md-12 col-sm-12">
                                        <div class="portlet blue-hoki box">
                                            <div class="portlet-title">
                                                <div class="caption">
                                                    <i style="font-size: 24px;margin-top: 2px;" class="fas fa-user"></i>Vendor details</div>
                                            </div>
                                            <div class="portlet-body">
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> User Name: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $UserInfo->first_name . " " . $UserInfo->last_name ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Customer ID: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= $UserInfo->customerId?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Email ID : </div>
                                                    <div class="col-md-3 value"> 
                                                        <?= $UserInfo->email ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Mobile Number : </div>
                                                    <div class="col-md-3 value"> 
                                                        <?= $UserInfo->mobile ?> 
                                                    </div>
                                                </div>

                                                <?php
                                                if($UserInfo->type == 1){
                                                    $type = "Individual";
                                                }else if($UserInfo->type == 2){
                                                    $type = "Company";
                                                }else if($UserInfo){
                                                    $type = "Small Business";
                                                }else if($UserInfo->type == 4){
                                                    $type = "Broker";
                                                }else{
                                                    $type = "Ecommerce";
                                                }
                                                ?>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Registered As : </div>
                                                    <div class="col-md-3 value"> 
                                                        <?= $type ?> 
                                                    </div>
                                                </div>
                                                
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Registration date: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?= date('M d, Y H:i:s A', strtotime($UserInfo->timestamp)) ?>
                                                    </div>
                                                </div>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> Verification Status: </div>
                                                    <div class="col-md-7 value"> 
                                                        <?php
                                                        if ($UserInfo->is_verify == 1) {
                                                        ?>
                                                            <span style="cursor: pointer;" >Verified User</span>
                                                            <?php
                                                        } else {
                                                            ?>
                                                            <span style="cursor: pointer;" >Verification Pending</span>
                                                            <?php
                                                        }
                                                        ?>
                                                    </div>
                                                </div>
                                                <?php
                                                if($UserInfo->license != '') 
                                                {
                                                ?>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> License: </div>
                                                    <div class="col-md-7 value"> 
                                                        <img src="<?=base_url()?>uploads/Register/<?=$UserInfo->license?>" width="100" height="100">
                                                    </div>
                                                </div>
                                                <?php
                                                }
                                                if($UserInfo->VAT != ''){
                                                ?>
                                                <div class="row static-info">
                                                    <div class="col-md-5 name"> VAT: </div>
                                                    <div class="col-md-7 value"> 
                                                        <img src="<?=base_url()?>uploads/Register/<?=$UserInfo->VAT?>" width="100" height="100">
                                                    </div>
                                                </div>
                                                <?php
                                                }
                                                ?>
                                            </div>
                                        </div>
                                    </div>
                            </div>

                        </div>
                    </div>
                </div>
                <!-- End: life time stats -->
            </div>
        </div>
    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
