
<style>
    .permission-div li{
    list-style:none;
}
.well {
    position: relative;
    display: block;
    min-height: 20px;
    padding: 10px;
    margin-bottom: 0;
    background-color: rgb(255, 255, 255);
    border: 1px solid #c2cad8 !important;
 }
.item {
    display: inline-block;
    padding: .25rem;
    width: 100%;
}
.permission-div {
	 -moz-column-width: 25em;
	 -webkit-column-width: 25em;
	 -moz-column-gap: 1em;
	 -webkit-column-gap:1em;
	 
	 
}

.permission-div .well > ul {
    margin-left: 0;
    padding-left: 0;
}


</style>
<?php
$userId = !empty($_REQUEST['userId']) ? base64_decode($_REQUEST['userId']) : '';
//$usersArray = GetInventoryusersById($userId);
$Session =  $this->session->userdata('Admin');
$ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;
if (empty($Session->id)) {
    redirect(base_url());
}
if (!in_array($Session->id, $ADMIN_PERMISSION_ARRAY)) {
    redirect(base_url());
}
?>

<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                              <span class="caption-subject bold uppercase">Permission Settings </span>
                        </div>
                    </div>
                <input type="hidden" id="user_id" value="<?= $userId ?>">
                        <input type="hidden" id="baseurl" value="<?= base_url(); ?>">
                     <div class="portlet-body form">
                         
                         
  <div class="col-md-12 permissions">
      
           <h5><strong>Permissions</strong></h5><br>
           
           <input type="checkbox" id="select_all"  class="chk_boxes" value="" name="menu" />
           All Permission
           
            <div class="permission-div __ctmpermission">
                                    <?php
                                     $sql1 = "SELECT * FROM user_permission where user_id='".$userId."'";
                                     $MenuArray = $this->Database->select_qry_array($sql1);
                                     $result_list = array();
                                    

                                    for ($i = 0; $i < count($MenuArray); $i++) {
                                     $d = $MenuArray[$i];

                                       $result_list[] = $MenuArray[$i]->menu_id;
                                    }
                                     
                                    $sql = "SELECT * FROM sidebar_menu  where parent='0' and archive='0' ORDER BY order_view ASC";

                                    $query =$this->Database->select_qry_array($sql);
                                    for ($i = 0; $i < count($query); $i++) {
                                     $d_p = $query[$i];
                                    ?>
                                    <div class="item">
                                      <div class="well">
                                        <ul>
                                        <li>
                                <input type="checkbox" class="checkbox-per permission"  MenuId="<?= $d_p->menu_id ?>" value="<?php echo $d_p->menu_id ?>" name="menu[]"  <?php if (in_array($d_p->menu_id,$result_list)) {?>  checked <?php }?> />
                                <?php echo $d_p->menu_name?>  
                                
                                
                                <ul>
                                    <?php 
                                  
                                $menu_json = json_decode($d_p->menu_json, true);

                                
                                $sql1_per = "SELECT * FROM user_permission where user_id='".$userId."' and menu_id='".$d_p->menu_id."' ";
                                $MenuArray_per = $this->Database->select_qry_array($sql1_per); 
                                $d_per= !empty($MenuArray_per)?$MenuArray_per[0]:'';
                                $perjson = !empty($d_per)? json_decode($d_per->additional_json, true):'';
                                $perjson=!empty($perjson) && is_array($perjson) ? $perjson : [];
                           if (!empty($menu_json))
                          {
                                            for ($j = 0; $j < count($menu_json); $j++) {
                                                $md = $menu_json[$j];
                                                $checked = '';
                                    ?>
                                    <li>
                                  
                                    <input  <?php if (in_array($md['actionId'], $perjson)) {?>  checked <?php }?>  class="permissionaction menuid<?= $d_p->menu_id ?> checkbox-per" type="checkbox" MenuId="<?= $d_p->menu_id ?>" actionId="<?= $md['actionId'] ?>"> <?= $md['actionName'] ?>

                                    </li>
                                    <?php 
}
                                    } ?>
                                <?php 
                           
                                 $sql_sub = "SELECT * FROM sidebar_menu  where parent='".$d_p->menu_id."'  and archive='0' ORDER BY order_view ASC";

                                    $query_sub =$this->Database->select_qry_array($sql_sub);?>
                                     
                                     <?php  for ($p = 0; $p < count($query_sub); $p++) {
                                                $md_sub = $query_sub[$p];?>
                                <li>
                                  
                                    <input type="checkbox"   MenuId="<?= $md_sub->menu_id ?>" class="checkbox-per permission" value="<?php echo $md_sub->menu_id?>" name="menu[]"<?php if (in_array($md_sub->menu_id,$result_list)) {?>  checked <?php }?> />
                                    <?php echo $md_sub->menu_name?>  
                                     <ul>
                                    <?php 
                                  
                                     $menu_json_sub = json_decode($md_sub->menu_json, true);
                                     $sql2_per = "SELECT * FROM user_permission where user_id='".$userId."' and menu_id='".$md_sub->menu_id."' ";
                                     $MenuArray2_per = $this->Database->select_qry_array($sql2_per); 
                                    $d_per2= !empty($MenuArray2_per)?$MenuArray2_per[0]:'';
                                $perjson2 = !empty($d_per2)? json_decode($d_per2->additional_json, true):"";
                                $perjson2=!empty($perjson2) && is_array($perjson2) ? $perjson2 : [];
                               
                                   
                                    $result_action = array();
                                 if (!empty($menu_json_sub))
                          {
                                     for ($s = 0; $s < count($menu_json_sub); $s++) {
                                                $md_s = $menu_json_sub[$s];
                                                $checked = '';
                                              
                                    ?>
                                    <li>

                            <input  <?php if (in_array($md_s['actionId'], $perjson2)) {?>  checked <?php }?>  class="permissionaction menuid<?= $md_sub->menu_id ?> checkbox-per" type="checkbox" MenuId="<?= $md_sub->menu_id ?>" actionId="<?= $md_s['actionId'] ?>"> <?= $md_s['actionName'] ?>

                                    </li>
                                    <?php 
                                    } } ?>
                                    </ul>
                                </li>
                                <?php }?>
                              </ul>
                            </li>
                          </ul>
                       </div>
                       </div>
                      <?php }?>
          </div>
</div> 
                </div>
             <div class="form-actions">
                                <div class="row">
                                    <div class="  col-md-12">
                                        <button type="button" id="Savepermission" class="btn btn-success ">Submit</button>
                                    </div>
                                </div>
                            </div>
                  
               </div>
                </div>
            </div>
        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
</div>
<!-- END CONTENT -->
