<style>
    #loader {      
        display: none;      
        position: fixed;      
        z-index: 999;      
        height: 2em;      
        width: 2em;      
        overflow: show;      
        margin: auto;      
        top: 0;      
        left: 0;      
        bottom: 0;      
        right: 0;    
    }    
    #loader:before {      
        content: '';      
        display: block;      
        position: fixed;      
        top: 0;      
        left: 0;      
        width: 100%;      
        height: 100%;      
        background-color: rgba(0,0,0,0.3);    
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <div id="loader" style="display:none;"><img src="<?= base_url('images/loading.gif') ?>"/></div>
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">New Recharge Request</span>

                        </div>
                    </div>

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover commontableXXXX DataTableClass"> 
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line("sl_no") ?> Sl No</th>
                                    <th><?php echo $this->lang->line("full_name") ?> Full Name</th>
                                    <th><?php echo $this->lang->line("email") ?> Customer Type</th>
                                    <th><?php echo $this->lang->line("mobile_number") ?>Recharge Amount </th>
                                    <th><?php echo $this->lang->line("mobile_number") ?>Request Type </th>
                                    <th>Approved Status</th>
                                    <th><?php echo $this->lang->line("action") ?>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $qry = "SELECT wallet_request.*,register.first_name,register.last_name,register.email,register.type,register.mobile FROM `wallet_request` LEFT JOIN register 
                                ON register.id=wallet_request.user_id WHERE wallet_request.archive='0' and wallet_request.is_approved='0' ORDER BY wallet_request.id DESC";
                                $request = $this->Database->select_qry_array($qry);
                                for ($i = 0; $i < count($request); $i++) {
                                    $d = $request[$i];
                                    if($d->type == 1){
                                        $type = "Individual";
                                    }else if($d->type == 2){
                                        $type = "Company";
                                    }else if($d->type == 3){
                                        $type = "Small Business";
                                    }else if($d->type == 4){
                                        $type = "Broker";
                                    }else{
                                        $type = "Ecommerce";
                                    }
                                    
                                    if($d->request_type == 1){
                                        $request_type = "Normal Request";
                                    }else{
                                        $request_type = "Offer Request";
                                    }
                                    ?>
                                    <tr>
                                        <td><?= $i + 1; ?></td>
                                        <td><?= $d->first_name . " " . $d->last_name ?></td>
                                        <td><?= $type ?></td>
                                        <td>AED <?= $d->recharge_amount ?></td>
                                        <td><?= $request_type ?></td>
                                        <td><span class="label label-sm label-<?= !empty($d->is_approved) ? 'success' : 'danger' ?>"><?= !empty($d->is_approved) ? 'Approved' : 'Pending' ?></span></td>
                                        <td>
                                            <a href="<?= base_url('Admin/request_info/' . base64_encode($d->id)) ?>" Id="<?= $d->id ?>"><span class="label label-sm label-success"><i class="fa fa-eye" aria-hidden="true" title="View" ></i></span></a>
                                           <?php if(empty($d->is_approved)){ ?>
                                            <a redrurl="walletRequest" class="commonApprove" table="wallet_request" Id="<?= $d->id ?>" user="<?=$d->user_id?>"><span class="label label-sm label-success"><i class="fa fa-check" aria-hidden="true" title="Approve" ></i></span></a> 
                                            <a redrurl="walletRequest" class="commonReject" table="wallet_request" Id="<?= $d->id ?>" user="<?=$d->user_id?>"><span class="label label-sm label-danger"><i class="fas fa-ban" title="Reject"></i></span></a>
                                           <?php } ?>
                                        </td>
                                    </tr>
                                <?php } ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
