<style>
    .__tamut label {
        padding: 4px 5px;
        text-align: center;
        color: #3598dc;
        font-weight: 600;
        border: #3598dc 1px solid;
        font-size: 13px;
    }
    
    .__tamut{
            /*width: 75%;*/
    }
    
  /*  .portlet.light .dataTables_wrapper .dt-buttons {
           margin-top: -78px;
    } */
</style><?php
$Session = $this->session->userdata('Admin');
$ADMIN_PERMISSION_ARRAY = ADMIN_PERMISSION_ARRAY;

?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">WALLET TRANSACTION REPORT</span>
                        </div>
                    </div>
                    <!--<form method="post" action="">  -->
                    <!--    <div class="panel-group accordion" id="accordion1">-->
                    <!--        <div class="panel panel-default">-->
                    <!--            <div class="panel-heading">-->
                    <!--                <h4 class="panel-title">-->
                    <!--                    <a class="advance_filter accordion-toggle collapsed" data-toggle="collapse" data-parent="#accordion1" href="#collapse_1" aria-expanded="false"> Advance Filter </a>-->
                    <!--                </h4>-->
                    <!--            </div>-->
                    <!--            <div id="collapse_1" class="panel-collapse collapse" aria-expanded="false" style="height: 0px;">-->
                    <!--                <div class="panel-body">-->
                    <!--                    <div class="col-sm-3">-->
                    <!--                        <div class="form-group">-->
                    <!--                            <label>Transaction No</label>-->
                    <!--                            <input name="Filterrequestno" value="<?= !empty($Filter) ? $Filter['Filterrequestno'] : '' ?>" type="text" class="form-control " autocomplete="off">-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="col-sm-3">-->
                    <!--                        <div class="form-group">-->
                    <!--                            <label>Request Type</label>-->
                    <!--                            <select name="Filtergarage" class="form-control" autocomplete="off">-->
                    <!--                                <option value=''>Select Type</option>-->
                    <!--                                <option value="1">Normal Request</option>-->
                    <!--                                <option value="2">Offer Request</option>-->
                    <!--                            </select>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="col-sm-3">-->
                    <!--                        <div class="form-group">-->
                    <!--                            <label>Payment Type</label>-->
                    <!--                            <select name="Filtergarage" class="form-control" autocomplete="off">-->
                    <!--                                <option value=''>Select Type</option>-->
                    <!--                                <option value="1">Card Payment</option>-->
                    <!--                                <option value="2">Account Transafer</option>-->
                    <!--                            </select>-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                    <div class="col-sm-3">-->
                    <!--                        <div class="form-group">-->
                    <!--                            <label style="width: 100%;">&nbsp; </label>-->
                    <!--                            <input type="submit" name="Filter" class="btn blue" value="search">-->
                    <!--                            <input type="button" onclick="window.location = '<?= base_url('Admin/walletTransactionReport'); ?>';"  class="btn blue" value="Clear">-->
                    <!--                        </div>-->
                    <!--                    </div>-->
                    <!--                </div>-->
                    <!--            </div>-->
                    <!--        </div>-->
                    <!--    </div>-->
                    <!--</form>-->

                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Customer Name</th>
                                    <th>Customer Phone</th>
                                    <th>Customer Type</th>
                                    <th>Request Type</th>
                                    <th>Transaction No</th>
                                    <th>Date</th>
                                    <th>Payment Type</th>
                                    <th>Payment Status</th>
                                    <th>Total Amount</th>
                                </tr>
                            </thead>
                            <tbody> 
                            <?php
                            $Qry = "SELECT * FROM  wallet_transactions";
                            $walletArray = $this->Database->select_qry_array($Qry);
                            for ($i = 0; $i < count($walletArray); $i++) {
                                $d = $walletArray[$i];
                                $customer = GetUserById($d->user_id);
                                if($d->payment_type == 1){
                                    $request = GetCardRequestById($d->card_request_id);
                                }
                                if($d->payment_type == 2){
                                    $request = GetRequestById($d->transfer_request_id);
                                }
                                
                                $offer = GetOfferByOfferId($d->offer_id);
                                if($customer[0]->type == 1){
                                    $type = "Individual";
                                }else if($customer[0]->type == 2){
                                    $type = "Company";
                                }else if($customer[0]->type == 3){
                                    $type = "Small Business";
                                }else if($customer[0]->type == 4){
                                    $type = "Broker";
                                }else{
                                    $type = "Ecommerce";
                                }
                                
                                if($request[0]->request_type == 1){
                                    $request = "Normal Request";
                                }else{
                                    $request = "Offer request";
                                }
                                
                                if($d->payment_type == 1){
                                    $payment = "Card";
                                }
                                if($d->payment_type == 2){
                                    $payment = "Bank Transfer";
                                }
                                
                                if($d->payment_status == 1){
                                    $payment_status = "Paid";
                                }else{
                                    $payment_status = "Pending";
                                }
                                ?>
                                <tr>
                                    <td><?= $i + 1; ?></td>
                                    <td><?=$customer[0]->first_name . " " . $customer[0]->last_name?></td>
                                    <td><?=$customer[0]->mobile?></td>
                                    <td><?=$type?></td>
                                    <td><?=$request?></td>
                                    <td><?=$d->transaction_id?></td>
                                    <td><?=date('d-m-Y', strtotime($d->timestamp))?></td>
                                    <td><?=$payment?></td>
                                    <td><?=$payment_status?></td>               
                                    <td><?=DecimalAmount($d->amount)?></td>               
                                </tr>
                                <?php
                                    }
                                    ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->
