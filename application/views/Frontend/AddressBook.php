
<?php
$sessionAr = GetSessionArrayLogin();

if (empty($sessionAr)) {
    redirect(base_url('login'));
}

/*
  if (isset($_POST['senderSearchBtn'])) {
  $session = GetSessionArrayLogin();
  $name = $_POST['senderSearchFullName'];
  $phone = $_POST['senderSearchPhone'];
  $country = $_POST['senderSearchCountry'];
  $city = $_POST['senderSearchCity'];
  $cond = '';
  if ($_POST['senderSearchFullName'] != '') {
  $cond .= " AND full_name='$name'";
  }
  if ($_POST['senderSearchPhone'] != '') {
  $cond .= " AND mobile='$phone'";
  }
  if ($country != '') {
  $cond .= " AND country='$country'";
  }
  if ($city != '') {
  $cond .= " AND city='$city'";
  }

  $qry = "SELECT * FROM sender_address WHERE user_id=$session->id AND archive=0 $cond";
  $resultArray = $this->Database->select_qry_array($qry);
  $senderAddress = $resultArray;
  } else {
  $senderAddress = GetAllSenderAddress($user_id);
  }

  if (isset($_POST['recipientSearchBtn'])) {
  $session = GetSessionArrayLogin();
  $name = $_POST['recipientSearchFullName'];
  $phone = $_POST['recipientSearchPhone'];
  $country = $_POST['recipientSearchCountry'];
  $city = $_POST['recipientSearchCity'];
  $cond = '';
  if ($_POST['senderSearchFullName'] != '') {
  $cond .= " AND full_name='$name'";
  }
  if ($_POST['senderSearchPhone'] != '') {
  $cond .= " AND mobile='$phone'";
  }
  if ($country != '') {
  $cond .= " AND country='$country'";
  }
  if ($city != '') {
  $cond .= " AND city='$city'";
  }

  $qry = "SELECT * FROM recipient_address WHERE user_id=$session->id AND archive=0 $cond";
  $resultArray = $this->Database->select_qry_array($qry);
  $recipientAddress = $resultArray;
  } else {
  $recipientAddress = GetAllRecipientAddress($user_id);
  }
 */
$prefix = '';
?>

<!-- Modal -->
<div class="modal fade" id="addAddress" tabindex="-1"
     aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header border-0">
                <button type="button" class="close" data-dismiss="modal"aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <h3 class="modal-title clrPrm text-center pb-4"><actionTitle></actionTitle> Recipient</h3>
                <form class="form my-5">
                    <div class="form-row">

                        <input type="hidden" id="primaryId">
                        <input type="hidden" id="addressType">
                        <div class="form-row text-left" style="width: 100%;">
                            <div class="form-group col-sm-4">
                                <label for="">Full Name</label>
                                <input type="text" class="form-control brdOrg" id="full_name<?= $prefix ?>" placeholder="">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">Father Name</label>
                                <input type="text" class="form-control brdOrg" id="father_name<?= $prefix ?>" placeholder="">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">Grand Father Name</label>
                                <input type="text" class="form-control brdOrg" id="grand_father_name<?= $prefix ?>" placeholder="">
                            </div>                       
                        </div>
                        <div class="form-row text-left" style="width: 100%;">
                            <div class="form-group col-sm-4">
                                <label for="">Family Name</label>
                                <input type="text" class="form-control brdOrg" id="family_name<?= $prefix ?>" placeholder="">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">Mobile Number</label>
                                <input type="number" class="form-control brdOrg" id="mobile<?= $prefix ?>" placeholder="">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">E-mail</label>
                                <input type="email" class="form-control brdOrg" id="email<?= $prefix ?>" placeholder="">
                            </div>                       
                        </div>
                        <div class="form-row text-left" style="width: 100%;">
                            <div class="form-group col-sm-12">
                                <label for="">Address</label>
                                <input type="text" class="form-control brdOrg" id="address<?= $prefix ?>" placeholder="">
                            </div>

                        </div>
                        <div class="form-row text-left" style="width: 100%;">
                            <div class="form-group col-sm-4">
                                <label for="">Company Name</label>
                                <input type="text" class="form-control brdOrg" id="company_name<?= $prefix ?>" placeholder="">
                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">Country</label>
                                <select countryId=""  class="form-control brdOrg select2" id="country<?= $prefix ?>">
                                    <option value="">Select Country</option>
                                    <?php
                                    $country = getcountry();
                                    foreach ($country as $con) {
                                        ?>
                                        <option  value="<?= $con->code ?>"><?= $con->country ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>

                            </div>
                            <div class="form-group col-sm-4">
                                <label for="">City</label>
                                <input type="text" class="form-control brdOrg" value="" id="city<?= $prefix ?>">
                            </div>    

                            <div class="form-group col-sm-4">
                                <label for="">Post / Zip Code</label>
                                <input type="text" class="form-control brdOrg" value=""  id="zip_code<?= $prefix ?>" placeholder="">
                            </div>
                        </div>





                        <div class="form-row text-left" style="width: 100%;">
                            <button type="button"class="btn btnPrm " onclick="savemyAddress();">Submit</button>
                        </div>
                        <br>
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<section class="mt-150 px-md-0">
    <div class="container-fluid">
        <h3 class="px-lg-5 px-2 pb-3">Your Address</h3>
        <div class="row">
            <div class="col-md-12">

                <ul class="nav nav-tabs regTablink addTablink border-0 justify-content-left  px-lg-5 px-1" role="tablist">
                    <li class="nav-item mx-1">
                        <a class="nav-link active fontSmall" data-toggle="tab" href="#sender">Sender Details</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link fontSmall mx-1" data-toggle="tab" href="#recipient">Recipient Details</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content bnrTab bgWht px-lg-5 p-2">
                    <div id="sender" class="container-fluid tab-pane active"><br>
                        <?= myFilter() ?>
                        <div class="row addCard py-4" id="addressList">
                            <div class="card col dotBdr">
                                <div class="card-body text-center">
                                    <a href="javascript:void(0)" class="text-secondary add_new_address" json="" prefix="0" onclick="showAddressPopup(this)" ><img src="<?= base_url() ?>css/images/plus.svg" style="opacity: .5;">
                                        <br>
                                        Add New Recipient</a>
                                </div>
                            </div>


                            <?php
                            $Sql = "SELECT SA.*,C.country AS country_name FROM `sender_address` SA LEFT JOIN country C ON C.code LIKE SA.country WHERE SA.user_id='$sessionAr->id' AND SA.archive=0 AND SA.type='0' ";
                            $senderAr = $this->Database->select_qry_array($Sql);
                            for ($i = 0; $i < count($senderAr); $i++) {
                                $sender = $senderAr[$i];
                                ?>
                                <div class="card col">
                                    <div class="card-body">
                                        <p class="card-text m-0"><?= $sender->full_name ?><br>
                                            <small class="text-secondary"><?= $sender->city ?></small>
                                        </p>
                                        <div id="collapseTwo" class="collapse clpText" data-parent="#addressList">
                                            <p class="text-secondary"><?= $sender->country_name ?><br>
                                                Phone : <?= $sender->mobile ?><br></p>
                                            <a href="javascript:void(0)" class="card-link xsFont edit_address" json="<?= base64_encode(json_encode($sender)) ?>" prefix="0" onclick="showAddressPopup(this)" >Edit </a>
                                            <a href="#" class="card-link pl-2 ml-1 border-left xsFont delete_address" addressId="<?= $sender->id ?>" >Delete</a>
                                        </div>
                                    </div>
                                    <div class="viewMore">
                                        <a class="collapsed card-link float-right xsFont" data-toggle="collapse"
                                           href="#collapseTwo">
                                            View More
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>
                    </div>

                    <!-----------------------------------------------------Recipient Section--------------------------------------------------------->

                    <div id="recipient" class="container-fluid tab-pane fade"><br>
                        <?= myFilter() ?>
                        <div class="row addCard py-4" id="addressList">
                            <div class="card col dotBdr">
                                <div class="card-body text-center">
                                    <a href="javascript:void(0)" class="text-secondary add_recipient_address" json="" prefix="1" onclick="showAddressPopup(this)" ><img src="<?= base_url('css/images/plus.svg') ?>" style="opacity: .5;">
                                        <br>
                                        Add New Recipient</a>
                                </div>
                            </div>

                            <?php
                            $Sql = "SELECT SA.*,C.country AS country_name FROM `sender_address` SA LEFT JOIN country C ON C.code LIKE SA.country WHERE SA.user_id='$sessionAr->id' AND SA.archive=0 AND SA.type='1' ";
                            $recipientAr = $this->Database->select_qry_array($Sql);
                            for ($i = 0; $i < count($recipientAr); $i++) {
                                $recipient = $recipientAr[$i];
                                ?>
                                <div class="card col">
                                    <div class="card-body">
                                        <p class="card-text m-0"><?= $recipient->full_name ?><br>
                                            <small class="text-secondary"><?= $recipient->city ?></small>
                                        </p>
                                        <div id="collapseTwo" class="collapse clpText" data-parent="#addressList">
                                            <p class="text-secondary"><?= $recipient->country_name ?><br>
                                                Phone : <?= $recipient->mobile ?><br></p>
                                            <a href="javascript:void(0)" class="card-link xsFont edit_recipient_address" json="<?= base64_encode(json_encode($recipient)) ?>" prefix="1" onclick="showAddressPopup(this)">Edit </a>
                                            <a href="#" class="card-link pl-2 ml-1 border-left xsFont delete_recipient_address" addressId="<?= $recipient->id ?>" >Delete</a>
                                        </div>
                                    </div>
                                    <div class="viewMore">
                                        <a class="collapsed card-link float-right xsFont" data-toggle="collapse"
                                           href="#collapseTwo">
                                            View More
                                        </a>
                                    </div>
                                </div>
                                <?php
                            }
                            ?>
                        </div>

                    </div>
                </div>
            </div>
        </div>
</section>
<style>
    .select2-container{
        display: block !important;
    }
    .select2-container .select2-selection--single{
        height: 37px !important;
    }
    .select2-selection__rendered {
        line-height: 33px !important;
    }
</style>
<?php

function myFilter() {
    ?>
    <form class="row" method="POST">
        <div class="form-row" style="width: 100%;">
            <div class="form-group col-sm-2">
                <input type="text" class="form-control brdOrg" id="senderSearchFullName" name="senderSearchFullName"
                       placeholder="First & Last Name">
            </div>
            <div class="form-group col-sm-2">
                <input type="number" class="form-control brdOrg" id="senderSearchPhone" name="senderSearchPhone"
                       placeholder="Phone Number">
            </div>
            <div class="form-group col-sm-2">
                <input type="text" class="form-control brdOrg" id="senderSearchCountry" name="senderSearchCountry" placeholder="Country">
            </div>
            <div class="form-group col-sm-2">
                <input type="text" class="form-control brdOrg" id="senderSearchCity" name="senderSearchCity" placeholder="City">
            </div>
            <div class="form-group col-auto ml-auto">
                <button type="submit"
                        class="btn btnPrm rounded btnRgt mr-2 px-4" id="senderSearchBtn" name="senderSearchBtn">Search</button>
            </div>
            <div class="form-group col-auto ml-auto">
                <button type="submit"
                        class="btn btnSec bg-white text-dark rounded btnRgt mr-2 px-4 noWrap">A -
                    Z</button>
            </div>
        </div>
    </form>
    <?php
}
?>
