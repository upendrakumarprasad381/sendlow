<?php
$sessionAr = GetSessionArrayLogin();
if (empty($sessionAr)) {
    redirect(base_url('login'));
}
?>
<section class="mt-150 py-7">
    <div class="container-fluid px-lg-5 px-3">
        <div class="row mb-4">
            <div class="col-md-8 crdAlgn">
                <img src="<?= $sessionAr->LogoFullURL ?>" class="round shadowL" width="200px">
                <h4 class="px-lg-5 px-3 my-3">Hi, Welcome back<br><small><?= $sessionAr->first_name . " " . $sessionAr->last_name ?></small></h4>
            </div>
            <div class="col-md-4 m-auto">
                <h4 class="semiBold text-lg-right text-center clrPrm" type="button" data-toggle="modal" data-target="#exampleModal"><img src="<?= base_url() ?>css/images/dashboardB.svg">&nbsp;Manage Dashboard</h4>
            </div>

            <!-- ----------------------------- Modal ------------------------- -->
            <!-- Modal -->
            <div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog modal-lg">
                    <div class="modal-content">
                        <div class="modal-header border-0">                         
                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">&times;</span>
                            </button>
                        </div>
                        <div class="modal-body" id="checkboxGroup">
                            <h3 class="modal-title clrPrm text-center pb-4" id="">Manage Dashboard<br><small class="xsFont text-secondary">Choose up to 5 items to appear</small></h3>
                               <!-- <input type="checkbox" name='hide_columns[]' value='0'> Tracking Id 
                                <input type="checkbox" name='hide_columns[]' value='1'> Status 
                                <input type="checkbox" name='hide_columns[]' value='2'> Name 
                                <input type="checkbox" name='hide_columns[]' value='3'> Country 
                                <input type="checkbox" name='hide_columns[]' value='4'> City
                                <input type="button" id="but_showhide" value='Show/Hide'> -->

                            <div class="form-row">
                                <div class="form-group col-sm-3">
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input1 check" id="check_1" name="check[]" value="1">
                                        <label class="custom-control-label1" for="recipient_name">Recipient Name</label>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3">
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input1 check" id="check_2" name="check[]" value="2">
                                        <label class="custom-control-label1" for="recipient_country">Recipient Country</label>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3">
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input1 check" id="check_3" name="check[]" value="3">
                                        <label class="custom-control-label1" for="recipient_city">Recipient City</label>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3">
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input1 check" id="check_4" name="check[]" value="4">
                                        <label class="custom-control-label1" for="sender_name">Sender Name</label>
                                    </div>
                                </div>
                            </div>
                            <div class="form-row">
                                <div class="form-group col-sm-3">
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input1 check" id="check_5" name="check[]" value="5">
                                        <label class="custom-control-label1" for="sender_country">Sender Country</label>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3">
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input1 check" id="check_6" name="check[]" value="6">
                                        <label class="custom-control-label1" for="sender_city">Sender City</label>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3">
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input1 check" id="check_7" name="check[]" value="7">
                                        <label class="custom-control-label1" for="received_by">Received By</label>
                                    </div>
                                </div>
                                <div class="form-group col-sm-3">
                                    <div class="custom-control custom-checkbox mb-3">
                                        <input type="checkbox" class="custom-control-input1 check" id="check_8" name="check[]" value="8">
                                        <label class="custom-control-label1" for="chargeable_weight">Chargeable weight </label>
                                    </div>
                                </div>
                            </div> 
                        </div>

                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="card  col-lg col-xs-6 border-0 bdrBtm mx-lg-2 mx-3 rounded-0">
                <div class="row g-0">
                    <div class="col-auto p-1 py-2">
                        <span class="material-icons text-danger">error</span>
                    </div>
                    <div class="col p-0">
                        <div class="card-body p-0 fontSmall">
                            <p class="card-text m-0">0</p>
                            <p class="card-text m-0">UNABLE TO DELIVER</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card  col-lg col-xs-6 border-0 bdrBtm mx-3 rounded-0">
                <div class="row g-0">
                    <div class="col-auto p-1 py-2">
                        <span class="material-icons text-success">check_circle</span>
                    </div>
                    <div class="col p-0">
                        <div class="card-body p-0 fontSmall">
                            <p class="card-text m-0">0</p>
                            <p class="card-text m-0">DELIVERED</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card  col-lg col-xs-6 border-0 bdrBtm mx-3 rounded-0">
                <div class="row g-0">
                    <div class="col-auto p-1 py-2">
                        <span class="material-icons clrPrm">arrow_circle_right</span>
                    </div>
                    <div class="col p-0">
                        <div class="card-body p-0 fontSmall">
                            <p class="card-text m-0">0</p>
                            <p class="card-text m-0">OUT FOR DELIVERY</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card  col-lg col-xs-6 border-0 bdrBtm mx-3 rounded-0">
                <div class="row g-0">
                    <div class="col-auto p-1 py-2">
                        <span class="material-icons text-danger">error</span>
                    </div>
                    <div class="col p-0">
                        <div class="card-body p-0 fontSmall">
                            <p class="card-text m-0">0</p>
                            <p class="card-text m-0">UNABLE TO DELIVER</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card  col-lg col-xs-6 border-0 bdrBtm mx-3 rounded-0">
                <div class="row g-0">
                    <div class="col-auto p-1 py-2">
                        <span class="material-icons text-success">check_circle</span>
                    </div>
                    <div class="col p-0">
                        <div class="card-body p-0 fontSmall">
                            <p class="card-text m-0">0</p>
                            <p class="card-text m-0">DELIVERED</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card  col-lg col-xs-6 border-0 bdrBtm mx-3 rounded-0">
                <div class="row g-0">
                    <div class="col-auto p-1 py-2">
                        <span class="material-icons clrPrm">arrow_circle_right</span>
                    </div>
                    <div class="col p-0">
                        <div class="card-body p-0 fontSmall">
                            <p class="card-text m-0">0</p>
                            <p class="card-text m-0">OUT FOR DELIVERY</p>
                        </div>
                    </div>
                </div>
            </div>
            <div class="card  col-lg col-xs-6 border-0 bdrBtm mx-lg-2 mx-3 rounded-0">
                <div class="row g-0">
                    <div class="col-auto p-1 py-2">
                        <span class="material-icons text-danger">error</span>
                    </div>
                    <div class="col p-0">
                        <div class="card-body p-0 fontSmall">
                            <p class="card-text m-0">0</p>
                            <p class="card-text m-0">DRAFT</p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row pt-5">
            <div class="table-responsive">
                <table style="width: 100%" class='display datatable'>
                    <thead class="border-0">
                        <tr>
                            <th>Sl</th>
                            <th>TRACKING ID</th>
                            <th>STATUS</th>
                            <th>RECIPIENT NAME </th>
                            <th>RECIPIENT ADDRESS</th>
                            <th>SENDER NAME </th>
                            <th>SENDER ADDRESS</th>
                            <th>WEIGHT</th>
                            <th>AMOUNT</th>
                            <th>ACTION</th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $join=" LEFT JOIN sender_address SEN ON SEN.id=B.sender_id LEFT JOIN sender_address REC ON REC.id=B.receiver_id LEFT JOIN country CS ON CS.code=SEN.country LEFT JOIN country CR ON CR.code=REC.country";
                        $select=",SEN.full_name AS sen_name,CONCAT(SEN.city,', ',SEN.zip_code,' - ',CS.country) AS sen_address,CONCAT(REC.city,', ',REC.zip_code,' - ',CS.country) AS rec_address,REC.full_name AS rec_name";
                        $Sql = "SELECT B.* $select FROM `booking` B $join WHERE B.user_id='$sessionAr->id' AND B.archive=0 ORDER BY B.booking_id DESC LIMIT 3000";
                        $dArray = $this->Database->select_qry_array($Sql);
                        for ($i = 0; $i < count($dArray); $i++) {
                            $d = $dArray[$i];
                            ?>
                            <tr>
                                <td class="pl-0"><?= $i + 1 ?></td>
                                <td class="pl-0"><?= $d->booking_no ?></td>
                                <td>In-process</td>
                                <td><?= $d->sen_name ?></td>
                                <td><?= $d->sen_address ?></td>
                                <td><?= $d->rec_name ?></td>
                                <td><?= $d->rec_address ?></td>
                                <td><?= $d->weight ?> Kg <?= $d->booking_type ?></td>
                                <td><?= DecimalAmount($d->total) ?></td>
                                <td><a href=""><i class="fa fa-external-link"></i></a></td>
                            </tr>
                        <?php } ?>
                    </tbody> 
                </table>
            </div>
        </div>

    </div>
</section>

