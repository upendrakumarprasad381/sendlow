
<section class="mt-150 py-7">
    <div class="container">
        <form>
            <input type="text" id="user_email_page" class="fadeIn second" name="login" placeholder="login">
            <input type="password" id="user_password_page" class="fadeIn third" name="login" placeholder="password">
            <input type="button" id="login_submit_page" class="fadeIn fourth" value="Log In">
            <p class="login_message"></p>
        </form>
        <!-- Remind Passowrd -->
        <div id="formFooter">
            <a class="underlineHover" href="#">Forgot Password?</a>
            <a class="underlineHover" href="#">CREATE ACCOUNT</a>
        </div>
    </div>
</section>