
<?php
$qry = "SELECT * FROM `home_banner` WHERE status=0 AND archive=0";
$bannerArray = $this->Database->select_qry_array($qry);
$bg = $bannerArray[0]->image;
?>

<section class="jumbotron mt-150 bnr py-7" style="background-image:  url(<?= base_url() ?>uploads/home_banner/<?= $bannerArray[0]->image ?>)"> 
    <div class="container ">
        <div class="row">
            <div class="offset-lg-3 col-lg-9">
                <h1><?= $bannerArray[0]->text ?></h1>
            </div>
            <div class="offset-lg-3 col-lg-9 pt-5">
                <ul class="nav nav-tabs bnrTabLink  border-0" role="tablist">
                    <li class="nav-item ml-2 mr-1">
                        <a class="nav-link searchTab active" attr="DOCUMENTS" data-toggle="tab" href="#package">DOCUMENTS</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link searchTab" attr="PACKAGE" data-toggle="tab" href="#package">PACKAGE</a>
                    </li>
                </ul>
                <!-- Tab panes -->
                <div class="tab-content bnrTab">

                    <div id="package" class="container tab-pane active"><br>
                        <p class="semiBld text-dark">Where you are sending your shipment From/To</p>
                        <form class="position-relative" name="packageForm" id="packageForm" method="POST" onsubmit="return false;">

                            <div class="form-row text-secondary">
                                <div class="form-group col-sm-3">
                                    <label for="packageFrom">From</label>
                                    <select class="form-control brdOrg select2" id="packageFrom" name="packageFrom">
                                        <option value="">Select Country</option>
                                        <?php
                                        $country = getcountry();
                                        foreach ($country as $con) {
                                            ?>
                                            <option code="<?= $con->code ?>" value="<?= $con->id ?>"><?= $con->country ?></option>
                                            <?php
                                        }
                                        ?>
                                    </select>
                                    <label for="packageCityFrom">City</label>
                                    <select class="form-control brdPrpl select2"  id="packageCityFrom" autocomplete="off" name="packageCityFrom">
                                        <option value="">--select--</option>

                                    </select>

                                    <label for="packageCityTo">Postal Code</label>
                                    <input type="number"  class="form-control brdPrpl"  autocomplete="off"  id="packageFromPincode" name="packageFromPincode">
                                </div>
                                <div class="form-group col-sm-3">
                                    <label for="packageTo">To</label>
                                    <select class="form-control brdOrg select2" id="packageTo" name="packageTo">
                                        <option value="">Select Country</option>

                                    </select>

                                    <label for="packageCityTo">City</label>
                                    <select class="form-control brdPrpl select2"  autocomplete="off"  id="packageCityTo" name="packageCityTo">
                                        <option value="">--select--</option>
                                    </select>



                                    <label for="packageCityTo">Postal Code</label>
                                    <input type="number"  class="form-control brdPrpl"  autocomplete="off"  id="packageToPincode" name="packageToPincode">

                                </div>
                                <div class="form-group col-sm-6 packageDiv" id="div_1">
                                    <label for="packageWeight">Enter the weight &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;</label>
                                    <select class="brdOrg form-control frmCtrlFxd " id="packageWeight" name="packageWeight">
                                        <?php
                                        $weightHtml = '<option value="">--select--</option>';
                                        for ($i = 1; $i <= MAX_WEIGHT; $i++) {
                                            $fullweight = $i;
                                            $decimal = $i . '.' . 5;

                                            $weightHtml = $weightHtml . '<option value="' . $fullweight . '">' . $fullweight . ' Kg</option>';
                                            $weightHtml = $weightHtml . '<option value="' . $decimal . '">' . $decimal . ' Kg</option>';
                                            ?> <option value="<?= $fullweight ?>"><?= $fullweight ?> Kg</option>
                                            <option value="<?= $decimal ?>"><?= $decimal ?> Kg</option>
                                            <?php
                                        }
                                        ?>

                                    </select>
                                </div>
                                <button style="display: none;" class="btn btnSec rounded mT32 form-control  mb-2 absBtn" type="button" id="addPackage">Add Package</button>
                                <button class="btn btnPrm rounded mT100 form-control mb-4 absBtn" id="getLowestPrice" button="button">Get Lowest Price</button>
                            </div>
                            <script>
                                var weightOption = '<?= $weightHtml ?>';
                            </script>
                            <div class="form-row text-secondary" id="packageList">


                            </div>


                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container ">
        <div class="row py-3">
            <div class="col-sm-7">
                <h3 class="clrSec">Sign up now and enjoy <span class="clrPrm">10% *</span> discount on
                    your next shipment
                </h3>
                <p class="semiBld">Just a few steps to opening your account online and save more
                    than 45% discount every time you ship with Sendlow. <br>
                    * <a href="">Terms & Conditions </a>apply.<br><br>
                    Shipping now to Middle East , Europe & UK , India .. are cheaper &
                    faster easier with SENDLOW</p>
                <a class="btn btnPrm" href="<?= base_url() ?>registration">Open An Account</a>
            </div>
            <div class="col-sm-5 text-right pr-lg-0 pt-4 pt-lg-0">
                <?php
                $rightbanner = getHomeRightBanner();
                ?>
                <img src="<?= base_url() ?>uploads/home_banner/<?= $rightbanner[0]->image ?>" class="img-fluid">
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container bg-light my-5 py-5">
        <div class="row">
            <div class="col text-center m-auto">
                <h3 class="lh-lg py-2">Are you Looking for Professional Courier Services.<br>
                    Please Contact Us</h3>
                <button class="btn btnSec py-2 px-5 fntL">Contact Us</button>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container ">
        <h6 class="clrPrm text-center">WORK PROCESS</h6>
        <h1 class="text-center">Make It Happen In 4 Steps</h1>
        <div class="row py-5">
            <div class="col-sm-3 text-center icnLst">
                <img src="<?= base_url() ?>css/images/list.svg" class="img-fluid my-2">
                <h5 class="">Book Your Service</h5>
                <p>Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit
                    , sed do eiusmod tempor.</p>
                <img src="<?= base_url() ?>css/images/arwTrans.png" class="arw">

            </div>
            <div class="col-sm-3 text-center icnLst pt-sm-0 pt-5">
                <img src="<?= base_url() ?>css/images/deliveryBox.svg" class="img-fluid my-2">
                <h5 class="">Pack Your Good</h5>
                <p>Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit
                    , sed do eiusmod tempor.</p>
                <img src="<?= base_url() ?>css/images/arwTrans.png" class="arw">
            </div>
            <div class="col-sm-3 text-center icnLst pt-sm-0 pt-5">
                <img src="<?= base_url() ?>css/images/lorry.svg" class="img-fluid my-2">
                <h5 class="">Safe Loading</h5>
                <p>Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit
                    , sed do eiusmod tempor.</p>
                <img src="<?= base_url() ?>css/images/arwTrans.png" class="arw">
            </div>
            <div class="col-sm-3 text-center icnLst pt-sm-0 pt-5">
                <img src="<?= base_url() ?>css/images/package.svg" class="img-fluid my-2">
                <h5 class="">Safe Delivery</h5>
                <p>Lorem ipsum dolor sit amet,
                    consectetur adipiscing elit
                    , sed do eiusmod tempor.</p>

            </div>
        </div>
    </div>
</section>
<section>
    <div class="container ">
        <div class="row">
            <div class="col-sm-5 pl-lg-0">
                <?php
                $leftbanner = getHomeLeftBanner();
                ?>
                <img src="<?= base_url() ?>uploads/home_banner/<?= $leftbanner[0]->image ?>" class="img-fluid">
            </div>
            <div class="col-sm-7">
                <h1 class="">Parcel Tracking</h1>
                <p>Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,sem
                    nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                    Morbi accumsan ipsum velit
                    Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,sem
                    nibh id elit. Duis sed odio
                    sit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit
                    Aenean sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,sem
                    nibh id elit. Duis sed odio sit amet nibh vulputate cursus a sit amet mauris.
                    Morbi accumsan ipsum velit
                    s</p>
                <button class="btn btnPrm">Learn More About Tracking Your Parcel</button>

            </div>
        </div>
    </div>
</section>
<section>
    <div class="container  bg-light my-5 py-5">
        <div class="row">
            <div class="col-sm-6 text-center">
                <?php
                $exportbanner = getHomeExportBanner();
                ?>
                <img src="<?= base_url() ?>uploads/home_banner/<?= $exportbanner[0]->image ?>" width="70%">
            </div>
            <div class="col-sm-6 text-center m-auto">
                <h4>Export</h4>
                <p>Send from the UK to over 220 countries worldwide</p>
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container ">
        <div class="row">
            <div class="col-sm-6 text-center m-auto ">
                <h4>Import</h4>
                <p class="font-weight-bold text-gray">Collect from anywhere in the world and ship to
                    the UK</p>
            </div>
            <div class="col-sm-6 text-center m-auto">
                <?php
                $importbanner = getHomeImportBanner();
                ?>
                <img src="<?= base_url() ?>uploads/home_banner/<?= $importbanner[0]->image ?>" width="70%">
            </div>
        </div>
    </div>
</section>
<section>
    <div class="container bg-light my-5 py-5">
        <div id="testimonial" class="carousel slide clientSay" data-ride="carousel">
            <!-- Indicators -->
            <ul class="carousel-indicators">
                <?php
                $testimonials = getTestimonials();
                for ($i = 0; $i < count($testimonials); $i++) {
                    if ($i == 0) {
                        $active = "active";
                    } else {
                        $active = '';
                    }
                    ?>
                    <li data-target="#testimonial" data-slide-to="<?= $testimonials[$i]->id ?>" class="<?= $active ?>"></li>
                    <?php
                }
                ?>
                <!--<li data-target="#testimonial" data-slide-to="1"></li>-->
                <!--<li data-target="#testimonial" data-slide-to="2"></li>-->
            </ul>
            <!-- The slideshow -->
            <h1 class="text-center">Client Testimonials</h1>
            <div class="carousel-inner testiMonial px-lg-5 px-1">
                <?php
                $testimonials = getTestimonials();
                for ($i = 0; $i < count($testimonials); $i++) {
                    if ($i == 0) {
                        $active = "active";
                    } else {
                        $active = '';
                    }
                    ?>
                    <div class="carousel-item <?= $active ?>">
                        <img src="<?= base_url() ?>uploads/testimonials/<?= $testimonials[$i]->image ?>" alt="" width="" height="" class=" my-3">
                        <p><?= $testimonials[$i]->comment ?> </p>
                        <h6><?= $testimonials[$i]->name ?></h6>
                    </div>
                    <?php
                }
                ?>
                <!--<div class="carousel-item">-->
                <!--    <img src="<?= base_url() ?>css/images/usr.png" alt="" width="" height="" class=" my-3">-->
                <!--    <p>Aenean sollicitudin, lorem quis bibendum auctor,sit amet nibh vulputate cursus a sit amet-->
                <!--        mauris. Morbi-->
                <!--        accumsan ipsum velit nisi elit consequat ipsum,sem nibh id elit. Duis sed odio </p>-->
                <!--    <h6>Joe Mathew</h6>-->
                <!--</div>-->
                <!--<div class="carousel-item">-->
                <!--    <img src="<?= base_url() ?>css/images/usr.png" alt="" width="" height="" class=" my-3">-->
                <!--    <p>Asit amet nibh vulputate cursus a sit amet mauris. Morbi accumsan ipsum velit enean-->
                <!--        sollicitudin, lorem quis bibendum auctor, nisi elit consequat ipsum,sem nibh id elit. Duis-->
                <!--        sed odio </p>-->
                <!--    <h6>Joe Mathew</h6>-->
                <!--</div>-->
            </div>
            <!-- Left and right controls -->
            <a class="carousel-control-prev" href="#testimonial" data-slide="prev">
                <span class="carousel-control-prev-icon"></span>
            </a>
            <a class="carousel-control-next" href="#testimonial" data-slide="next">
                <span class="carousel-control-next-icon"></span>
            </a>
        </div>
    </div>
</section>
<style>
    .select2-container--default .select2-selection--single .select2-selection__rendered{
            line-height: 38px !important;
    }
    .select2-container .select2-selection--single{
           height: 40px !important; 
    }
</style>

