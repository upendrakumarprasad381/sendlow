<footer class="bg-light pt-5">
    <div class="container-fluid px-7">
        <div class="row pb-5 pb-lg-0">
            <div class="col-lg-4 col-md-6 ">
                <img src="<?= base_url() ?>css/images/LogoClr.svg" width="180px">
                <p class="py-3  px-0 px-lg-2">If you are going to use a passage of Lorem
                    Ipsum you need to be sure there isn't anything
                    embarrassing hidden in the middle of text.
                </p>
                <span class="icnFnt"><a href="http://facebook.com/" target="_blank"><i class="fa fa-facebook"
                                                                                       aria-hidden="true"></i></a></span>
                <span class="icnFnt"><a href="https://twitter.com/" target="_blank"><i class="fa fa-twitter"
                                                                                       aria-hidden="true"></i></a></span>
                <span class="icnFnt"><a href="https://www.linkedin.com/" target="_blank"><i class="fa fa-linkedin"
                                                                                            aria-hidden="true"></i></a></span>
                <span class="icnFnt"><a href="https://www.instagram.com/" target="_blank"><i class="fa fa-instagram"
                                                                                             aria-hidden="true"></i></a></span>
                <p class="py-4  px-0 px-lg-2">© 2021 All Rights Reserved by Sendlow</p>

            </div>
            <div class=" col-lg-3 col-md-6">
                <ul class="list-unstyled">
                    <li class="lh-lg">
                        <h5>USEFUL LINKS</h5>
                    </li>
                    <li class="lh-lg"><a class="text-200 text-dark" href="#!">Home</a></li>
                    <li class="lh-lg"><a class="text-200 text-dark" href="#!">My Shipment / Dashboard</a></li>
                    <li class="lh-lg"><a class="text-200 text-dark" href="#!">Send shipment</a></li>
                    <li class="lh-lg"><a class="text-200 text-dark" href="#!">Receive shipment</a></li>
                    <li class="lh-lg"><a class="text-200 text-dark" href="#!">Address Book</a></li>
                    <li class="lh-lg"><a class="text-200 text-dark" href="#!">My Wallet</a></li>
                </ul>
            </div>
            <div class=" col-lg-2 col-md-6">
                <ul class="list-unstyled">
                    <li class="lh-lg">
                        <h5>INFORMATION</h5>
                    </li>
                    <li class="lh-lg"><a class="text-200 text-dark" href="#!">FAQ</a></li>
                    <li class="lh-lg"><a class="text-200 text-dark" href="#!">About Us</a></li>
                    <li class="lh-lg"><a class="text-200 text-dark" href="#!">Delivery Information</a></li>
                    <li class="lh-lg"><a class="text-200 text-dark" href="#!">Privacy Policy</a></li>
                    <li class="lh-lg"><a class="text-200 text-dark" href="#!">Terms & Conditions </a></li>
                </ul>
            </div>
            <div class=" col-lg-3 col-md-6">
                <h5>DOWNLOAD APP</h5>
                <img src="<?= base_url() ?>css/images/QR_code.png" width="30%"><img src="<?= base_url() ?>css/images/appAdr.png" class="py-3" width="70%">
                <br>
                <img src="<?= base_url() ?>css/images/QR_code.png" width="30%"><img src="<?= base_url() ?>css/images/appApl.png" width="70%">
            </div>
        </div>
    </div>
</footer>

</body>

<?php
$Controller = $this->router->fetch_class();
$Method = $this->router->fetch_method();
$segment3 = $this->uri->segment(3);
$googleSearh = ['index'];
?>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
<link rel="stylesheet" href="https://code.jquery.com/ui/1.13.0/themes/base/jquery-ui.css">
<script src="https://code.jquery.com/ui/1.13.0/jquery-ui.js"></script>
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/css/select2.min.css" rel="stylesheet" />
<script src="https://cdn.jsdelivr.net/npm/select2@4.1.0-rc.0/dist/js/select2.min.js"></script>
<script src="<?= base_url() ?>js/jquery-confirm.min.js"></script>


<script src="<?= base_url(); ?>js/Frontend/common.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
<?php if (in_array($Method, $googleSearh)) { ?>
    <script src="https://maps.googleapis.com/maps/api/js?key=<?= MAP_API_KEY ?>&libraries=geometry,places&callback=initMap&language=en" ></script>
    <script src="<?= base_url(); ?>js/Frontend/googleSearch.js?<?php echo date('l jS \of F Y h:i:s A'); ?>"></script>
<?php } ?>
<script>
    var base_url = '<?= base_url(); ?>';
    var Controller = '<?= $Controller ?>';
    var Method = '<?= $Method ?>';
    var segment3 = '<?= $segment3 ?>';


</script>
<script>
    $(document).ready(function () {
        $('.select2').select2();
    });
    function isValidEmailAddress(emailAddress) {
        var pattern = /^([a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+(\.[a-z\d!#$%&'*+\-\/=?^_`{|}~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]+)*|"((([ \t]*\r\n)?[ \t]+)?([\x01-\x08\x0b\x0c\x0e-\x1f\x7f\x21\x23-\x5b\x5d-\x7e\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|\\[\x01-\x09\x0b\x0c\x0d-\x7f\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]))*(([ \t]*\r\n)?[ \t]+)?")@(([a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\d\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.)+([a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]|[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF][a-z\d\-._~\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF]*[a-z\u00A0-\uD7FF\uF900-\uFDCF\uFDF0-\uFFEF])\.?$/i;
        return pattern.test(emailAddress);
    }
    function getMypackageList() {
        var packageList = [];
        var weight = $('#packageWeight').val();
        var isValid = true;

        var d = {};
        d['weight'] = weight;
        packageList.push(d);
        $('.packageWeightlist').each(function () {
            var myweight = $(this).val();
            var d = {};
            if (!$.isNumeric(myweight) || myweight == '') {
                isValid = false;
                alertsimple('Enter the all weight');
                $(this).focus();
            }
            d['weight'] = myweight;
            weight = parseFloat(weight) + parseFloat(myweight);
            packageList.push(d);
        });
        if (isValid == false) {
            return false;
        }
        var returnObj = {};
        returnObj['packageList'] = packageList;
        returnObj['total_weight'] = weight;
        return returnObj;
    }
    $("#packageFrom").change(function () {
        var city = $("#packageFrom").val();
        commonFn.getMyDestinationList();
        var data = {function: 'GetcitylistBycountryId', countryId: city, encode: true};
        $.ajax({
            url: base_url + "Frontend/Helper",
            type: 'POST',
            data: data,
            async: true,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == true) {
                        var html = '<option value="">--select--</option>';
                        var data = json.data;
                        for (var i = 0; i < data.length; i++) {
                            var d = data[i];
                            html = html + '<option pincode="' + d.post_code + '" value="' + d.id + '">' + d.city + '</option>';
                        }
                        $('#packageCityFrom').html(html);
                        $('.selectpicker').selectpicker('refresh');
                    }
                } catch (e) {

                }
            }
        });

//        $('#packageCityFrom,#packageFromPincode').val('');
//        if (city != '') {
//            loadFromcity();
//            $('#packageCityFrom,#packageFromPincode').attr('readonly', false);
//        } else {
//            $('#packageCityFrom,#packageFromPincode').attr('readonly', true);
//
//        }
    });
    $("#packageTo").change(function () {
        var city = $("#packageTo").val();

        var data = {function: 'GetcitylistBycountryId', countryId: city, encode: true};
        $.ajax({
            url: base_url + "Frontend/Helper",
            type: 'POST',
            data: data,
            async: true,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == true) {
                        var html = '<option value="">--select--</option>';
                        var data = json.data;
                        for (var i = 0; i < data.length; i++) {
                            var d = data[i];
                            html = html + '<option pincode="' + d.post_code + '" value="' + d.id + '">' + d.city + '</option>';
                        }
                        $('#packageCityTo').html(html);
                        $('.selectpicker').selectpicker('refresh');
                    }
                } catch (e) {

                }
            }
        });
    });
    $('#docSubmit').click(function () {
        // var fromCountry = $('#').val();
        if ($('#docFrom').val() == '') {
            $('#docFrom').focus();
            $('#docFrom').css('border-color', 'red');
            return false;
        } else {
            $('#docFrom').css('border-color', '');
        }

        if ($('#cityFrom').val() == '') {
            $('#cityFrom').focus();
            $('#cityFrom').css('border-color', 'red');
            return false;
        } else {
            $('#cityFrom').css('border-color', '');
        }

        if ($('#docTo').val() == '') {
            $('#docTo').focus();
            $('#docTo').css('border-color', 'red');
            return false;
        } else {
            $('#docTo').css('border-color', '');
        }

        if ($('#cityTo').val() == '') {
            $('#cityTo').focus();
            $('#cityTo').css('border-color', 'red');
            return false;
        } else {
            $('#cityTo').css('border-color', '');
        }

        if ($('#docWeight').val() == '') {
            $('#docWeight').focus();
            $('#docWeight').css('border-color', 'red');
            return false;
        } else {
            $('#docWeight').css('border-color', '');
        }

        $('#docForm').submit();
    });
    $('.searchTab').click(function () {
        var attr = $(this).attr('attr');
        if (attr == 'PACKAGE') {
            $('#addPackage,#packageList').show();
        } else {
            $('#addPackage,#packageList').hide();
        }

    });
    $('#getLowestPrice').click(function () {
        var data = {};
        data['function'] = 'setSessionSearchData';
        data['packageFrom'] = $('#packageFrom').val();
        data['packageCityFrom'] = $('#packageCityFrom').val();
        data['packageTo'] = $('#packageTo').val();
        data['packageCityTo'] = $('#packageCityTo').val();
        data['packageFromPincode'] = $('#packageFromPincode').val();
        data['packageToPincode'] = $('#packageToPincode').val();
        data['packageWeight'] = $('#packageWeight').val();
        data['search_type'] = $('.searchTab.active').attr('attr');
        data['packageList'] = [];







        if ($('#packageFrom').val() == '') {
            alertsimple('Please select your origin country');
            return false;
        }

        if ($('#packageCityFrom').val() == '') {
            alertsimple('Please select your origin city');
            return false;
        }
        if ($('#packageTo').val() == '') {
            alertsimple('Please select your destination country');
            return false;
        }
        if ($('#packageCityTo').val() == '') {
            alertsimple('Please select your destination city');
            return false;
        }
        if ($('#packageFromPincode').val() == '') {
            alertsimple('Please enter your origin postal/zip code');
            return false;
        }
        if ($('#packageToPincode').val() == '') {
            alertsimple('Please enter your destination postal/zip code');
            return false;
        }
        if ($('#packageWeight').val() == '') {
            alertsimple('Please select weight');
            return false;
        }
        if (data['search_type'] == 'PACKAGE') {
            var packageList = getMypackageList();
            if (packageList == false) {
                return false;
            }
            data['packageWeight'] = packageList.total_weight;
            data['packageList'] = packageList.packageList;
        }
        console.log(data);


        $.ajax({
            url: base_url + "Frontend/Helper",
            type: 'POST',
            data: data,
            async: true,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == true) {
                        window.location = json.url;
                    }
                } catch (e) {

                }
            }
        });
    });

    var commonFn = (function () {
        var fn = {};
        fn.getMyDestinationList = function (e) {
            var countryId = $('#packageFrom').val();
            var data = {function: 'getMyDestinationList', origincountryId: countryId, encode: true};
            $.ajax({
                url: base_url + "Frontend/Helper",
                type: 'POST',
                data: data,
                async: true,
                success: function (data) {
                    try {
                        var json = jQuery.parseJSON(data);
                        if (json.status == true) {
                            var html = '<option value="">Select Country</option>';
                            var data = json.data;
                            for (var i = 0; i < data.length; i++) {
                                var d = data[i];
                                html = html + '<option   value="' + d.id + '">' + d.country + '</option>';
                            }
                            $('#packageTo').html(html);
                            //$('.selectpicker').selectpicker('refresh');
                        }
                    } catch (e) {

                    }
                }
            });
        }
        return fn;
    })();
<?php
if ($Method == 'documentSubmit') {
    ?>
        var myBooking = (function () {
            var fn = {};
            fn.loadMycouriercompany = function () {
                var data = {function: 'loadMycouriercompany'};
                $.ajax({
                    url: base_url + "Frontend/Helper",
                    type: 'POST',
                    data: data,
                    async: true,
                    success: function (data) {
                        try {
                            $('#couriercompanyDetails').html(data);
                            myBooking.loadMyBookingSummary();
                        } catch (e) {

                        }
                    }
                });
            }
            fn.chooseYourCourier = function (e) {
                var courierId = $(e).attr('courierId');
                var schedule_date = $('#schedule_date_' + courierId).val();

                var data = {function: 'chooseYourCourier', courierId: courierId, schedule_date: schedule_date};


                data['price'] = $('#schedule_date_' + courierId).attr('pbase');
                data['pickup_dropoff'] = $('input[name="pickup_dropoff' + courierId + '"]:checked').val();
                data['estimated_delivery'] = $('#estimated_delivery').attr('value');

                console.log(data);
                if ($('#schedule_date_' + courierId).val() == '' && data['pickup_dropoff'] == '0') {
                    $('#schedule_date_' + courierId).focus();
                    alertsimple('Please update schedule date for collection');
                    return false;
                }
                $.ajax({
                    url: base_url + "Frontend/Helper",
                    type: 'POST',
                    data: data,
                    async: true,
                    success: function (data) {
                        try {
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                $('.collapse').removeClass('show');
                                $('#shippingTab').addClass('show');
                                $('#licouriercompany').addClass('checked');
                                myBooking.loadMyBookingSummary();
                            }
                        } catch (e) {

                        }
                    }
                });

            }
            fn.chooseshipmentDetails = function (e) {
                var list = [];
                var searchType = $('#searchType').val();
                var fill_dimensions = $('#dimensions').is(':checked') ? 1 : 0;
                if (searchType == 'PACKAGE') {
                    var isValid = true;
                    $('.mypackagelist').each(function () {
                        var d = {};
                        var uniqid = $(this).attr('uniqid');
                        d['parcel_title'] = $('#parcel_title' + uniqid).val();
                        d['gross_weight'] = $('#gross_weight' + uniqid).attr('weight');
                        d['chargeable_weight'] = $('#chargeable_weight' + uniqid).attr('weight');
                        d['unit_declared_value'] = $('#unit_declared_value' + uniqid).val();

                        d['length'] = $('#length' + uniqid).val();
                        d['height'] = $('#height' + uniqid).val();
                        d['width'] = $('#width' + uniqid).val();
                        d['weight'] = $('#weight' + uniqid).val();

                        if ($('#parcel_title' + uniqid).val() == '') {
                            alertsimple('Please enter package name');
                            isValid = false;
                        }
                        list.push(d);
                    });
                    if (isValid == false) {
                        return false;
                    }
                } else if (searchType == 'DOCUMENTS') {
                    if ($('#document_content').val() == '') {
                        alertsimple('Please enter document name');
                        return false;
                    }
                    var d = {};
                    d['parcel_title'] = $('#document_content').val();
                    d['gross_weight'] = $('#gross_weight').attr('weight');
                    d['chargeable_weight'] = $('#chargeable_weight').attr('weight');
                    d['unit_declared_value'] = $('#unit_declared_value').val();
                    list.push(d);
                }

                var data = {function: 'chooseshipmentDetails', list: list, fill_dimensions: fill_dimensions};
                $.ajax({
                    url: base_url + "Frontend/Helper",
                    type: 'POST',
                    data: data,
                    async: true,
                    success: function (data) {
                        try {
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                $('.collapse').removeClass('show');
                                $('#senderDetails').addClass('show');
                                myBooking.loadMyBookingSummary();
                                $('#li_shipmentdetails').addClass('checked');
                            }
                        } catch (e) {

                        }
                    }
                });

            }
            fn.Iknowthedimensions = function () {
                var checked = $('#dimensions').is(':checked') ? 1 : 0;
                if (checked == '1') {
                    $('dimensions').show();
                } else {
                    $('dimensions').hide();
                }
            }
            fn.loadMysenderDetails = function (prefix) {

                var data = {function: 'loadMySenderReciverDetails', prefix: prefix};
                $.ajax({
                    url: base_url + "Frontend/Helper",
                    type: 'POST',
                    data: data,
                    async: true,
                    success: function (data) {
                        try {
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                $('#myuser' + prefix).html(json.HTML);
                                var addressId = $("input[name='address_id" + prefix + "']:checked").val();
                                $('#addressIdr' + addressId).click();
                            }
                        } catch (e) {

                        }
                    }
                });
            }
            fn.loadMyFormsenderReceiver = function (e) {
                var prefix = $(e).attr('prefix');
                var addressid = $(e).attr('addressid');
                var data = {function: 'GetSenderAddressByAddressId', addressId: addressid, encode: true};
                $.ajax({
                    url: base_url + "Frontend/Helper",
                    type: 'POST',
                    data: data,
                    async: true,
                    success: function (data) {
                        try {
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                var jsondata = json.data;
                                $('#full_name' + prefix).val(jsondata.full_name);
                                $('#mobile' + prefix).val(jsondata.mobile);
                                $('#father_name' + prefix).val(jsondata.father_name);
                                $('#grand_father_name' + prefix).val(jsondata.grand_father_name);
                                $('#family_name' + prefix).val(jsondata.family_name);
                                $('#email' + prefix).val(jsondata.email);
                                $('#address' + prefix).val(jsondata.address);
                                $('#company_name' + prefix).val(jsondata.company_name);
                            }
                        } catch (e) {

                        }
                    }
                });
            }
            fn.saveSessionForsenderReceiver = function (e) {
                var prefix = $(e).attr('prefix');
                var json = {};
                var primaryId = $("input[name='address_id" + prefix + "']:checked").val();
                json['full_name'] = $('#full_name' + prefix).val();
                json['father_name'] = $('#father_name' + prefix).val();
                json['grand_father_name'] = $('#grand_father_name' + prefix).val();
                json['family_name'] = $('#family_name' + prefix).val();
                json['mobile'] = $('#mobile' + prefix).val();
                json['email'] = $('#email' + prefix).val();
                json['address'] = $('#address' + prefix).val();
                json['company_name'] = $('#company_name' + prefix).val();

                json['country'] = $('#country' + prefix).attr('countryId');
                json['city'] = $('#city' + prefix).val();
                json['zip_code'] = $('#zip_code' + prefix).val();

                json['is_new'] = $('#is_new' + prefix).is(':checked') ? '1' : '0';
                json['pickup_type'] = $("input[name='pickup_type']:checked").val();

                json['type'] = prefix == '_sen' ? '0' : '1';

                json['id'] = primaryId;
                if ($('#full_name' + prefix).val() == '') {
                    $('#full_name' + prefix).focus();
                    return false;
                }
                if ($('#father_name' + prefix).val() == '') {
                    $('#father_name' + prefix).focus();
                    return false;
                }
                if ($('#grand_father_name' + prefix).val() == '') {
                    $('#grand_father_name' + prefix).focus();
                    return false;
                }
                if ($('#family_name' + prefix).val() == '') {
                    $('#family_name' + prefix).focus();
                    return false;
                }
                if ($('#mobile' + prefix).val() == '') {
                    $('#mobile' + prefix).focus();
                    return false;
                }
                if (!isValidEmailAddress($('#email' + prefix).val())) {
                    $('#email' + prefix).focus();
                    return false;
                }
                if ($('#address' + prefix).val() == '') {
                    $('#address' + prefix).focus();
                    return false;
                }
                if ($('#address' + prefix).val() == '') {
                    $('#address' + prefix).focus();
                    return false;
                }
                if ($('#address' + prefix).val() == '') {
                    $('#address' + prefix).focus();
                    return false;
                }
                var data = {function: 'saveSessionForsenderReceiver', prefix: prefix, json: json};
                $.confirm({
                    title: (json['is_new'] == 1 ? 'Are you sure want to add new address' : 'Are you sure want to update address'),
                    content: false,
                    type: 'green',
                    typeAnimated: true,
                    buttons: {
                        confirm: {
                            text: 'Submit',
                            btnClass: 'btn-green',
                            action: function () {
                                b();
                            }
                        },
                        cancel: {
                            text: 'Cancel',
                            btnClass: 'btn-warning',
                            action: function () {
                            }
                        },
                    }
                });
                function b() {
                    $.ajax({
                        url: base_url + "Frontend/Helper",
                        type: 'POST',
                        data: data,
                        async: true,
                        success: function (data) {
                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    myBooking.loadMysenderDetails(prefix);
                                    $('#' + prefix + 'SubmitBtn').attr('issubmitted', '1');
                                    if (prefix == '_sen') {
                                        $('#receiverAddress').click();
                                    } else {
                                        $('#bookingSummaryPannel').click();
                                    }
                                    myBooking.senderDetailscheckedStatus();
                                    myBooking.loadMyBookingSummary();
                                }
                            } catch (e) {

                            }
                        }
                    });
                }

            }
            fn.loadMyBookingSummary = function () {
                var data = {function: 'loadMyBookingSummary', encode: true};
                $.ajax({
                    url: base_url + "Frontend/Helper",
                    type: 'POST',
                    data: data,
                    async: true,
                    success: function (data) {
                        try {
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                $('#bookSumHTML').html(json.bookingSummery);
                                $('#paymentDetailHTML').html(json.paymentDetails);

                            }
                        } catch (e) {

                        }
                    }
                });
            }
            fn.submitMyorder = function () {
                   var TermsAndConditions = $('#TermsAndConditions').is(':checked') ? 1 : 0;
                   if(TermsAndConditions=='0'){
                       alertsimple('Please accept terms and conditions');
                       $('#bookingSummaryPannel').click();
                       return false;
                   }
                var data = {function: 'submitMyorder'};
                $.ajax({
                    url: base_url + "Frontend/Helper",
                    type: 'POST',
                    data: data,
                    async: true,
                    success: function (data) {
                        try {
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                window.location = json.url;
                            }
                        } catch (e) {

                        }
                    }
                });
            }
            fn.pickupdropoffOnchange = function () {
                var pickup_dropoff = $('input[name="pickup_dropoff"]:checked').val();
                if (pickup_dropoff == '0') {
                    $('#schedule_date').show();
                } else {
                    $('#schedule_date').hide();
                }
            }
            fn.senderDetailscheckedStatus = function () {
                var _sen = $('#_senSubmitBtn').attr('issubmitted');
                var _rec = $('#_recSubmitBtn').attr('issubmitted');
                if (_sen == '1' && _rec == '1') {
                    $('#li_sender_details').addClass('checked');
                }
            }
            return fn;
        })();
        $(document).ready(function () {
            myBooking.loadMycouriercompany();
            myBooking.loadMysenderDetails('_sen');
            myBooking.loadMysenderDetails('_rec');
            myBooking.senderDetailscheckedStatus();
        });
    <?php
}
?>
</script>
