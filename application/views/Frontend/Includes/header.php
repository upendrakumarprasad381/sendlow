
<?php
$session_user = $this->session->userdata('UserLogin');
if (!empty($session_user)) {
    $userdetails = GetUserArrayBy($session_user->id);
    $wallet = getUserWallet($session_user->id);
}
$account_balance=!empty($wallet[0]->account_balance) ? $wallet[0]->account_balance :  '0';
?>
<!DOCTYPE html>
<html>
    <head>
        <title>Welcome To Sendlow</title>
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
        <!---------------------Fonts------------------->
        <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
        <link rel="stylesheet" href="https://fonts.googleapis.com/icon?family=Material+Icons">
        <link href='//cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css' rel='stylesheet' type='text/css'>
        <!-- Select2 CSS --> 
        <link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css">
        <link href="https://cdn.jsdelivr.net/npm/select2@4.1.0-beta.1/dist/css/select2.min.css" rel="stylesheet" /> 
        <link rel="stylesheet" href="<?= base_url() ?>css/jquery-confirm.min.css"> 
        <link rel="stylesheet" href="<?= base_url() ?>css/Frontend/style.css">
        <?php
        $controller = $this->router->fetch_class();
        $method = $this->router->fetch_method();
        ?>
        <script>var base_url = '<?= base_url(); ?>';</script>
        <?php
        $directoryURI = $_SERVER['REQUEST_URI'];
        $path = parse_url($directoryURI, PHP_URL_PATH);
        $components = explode('/', $path);
        $first_part = $components[1];
        ?>
    </head>
    <body>
        <style>
            input[readonly] {
                background-color: white !important;
            }
        </style>
        <section class="prmClr fxdHdr">
            <div class="container-fluid">
                <div class="row topBar">
                    <div class="col-sm-4 text-lefts">
                        <span class=""><a href="http://facebook.com/" target="_blank" class=" text-white px-1"><i
                                    class="fa fa-facebook" aria-hidden="true"></i></a></span>
                        <span class=""><a href="https://twitter.com/" target="_blank" class=" text-white px-1"><i
                                    class="fa fa-twitter" aria-hidden="true"></i></a></span>
                        <span class=""><a href="https://www.linkedin.com/" target="_blank" class=" text-white px-1"><i
                                    class="fa fa-linkedin" aria-hidden="true"></i></a></span>
                        <span class=""><a href="https://www.instagram.com/" target="_blank" class=" text-white px-1"><i
                                    class="fa fa-instagram" aria-hidden="true"></i></a></span>
                        <span class="px-2 text-white d-md-none d-sm-block float-right"><a href="tell:+00 888.66.88"><img
                                    src="<?= base_url() ?>css/images/icnPhn.svg" height="15px"></a></span>
                        <span class="px-2 text-white d-md-none d-sm-block float-right"><a href="tell:+971 00.555.6666"><img
                                    src="<?= base_url() ?>css/images/icnWap.png" height="15px"></a></span>
                    </div>
                    <div class="col-sm-8 text-right d-none d-sm-block">
                        <span class="text-white px-1"><a href="tell:+00 888.66.88"><img src="<?= base_url() ?>css/images/icnPhn.svg"
                                                                                        height="15px"></a>&nbsp;(+00) 888.66.88</span>
                        <span class="text-white px-1"><a href="tell:+971 00.555.6666"><img src="<?= base_url() ?>css/images/icnWap.png"
                                                                                           height="15px"></a>&nbsp;(+971) 00.555.6666</span>
                    </div>
                </div>
                <nav class="navbar navbar-expand-lg px-0">
                    <a class="navbar-brand" href="<?= base_url() ?>"><img src="<?= base_url() ?>css/images/logoW.png" width="150px"></a>
                    <button class="navbar-toggler" type="button" data-toggle="collapse"
                            data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false"
                            aria-label="Toggle navigation">
                        <span class="navbar-toggler-icon"></span>
                    </button>
                    <div class="collapse navbar-collapse" id="navbarSupportedContent">
                        <ul class="navbar-nav m-auto">
                            <li class="nav-item active">
                                <a class="nav-link fontSmall text-white" href="<?= base_url() ?>dashboard"><img src="<?= base_url() ?>css/images/dashboard.svg"
                                                                                                                height="15px">&nbsp;Dashboard</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link fontSmall text-white" href="<?= base_url() ?>shipping"><img src="<?= base_url() ?>css/images/shipping.svg"
                                                                                                               height="15px">&nbsp;Shipping</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link fontSmall text-white" href="<?= base_url() ?>recieve"><img src="<?= base_url() ?>css/images/recv.svg"
                                                                                                              height="15px">&nbsp;Receive</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link fontSmall text-white" href="<?= base_url() ?>track"><img src="<?= base_url() ?>css/images/track.svg"
                                                                                                            height="15px">&nbsp;Track</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link fontSmall text-white" href="<?= base_url() ?>addressbook"><img src="<?= base_url() ?>css/images/addbk.svg"
                                                                                                                  height="15px">&nbsp;Address Book</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link fontSmall text-white" href="<?= base_url() ?>myWallet"><img src="<?= base_url() ?>css/images/wallet.svg"
                                                                                                               height="15px">&nbsp;My Wallet : AED <?= DecimalAmount($account_balance) ? DecimalAmount($account_balance) : '' ?></a>
                            </li>
                        </ul>

                        <ul class="navbar-nav mr-right">
                            <li class="nav-item dropdown">
                                <a class="nav-link dropdown-toggle fontSmall text-white fxdWidth_1 px-1" href="#" id=""
                                   data-toggle="dropdown">
                                    English
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mrgRmv border-0 rounded-0 fxdWidth_1">
                                    <a class="dropdown-item fontSmall px-2 " href="#">English</a>
                                    <a class="dropdown-item fontSmall px-2 " href="#">Arabic</a>
                                </div>
                            </li>
                            <li class="nav-item dropdown">
                                <?php
                                if (!empty($userdetails)) {
                                    ?>
                                    <a class="nav-link dropdown-toggle fxdWidth fontSmall text-white px-1" href="#" id=""
                                       data-toggle="dropdown">
                                        <i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp;<?= $userdetails->first_name; ?>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right fontSmall mrgRmv border-0 rounded-0 shadowL">
                                        <span class="login_message"></span>
                                        <a class="btn btnSec p-0 px-3 my-2" href="<?= base_url() ?>logout">LOGOUT</a>
                                        <!--<button type="button" class="btn btnSec p-0 px-3 my-2 ">LOGOUT</button>-->
                                        <a class="dropdown-item px-2" href="#">CONTACT US</a>
                                        <a class="dropdown-item px-2" href="#">SETTINGS</a>
                                    </div>
                                    <?php
                                } else {
                                    ?>
                                    <a class="nav-link dropdown-toggle fxdWidth fontSmall text-white px-1" href="#" id=""
                                       data-toggle="dropdown">
                                        <i class="fa fa-user-circle-o" aria-hidden="true"></i>&nbsp;Sign Up / Log In
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right fontSmall mrgRmv border-0 rounded-0 shadowL">
                                        <?php
                                        $username = $_COOKIE["user_login"];
                                        $password = $_COOKIE["userpassword"];
                                        // if(isset($_COOKIE["user_login"])){
                                        //     $check = 
                                        // }
                                        ?>
                                        <form class="px-2 py-3">
                                            <div class="form-group">
                                                <input type="email" class="form-control border-0 bg-light fontSmall" id="user_email"
                                                       placeholder="USER NAME" value="<?= isset($username) ? $username : '' ?>">
                                            </div>
                                            <div class="form-group">
                                                <input type="password" class="form-control border-0 bg-light fontSmall "
                                                       id="user_password" placeholder="PASSWORD" value="<?= isset($password) ? $password : '' ?>">
                                            </div>
                                            <div class="form-check">
                                                <input type="checkbox" class="form-check-input" name="remember" id="remember" <?php if (isset($_COOKIE["user_login"])) { ?> checked <?php } ?>>
                                                <label class="form-check-label" for="dropdownCheck">
                                                    Remember me
                                                </label>
                                            </div>
                                            <span class="login_message"></span>
                                            <button type="button" class="btn btnSec p-0 px-3 my-2 " id="login_submit">LOG IN</button>
                                        </form>
                                        <a class="dropdown-item px-2" href="<?= base_url() ?>forgotPassword">FORGOT PASSWORD OR USER ID?</a>
                                        <div class="dropdown-divider"></div>
                                        <a class="dropdown-item px-2" href="<?= base_url() ?>registration">CREATE ACCOUNT</a>
                                        <a class="dropdown-item px-2" href="#">CONTACT US</a>
                                        <a class="dropdown-item px-2" href="#">SETTINGS</a>
                                    </div>
                                    <?php
                                }
                                ?>
                            </li>
                        </ul>

                    </div>
                </nav>
            </div>
        </section>








