<?php
$session_user= $this->session->userdata('UserLogin');
if(!empty($session_user))
{
$userdetails=GetUserArrayBy($session_user->id);
}else{
    redirect(base_url('home'));
}

if(isset($userdetails)){
    $wallet = getUserWallet($session_user->id);
?>
    <section>
        <div class="container mt-150">
            <h4 class="mt-5 mb-4">My Wallet</h4>  
            <div class="row card flxRow shadowL px-sm-5 px-2 round15 mx-sm-0 mx-1 ">             
                <div class="col-sm-7 py-4 m-auto">
                    <h5 class="card-title lineH2">AED <?=DecimalAmount($wallet[0]->account_balance)?> <br>
                        <span class="clrSec">Current Wallet Balance</span>
                    </h5>
                    </div>
                    <div class="col-sm-5 py-4">
                        <p class="card-text clrPrm text-center">ADD MONEY TO YOUR WALLET</p>
                        <div class="input-group py-3">                                                                 
                            <div class="input-group-prepend">
                                <span class="input-group-text btn btnSec p-0 px-4 brRdsL" id="inputGroupPrepend2">Value</span>
                              </div> 
                            <input type="text" class="form-control" id="amount" placeholder="AED 250.00" aria-describedby="" required>
                            <input type="hidden" id="user_id" value="<?=$session_user->id?>">
                        </div>  
                        <button class="btn btnPrm form-control fxdWid4 rounded my-lg-0 my-2" type="button" id="walletByCard">PAY BY CARD</button>
                        <button class="btn btnPrm form-control fxdWid6 rounded" type="button" id="walletByTransafer">PAY BY BANK TRANSFER</button>
                        <span class="common_message"></span>
                        <div class="pbtWrap d-none" id="uploadDiv">
                            <form method="POST" id="bankTransaferForm" enctype="multipart/form-data">
                                <div class="form-group __pbtfitexfd">
                                    <input type="hidden" id="userId" name="userId" value="<?=$session_user->id?>">
                                    <input type="hidden" id="transfer_amount" name="transfer_amount" value="">
                                    <label for="walletReciept">Upload Your </label>
                                    <input type="file" class="form-control-file form-control" name="walletReciept" id="walletReciept">
                                </div>
                                <div class="form-group __pbtfitexfd">
                                    <input type="text" class="form-control" id="transactionNum" name="transactionNum" value="" placeholder="Transaction Number">
                                </div>
                                <div class="form-group __pbtfitexBtn">
                                    <button type="submit" class="btn btnPrm" id="bankTransferBtn">Upload</button>
                                </div> 
                            </form>
                        </div>
                  </div>
            </div>
        </div>
    </section>
<?php
}
?>
    
    <section>
        <div class="container">
            <div class="row card flxRow shadowL px-sm-5 px-2 my-4 round15 mx-sm-0 mx-1 ">             
                <div class="col-sm-7 py-4">
                    <h5 class="card-title">View bank Details </h5>
                    <div class="table-responsive">
                    <table class="table table-borderless py5">
                        <?php
                        $Qry = "SELECT * FROM `company_account_info`";
                        $accountArray = $this->Database->select_qry_array($Qry);
                        ?>
                        <tr>
                            <td>Bank Name</td>
                            <td>: &nbsp; <?=$accountArray[0]->bank_name?></td>
                        </tr>
                        <tr>
                            <td>Account Name</td>
                            <td>: &nbsp; <?=$accountArray[0]->account_name?></td>
                        </tr>
                        <tr>
                            <td>Account Number</td>
                            <td>: &nbsp; <?=$accountArray[0]->account_number?></td>
                        </tr>
                        <tr>
                            <td>Swift Code</td>
                            <td>:  &nbsp;<?=$accountArray[0]->swift_code?></td>
                        </tr>
                        <tr>
                            <td>IBAN Number</td>
                            <td>: &nbsp; <?=$accountArray[0]->iban_number?></td>
                        </tr>                            
                    </table>
                </div>
                    </div>
                    <div class="col-sm-5 py-sm-5 py-2 m-auto">
                        <p class="card-text clrSec">PLEASE SEND YOUR RECEPIT TO :</p>
                        <p href="#" class=""><img src="<?= base_url() ?>css/images/icnWap.png" height="20px">&nbsp;<?=$accountArray[0]->mobile?></p>
                        <p href="#" class=""><img src="<?= base_url() ?>css/images/icnPhn.svg" height="20px">&nbsp;<?=$accountArray[0]->email?></p>
                  </div>
            </div> 
        </div>
    </section>
    
    <section>
        <div class="container">
            <h4 class="mt-5 mb-3">Promotional offer</h4> 
            <div class="card-deck hvrCard lineH2">
            <?php
            // $offersArray = GetAllOffers();
            $qry = "SELECT offer_id FROM `redeemed_offers` WHERE is_redeem=1 AND user_id=$session_user->id";
            $redeemArray = $this->Database->select_qry_array($qry);
            if(!empty($redeemArray)){
                foreach($redeemArray as $redeem){
                     $offerArray[] = $redeem->offer_id;
                }
                $offer = implode(',',$offerArray);
                $qry1 = "SELECT * FROM `offers` WHERE archive=0 AND CURDATE() BETWEEN start_date AND expiry_date AND id NOT IN ($offer)";
                $userOffers = $this->Database->select_qry_array($qry1);
            }else{
                $userOffers = GetAllOffers();
            }
            foreach($userOffers as $uOffers)
            {
            ?>
                <div class="card col-sm-3 shadowL round15 justifyCont offers" id="offers_<?=$uOffers->id?>">
                    <input type="hidden" id="paid_<?=$uOffers->id?>" value="<?=$uOffers->paid_amount?>"> 
                    <input type="hidden" id="get_<?=$uOffers->id?>" value="<?=$uOffers->get_amount?>">
                    <input type="hidden" id="offerId_<?=$uOffers->id?>" value="<?=$uOffers->id?>">
                    <div class="card-body p-lg-3 p-0">
                        <h6><small>PAY</small><br>
                            GET
                        </h6>
                      <h6><small>AED <?=$uOffers->paid_amount?> </small><br>
                        AED <?=$uOffers->get_amount?>
                      </h6>                          
                  </div>
                </div>
            <?php
            }
            ?>
            </div>
            
            <div class="row my-4 flxEnd  px-sm-0 px-2">
                <input type="hidden" id="paidAmt" value=""> 
                <input type="hidden" id="getAmt" value="">
                <input type="hidden" id="offer_id" value="">
                <input type="hidden" id="user_id" value="<?=$session_user->id?>">
                <button class="btn btnPrm form-control fxdWid2 rounded mx-2 my-2" id="offerByCard">PAY BY CARD</button>
                <button class="btn btnPrm form-control fxdWid3 rounded  mx-2 my-2" id="offerByTransfer">PAY BY BANK TRANSFER</button>
                <span class="" id="common_message"></span>
                <div class="__pbtWrap d-none" id="offerUploadDiv">
                    <form method="POST" id="offerBankTransaferForm" enctype="multipart/form-data">
                        <div class="form-group __pbtfitexfd">
                            <input type="hidden" id="offer_userId" name="offer_userId" value="<?=$session_user->id?>">
                            <input type="hidden" id="offer_paid_amount" name="offer_paid_amount" value="">
                            <input type="hidden" id="offer_get_amount" name="offer_get_amount" value="">
                            <input type="hidden" id="offerId" name="offerId" value="">
                            <label for="walletReciept">Upload Your </label>
                            <input type="file" class="form-control-file form-control" name="offerWalletReciept" id="offerWalletReciept">
                        </div>
                        <div class="form-group __pbtfitexfd">
                            <input type="text" class="form-control" id="offerTransactionNum" name="offerTransactionNum" value="" placeholder="Transaction Number">
                        </div>
                        <div class="form-group __pbtfitexBtn">
                            <button type="submit" class="btn btnPrm" id="offerBankTransferBtn">Upload</button>
                        </div> 
                    </form>
                </div>
             </div>
        </div>
    </section>
    <?php
    if(!empty($session_user))
    {
    ?>
    <section>
        <div class="container mb-5">
            <h4 class="my-4">My Transaction</h4> 
            <div class="table-responsive">
            <table class="table table-light table-bordered table-striped bdrB">
                <thead>
                  <tr>
                    <th width="15%">Date</th>
                    <th width="20%">Order Id</th>
                    <th width="20%">Transaction Number</th>
                    <th width="15%">Amount</th>
                    <th width="30%">Description</th>
                  </tr>
                </thead>
                <tbody>
                    <?php
                    $Qry = "SELECT * FROM  wallet_transactions WHERE user_id=$session_user->id";
                    $walletArray = $this->Database->select_qry_array($Qry);
                    if(!empty($walletArray)){
                        foreach($walletArray as $wallet)
                        {
                            if($wallet->payment_status == 1){
                                $payment_status = "Transaction completed successfully";
                            }else{
                                $payment_status = "Transaction failed";
                            }
                    ?>
                    <tr>
                       <td width="15%"><?=date('d/m/Y',strtotime($wallet->timestamp))?></td>
                       <td width="20%"><?=$wallet->order_id?></td>
                       <td width="20%"><?=$wallet->transaction_id?></td>
                       <td width="15%"><?=DecimalAmount($wallet->amount)?></td>
                       <td width="30%" class="spcWrap"><?=$payment_status?></td>
                    </tr>
                  <?php
                        }
                    }else{
                  ?>
                    <tr>
                        <td style="text-align: center;" colspan="5">No Transaction Found</td>
                    </tr>
                <?php
                    }
                ?>
                </tbody>
              </table>
            </div> 
        </div>
    </section>
    <?php
    }
    ?>
    