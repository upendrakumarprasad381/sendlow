
    <section class="jumbotron mb-0 px-md-0 mt-150">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 col-md-12">
                    <ul class="nav nav-tabs regTablink border-0 justify-content-center w100 register_type" role="tablist" id="register_type">
                        <li class="nav-item mx-1">
                            <a class="nav-link active fontSmall" data-toggle="tab" href="#individualReg">Register as
                                Individual</a>
                        </li>
                        <li class="nav-item" value="2">
                            <a class="nav-link fontSmall mx-1" data-toggle="tab" href="#companyReg">Register as
                                Company</a>
                        </li>
                        <li class="nav-item" value="3">
                            <a class="nav-link mx-1 fontSmall" data-toggle="tab" href="#businessReg">Small Business</a>
                        </li>
                        <li class="nav-item" value="4">
                            <a class="nav-link mx-1 fontSmall" data-toggle="tab" href="#borkerReg">Register as
                                Broker</a>
                        </li>
                        <li class="nav-item" value="5">
                            <a class="nav-link mx-1 fontSmall" data-toggle="tab" href="#ecomReg">Ecommerce</a>
                        </li>

                    </ul>
                    <!-- Tab panes -->
                    <div class="tab-content bnrTab bgWht p-lg-5 p-2 shadow">
                        <div id="individualReg" class="container tab-pane active"><br>
                            <p class="text-secondary">Lorem Ipsum is simply dummy text of the printing and typesetting
                                industry. Lorem Ipsum has been the
                                industry's standard dummy text ever since the 1500s, when an unknown printer took a
                                galley of type and
                                scrambled it Lorem Ipsum is simply dummy text of the printing and typesetting industry.
                                Lorem Ipsum has
                                been the industry's standard dummy text ever since the 1500s, when an unknown printer
                                took a galley of type
                                and scrambled it</p>
                            <form class="ind_form" name="ind_form" id="ind_form" >
                                <div class="form-row">
                                    <!--<input type="hidden" class="form-control brdOrg " name="ind_type" id="ind_type" value="1">-->
                                    <div class="form-group col-sm-4">
                                        <label for="inputEmail4">First Name</label>
                                        <input type="text" class="form-control brdOrg" name="ind_fname" id="ind_fname">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputPassword4">Last Name</label>
                                        <input type="text" class="form-control brdOrg" name="ind_lname" id="ind_lname">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">E-mail</label>
                                        <input type="email" class="form-control brdOrg" name="ind_email" id="ind_email" placeholder="">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">Mobile Number</label>
                                        <input type="number" class="form-control brdOrg" name="ind_phone" id="ind_phone" placeholder="">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">Password</label>
                                        <input type="password" class="form-control brdOrg" id="ind_password" name="ind_password" placeholder="">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">Confirm Password</label>
                                        <input type="password" class="form-control brdOrg" id="ind_confPassword" name="ind_confPassword" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-auto ml-auto text-right">
                                    <span class="register_message"></span>
                                    <button type="button"class="btn btnPrm rounded mr-2 px-4" id="individual_reg">Submit</button>
                                </div>
                            </form>
                        </div>
                        <div id="companyReg" class="container tab-pane fade"><br>
                            <p class="text-secondary"> If you have bulk shipments monthly,
                             we will contact you for special rates or contact us @ Email,Mob</p>
                            <form class="comp_form" name="comp_form" id="comp_form" enctype="multipart/form-data" method="POST">
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <input type="hidden" class="form-control brdOrg" id="comp_type" value="2" name="type">
                                        <label for="inputEmail4">First Name</label>
                                        <input type="text" class="form-control brdOrg" id="comp_fname" name="comp_fname">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputPassword4">Last Name</label>
                                        <input type="text" class="form-control brdOrg" id="comp_lname" name="comp_lname">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">E-mail</label>
                                        <input type="email" class="form-control brdOrg" id="comp_email" placeholder="" name="comp_email">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label for="inputEmail4">Mobile Number</label>
                                        <input type="text" class="form-control brdOrg" id="comp_phone" name="comp_phone">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputPassword4">Landline Number</label>
                                        <input type="text" class="form-control brdOrg" id="comp_landphone" name="comp_landphone">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">Upload(CR / License *)</label>
                                        <div class="input-group mb-3 ">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input comp_license" id="inputGroupFile01" name="comp_license">
                                                <label class="custom-file-label brdOrg" for="inputGroupFile01"></label>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">Upload VAT(Optional)</label>
                                        <div class="input-group mb-3 ">
                                            <div class="custom-file">
                                                <input type="file" class="custom-file-input comp_vat" id="inputGroupFile01" name="comp_vat">
                                                <label class="custom-file-label brdOrg" for="inputGroupFile01"></label>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">Password</label>
                                        <input type="password" class="form-control brdOrg" id="comp_password" name="comp_password" placeholder="">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">Confirm Password</label>
                                        <input type="password" class="form-control brdOrg" id="comp_confPassword" name="comp_confPassword" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-auto ml-auto text-right">
                                    <span class="register_message"></span>
                                    <button type="submit" id="comp_reg"
                                        class="btn btnPrm rounded mr-2 px-4">Submit</button>
                                </div>
                            </form>
                        </div>
                        <div id="businessReg" class="container tab-pane"><br>
                            <p class="text-secondary">If you have business on social media(facebook, WhatsApp etc) If you have bulk shipments monthly,
                             we will contact you for special rates or contact us @ Email,Mob</p>
                            <form class="bus_form">
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <!--<input type="hidden" class="form-control brdOrg " name="bus_type" id="bus_type" value="3">-->
                                        <label for="inputEmail4">First Name</label>
                                        <input type="text" class="form-control brdOrg" id="bus_fname" name="bus_fname">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputPassword4">Last Name</label>
                                        <input type="text" class="form-control brdOrg" id="bus_lname" name="bus_lname">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">E-mail</label>
                                        <input type="email" class="form-control brdOrg" id="bus_email" name="bus_email" placeholder="">
                                    </div>
                                </div>
                                <div class="form-row">
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">Mobile Number</label>
                                        <input type="number" class="form-control brdOrg" id="bus_phone" name="bus_phone" placeholder="">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">Password</label>
                                        <input type="password" class="form-control brdOrg" id="bus_password" name="bus_password" placeholder="">
                                    </div>
                                    <div class="form-group col-sm-4">
                                        <label for="inputAddress">Confirm Password</label>
                                        <input type="password" class="form-control brdOrg" id="bus_confPassword" name="bus_confPassword" placeholder="">
                                    </div>
                                </div>
                                <div class="form-group col-auto ml-auto text-right">
                                    <span class="register_message"></span>
                                    <button type="button" id="business_reg"
                                        class="btn btnPrm rounded mr-2 px-4">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
