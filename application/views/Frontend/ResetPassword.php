
    <section class="jumbotron mb-0 px-md-0 mt-150">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-10 offset-lg-1 col-md-12">
                    <!-- Tab panes -->
                    <div class="tab-content bnrTab bgWht p-lg-5 p-2 shadow">
                        <div id="individualReg" class="container tab-pane active"><br>
                            <form class="ind_form" name="ind_form" id="ind_form">
                                <input type="hidden" id="userid" value="<?=$id?>">
                                <div class="form-row">
                                    <div class="form-group col-sm-6">
                                        <label for="password">Password</label>
                                        <input type="password" class="form-control brdOrg" name="password" id="password">
                                    </div>
                                    <div class="form-group col-sm-6">
                                        <label for="confPassword">Confirm Password</label>
                                        <input type="password" class="form-control brdOrg" name="confPassword" id="confPassword">
                                    </div>
                                </div>
                                <div class="form-group col-auto ml-auto text-right">
                                    <span class="common_message"></span>
                                    <button type="button"class="btn btnPrm rounded mr-2 px-4" id="resetPassword">Submit</button>
                                </div>
                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
