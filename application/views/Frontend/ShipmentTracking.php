
    <section class="mt-150 py-7">
        <div class="container">
            <div class="row">
                <div class="col-sm-12">
                    <article class="card border-0">        
                        <div class="card-body">
                            <article class="card">
                                <div class="card-body row bgPrm border-bottom">
                                    <div class="col text-white"> SHIPMENT TRACKING</div>
                                    <div class="col clrSec"> Shipment Tracking Number</div>
                                    <div class="col text-white"> 235687596621253</div>                                
                                </div>
                                <div class="card-body row bgPrm">
                                    <div class="col clrSec"> From<br><span class="text-white">SHAN MOHAMMED
                                            DUBAI</span> </div>
                                    <div class="col clrSec"> Status<br><span class="text-white">ARRIVED DELIVERY FACILITY </span> </div>
                                    <div class="col clrSec"> To<br> <span class="text-white">KHALID AHAMED
                                        DUBAI</span> </div>                                   
                                </div>
                            </article>            
                            <article class="card mt-4">
                                <div class="card-body row bgPrm">
                                    <div class="col clrSec"> Shipment Weight<br><span class="text-white">0.5 KG</span> </div>
                                    <div class="col clrSec"> Expected Delivery Date<br><span class="text-white">04/10/2021 </span> </div>
                                    <div class="col clrSec"> Shipment Quantity <br> <span class="text-white">1</span> </div>                                   
                                </div>
                            </article>
                            <div class="track">
                                <div class="step active"> <span class="icon"> <i class="fa fa-truck" aria-hidden="true"></i></span> <span class="text">Picked Up</span> </div>
                                <div class="step active"> <span class="icon"> <i class="fa fa-user"></i> </span> <span class="text"> In Transit</span> </div>
                                <div class="step"> <span class="icon"> <i class="fa fa-truck"></i> </span> <span class="text"> Out For Delivery </span> </div>
                                <div class="step"> <span class="icon"> <i class="fa fa-truck" aria-hidden="true"></i></span> <span class="text">Delivered</span> </div>
                            </div>
                        </div>
                    </article>
                </div>
            </div>
            <div class="row mb-5">
                <div class="col-sm-12">
                    <div class="timeline2">
                        <div class="tmLineCont tLeft ">
                          <div class="content">
                            <h6 class="clrSec">15-Sept-2021 </h6>
                            <p> Saturday</p> 
                        </div>
                        </div>
                        <div class="tmLineCont tRight processed">
                          <div class="content">
                            <article class="card border-0 rounded-0">
                                <div class="card-body row bgDarkGry">
                                    <div class="col-sm-12 text-dark semiBld"> Arrived Delivery Facility</div>
                                    <div class="col text-dark"> <span class="material-icons vAlgn clrPrm px-2">
                                        watch_later
                                        </span>04:44 AM</div>
                                    <div class="col text-dark"><span class="material-icons vAlgn clrPrm px-2">
                                        place
                                        </span> Dubai </div>                                   
                                </div>
                            </article>
                        </div>
                        </div>
                        <div class="tmLineCont tLeft">
                          <div class="content">
                            <h6 class="clrSec">15-Sept-2021 </h6>
                            <p> Saturday</p>
                          </div>
                        </div>
                        <div class="tmLineCont tRight">
                          <div class="content">
                            <article class="card border-0 rounded-0">
                                <div class="card-body row bgDarkGry">
                                    <div class="col-sm-12 text-dark semiBld"> Shipment Departed SENDLOW Sorting Facility</div>
                                    <div class="col text-dark"> <span class="material-icons vAlgn clrPrm px-2">
                                        watch_later
                                        </span>04:44 AM</div>
                                    <div class="col text-dark"><span class="material-icons vAlgn clrPrm px-2">
                                        place
                                        </span> Dubai </div>                                   
                                </div>
                                <div class="card-body row bgDarkGry">
                                    <div class="col-sm-12 text-dark semiBld"> Shipment Departed SENDLOW Sorting Facility</div>
                                    <div class="col text-dark"> <span class="material-icons vAlgn clrPrm px-2">
                                        watch_later
                                        </span>04:44 AM</div>
                                    <div class="col text-dark"><span class="material-icons vAlgn clrPrm px-2">
                                        place
                                        </span> Dubai </div>                                   
                                </div>
                            </article>
                        </div>
                        </div>
                        <div class="tmLineCont tLeft">
                          <div class="content">
                            <h6 class="clrSec">15-Sept-2021 </h6>
                            <p> Saturday</p>
                        </div>
                        </div>
                        <div class="tmLineCont tRight">
                          <div class="content">
                            <article class="card border-0 rounded-0">
                                <div class="card-body row bgDarkGry">
                                    <div class="col-sm-12 text-dark semiBld"> Arrived Delivery Facility</div>
                                    <div class="col text-dark"> <span class="material-icons vAlgn clrPrm px-2">
                                        watch_later
                                        </span>04:44 AM</div>
                                    <div class="col text-dark"><span class="material-icons vAlgn clrPrm px-2">
                                        place
                                        </span> Dubai </div>  
                                                                         
                                </div>
                                <div class="card-body row bgDarkGry">
                                    <div class="col-sm-12 text-dark semiBld"> Arrived Delivery Facility</div>
                                    <div class="col text-dark"> <span class="material-icons vAlgn clrPrm px-2">
                                        watch_later
                                        </span>04:44 AM</div>
                                    <div class="col text-dark"><span class="material-icons vAlgn clrPrm px-2">
                                        place
                                        </span> Dubai </div>  
                                                                         
                                </div>
                            </article>
                        </div>
                        </div>
                      </div>

                </div>
            </div>
        </div>
    </section>

    