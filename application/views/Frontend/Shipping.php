<?php
$searchData = getSessionSearchData();
$sessionAr = GetSessionArrayLogin();

if (empty($sessionAr)) {
    redirect(base_url('login'));
} else if (empty($searchData)) {
    redirect(base_url());
}

$packageWeight = !empty($searchData['packageWeight']) ? $searchData['packageWeight'] : '0';
$searchType = !empty($searchData['search_type']) ? $searchData['search_type'] : '';

//echo '<pre>';
//print_r($searchData);
//echo '</pre>';
?>
<input type='hidden' id="searchType" value="<?= $searchType ?>">
<section class="mt-150 mb-5 py-7">
    <div class="container-fluid">




        <div class="row">
            <div class="col-lg-9 col-sm-12 trackTimeline pl-5">
                <div id="shippingDtl">
                    <div class="card border-0 my-2">
                        <div class="card-header bg-white border-0 p-0 pb-3" id="headingOne">
                            <h5 class="mb-0">
                                <a href="<?= base_url() ?>"> <Button  class="btn btn-link text-dark px-1  visitedTab cnt1 checked" XXdata-toggle="collapse"
                                                                      XX-data-target="#wgtDesti" XXaria-expanded="true" XXaria-controls="collapseOne">
                                        Weight / Destination
                                    </Button></a>
                            </h5>
                        </div>
                        <div id="wgtDestiXX" class="collapse" aria-labelledby="headingOne" data-parent="#shippingDtl">
                            <div class="card-body">
                                <div class="row g-0 text-lg-center text-left shadowL py-3 ">

                                </div>

                            </div>
                        </div>
                    </div>

                    <div class="card border-0 my-2">
                        <div class="card-header bg-white border-0 p-0 pb-3" id="headingOne">
                            <h5 class="mb-0">
                                <Button class="btn btn-link text-dark px-1 visitedTab active cnt2 <?= !empty($searchData['courierId']) ? 'checked' : '' ?>" id="licouriercompany" data-toggle="collapse"
                                        data-target="#chooseCompanyTab" aria-expanded="true" aria-controls="collapseOne">Choose Your Courier Company
                                </Button>
                            </h5>
                        </div>
                        <div id="chooseCompanyTab" class="collapse show" aria-labelledby="headingOne" data-parent="#shippingDtl">
                            <div class="card-body" id="couriercompanyDetails">
                                <img height="50" src="https://miro.medium.com/max/1400/1*CsJ05WEGfunYMLGfsT2sXA.gif">
                            </div>
                        </div>
                    </div>
                    <!-- -----------------------------choose company ended ------------------------------ -->
                    <div class="card border-0 my-2">
                        <div class="card-header bg-white border-0 p-0 pb-3" id="headingOne">
                            <h5 class="mb-0">
                                <Button class="btn btn-link text-dark px-1 visitedTab cnt3 <?= !empty($searchData['shipmentDetails']) ? 'checked' : '' ?>" data-toggle="collapse"
                                        data-target="#shippingTab" aria-expanded="true" id="li_shipmentdetails" aria-controls="collapseOne">Shipment Details
                                </Button>
                            </h5>
                        </div>
                        <div id="shippingTab" class="collapse" aria-labelledby="headingOne" data-parent="#shippingDtl">
                            <?php if ($searchType == 'DOCUMENTS') { ?>
                                <div class="card-body p-0">
                                    <h6 class="clrPrm">How many Document</h6>
                                    <hr>
                                    <div class="row py-3 ">
                                        <div class="col-sm-9">
                                            <form class="row">
                                                <div class="form-group col-sm-4">
                                                    <label for="">Pieces</label>
                                                    <input type="number" class="form-control" value="1" id="pieces" readonly autocomplete="off" >
                                                </div>
                                                <div class="form-group col-sm-8">
                                                    <label for="">Document Content</label>
                                                    <input type="text" class="form-control" id="document_content" autocomplete="off">
                                                </div>
                                            </form>
                                        </div>
                                    </div>

                                    <h6 class="clrPrm">Document Details</h6>
                                    <hr>
                                    <div class="row py-3 ">
                                        <div class="col-sm-9">
                                            <form class="row">

                                                <div class="form-group col-sm-4">
                                                    <label for="">Gross Weight</label>
                                                    <input type="text" class="form-control" weight='<?= $packageWeight ?>' value="<?= $packageWeight ?> Kg" id="gross_weight"  readonly="">
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label for="">Chargeable Weight</label>
                                                    <input type="text" class="form-control" weight='<?= $packageWeight ?>' value="<?= $packageWeight ?> Kg" id="chargeable_weight"  readonly="">
                                                </div>
                                                <div class="form-group col-sm-4">
                                                    <label for="">Unit Declared Value</label>
                                                    <input type="number" class="form-control" id="unit_declared_value">
                                                </div>
                                                <div class="form-group col-sm-12">
                                                    <small class=" brdOrg rounded dTbl p-1 form-control"><span class="material-icons text-danger px-1 vAlgn">
                                                            error_outline
                                                        </span>Ship Tip : Package weight and dimensions directly impact your cost.</small>
                                                </div>
                                            </form>
                                        </div>

                                    </div>

                                    <h6 class="clrPrm">Document Dimension Guide</h6>
                                    <hr>
                                    <div class="row py-3 ">
                                        <div class="col-sm-4 text-center my-lg-1 my-3">
                                            <img src="<?= base_url('files/images/parcel.jpg') ?>" class="img-fluid">
                                        </div>
                                        <div class="col-sm-4 col-sm-offset-4 ovrHide">
                                            <img src="<?= base_url('files/images/parcel2.jpg') ?>" class="img-fluid rotate15">
                                        </div>

                                    </div>
                                </div>

                                <?php
                            } else if ($searchType == 'PACKAGE') {
                                $totalPackage = !empty($searchData['packageList']) && is_array($searchData['packageList']) ? $searchData['packageList'] : [];
                                ?>
                                <!-- ------------------------------- another package ---------------------------------- -->
                                <div class="card-body p-0">
                                    <!--                                    <h6 class="clrPrm">How Many Package</h6>
                                                                        <hr>
                                                                        <div class="row py-3 ">
                                                                            <div class="col-sm-9">
                                                                                <form class="row">
                                                                                    <php
                                                                                    for ($i = 0; $i < count($totalPackage); $i++) {
                                                                                        ?>
                                                                                        <div class="form-group col-sm-12">
                                                                                            <label for="">Parcel <= $i + 1 ?></label>
                                                                                            <input type="text" class="form-control" >
                                                                                        </div>
                                                                                        <php
                                                                                    }
                                                                                    ?>
                                                                                </form>
                                                                            </div>
                                                                        </div>-->

                                    <h6 class="clrPrm">Package Details</h6>
                                    <hr>
                                    <div class="row py-3 ">
                                        <div class="col-sm-9">

                                            <?php
                                            $lengthBox = '';
                                            $heightBox = '';
                                            $widthBox = '';
                                            $weightBox = '';
                                            for ($i = 0; $i < count($totalPackage); $i++) {
                                                $d = $totalPackage[$i];
                                                $myWeight = $d['weight'];
                                               $uniqid= uniqid();
                                                ?>
                                                <div class="row mypackagelist" uniqid='<?= $uniqid ?>'>
                                                    <div class="form-group col-sm-3">
                                                        <label for="">Parcel name <?= $i + 1 ?></label>
                                                        <input type="text" class="form-control " id="parcel_title<?= $uniqid ?>"  autocomplete="off">
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="">Gross Weight</label>
                                                        <input type="text" class="form-control " weight='<?= $myWeight ?>' id="gross_weight<?= $uniqid ?>" value="<?= $myWeight ?> Kg" readonly>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="">Chargeable Weight</label>
                                                        <input type="text" class="form-control " weight='<?= $myWeight ?>' id="chargeable_weight<?= $uniqid ?>" value="<?= $myWeight ?> Kg" readonly>
                                                    </div>

                                                    <div class="form-group col-sm-3">
                                                        <label for="">Unit Declared Value</label>
                                                        <input type="number" class="form-control" id="unit_declared_value<?= $uniqid ?>">
                                                    </div>
                                                    <div class="form-group col-sm-3 text-center mdlCont" style="display: none;">
                                                        <label for=""><span class="material-icons">
                                                                highlight_off
                                                            </span></label>
                                                    </div>
                                                </div>
                                                <?php
                                                $lengthBox = $lengthBox . '<input type="number" id="length' . ($uniqid ) . '" placeholder="Parcel ' . ($i + 1) . '" class="form-control heightmanage" >';
                                                $heightBox = $heightBox . '<input type="number" id="height' . ($uniqid ) . '" placeholder="Parcel ' . ($i + 1) . '" class="form-control heightmanage" >';
                                                $widthBox = $widthBox . '<input type="number" id="width' . ($uniqid ) . '" placeholder="Parcel ' . ($i + 1) . '" class="form-control heightmanage" >';
                                                $weightBox = $weightBox . '<input type="number" id="weight' . ($uniqid) . '" placeholder="Parcel ' . ($i + 1) . '" class="form-control heightmanage" >';
                                            }
                                            ?>


                                            <div class="row">
                                                <div class="form-group col-auto text-center">
                                                    <small class=" brdOrg rounded dTbl p-1 form-control"><span class="material-icons text-danger px-1 vAlgn">
                                                            error_outline
                                                        </span>Ship Tip : Package weight and dimensions directly impact your cost.</small>
                                                </div>
                                                <div class="form-group col-sm-12 ">
                                                    <div class="form-check-inline px-2">
                                                        <label class="form-check-label">
                                                            <input type="checkbox" id="dimensions" onchange="myBooking.Iknowthedimensions();" class="form-check-input" >I know the dimensions of my shipment
                                                        </label>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

                                    </div>
                                    <dimensions style="display: none;">
                                        <h6>Fill in the Dimensions (L, W, H) Per Each Parcel </h6>

                                        <hr>
                                        <div class="row py-3 ">
                                            <div class="col-sm-9">
                                                <form class="row">
                                                    <div class="form-group col-sm-3">
                                                        <label for="exampleFormControlSelect1">Length </label>
                                                        <?= $lengthBox ?>

                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="exampleFormControlSelect1">Width</label>
                                                        <?= $widthBox ?>

                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="exampleFormControlSelect1">Height </label>
                                                        <?= $heightBox ?>
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <label for="exampleFormControlSelect1">Vol. Weight </label>
                                                        <?= $weightBox ?>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </dimensions>
                                    <h6 class="clrPrm">Parcel Dimension Guide</h6>
                                    <hr>
                                    <div class="row py-3 ">
                                        <div class="col-sm-4 text-center my-lg-1 my-3">
                                            <img src="<?= base_url('files/images/parcel3.jpg') ?>" class="img-fluid">
                                        </div>
                                    </div>
                                </div>
                            <?php } if (!empty($searchType)) { ?>
                                <div class="my-4 float-right">
                                    <!--                                <div class="form-check-inline px-2">
                                                                        <label class="form-check-label">
                                                                            <input type="checkbox" class="form-check-input" value="">Save as Draft
                                                                        </label>
                                                                    </div>-->
                                    <button onclick="myBooking.chooseshipmentDetails();" class="btn btnPrm py-0 rounded ">Continue</button>

                                </div>
                            <?php } ?>
                        </div>
                    </div>
                    <!-- ----------------------------- shipment details ended ------------------------------ -->

                    <div class="card border-0 my-2">
                        <div class="card-header bg-white border-0 p-0 pb-3" id="headingOne">
                            <h5 class="mb-0">
                                <Button class="btn btn-link text-dark px-1 visitedTab cnt4" data-toggle="collapse"
                                        data-target="#senderDetails" aria-expanded="true" id="li_sender_details" aria-controls="collapseOne">Sender Details
                                </Button>
                            </h5>
                        </div>
                        <div id="senderDetails" class="collapse" aria-labelledby="headingOne" data-parent="#shippingDtl">
                            <!--<a href="javascript:void(0)" class="senderDtlTab">Sender Details</a>-->



                            <div class="accordion" id="accordionExample">
                                <div class="card border-0 ">
                                    <div class="card-header border-0 senderDtlTab" id="headingOne">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link btnTabLink btn-block text-left icnArwBlack" id="senderAddress" type="button" data-toggle="collapse" data-target="#collapseOne" aria-expanded="true" aria-controls="collapseOne">Sender Details
                                            </button>
                                        </h2>
                                    </div>

                                    <div id="collapseOne" class="collapse show" aria-labelledby="headingOne" data-parent="#accordionExample">
                                        <div class="card-body" id="details_sen">
                                            <?php
                                            $prefix = '_sen';
                                            ?>
                                            <form class="row">
                                                <div class="form-row" style="width: 100%;">
                                                    <div class="form-group col-sm-3">
                                                        <input type="text" class="form-control brdOrg" id="filter_name<?= $prefix ?>" placeholder="Name">
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <input type="number" class="form-control brdOrg" id="filter_phone<?= $prefix ?>" placeholder="Phone Number">
                                                    </div>
                                                    <div class="form-group col-sm-2">
                                                        <input type="email" class="form-control brdOrg" id="filter_country<?= $prefix ?>" placeholder="Country">
                                                    </div>
                                                    <div class="form-group col-sm-2">
                                                        <input type="email" class="form-control brdOrg" id="filter_city<?= $prefix ?>" placeholder="City">
                                                    </div>
                                                    <div class="form-group col-auto ml-auto">
                                                        <button type="submit" class="btn btnPrm rounded btnRgt mr-2 px-4">Filter</button>
                                                    </div>
                                                </div>

                                                <div class="form-row rdoList" style="width: 100%;">
                                                    <div class="form-group col-sm-12" id="myuser<?= $prefix ?>">


                                                    </div>
                                                </div>
                                                <?= senderReciverFroms($prefix) ?>
                                            </form>
                                        </div>
                                        <div class=" my-4 float-right">
                                            <button prefix="_sen" issubmitted="<?= !empty($searchData['_sen']) ? '1' : '0' ?>" id="_senSubmitBtn" onclick="myBooking.saveSessionForsenderReceiver(this);" class="btn btnPrm py-0 rounded ">Continue</button>
                                        </div> 
                                    </div>
                                </div>
                                <div class="card border-0 ">
                                    <div class="card-header border-0 senderDtlTab" id="headingTwo">
                                        <h2 class="mb-0">
                                            <button class="btn btn-link  btnTabLink btn-block text-left collapsed" id="receiverAddress" type="button" data-toggle="collapse" data-target="#collapseTwo" aria-expanded="false" aria-controls="collapseTwo">
                                                Receiver Details
                                            </button>
                                        </h2>
                                    </div>
                                    <div id="collapseTwo" class="collapse" aria-labelledby="headingTwo" data-parent="#accordionExample">
                                        <div class="card-body">
                                            <?php
                                            $prefix = '_rec';
                                            ?>
                                            <form class="row">
                                                <div class="form-row px-0" style="width: 100%;">
                                                    <div class="form-group col-sm-3">
                                                        <input type="text" class="form-control brdOrg" id="filter_name<?= $prefix ?>" placeholder="Name">
                                                    </div>
                                                    <div class="form-group col-sm-3">
                                                        <input type="number" class="form-control brdOrg" id="filter_phone<?= $prefix ?>" placeholder="Phone Number">
                                                    </div>
                                                    <div class="form-group col-sm-2">
                                                        <input type="email" class="form-control brdOrg" id="filter_country<?= $prefix ?>" placeholder="Country">
                                                    </div>
                                                    <div class="form-group col-sm-2">
                                                        <input type="email" class="form-control brdOrg" id="filter_city<?= $prefix ?>" placeholder="City">
                                                    </div>
                                                    <div class="form-group col-auto ml-auto">
                                                        <button type="submit" class="btn btnPrm rounded btnRgt mr-2 px-4">Filter</button>
                                                    </div>
                                                </div>
                                                <div class="form-row px-0 rdoList" style="width: 100%;">
                                                    <div class="form-group col-sm-12" id="myuser<?= $prefix ?>">

                                                    </div>
                                                </div>
                                                <?= senderReciverFroms($prefix) ?>
                                            </form>
                                        </div>
                                        <div class=" my-4 float-right">
                                            <button prefix="_rec" issubmitted="<?= !empty($searchData['_rec']) ? '1' : '0' ?>" id="_recSubmitBtn" onclick="myBooking.saveSessionForsenderReceiver(this);" class="btn btnPrm py-0 rounded ">Continue</button>
                                        </div> 
                                    </div>
                                </div>

                            </div>

                        </div>
                    </div>

                    <!-- ----------------------------- Sender and recipient details ended------------------------------ -->
                    <div class="card border-0 my-2">
                        <div class="card-header bg-white border-0 p-0 pb-3" id="headingOne">
                            <h5 class="mb-0">
                                <Button class="btn btn-link text-dark px-1 visitedTab cnt5" id="bookingSummaryPannel" data-toggle="collapse"
                                        data-target="#bookSum" aria-expanded="true" aria-controls="collapseOne">
                                    Booking Summary
                                </Button>
                            </h5>
                        </div>
                        <div id="bookSum" class="collapse" aria-labelledby="headingOne" data-parent="#shippingDtl">
                            <div class="card-body" id="bookSumHTML">


                            </div>
                        </div>
                    </div>
                    <!-- ----------------------------- Booking Summary  end ------------------------------ -->
                </div>
            </div>
            <div class="col-lg-3 col-sm-12">
                <h6>Payment Details</h6>
                <div id="paymentDetailHTML">

                </div>
            </div>
        </div>
    </div>
</section>



<style>
    input.form-control.heightmanage {
        margin-top: 14px;
    }
</style>
