
    <section class="mt-150 py-7">
        <div class="container">
            <div class="row">
                <div class="col contCtr">
                    <span class="material-icons icnSuc">done</span>
                    <h1 class="clrPrm semiBld">Thank You</h1>
                    <h5>Your order was completed successfully</h5>
                    <p class="clrSec">Order Number 235689243</p>
                    <p class="fontSmall brdOrg p-1 rounded">Please print AWB <a href="" class="clrPrm text-underline">
                            (Download here ) </a>and drop your shipment at drop o address</p>
                </div>
            </div>
        </div>
    </section>

    