
    <section class="mt-150 py-7 bnr">
        <div class="container">
            <div class="row">
                <div class="col-lg-8 offset-lg-2 col-md-12">
                    <form class="bgWht rounded p-4 px-5">
                        <div class="form-row">
                            <div class="form-group col-12">
                                <label for=""><h4 class="text-dark">Track</h4></label>  
                                <div class="input-group">                                                                 
                                    <input type="text" class="form-control brdBtm bgTrans" id="" placeholder="Tracking Number" aria-describedby="" required>
                                    <div class="input-group-append icnTrack">
                                        <span class="input-group-text" id="inputGroupPrepend2"><i class="fa fa-qrcode" aria-hidden="true"></i></span>
                                      </div> 
                                </div>                             
                            </div>                           
                            <div class="form-group col-12 text-center">
                                <button class="btn btnPrm rounded px-4 fontSmall">Submit</button>

                            </div>
                            
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </section>

    