<html lang="en">
    <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
        <title>Login</title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link rel="icon" type="image/png" href="images/icons/favicon.ico"/>
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/bootstrap/css/'); ?>bootstrap.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/fonts/font-awesome-4.7.0/css/'); ?>font-awesome.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/fonts/Linearicons-Free-v1.0.0/'); ?>icon-font.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/fonts/iconic/css/'); ?>material-design-iconic-font.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/animate/'); ?>animate.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/css-hamburgers/'); ?>hamburgers.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/animsition/css/'); ?>animsition.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/select2/'); ?>select2.min.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/vendor/daterangepicker/'); ?>daterangepicker.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/css/'); ?>util.css">
        <link rel="stylesheet" type="text/css" href="<?= base_url('assets/AdminLogin/css/'); ?>main.css">
         <link rel="shortcut icon" type="image/x-icon" href="<?= base_url() ?>images/favicon/favicon.png" />

        <link rel="apple-touch-icon" sizes="57x57" href="<?= base_url() ?>images/favicons/apple-icon-57x57.png">
    	<link rel="apple-touch-icon" sizes="60x60" href="<?= base_url() ?>images/favicons/apple-icon-60x60.png">
    	<link rel="apple-touch-icon" sizes="72x72" href="<?= base_url() ?>images/favicons/apple-icon-72x72.png">
    	<link rel="apple-touch-icon" sizes="76x76" href="<?= base_url() ?>images/favicons/apple-icon-76x76.png">
    	<link rel="apple-touch-icon" sizes="114x114" href="<?= base_url() ?>images/favicons/apple-icon-114x114.png">
    	<link rel="apple-touch-icon" sizes="120x120" href="<?= base_url() ?>images/favicons/apple-icon-120x120.png">
    	<link rel="apple-touch-icon" sizes="144x144" href="<?= base_url() ?>images/favicons/apple-icon-144x144.png">
    	<link rel="apple-touch-icon" sizes="152x152" href="<?= base_url() ?>images/favicons/apple-icon-152x152.png">
    	<link rel="apple-touch-icon" sizes="180x180" href="<?= base_url() ?>images/favicons/apple-icon-180x180.png">
    	<link rel="icon" type="image/png" sizes="192x192" href="<?= base_url() ?>images/favicons/android-icon-192x192.png">
    	<link rel="icon" type="image/png" sizes="32x32" href="<?= base_url() ?>images/favicons/favicon-32x32.png">
    	<link rel="icon" type="image/png" sizes="96x96" href="<?= base_url() ?>images/favicons/favicon-96x96.png">
    	<link rel="icon" type="image/png" sizes="16x16" href="<?= base_url() ?>images/favicons/favicon-16x16.png">
    </head>
    <body style="background-color: #999999;">

        <div class="limiter">
            <div class="container-login100">
                <div class="login100-more" style="background-image: url('<?= base_url('assets/AdminLogin/') ?>images/');"></div>

                <div class="wrap-login100 p-l-50 p-r-50 p-t-72 p-b-50">
                    <form class="login100-form validate-form">
                        <span class="login100-form-title p-b-59">
                            <img class="img-fluid" src="<?= base_url()?>css/images/logo.png" alt="alfabee logo" style=" width: 138px;">
                        </span>
                        <div style="width: 100%;text-align: center;" id="MessageDiv">
                           
                        </div>
                        <br>
                        <div class="wrap-input100 validate-input" data-validate="Username is required">
                            <span class="label-input100">Username</span>
                            <input id="UserName" class="input100" type="text" name="username" placeholder="Username..." autocomplete="off">
                            <span class="focus-input100"></span>
                        </div>

                        <div class="wrap-input100 validate-input" data-validate = "Password is required">
                            <span class="label-input100">Password</span>
                            <input class="input100" type="password" id="Password" name="pass" placeholder="*************">
                            <span class="focus-input100"></span>
                        </div>


                        <div class="container-login100-form-btn">
                            <div class="wrap-login100-form-btn">
                                <div class="login100-form-bgbtn"></div>
                                <button type="button" class="login100-form-btn login_vendor">
                                    Login   
                                </button>
                            </div>
                            
                            <p><a href="<?= base_url('Login/vendor_forgot_password') ?>">Forgot password ?</a></p>
                        </div>
                        <span class="login_message common_message"></span>
                    </form>
                </div>
            </div>
        </div>
        <?php
        $sess_value = $this->session->userdata('language');
        ?>
        <script>var base_url = '<?= base_url() ?>';</script>
        <script>
       
        var sessionValue = '<?= $sess_value ?>';
        </script>
        <?php
$Controller = $this->router->fetch_class();
$Method = $this->router->fetch_method();
$segment3 = $this->uri->segment(3);
$segment2 = $this->uri->segment(2);
$sess_value = $this->session->userdata('language');
?>
<script>
    var base_url = '<?= base_url(); ?>';
    var Controller = '<?= $Controller ?>';
    var Method = '<?= $Method ?>';
    var segment3 = '<?= $segment3 ?>';
    var segment2 = '<?= $segment2 ?>';
    var sessionValue = '<?= $sess_value ?>';
</script>
        <script src="<?= base_url('assets/AdminLogin/vendor/jquery/') ?>jquery-3.2.1.min.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/animsition/js/') ?>animsition.min.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/bootstrap/js/') ?>popper.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/bootstrap/js/') ?>bootstrap.min.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/select2/') ?>select2.min.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/daterangepicker/') ?>moment.min.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/daterangepicker/') ?>daterangepicker.js"></script>
        <script src="<?= base_url('assets/AdminLogin/vendor/countdowntime/') ?>countdowntime.js"></script>
        <script src="<?= base_url('js/User/') ?>user.js"></script>
    </body>
</html>