</div>
<!-- END CONTAINER -->


<!-- BEGIN FOOTER -->
<div class="page-footer">
    <div class="page-footer-inner"> <?= date('Y') ?> &copy; Car Zoom &nbsp;&nbsp;&nbsp;
        <a target="_blank" href="https://alwafaagroup.com">Alwafaa Group</a> 

    </div>
    <div class="scroll-to-top">
        <i class="icon-arrow-up"></i>
    </div>
</div>
<!-- END FOOTER -->
</div>
<!-- BEGIN QUICK NAV -->
<div class="quick-nav-overlay"></div>
<!-- END QUICK NAV -->
<?php
$Controller = $this->router->fetch_class();
$Method = $this->router->fetch_method();
$segment3 = $this->uri->segment(3);
$segment2 = $this->uri->segment(2);
$sess_value = $this->session->userdata('language');
?>
<script>
    var base_url = '<?= base_url(); ?>';
    var Controller = '<?= $Controller ?>';
    var Method = '<?= $Method ?>';
    var segment3 = '<?= $segment3 ?>';
    var segment2 = '<?= $segment2 ?>';
    var sessionValue = '<?= $sess_value ?>';
</script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap/js/bootstrap.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-slimscroll/jquery.slimscroll.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-switch/js/bootstrap-switch.min.js" type="text/javascript"></script>
<!-- END CORE PLUGINS -->
<!-- BEGIN THEME GLOBAL SCRIPTS -->
<script src="<?= base_url(); ?>assets/global/scripts/app.min.js" type="text/javascript"></script>

<script src="<?= base_url(); ?>js/jquery-confirm.min.js"></script>
<!-- END THEME GLOBAL SCRIPTS -->
<script src="<?= base_url(); ?>assets/global/scripts/datatable.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/datatables.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/pages/scripts/DatatableClass.js" type="text/javascript"></script> 
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-summernote/summernote.min.js" type="text/javascript"></script>  
<script src="<?= base_url(); ?>assets/pages/scripts/components-editors.min.js" type="text/javascript"></script>


<!-- BEGIN THEME LAYOUT SCRIPTS -->
<script src="<?= base_url(); ?>assets/layouts/layout/scripts/layout.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/layouts/global/scripts/quick-sidebar.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/layouts/global/scripts/quick-nav.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/lobibox-master/js/lobibox.js"></script>
<script src="<?= base_url(); ?>assets/js/bootstrap-select.min.js"></script>
<script src="<?= base_url(); ?>assets/global/plugins/jquery-repeater/jquery.repeater.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/pages/scripts/form-repeater.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/pages/scripts/table-datatables-editable.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/global/plugins/bootstrap-multiselect/js/bootstrap-multiselect.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>assets/pages/scripts/components-bootstrap-multiselect.min.js" type="text/javascript"></script>
<script src="<?= base_url(); ?>js/jquery-ui.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>

<?php if ($Method == 'productsimage') { ?>
    <script src="<?= base_url('js/additional/'); ?>dropzone.min.js" type="text/javascript"></script>
    <script src="<?= base_url('js/additional/'); ?>form-dropzone.min.js" type="text/javascript"></script>
    <script src="<?= base_url('js/additional/'); ?>fileupload.js" type="text/javascript"></script>
    <link href="<?= base_url('js/additional/css/'); ?>dropzone.min.css" rel="stylesheet" type="text/css" />
    <link href="<?= base_url('js/additional/css/'); ?>basic.min.css" rel="stylesheet" type="text/css" /> 
<?php } ?>

<link rel="stylesheet" href="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/build/css/bootstrap-datetimepicker.css">
<script src="https://cdnjs.cloudflare.com/ajax/libs/moment.js/2.9.0/moment-with-locales.js"></script>
<script src="https://cdn.rawgit.com/Eonasdan/bootstrap-datetimepicker/e8bddc60e73c1ec2475f827be36e1957af72e2ea/src/js/bootstrap-datetimepicker.js"></script>


<script src="<?= base_url(); ?>js/User/user.js"></script>
<script src="<?= base_url(); ?>js/Admin/common.js"></script>
<!--<script src="<?= base_url(); ?>js/common.js" type="text/javascript"></script>-->
<!--<script src="<?= base_url(); ?>js/admin_vendor.js" type="text/javascript"></script>-->
<!--<script src="<?= base_url(); ?>js/vendornew.js"></script>-->

<script>
  
</script>

</body>

</html>