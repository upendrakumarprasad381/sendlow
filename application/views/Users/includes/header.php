<?php
$Controller = $this->router->fetch_class();
$Method = $this->router->fetch_method();

$user_session = $this->session->userdata('Users');
$segment2 = $this->uri->segment(2);
?>
<!DOCTYPE html>
<html lang="en">
    <head>
        <meta charset="utf-8" />
        <title>SENDLOW</title>
        <!-- BEGIN GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.4.2/css/all.css" integrity="sha384-/rXc/GQVaYpyDdyxK+ecHPVYJSN9bmVFBvjA/9eOB+pb3F2w2N6fc5qB9Ew5yIns" crossorigin="anonymous">
        <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,300,600,700&subset=all" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/simple-line-icons/simple-line-icons.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap/css/bootstrap.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-switch/css/bootstrap-switch.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-multiselect/css/bootstrap-multiselect.css" rel="stylesheet" type="text/css" />
        <!-- END GLOBAL MANDATORY STYLES -->
        <link rel="stylesheet" href="https://www.ontime24.ae/assets/users/css/ekko-lightbox.css">
            <link href="<?= base_url() ?>assets/global/plugins/bootstrap-summernote/summernote.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN THEME GLOBAL STYLES -->
        <link href="<?= base_url() ?>assets/global/css/components.min.css" rel="stylesheet" id="style_components" type="text/css" />
        <link href="<?= base_url() ?>assets/global/css/plugins.min.css" rel="stylesheet" type="text/css" />
        <!-- END THEME GLOBAL STYLES -->
        <?php if ($Method == 'MediaUpload') { ?>
            <link href="<?= base_url('css/' . ADMIN_DIR); ?>dropzone.min.css" rel="stylesheet" type="text/css" />
            <link href="<?= base_url('css/' . ADMIN_DIR); ?>basic.min.css" rel="stylesheet" type="text/css" />   
        <?php } ?>
        <link href="<?= base_url() ?>assets/global/plugins/datatables/datatables.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/datatables/plugins/bootstrap/datatables.bootstrap.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
       
       <link href="<?= base_url() ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/bootstrap-datetimepicker/css/bootstrap-datetimepicker.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/global/plugins/jquery-nestable/jquery.nestable.css" rel="stylesheet" type="text/css" />
        <!-- BEGIN THEME LAYOUT STYLES -->
        <link href="<?= base_url() ?>assets/layouts/layout/css/layout.min.css" rel="stylesheet" type="text/css" />
        <link href="<?= base_url() ?>assets/layouts/layout/css/themes/darkblue.min.css" rel="stylesheet" type="text/css" id="style_color" />
        <link href="<?= base_url() ?>assets/layouts/layout/css/custom.min.css" rel="stylesheet" type="text/css" />
        <link rel="stylesheet" href="<?= base_url() ?>assets/lobibox-master/dist/css/lobibox.min.css"/>

        <!-- END THEME LAYOUT STYLES -->
        <link rel="stylesheet" href="<?= base_url() ?>css/jquery-ui.css">
        <link rel="stylesheet" href="<?= base_url('css/jquery-confirm.min.css') ?>">
        <link href="<?= base_url(); ?>assets/css/bootstrap-select.min.css" rel="stylesheet">
        <link href="<?= base_url('css/' . ADMIN_DIR); ?>main.css" rel="stylesheet" type="text/css" /> 
         <link rel="stylesheet" href="<?= base_url() ?>css/main.css"/>
        <!--<link rel="shortcut icon" href="<?= base_url('files/images') ?>/favicon.ico" type="image/x-icon">-->
                <link href="https://cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.min.css" rel="stylesheet">

        <!-- END HEAD -->

    <body class="page-header-fixed page-sidebar-closed-hide-logo page-content-white">
        <div class="page-wrapper">
            <!-- BEGIN HEADER -->
            <div class="page-header navbar navbar-fixed-top">
                <!-- BEGIN HEADER INNER -->
                <div class="page-header-inner ">
                    <!-- BEGIN LOGO -->
                    <div class="page-logo">
                        <a href="<?= base_url('vendor_dashboard') ?>">
                            <!--<img src="<?= base_url() ?>assets/layouts/layout/img/ohr.png" alt="logo" class="logo-default" /> </a>-->
                            <!--<img src="<?= base_url() ?>assets/layouts/layout/img/fix.png" alt="logo" class="logo-default" /> </a>-->

                        <div class="menu-toggler sidebar-toggler">
                            <span></span>
                        </div>
                    </div>
                    <!-- END LOGO -->
                    <!-- BEGIN RESPONSIVE MENU TOGGLER -->
                    <a href="javascript:;" class="menu-toggler responsive-toggler" data-toggle="collapse" data-target=".navbar-collapse">
                        <span></span>
                    </a>
                    <!-- END RESPONSIVE MENU TOGGLER -->
                    <!-- BEGIN TOP NAVIGATION MENU -->
                    <div class="top-menu">
                        <ul class="nav navbar-nav pull-right">
                            <li  class="dropdown dropdown-extended dropdown-notification" id="HeaderNotificationHtml">
                                <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                    <i class="icon-bell"></i>
                                    <!--<span class="badge badge-default"> <span class="TodayNotifaction">-->
                                         <?php
                                        // $get_not_count = Getcountnotification($admin_session->id);
                                        // echo $get_not_count[0]->count;
                                        ?>
                                        <!--</span>  </span>-->
                                </a>
                                <ul class="dropdown-menu">
                                    <li class="external">
                                        <h3>
                                            <span class="bold"><span class="TodayNotifaction"><?php
                                            // echo $get_not_count[0]->count;
                                        ?></span> new</span> notifications</h3>
                                        <a onclick='make_read()'>Make All Read</a>

                                    </li>
                                    <li>
                                        <ul class="dropdown-menu-list scroller" style="height: 250px;" data-handle-color="#637283" id="HeaderNotificationHtml">
                                             <?php
                                        // $get_notification = GetNotification($admin_session->id);
                                        // for ($i = 0; $i < count($get_notification); $i++) {
                                            ?>
                                            <!--<li>
                                                <a onclick="change_notification_status(<?= $get_notification[$i]->id ?>,<?= $admin_session->id ?>, '<?= $get_notification[$i]->redirect_url ?>')" >
                                                    <span class="time"><?php echo $get_notification[$i]->timestamp ?></span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-plus"></i>
                                                        </span><?php echo $get_notification[$i]->notification_message ?></span>
                                                </a>
                                            </li>-->
                                            
                                            <li>
                                                <a onclick="change_notification_status" >
                                                    <span class="time"></span>
                                                    <span class="details">
                                                        <span class="label label-sm label-icon label-success">
                                                            <i class="fa fa-plus"></i>
                                                        </span></span>
                                                </a>
                                            </li>
                                            
                                        <?php 
                                        // } 
                                        ?>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                            <?php
                            if (!empty($user_session)) {
                                if (isset($user_session->image) && $user_session->image != '') {
                                    $image = base_url() . 'uploads/vendor_images/' . $user_session->image;
                                } else {
                                    $image = base_url('images/profile-default.png');
                                }
                                ?>
                                <li class="dropdown dropdown-user">
                                    <a href="javascript:;" class="dropdown-toggle" data-toggle="dropdown" data-hover="dropdown" data-close-others="true">
                                        <img alt="" class="img-circle" src="<?= $image ?>" />
                                        <span class="username username-hide-on-mobile"><?= $user_session->first_name ?></span>
                                        <i class="fa fa-angle-down"></i>
                                    </a>
                                    <ul class="dropdown-menu dropdown-menu-default">
                                       
                                        <li>
                                            <a href="<?= base_url('UserLoginout'); ?>">
                                                <i class="icon-key"></i>logout</a>
                                        </li>
                                    </ul>
                                </li>
                            <?php } ?>
                        </ul>
                    </div>
                    <!-- END TOP NAVIGATION MENU -->
                </div>
                <!-- END HEADER INNER -->
            </div>
            <!-- END HEADER -->
            <!-- BEGIN HEADER & CONTENT DIVIDER -->
            <div class="clearfix"> </div>
            <!-- END HEADER & CONTENT DIVIDER -->

            <!-- BEGIN CONTAINER -->
            <div class="page-container">
                <div class="page-sidebar-wrapper">
                    <div class="page-sidebar navbar-collapse collapse">
                        <ul class="page-sidebar-menu  page-header-fixed " data-keep-expanded="false" data-auto-scroll="true" data-slide-speed="200" style="padding-top: 20px">
                            
                            <li class="nav-item start">
                                <a href="<?= base_url('Users/manage_account'); ?>" class="nav-link nav-toggle">
                                    <i class="fas fa-user"></i>
                                    <span class="title">Account</span>
                                </a>
                            </li>

                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('vendor/products'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-box-open"></i>-->
                            <!--        <span class="title">Products</span>-->
                            <!--    </a>-->
                            <!--</li>-->
                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('vendor/products?type=offer'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-box-open"></i>-->
                            <!--        <span class="title">Offer Products</span>-->
                            <!--    </a>-->
                            <!--</li>-->

                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('vendor/orders'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-shopping-cart"></i>-->
                            <!--        <span class="title">Orders</span>-->
                            <!--    </a>-->
                            <!--</li>-->

                            <!--<li class="nav-item start">-->
                            <!--    <a href="<?= base_url('vendor/feedback'); ?>" class="nav-link nav-toggle">-->
                            <!--        <i class="fas fa-comments"></i>-->
                            <!--        <span class="title">Feedback</span>-->
                            <!--    </a>-->
                            <!--</li>-->

                        </ul>
                        <!-- END SIDEBAR MENU -->
                        <!-- END SIDEBAR MENU -->
                    </div>
                    <!-- END SIDEBAR -->
                </div>
                <!-- END SIDEBAR -->