<?php
$Session = GetSessionArrayUser();
$todays = date("Y-m-d");
$oneWeek = date("Y-m-d", strtotime("-1 week"));


// $weekly = ",(SELECT SUM(OD.sub_total) FROM `order_details` OD LEFT JOIN orders O ON O.order_id=OD.order_id WHERE O.archive=0 AND DATE(O.timestamp) BETWEEN '$oneWeek' AND '$todays' AND OD.vendor_id='$Session->id') AS weekly";
// $total = ",(SELECT SUM(OD.sub_total) FROM `order_details` OD LEFT JOIN orders O ON O.order_id=OD.order_id WHERE O.archive=0 AND OD.vendor_id='$Session->id') AS total";

// $total_order = ",(SELECT COUNT(a.total_order)  FROM (SELECT COUNT(OD.order_id) AS total_order FROM `order_details` OD LEFT JOIN orders O ON O.order_id=OD.order_id WHERE O.archive=0 AND OD.vendor_id='$Session->id' GROUP BY OD.order_id) AS a) AS total_order";


// $qry = "SELECT COUNT(product_id) AS total_prod $weekly $total $total_order FROM `products` WHERE archive=0 AND status=0 AND main_product_id=0 AND vendor_id='$Session->id'";
// $list = $this->Database->select_qry_array($qry);
// $list = !empty($list) ? $list[0] : '';

?>
<style>
    .fasCd {
        opacity: 1 !important;
        line-height: 50px !important;
        margin-left: -5px !important;
        font-size: 50px !important;
    }
</style>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">


            <!--<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">-->
            <!--    <a class="dashboard-stat dashboard-stat-v2 green" href="<?= base_url('vendor/products') ?>">-->
            <!--        <div class="visual">-->
            <!--            <i class="fasCd fas fa-box-open"></i>-->
            <!--        </div>-->
            <!--        <div class="details">-->
            <!--            <div class="number">-->
            <!--                <span data-counter="counterup" data-value=""><?= !empty($list->total_prod) ? $list->total_prod : '0' ?></span>-->
            <!--            </div>-->
            <!--            <div class="desc">Products</div>-->
            <!--        </div>-->
            <!--    </a>-->
            <!--</div>-->
            
            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a class="dashboard-stat dashboard-stat-v2 red" href="<?= base_url('vendor/orders') ?>">
                    <div class="visual">
                        <i class="fasCd fa fa-shopping-cart"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""></span>
                        </div>
                        <div class="desc"> Orders</div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a style="background: #1665a3;" class="dashboard-stat dashboard-stat-v2 purple" href="<?= base_url('vendor/orders') ?>">
                    <div class="visual">
                        <i class="fasCd fa"></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""></span>
                        </div>
                        <div class="desc"> Weekly Order</div>
                    </div>
                </a>
            </div>

            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
                <a style="background: #795548;" class="dashboard-stat dashboard-stat-v2 purple" href="<?= base_url('vendor/orders') ?>">
                    <div class="visual">
                        <i class="fasCd fa "></i>
                    </div>
                    <div class="details">
                        <div class="number">
                            <span data-counter="counterup" data-value=""></span>
                        </div>
                        <div class="desc"> Total Order</div>
                    </div>
                </a>
            </div>
        </div>


        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Todays Order</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <table class="table table-striped table-bordered table-hover DataTableClass">
                            <thead>
                                <tr>
                                    <th>Sl</th>
                                    <th>Order No</th>
                                    <th>Customer Name</th>
                                    <th>Price</th>
                                    <th>Delivery Country</th>
                                    <th>Order Date</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                // $join1 = "LEFT JOIN user_order_address OA ON OA.user_id=U.id LEFT JOIN country C ON C.id= OA.country ";
                                // $join2 = "LEFT JOIN orders O ON O.order_id=OD.order_id LEFT JOIN users U ON U.id=O.user_id";
                                // $select = ",O.order_no,U.name,O.order_status,O.timestamp AS order_date,SUM(OD.sub_total) AS subtot";
                                // $qry = "SELECT  OD.* $select FROM `order_details` OD $join2 $join1 WHERE O.archive=0 AND OD.vendor_id='$Session->id' AND DATE(O.timestamp)='" . date('Y-m-d') . "' GROUP BY O.order_id ORDER BY O.order_id DESC LIMIT 2000";
                                // $order = $this->Database->select_qry_array($qry);
                                // for ($i = 0; $i < count($order); $i++) {
                                //     $d = $order[$i];
                                    ?>
                                    <tr>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td></td>
                                        <td>
                                            <a href="" title="Order summary"><span class="label label-sm label-danger"><i class="fas fa-external-link-alt"></i></span></a><!--base_url('vendor/OrderDetails?orderId=' . base64_encode($d->order_id))-->
                                        </td>
                                    </tr>
                                <?php 
                                // }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>



        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN EXAMPLE TABLE PORTLET-->
                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Last 12 month Order List</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">

                        <div id="newchartdiv"></div>
                        <?php
                        // $todays = date('Y-m-d');
                        // $gAryrrr = [];
                        // for ($i = 0; $i < 12; $i++) {
                        //     $date = date("Y-m-d", strtotime("-$i month", strtotime($todays)));
                        //     $month = date("m", strtotime($date));
                        //     $year = date("Y", strtotime($date));
                        //     $qry = "SELECT COUNT(OD.order_id) AS total FROM `order_details` OD LEFT JOIN orders O ON O.order_id=OD.order_id WHERE MONTH(OD.timestamp)='" . $month . "' AND YEAR(OD.timestamp)='" . $year . "' AND OD.vendor_id='$Session->id' AND O.archive=0";
                        //     $order = $this->Database->select_qry_array($qry);
                        //     $toto = !empty($order[0]->total) ? $order[0]->total : 0;
                        //     $gAryrrr[] = array('year' => date('M Y', strtotime($date)), 'income' => $toto);
                        // }
                        ?>

                    </div>
                </div>
                <!-- END EXAMPLE TABLE PORTLET-->

            </div>
        </div>


        <div class="row">
            <div class="col-md-12">


                <div class="portlet light bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase">Receiving Total Order One Month Graph</span>
                        </div>
                        <div class="tools"> </div>
                    </div>
                    <div class="portlet-body">
                        <div id="chartdiv"></div>	
                    </div>
                </div>



            </div>

        </div>





    </div>
    <!-- END CONTENT BODY -->
</div>
<!-- END CONTENT -->





<style>
    #chartdiv,#newchartdiv {
        width: 100%;
        height: 500px;
    }
</style>

<!-- Resources -->
<script src="https://cdn.amcharts.com/lib/4/core.js"></script>
<script src="https://cdn.amcharts.com/lib/4/charts.js"></script>
<script src="https://cdn.amcharts.com/lib/4/themes/animated.js"></script>
<?php
// $todays = date('Y-m-d');
// $gAry = [];
// for ($i = 0; $i < 30; $i++) {
//     $date = date("Y-m-d", strtotime("-$i days", strtotime($todays)));

//     $qry = "SELECT COUNT(OD.order_id) AS total FROM `order_details` OD LEFT JOIN orders O ON O.order_id=OD.order_id WHERE DATE(OD.timestamp)='" . $date . "' AND OD.vendor_id='$Session->id' AND O.archive=0";

//     $order = $this->Database->select_qry_array($qry);
//     $toto = !empty($order[0]->total) ? $order[0]->total : 0;

//     $gAry[] = array('category' => date('d M Y', strtotime($date)), 'value' => $toto);
// }
?>
<!-- Chart code -->
<script>
    // am4core.ready(function () {

// Themes begin
        // am4core.useTheme(am4themes_animated);
// Themes end

    //     var chart = am4core.create("chartdiv", am4charts.XYChart);



    //     chart.data = JSON.parse('<?= json_encode($gAry) ?>');
    //     var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
    //     categoryAxis.renderer.grid.template.location = 0;
    //     categoryAxis.dataFields.category = "category";
    //     categoryAxis.renderer.minGridDistance = 15;
    //     categoryAxis.renderer.grid.template.location = 0.5;
    //     categoryAxis.renderer.grid.template.strokeDasharray = "1,3";
    //     categoryAxis.renderer.labels.template.rotation = -90;
    //     categoryAxis.renderer.labels.template.horizontalCenter = "left";
    //     categoryAxis.renderer.labels.template.location = 0.5;

    //     categoryAxis.renderer.labels.template.adapter.add("dx", function (dx, target) {
    //         return -target.maxRight / 2;
    //     })

    //     var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
    //     valueAxis.tooltip.disabled = true;
    //     valueAxis.renderer.ticks.template.disabled = true;
    //     valueAxis.renderer.axisFills.template.disabled = true;

    //     var series = chart.series.push(new am4charts.ColumnSeries());
    //     series.dataFields.categoryX = "category";
    //     series.dataFields.valueY = "value";
    //     series.tooltipText = "{valueY.value}";
    //     series.sequencedInterpolation = true;
    //     series.fillOpacity = 0;
    //     series.strokeOpacity = 1;
    //     series.strokeDashArray = "1,3";
    //     series.columns.template.width = 0.01;
    //     series.tooltip.pointerOrientation = "horizontal";

    //     var bullet = series.bullets.create(am4charts.CircleBullet);

    //     chart.cursor = new am4charts.XYCursor();

    //     chart.scrollbarX = new am4core.Scrollbar();
    //     chart.scrollbarY = new am4core.Scrollbar();


    // }); // end am4core.ready()
</script>

<script>
    // am4core.ready(function () {
// Themes begin
        // am4core.useTheme(am4themes_animated);
// Themes end
// Create chart instance
        // var chart = am4core.create("newchartdiv", am4charts.XYChart);
// Export
        // chart.exporting.menu = new am4core.ExportMenu();
// Data for both series
        // var data = JSON.parse('<?= json_encode($gAryrrr) ?>');
        

        /* Create axes */
        // var categoryAxis = chart.xAxes.push(new am4charts.CategoryAxis());
        // categoryAxis.dataFields.category = "year";
        // categoryAxis.renderer.minGridDistance = 30;

        /* Create value axis */
        // var valueAxis = chart.yAxes.push(new am4charts.ValueAxis());
        /* Create series */
    //     var columnSeries = chart.series.push(new am4charts.ColumnSeries());
    //     columnSeries.name = "Income";
    //     columnSeries.dataFields.valueY = "income";
    //     columnSeries.dataFields.categoryX = "year";

    //     columnSeries.columns.template.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]"
    //     columnSeries.columns.template.propertyFields.fillOpacity = "fillOpacity";
    //     columnSeries.columns.template.propertyFields.stroke = "stroke";
    //     columnSeries.columns.template.propertyFields.strokeWidth = "strokeWidth";
    //     columnSeries.columns.template.propertyFields.strokeDasharray = "columnDash";
    //     columnSeries.tooltip.label.textAlign = "middle";

    //     var lineSeries = chart.series.push(new am4charts.LineSeries());
    //     lineSeries.name = "Expenses";
    //     lineSeries.dataFields.valueY = "expenses";
    //     lineSeries.dataFields.categoryX = "year";

    //     lineSeries.stroke = am4core.color("#fdd400");
    //     lineSeries.strokeWidth = 3;
    //     lineSeries.propertyFields.strokeDasharray = "lineDash";
    //     lineSeries.tooltip.label.textAlign = "middle";

    //     var bullet = lineSeries.bullets.push(new am4charts.Bullet());
    //     bullet.fill = am4core.color("#fdd400"); // tooltips grab fill from parent by default
    //     bullet.tooltipText = "[#fff font-size: 15px]{name} in {categoryX}:\n[/][#fff font-size: 20px]{valueY}[/] [#fff]{additional}[/]"
    //     var circle = bullet.createChild(am4core.Circle);
    //     circle.radius = 4;
    //     circle.fill = am4core.color("#fff");
    //     circle.strokeWidth = 3;
    //     chart.data = data;

    // }); // end am4core.ready()
</script>
