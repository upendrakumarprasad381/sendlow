<?php
$user_session = $this->session->userdata('Users');
$customer = GetUserArrayBy($user_session->id);
?>
<!-- BEGIN CONTENT -->
<div class="page-content-wrapper">
    <!-- BEGIN CONTENT BODY -->
    <div class="page-content">
        <!-- BEGIN PAGE HEADER-->
        <!-- END PAGE HEADER-->
        <div class="row">
            <div class="col-md-12">
                <!-- BEGIN PORTLET-->
                <div class="portlet light form-fit bordered">
                    <div class="portlet-title">
                        <div class="caption font-dark">
                            <span class="caption-subject bold uppercase"> Manage Account</span>
                        </div>
                    </div>
                    <div class="portlet-body form">
                        <!-- BEGIN FORM-->
                        <form  class="form-horizontal form-bordered" method="post" enctype="multipart/form-data" action="<?=base_url()?>Users/updateUser">
                            <input type="hidden" name="id" value="<?= !empty($customer->id) ? $customer->id : '' ?>" id="id">
                            <div class="form-body">
                                <div class="form-group">
                                    <input type="hidden" name="id" value="<?=!empty($customer->id) ? $customer->id : '' ?>">
                                    <label class='control-label col-md-3'>First Name  </label> 
                                    <div class="col-md-4">    
                                        <input type="text" class="form-control" name="fname" id="fname" value="<?=!empty($customer->first_name) ? $customer->first_name : '' ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class='control-label col-md-3'>Last Name  </label> 
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="lname" id="lname" value="<?=!empty($customer->last_name) ? $customer->last_name : '' ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Email</label>
                                    <div class="col-md-4">
                                        <input type="text" class="form-control" name="email" id="email_id" value="<?=!empty($customer->email) ? $customer->email : '' ?>">
                                    </div>
                                </div>
                               
                                <div class="form-group">
                                    <label class="control-label col-md-3">Mobile Number</label>
                                    <div class="col-md-3">
                                        <input type="text" class="form-control" name="phone" id="phone" value="<?=!empty($customer->mobile) ? $customer->mobile : '' ?>">
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">License</label>
                                    <div class="col-md-3">
                                        <!--<form method="post" id="file_upload_form_trade_license" name="file_upload_form_trade_license">-->
                                            <input type="file" id="license" name="license" value="">
                                        <!--</form>-->
                                    </div>
                                    <?php
                                    if(!empty($customer->license))
                                    {
                                    ?>
                                    <div class="col-md-4">
                                        <?php 
                                            $file = !empty($customer->license) ? $customer->license : '';
                                            $fileType = explode('.',$file);
                                            $extension = $fileType[1];
                                            if(($extension == 'PDF') || ($extension == 'pdf'))
                                            {
                                        ?>
                                        Existing Record : <a href="<?=base_url()?>uploads/Register/<?=!empty($customer->license) ? $customer->license : '' ?>" target="_blank"><?=$customer->license?> </a>
                                        <?php
                                            }
                                            else
                                            {
                                        ?>
                                        Existing Record : <img src="<?=base_url()?>uploads/Register/<?=!empty($customer->license) ? $customer->license : '' ?>" width="100" height="100" />
                                        <?php
                                            }
                                        ?>
                                        <p><a class="remove_user_doc" path="uploads/Register/" name="<?=isset($customer->license)?$customer->license:''?>" table="register" id="<?=$customer->id?>" type="multiple" field="license">Remove </a></p>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">VAT</label>
                                    <div class="col-md-3">
                                        
                                       <!--<form method="post" id="file_upload_form_id_proof" name="file_upload_form_id_proof">-->
                                            <input type="file" id="VAT" name="VAT" value="">
                                        <!--</form> -->
                                    </div>
                                    <?php
                                    if(!empty($customer->VAT))
                                    {
                                    ?>
                                    <div class="col-md-4">
                                        <?php 
                                            $file = !empty($customer->VAT) ? $customer->VAT : '';
                                            $fileType = explode('.',$file);
                                            $extension = $fileType[1];
                                            if(($extension == 'PDF') || ($extension == 'pdf'))
                                            {
                                        ?>
                                        Existing Record : <a href="<?=base_url()?>uploads/Register/<?=!empty($customer->VAT) ? $customer->VAT : '' ?>" target="_blank"><?=$customer->VAT?> </a>
                                        <?php
                                            }
                                            else
                                            {
                                        ?>
                                        Existing Record : <img src="<?=base_url()?>uploads/Register/<?=!empty($customer->VAT) ? $customer->VAT : '' ?>"  width="100" height="100" />
                                        <?php
                                            }
                                        ?>
                                        <p><a class="remove_user_doc" path="uploads/Register/" name="<?=isset($customer->VAT)?$customer->VAT:''?>" table="register" id="<?=$customer->id?>" type="multiple" field="VAT">Remove </a></p>
                                    </div>
                                    <?php
                                    }
                                    ?>
                                </div>
                                <div class="form-group">
                                    <label class="control-label col-md-3">Password</label>
                                    <div class="col-md-4">
                                        <input type="password" class="form-control" name="password" id="password" value="">
                                    </div>
                                </div>
                                <div class="form-actions">
                                    <div class="row">
                                        <div class="col-md-offset-3 col-md-9">
                                            <button type="submit" class="btn blue" name="updateSeller" id='updateSeller'>
                                                <i class="fa fa-check"></i> Submit</button>
                                            <!--<button type="button" onclick="window.location = '<?= base_url('admin/offer_list'); ?>';" class="btn default">Cancel</button>-->
                                        </div>
                                    </div>
                                </div>

                                <!-- END FORM-->
                            </div>
                        </form>
                    </div>
                </div>
            </div>





        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->
