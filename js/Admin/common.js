$(document).ready(function () {

    $('[data-toggle="popover-hover"]').popover({
        html: true,
        trigger: 'hover',
        placement: 'middle',
        content: function () {
            return '<img  src="' + $(this).data('img') + '" width="450px" height="450px"/>';
        }
    });
    if (location.protocol !== "https:") {
        location.protocol = "https:";
    }
    $(document).on('click', '[data-toggle="lightbox"]', function (event) {
        event.preventDefault();
        $(this).ekkoLightbox();
    });
    $('.maxlength').keypress(function () {
        var maxlength = $(this).attr('maxlength');
        if (maxlength == undefined) {
            maxlength = 5;
        }
        if (this.value.length >= maxlength) {
            return false;
        }
    });
    $(function () {
        var Obj = {dateFormat: 'yy-mm-dd', minDate: 0};
        $(".datepicker").datepicker(Obj);
    });
    $(function () {
        var Obj = {dateFormat: 'yy-mm-dd'};
        $(".datepicker1").datepicker(Obj);
    });
    $(function () {
        var Obj = {dateFormat: 'yy-mm-dd'};
        $(".datepicker2").datepicker(Obj);
    });

    $('body').on('focus', ".datetimepicker", function () {

        $(".datetimepicker").datetimepicker({startDate: new Date()}).datetimepicker('update', new Date());


    });
    $('body').on('click', '.autoupdate', function () {
        var updatejson = $(this).attr('updatejson');
        var condjson = $(this).attr('condjson');
        var dbtable = $(this).attr('dbtable');
        var data = {function: 'autoupdate', updatejson: updatejson, condjson: condjson, dbtable: dbtable};
        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are you sure want to delete!.';
        $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        });
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
    });
    $('.UserDelete').click(function () {

        var UserId = $(this).attr('UserId');
        var UserName = $(this).attr('UserName');
        var baseurl = $(this).attr('baseurl');
        var data = {function: 'UserDelete', UserId: UserId};
        Lobibox.confirm({
            msg: "Are you sure you want to delete this User : " + UserName + " ?",
            title: 'User details',
            callback: function ($this, type, ev) {
                if (type == 'yes') {
                    // alert(baseurl);
                    $.ajax({
                        url: baseurl + "Admin/Helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                window.location = '';
                            }
                        }
                    });
                }
            }
        });
    });
    $('body').on('click', '.autodelete', function () {
        var condjson = $(this).attr('condjson');
        var dbtable = $(this).attr('dbtable');
        var removefile = $(this).attr('removefile');
        var data = {function: 'autodelete', condjson: condjson, dbtable: dbtable, removefile: removefile};
        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are you sure want to delete!.';
        $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Admin/Helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == true) {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        });
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
    });
    // tooltip();
});

$('#SubmitNewUser').click(function () {
    var json = '';
    json = json + '{';
    if ($('#UserName').val() == '') {
        $('#UserName').focus();
        $('#UserName').css('border-color', 'red');
        return false;
    } else {
        $('#UserName').css('border-color', '');
        json = json + '"name":"' + $('#UserName').val() + '",';
    }

    if ($('#UserEmail').val() == '') {
        $('#UserEmail').focus();
        $('#UserEmail').css('border-color', 'red');
        return false;
    } else {
        $('#UserEmail').css('border-color', '');
        json = json + '"email":"' + $('#UserEmail').val() + '",';
    }
    if ($('#UserType').val() == '') {
        $('#UserType').focus();
        $('#UserType').css('border-color', 'red');
        return false;
    } else {
        $('#UserType').css('border-color', '');
        json = json + '"user_type":"' + $('#UserType').val() + '",';
    }


    if ($('#UserPassword').val() == '') {
        // $('#UserPassword').focus();
        // $('#UserPassword').css('border-color', 'red');
        // return false;
    } else {
        $('#UserPassword').css('border-color', '');
        json = json + '"password":"' + $('#UserPassword').val() + '",';
    }

    json = json + '"mobile":"' + $('#mobile').val() + '",';
    json = json + '"user_type":"' + $('#UserType').val() + '",';
    json = json + '"status":"' + $('#UserStatus').val() + '",';
    json = json + '"UserId":"' + $('#UserId').val() + '"';
    json = json + '}';
    var data = {json: json};
    var baseurl = $('#baseurl').val();
    $.confirm({
        title: 'Are you sure you want to Save',
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        data: data,
                        type: 'POST',
                        async: false,
                        url: base_url + "Admin/AddNewUser",
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                toastr.success(json.message, 'Success Alert', {timeOut: 5000})
                                setTimeout(function () {
                                    window.location = base_url + 'Admin/UserManagement';

                                }, 2000);
                            } else
                            {
                                toastr.error(json.message, 'Error Alert', {timeOut: 5000})
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#Submitcountry').click(function () {
    var json = {};
    json['country'] = $('#Name').val();
    json['country_ar'] = $('#Name_ar').val();
    json['code'] = $('#code').val();
    json['archive'] = $('#archive').val();

    json['country_from'] = ($('#from').is(':checked') ? 1 : 0);
    json['country_to'] = ($('#to').is(':checked') ? 1 : 0);
    json['id'] = $('#Id').val();
    if ($('#Name').val() == '') {
        $('#Name').focus();
        return false;
    }
    if ($('#code').val() == '') {
        $('#code').focus();
        return false;
    }
    var myAr = [];
    $('.courier_id:checked').each(function () {
        myAr.push($(this).val());
    });
    json['courier_id'] = myAr;

    var data = {
        json: json,

    };
    $.confirm({
        title: "Do you want to submit?",
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: "submit",
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/country/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {

                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'admin/country';
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            } catch (e) {
                                alert(e);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: "cancel",
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#Submitcity').click(function () {
    var json = '';
    json = json + '{';
    var Name = $('#Name');
    var country_id = $('#country_id');
    if (Name.val() == '') {
        Name.focus();
        Name.css('border-color', 'red');
        return false;
    } else {
        Name.css('border-color', '');
        json = json + '"city":"' + Name.val() + '",';
    }
    json = json + '"country_id":"' + country_id.val() + '",';
    json = json + '"post_code":"' + $('#post_code').val() + '",';
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';
    var data = {
        json: json
    };

    $.confirm({
        title: "Do you want to submit?",
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: "Submit",
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "admin/city/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {

                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'admin/city';
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            } catch (e) {
                                alert(e);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: "Cancel",
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#SubmitDiscount').click(function () {
    var json = '';
    json = json + '{';
    var discount = $('#discount');
    var register_type = $('#type');
    var amount = $('#amount');
    if (discount.val() == '') {
        discount.focus();
        discount.css('border-color', 'red');
        return false;
    } else {
        discount.css('border-color', '');
        json = json + '"discount":"' + discount.val() + '",';
    }
    if (amount.val() == '') {
        amount.focus();
        amount.css('border-color', 'red');
        return false;
    } else {
        amount.css('border-color', '');
        json = json + '"amount":"' + amount.val() + '",';
    }
    json = json + '"register_type":"' + register_type.val() + '",';
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';
    var data = {
        json: json
    };

    $.confirm({
        title: "Do you want to submit?",
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: "Submit",
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/autoDiscount/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {

                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'Admin/autoDiscount';
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            } catch (e) {
                                alert(e);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: "Cancel",
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#add_offer').click(function () {
    var json = {};
    if ($('#paidAmount').val() == '') {
        $('#paidAmount').focus();
        $('#paidAmount').css('border-color', 'red');
        return false;
    } else {
        $('#paidAmount').css('border-color', '');
        json['paid_amount'] = $('#paidAmount').val();
    }
    if ($('#discountAmount').val() == '') {
        $('#discountAmount').focus();
        $('#discountAmount').css('border-color', 'red');
        return false;
    } else {
        $('#discountAmount').css('border-color', '');
        json['get_amount'] = $('#discountAmount').val();
    }
    if ($('#start_date').val() == '') {
        $('#start_date').focus();
        $('#start_date').css('border-color', 'red');
        return false;
    } else {
        $('#start_date').css('border-color', '');
        json['start_date'] = $('#start_date').val();
    }
    if ($('#expiry_date').val() == '') {
        $('#expiry_date').focus();
        $('#expiry_date').css('border-color', 'red');
        return false;
    } else {
        $('#expiry_date').css('border-color', '');
        json['expiry_date'] = $('#expiry_date').val();
    }

    json['id'] = $('#offerId').val();
    var data = {json: json};
    $.confirm({
        title: "Are You Sure Want To Submit?",
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-green',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/add_offers",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            if (jQuery.parseJSON(data)) {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    window.location = base_url + 'Admin/offers';
                                } else {
                                    alert(json.message)
                                }
                            }
                        }
                    });


                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-warning',
                action: function () {
                }
            },
        }
    });
});

$('#SubmitHomeBanner').click(function () {
    var json = '';
    json = json + '{';
    json = json + '"text":"' + $('#image_text').val() + '",';
    json = json + '"status":"' + $('#HomeStatus').val() + '",';
    json = json + '"url":"' + $('#url').val() + '",';
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';

    var OldBanner = $('#ImagesEncode').attr('OldImage');
    var NewBanner = $('#ImagesEncode').attr('newimage');
    var data = {
        json: json,
        OldBanner: OldBanner,
        NewBanner: NewBanner
    };

    $.confirm({
        title: 'Are you sure you want to Save',
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/home_banner/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    window.location = base_url + 'Admin/home_banner';
                                } else {
                                    alert(json.message)
                                }
                            } catch (e) {
                                alert(e)
                            }

                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#SubmitHomeRightBanner').click(function () {
    var json = '';
    json = json + '{';
    json = json + '"status":"' + $('#HomeRightStatus').val() + '",';
    json = json + '"url":"' + $('#url').val() + '",';
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';

    var OldBanner = $('#ImagesEncode').attr('OldImage');
    var NewBanner = $('#ImagesEncode').attr('newimage');
    var data = {
        json: json,
        OldBanner: OldBanner,
        NewBanner: NewBanner
    };

    $.confirm({
        title: 'Are you sure you want to Save',
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/rightBanner/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    window.location = base_url + 'Admin/rightBanner';
                                } else {
                                    alert(json.message)
                                }
                            } catch (e) {
                                alert(e)
                            }

                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#SubmitHomeLeftBanner').click(function () {
    var json = '';
    json = json + '{';
    json = json + '"status":"' + $('#HomeLeftStatus').val() + '",';
    json = json + '"url":"' + $('#url').val() + '",';
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';

    var OldBanner = $('#ImagesEncode').attr('OldImage');
    var NewBanner = $('#ImagesEncode').attr('newimage');
    var data = {
        json: json,
        OldBanner: OldBanner,
        NewBanner: NewBanner
    };

    $.confirm({
        title: 'Are you sure you want to Save',
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/leftBanner/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    window.location = base_url + 'Admin/leftBanner';
                                } else {
                                    alert(json.message)
                                }
                            } catch (e) {
                                alert(e)
                            }

                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#SubmitHomeImportBanner').click(function () {
    var json = '';
    json = json + '{';
    json = json + '"status":"' + $('#HomeImportStatus').val() + '",';
    json = json + '"url":"' + $('#url').val() + '",';
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';

    var OldBanner = $('#ImagesEncode').attr('OldImage');
    var NewBanner = $('#ImagesEncode').attr('newimage');
    var data = {
        json: json,
        OldBanner: OldBanner,
        NewBanner: NewBanner
    };

    $.confirm({
        title: 'Are you sure you want to Save',
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/importBanner/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    window.location = base_url + 'Admin/importBanner';
                                } else {
                                    alert(json.message)
                                }
                            } catch (e) {
                                alert(e)
                            }

                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#SubmitHomeExportBanner').click(function () {
    var json = '';
    json = json + '{';
    json = json + '"status":"' + $('#HomeExportStatus').val() + '",';
    json = json + '"url":"' + $('#url').val() + '",';
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';

    var OldBanner = $('#ImagesEncode').attr('OldImage');
    var NewBanner = $('#ImagesEncode').attr('newimage');
    var data = {
        json: json,
        OldBanner: OldBanner,
        NewBanner: NewBanner
    };

    $.confirm({
        title: 'Are you sure you want to Save',
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/exportBanner/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    window.location = base_url + 'Admin/exportBanner';
                                } else {
                                    alert(json.message)
                                }
                            } catch (e) {
                                alert(e)
                            }

                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#SubmitTestimonial').click(function () {
    var json = '';
    json = json + '{';
    if ($('#profile_image').val() == '') {
        $('#profile_image').focus();
        $('#profile_image').css('border-color', 'red');
        return false;
    } else {
        $('#profile_image').css('border-color', '');
    }
    if ($('#name').val() == '') {
        $('#name').focus();
        $('#name').css('border-color', 'red');
        return false;
    } else {
        $('#name').css('border-color', '');
        json = json + '"name":"' + $('#name').val() + '",';
    }
    if ($('#comment').val() == '') {
        $('#comment').focus();
        $('#comment').css('border-color', 'red');
        return false;
    } else {
        $('#comment').css('border-color', '');
        json = json + '"comment":"' + $('#comment').val() + '",';
    }
    json = json + '"name_ar":"' + $('#name_ar').val() + '",';
    json = json + '"comment_ar":"' + $('#comment_ar').val() + '",';
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';

    var OldImage = $('#ImagesEncode').attr('OldImage');
    var NewImage = $('#ImagesEncode').attr('newimage');
    var data = {
        json: json,
        OldImage: OldImage,
        NewImage: NewImage
    };

    $.confirm({
        title: 'Are you sure you want to Save',
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/testimonials/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    window.location = base_url + 'Admin/testimonials';
                                } else {
                                    alert(json.message)
                                }
                            } catch (e) {
                                alert(e)
                            }

                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('.CommonImages').change(function () {
    var fileTypes = ['jpg', 'jpeg', 'png'];
    if (this.files && this.files[0]) {
        var extension = this.files[0].name.split('.').pop().toLowerCase(),
                isSuccess = fileTypes.indexOf(extension) > -1;
        var fileName = this.files[0].name;
        var page = $(this).attr('page');
        if (isSuccess) {
            var FR = new FileReader();
            FR.readAsDataURL(this.files[0]);
            if (page == 'add_country')
            {
                FR.onload = function (e) {
                    var img = new Image();
                    img.src = e.target.result;
                    img.onload = function () {
                        var w = this.width;
                        var h = this.height;
                        if (w > '1500' || h > '750' || w < '1450' || h < '700')
                        {
                            toastr.error('Image dimension should be within 1500*750', 'Error Alert', {timeOut: 5000})
                            out = '1';
                        } else
                        {
                            document.getElementById('ImagesEncode').src = e.target.result;
                            $('#ImagesEncode').attr('newimage', e.target.result);
                            $('#ImagesEncode').show();
                        }
                    }
                };
            } else if (page == '')
            {
                FR.onload = function (e) {
                    var img = new Image();
                    img.src = e.target.result;
                    img.onload = function () {
                        var w = this.width;
                        var h = this.height;
                        if (w > '500' || h > '500' || w < '500' || h < '500')
                        {
                            toastr.error('Image dimension should be within 500*500', 'Error Alert', {timeOut: 5000})
                            out = '1';
                        } else
                        {
                            document.getElementById('ImagesEncode').src = e.target.result;
                            $('#ImagesEncode').attr('newimage', e.target.result);
                            $('#ImagesEncode').show();
                        }
                    }
                };
            } else
            {
                FR.addEventListener("load", function (e) {
                    document.getElementById('ImagesEncode').src = e.target.result;
                    $('#ImagesEncode').attr('newimage', e.target.result);
                    $('#ImagesEncode').show();
                });
            }
        } else {
            alert('Invalid File')
        }
    }
});

$('#SubmitCourier').click(function () {
    var json = '';
    json = json + '{';
    var Name = $('#Name');
    if (Name.val() == '') {
        Name.focus();
        Name.css('border-color', 'red');
        return false;
    } else {
        Name.css('border-color', '');
        json = json + '"courier":"' + Name.val() + '",';
    }
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';
    var data = {
        json: json
    };
    $.confirm({
        title: "Do you want to submit?",
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: "submit",
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/couriers/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {

                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'admin/couriers';
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            } catch (e) {
                                alert(e);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: "cancel",
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('.remove_image_common').click(function () {
    var id = $(this).attr('id');
    var path = $(this).attr('path');
    var name = $(this).attr('name');
    var table = $(this).attr('table');
    var type = $(this).attr('type');
    var field = $(this).attr('field');
    var data = {
        function: 'common_remove_image'
        , id: id
        , path: path
        , name: name
        , table: table
        , type: type
        , field: field
    };
    console.log(data);
    $.confirm({
        title: 'Do you want to remove image?',
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/Helper"
                        , async: true
                        , type: 'POST'
                        , data: data
                        , success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                $('#success-message').html('Removed successfully');
                                $('.alert-success').show();
                                $('.alert-success').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = '';
                                }, 2000);
                            } else {
                                $('.alert-danger').show();
                                $('#success-error').html('Try Again')
                                $('.alert-danger').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                }, 2000);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-danger',
                action: function () {}
            },
        }
    });
});

$('body').on('click', '.commonApprove', function () {
    var id = $(this).attr('id');
    var table = $(this).attr('table');
    var url = $(this).attr('redrurl');
    var user_id = $(this).attr('user');
    var data = {
        function: 'common_approve',
        id: id,
        table: table,
        user: user_id
    };
    $.confirm({
        content: "Are you sure want to approve?",
        title: 'Approve!!',
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: "Submit",
                action: function () {
                    $("#loader").css('display', 'block');
                    $.ajax({
                        url: base_url + "Admin/Helper",
                        type: 'POST',
                        data: data,
                        beforeSend: function () {
                            $("#loader").css('display', 'block');

                        },
                        // async: false,
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                $("#loader").css('display', 'none');
                                $('#success-message').html(json.message);
                                $('.alert-success').show();
                                $('.alert-success').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = base_url + 'Admin/' + url;
                                }, 2000);

                            } else {
                                $('.alert-danger').show();
                                $('#error-message').html(json.message)
                                $('.alert-danger').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                }, 2000);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: "Cancel",
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('body').on('click', '.commonReject', function () {

    var id = $(this).attr('id');
    var table = $(this).attr('table');
    var url = $(this).attr('redrurl');
    var user_id = $(this).attr('user');
    var data = {
        function: 'common_reject',
        id: id,
        table: table,
        user: user_id
    };
    $.confirm({
        content: "Are you sure want to reject?",
        title: 'Rejected!!',
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: "Submit",
                action: function () {
                    $("#loader").css('display', 'block');
                    $.ajax({
                        url: base_url + "Admin/Helper",
                        type: 'POST',
                        data: data,
                        beforeSend: function () {
                            $("#loader").css('display', 'block');

                        },
                        // async: false,
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == true) {
                                $("#loader").css('display', 'none');
                                $('#success-message').html(json.message);
                                $('.alert-success').show();
                                $('.alert-success').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                    window.location = base_url + 'Admin/' + url;
                                }, 2000);

                            } else {
                                $('.alert-danger').show();
                                $('#error-message').html(json.message)
                                $('.alert-danger').slideDown(500);
                                setTimeout(function () {
                                    $('.alert-danger').slideUp(500);
                                }, 2000);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: "Cancel",
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#update_details').click(function () {
    var recharge_amount = $('#recharge_amount').val();
    var request_id = $('#request_id').val();
    var user_id = $('#user_id').val();
    var offer_id = $('#offer_id').val();
    var request_type = $('#request_type').val();
    var transaction_id = $('#transaction_num').val();
    var data = {recharge_amount: recharge_amount, request_id: request_id, user_id: user_id, offer_id: offer_id, request_type: request_type, transaction_id: transaction_id};
    $.confirm({
        content: "Are you sure want to add?",
        title: 'Added!!',
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: "Submit",
                action: function () {
                    $("#loader").css('display', 'block');
                    $.ajax({
                        url: base_url + "Admin/updateWalletAmount",
                        type: 'POST',
                        data: data,
                        beforeSend: function () {
                            $("#loader").css('display', 'block');

                        },
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                $('#common_message').css('color', 'green');
                                $('#common_message').html(json.message);
                                setTimeout(function () {
                                    window.location = '';
                                }, 2000);
                            } else {
                                $('#common_message').css('color', 'green');
                                $('#common_message').html(json.message);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: "Cancel",
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#SubmitNewinfo').click(function () {
    var json = '';
    json = json + '{';
    if ($('#bank_name').val() == '') {
        $('#bank_name').focus();
        $('#bank_name').css('border-color', 'red');
        return false;
    } else {
        $('#bank_name').css('border-color', '');
        json = json + '"bank_name":"' + $('#bank_name').val() + '",';
    }

    if ($('#account_name').val() == '') {
        $('#account_name').focus();
        $('#account_name').css('border-color', 'red');
        return false;
    } else {
        $('#account_name').css('border-color', '');
        json = json + '"account_name":"' + $('#account_name').val() + '",';
    }

    if ($('#email').val() == '') {
        $('#email').focus();
        $('#email').css('border-color', 'red');
        return false;
    } else {
        $('#email').css('border-color', '');
        json = json + '"email":"' + $('#email').val() + '",';
    }

    if ($('#mobile').val() == '') {
        $('#mobile').focus();
        $('#mobile').css('border-color', 'red');
        return false;
    } else {
        $('#mobile').css('border-color', '');
        json = json + '"mobile":"' + $('#mobile').val() + '",';
    }

    if ($('#account_number').val() == '') {
        $('#account_number').focus();
        $('#account_number').css('border-color', 'red');
        return false;
    } else {
        $('#account_number').css('border-color', '');
        json = json + '"account_number":"' + $('#account_number').val() + '",';
    }

    if ($('#swift_code').val() == '') {
        $('#swift_code').focus();
        $('#swift_code').css('border-color', 'red');
        return false;
    } else {
        $('#swift_code').css('border-color', '');
        json = json + '"swift_code":"' + $('#swift_code').val() + '",';
    }

    if ($('#iban_number').val() == '') {
        $('#iban_number').focus();
        $('#iban_number').css('border-color', 'red');
        return false;
    } else {
        $('#iban_number').css('border-color', '');
        json = json + '"iban_number":"' + $('#iban_number').val() + '",';
    }
    json = json + '"UserId":"' + $('#UserId').val() + '"';
    json = json + '}';

    var data = {json: json};
    $.confirm({
        title: 'Are you sure you want to Save?',
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        data: data,
                        type: 'POST',
                        async: false,
                        url: base_url + "Admin/companyAccountInfo",
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                toastr.success(json.message, 'Success Alert', {timeOut: 5000})
                                setTimeout(function () {
                                    window.location = base_url + 'Admin/companyAccountInfo';
                                }, 2000);
                            } else
                            {
                                toastr.error(json.message, 'Error Alert', {timeOut: 5000})
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

$('#SubmitTickets').click(function () {
    var json = '';
    json = json + '{';
    if ($('#ticket_no').val() == '') {
        $('#ticket_no').focus();
        $('#ticket_no').css('border-color', 'red');
        return false;
    } else {
        $('#ticket_no').css('border-color', '');
        json = json + '"ticket_no":"' + $('#ticket_no').val() + '",';
    }
    if ($('#customer_name').val() == '') {
        $('#customer_name').focus();
        $('#customer_name').css('border-color', 'red');
        return false;
    } else {
        $('#customer_name').css('border-color', '');
        json = json + '"customer_name":"' + $('#customer_name').val() + '",';
    }
    if ($('#customer_email').val() == '') {
        $('#customer_email').focus();
        $('#customer_email').css('border-color', 'red');
        return false;
    } else {
        $('#customer_email').css('border-color', '');
        json = json + '"customer_email":"' + $('#customer_email').val() + '",';
    }
    if ($('#order_no').val() == '') {
        $('#order_no').focus();
        $('#order_no').css('border-color', 'red');
        return false;
    } else {
        $('#order_no').css('border-color', '');
        json = json + '"order_no":"' + $('#order_no').val() + '",';
    }
    if ($('#issue').val() == '') {
        $('#issue').focus();
        $('#issue').css('border-color', 'red');
        return false;
    } else {
        $('#issue').css('border-color', '');
        json = json + '"issue":"' + $('#issue').val() + '",';
    }
    if ($('#status').val() == '') {
        $('#status').focus();
        $('#status').css('border-color', 'red');
        return false;
    } else {
        $('#status').css('border-color', '');
        json = json + '"status_id":"' + $('#status').val() + '",';
    }
    json = json + '"Id":"' + $('#Id').val() + '"';
    json = json + '}';
    var data = {
        json: json
    };

    $.confirm({
        title: "Do you want to submit?",
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: "Submit",
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/ticketSettings/add-new",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {

                                var json = jQuery.parseJSON(data);
                                if (json.status == 'success') {
                                    $('#success-message').html(json.message);
                                    $('.alert-success').show();
                                    $('.alert-success').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                        window.location = base_url + 'Admin/ticketSettings';
                                    }, 2000);

                                } else {
                                    $('.alert-danger').show();
                                    $('#error-message').html(json.message)
                                    $('.alert-danger').slideDown(500);
                                    setTimeout(function () {
                                        $('.alert-danger').slideUp(500);
                                    }, 2000);
                                }
                            } catch (e) {
                                alert(e);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: "Cancel",
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});

var ADMIN = (function () {
    var fn = {};

    fn.notificationsOnclick = function (e) {
        var data = {function: 'notificationsOnclick'};
        var fhref = $(e).attr('fhref');
        data['notificationId'] = $(e).attr('notificationId');
        $.ajax({
            url: base_url + "Admin/Helper",
            data: data,
            type: 'POST',
            async: false,
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.status == true) {
                    if (fhref != '') {
                        window.location = base_url + fhref;
                    } else {
                        window.location = '';
                    }
                }
            }
        });
    }
    fn.notificationsAdminReadAll = function (e) {
        var data = {function: 'notificationsAdminReadAll'};
        $.ajax({
            url: base_url + "Admin/Helper",
            data: data,
            type: 'POST',
            async: false,
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.status == true) {
                    window.location = '';
                }
            }
        });
    }
    return fn;
})();
function alertsimple(content) {
    $.confirm({
        closeIcon: true,
        title: false,
        content: content,
        type: 'blue',
        typeAnimated: true,
        buttons: {
            CLOSE: {
                text: 'CLOSE',
                action: function () {
                }
            }
        },
    });
}






