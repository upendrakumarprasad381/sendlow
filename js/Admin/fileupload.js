Dropzone.autoDiscover = false;
var mt_rant = '';
var data = JSON.stringify({x: 5, y: 6});
var myDropzone = new Dropzone("div#myDrop", {
    url: base_url + "Admin/UploadProductMedia",

    success: function (file, response) {
        var json = jQuery.parseJSON(response);
        if (json.status == false) {
            AlertBootstrapMessage('Error', json.message, 'BtAlert');
        } else if (json.status == true) {
            AlertBootstrapMessage('Success', json.message, 'BtAlert');
            if (json.Redirect == true) {
                window.location = '';
            }
        }
    },
    init: function () {
        this.on("sending", function (file, xhr, formData) {
            formData.append("ProductId", segment3);
            formData.append("ProductMediaId", $('#ProductMediaId').val());
        });
        this.on("removedfile", function (file) {
            console.log(file);
            var MediaType = $('#MediaType').val();
            var data = {function: 'RemoveProductMedia', MediaType: MediaType, ProductId: segment3, file_name: file.name, mt_rant: mt_rant};
            $.ajax({
                url: base_url + "Admin/Helper",
                data: data,
                type: 'POST',
                success: function (data) {

                }
            })
        });
    }
});
$('.RemoveMedia').click(function () {
    var MediaId = $(this).attr('MediaId');
    var FilePath = $(this).attr('FilePath');
    var data = {function: 'AdminDeleteMedia', MediaId: MediaId, FilePath: FilePath};
    $.confirm({
        title: 'Are you sure want to delete.',
        content: false,
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Delete',
                btnClass: 'btn-danger',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/Helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                window.location = '';
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });

});

$('.RemoveSubMedia').click(function () {
    var MediaId = $(this).attr('MediaId');
    var MediaType = $(this).attr('MediaType');
    var FilePath = $(this).attr('FilePath');
    var data = {function: 'AdminDeleteSubMedia', MediaId: MediaId, MediaType: MediaType, FilePath: FilePath};
    $.confirm({
        title: 'Are you sure want to delete.',
        content: false,
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Delete',
                btnClass: 'btn-danger',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/Helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                window.location = '';
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});
var Counter = 1000001;
$('#AddNewSize').click(function () {
    var html = '';
    html = html + '<div class="row ProductColorSize" id="RowId_' + Counter + '" RowId="' + Counter + '">';
    html = html + '    <div class="col-sm-5">';
    html = html + '        <div class="form-group">';
    html = html + '            <label>Size</label>';
    html = html + '            <input type="text" class="form-control" id="ProductSize' + Counter + '" placeholder="Ex :- XL">';
    html = html + '        </div>';
    html = html + '    </div>';
//    html = html + '    <div class="col-sm-4">';
//    html = html + '        <div class="form-group">';
//    html = html + '            <label>Color</label>';
//    html = html + '            <input type="color" class="form-control ColorChange" id="ProductColor' + Counter + '">';
//    html = html + '        </div>';
//    html = html + '    </div>';
    html = html + '    <div class="col-sm-5">';
    html = html + '        <div class="form-group">';
    html = html + '            <label>Quantity</label>';
    html = html + '            <input type="number" class="form-control" id="ProductQty' + Counter + '" placeholder="20">';
    html = html + '        </div>';
    html = html + '    </div>';
    html = html + '    <div class="col-sm-1">';
    html = html + '        <div class="form-group" >';
    html = html + '            <label>&nbsp;</label>';
    html = html + '            <br>';
    html = html + '            <a href="javascript:void(0)" RemoveId="RowId_' + Counter + '" class="RemoveColor"><i style="font-size: 22px; margin-top: 9px;color: #5f56ad;" class="fas fa-minus-circle"></i></a>';
    html = html + '        </div>';
    html = html + '    </div>';
    html = html + '</div>';
    Counter++;
    $('#JqueryAddedColor').append(html);
});
$('body').on('click', '.RemoveColor', function () {
    var RemoveId = $(this).attr('RemoveId');
    $('#' + RemoveId).remove();
});
$('#SubmitColorSize').click(function () {
    var RowId = $('.ProductColorSize').length;
    var MainArray = [];
    for (var i = 0; i < RowId; i++) {
        var NodeId = $('.ProductColorSize')[i].attributes.rowid.nodeValue;
        var ProductSize = $('#ProductSize' + NodeId);
        var ProductQty = $('#ProductQty' + NodeId);
        if (ProductSize.val() == '') {
            ProductSize.css('border-color', 'red');
            ProductSize.focus();
            return false;
        }
        var data = {ProductSize: ProductSize.val(), ProductQty: ProductQty.val()};
        MainArray.push(data);
    }
    var ProductId = $('#ProductId').val();
    var data2 = {function: 'SubmitColorSize', MainArray: MainArray, ProductId: ProductId};
    $.confirm({
        title: 'Are you sure want to add.',
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Add',
                btnClass: 'btn-success',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/Helper",
                        type: 'POST',
                        data: data2,
                        async: false,
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                window.location = '';
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});
$('#RemoveAllProductSizeAndColor').click(function () {
    var ProductId = $('#ProductId').val();
    var data = {function: 'RemoveAllProductSizeAndColor', ProductId: ProductId};
    $.confirm({
        title: 'Are you sure want to remove all size this product?.',
        content: false,
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Remove',
                btnClass: 'btn-danger',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/Helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            var json = jQuery.parseJSON(data);
                            if (json.status == 'success') {
                                window.location = '';
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});
$('#VideoId').change(function () {
    $('#VideoPlayLink').hide(500);
    if (jQuery.parseJSON($(this).val())) {
        $('#VideoPlayLink').show(500);
    }
});


$('body').on('change', '#AddSimilorProductColor', function () {
    var ProductId = $('#ProductId').val();
    var SimilorProductId = $(this).val();
    var data = {function: 'AddSimilorProductColor', ProductId: ProductId, SimilorProductId: SimilorProductId};
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        data: data,
        async: false,
        success: function (data) {
            try {
                var json = jQuery.parseJSON(data);
                if (json.status == true) {
                    $('#AddedSimilarColorDiv').append(json.HTML);
                } else {
                    alert(json.message)
                }
            } catch (e) {
                alert(e);
            }

        }
    });
});
$('body').on('click', '.RemoveSimilorProduct', function () {
    var ProductId = $('#ProductId').val();
    var SimProductId = $(this).attr('ProductId');
    var RemoveId = $(this).attr('RemoveId');
    var data = {function: 'RemoveSimilorProduct', ProductId: ProductId, SimProductId: SimProductId};
    $.confirm({
        title: 'Are you sure want to remove this similor product?.',
        content: false,
        type: 'red',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Remove',
                btnClass: 'btn-danger',
                action: function () {
                    $.ajax({
                        url: base_url + "Admin/Helper",
                        type: 'POST',
                        data: data,
                        async: false,
                        success: function (data) {
                            try {
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                    $('#' + RemoveId).remove();
                                } else {
                                    alert(json.message)
                                }
                            } catch (e) {
                                alert(e);
                            }
                        }
                    });
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-info',
                action: function () {
                }
            },
        }
    });
});