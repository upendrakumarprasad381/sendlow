$(document).ready(function () {
      $('#select_all').on('click',function(){
        if(this.checked){
            var date_Appt=$(this).closest("tr").find("input[name='date_appt']").val();
            $('.checkbox-per').each(function(){
               this.checked = true;
               //$(this).closest("tr").find("input[name='date_appt']").val()=date_Appt;
               
            });
        }else{
             $('.checkbox-per').each(function(){
                this.checked = false;
            });
        }
    });
     $('.checkbox-per').on('click',function(){
        if($('.checkbox-per:checked').length == $('.checkbox-per').length){
            $('#select_all').prop('checked',true);
        }else{
            $('#select_all').prop('checked',false);
        }
        if ($(this).parent('li').children('ul').length) {
            if(this.checked == true){
            $(this).closest("li").find("ul").find(":checkbox").prop("checked", true);      
            }else{
             $(this).closest("li").find("ul").find(":checkbox").prop("checked", false);   
            }
            
        } 
        
    });
    // $('.permission').click(function () {
    //     var MenuId = $(this).attr('MenuId');
    //     var checkStatus = '';
    //     if (this.checked) {
    //         checkStatus = true;
    //     } else {
    //         checkStatus = false;
    //     }
    //     if (MenuId !== undefined) {
    //         $('.menuid' + MenuId).each(function () {
    //             $(this).prop('checked', checkStatus);
    //         });
    //     }
    // });
    // $('.permissionaction').click(function () {
    //     var MenuId = $(this).attr('MenuId');
    //     var leng = $('input:checkbox.menuid' + MenuId + ':checked').length;
    //     if (leng > 0) {
    //         $('#permissionId' + MenuId).prop('checked', true);
    //     }
    // });
    $('#Savepermission').click(function () {
        var permission = [];
        $('.permission').each(function () {
            var MenuId = $(this).attr('MenuId');
           
            if (MenuId != undefined) {
                if (this.checked) {
                    var actionpermission = [];
                    $('.menuid' + MenuId).each(function () {
                        if (this.checked) {
                            actionpermission.push($(this).attr('actionId'));
                        }
                    });
                    var data = {MenuId: MenuId, actionpermission: actionpermission};
                    permission.push(data);
                }
            }
        });
        var user_id = $('#user_id').val();
        var baseurl =  $('#baseurl').val();
        var data = {permission: permission, SetUserId: user_id};
    // alert(JSON.stringify(data));die();
        $.confirm({
            title: 'Are you sure want to save!.',
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: baseurl + "Admin/userpermission",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                            
                                var json = jQuery.parseJSON(data);
                                if (json.status == true) {
                                  
                                    window.location = baseurl + "Admin/UserManagement";
                                }
                            }
                        });
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
    });
});