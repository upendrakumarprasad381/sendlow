
$(document).ready(function () {

    $(document).on('click', '.datepicker', function () {
        $(this).datepicker({
            dateFormat: 'dd-mm-yy',
        }).focus();
        $(this).removeClass('datepicker');

    });
    $('body').on('focus', ".datetimepicker", function () {
        $(this).datetimepicker({
            icons: {
                time: "fa fa-clock-o",
                date: "fa fa-calendar",
                up: "fa fa-arrow-up",
                down: "fa fa-arrow-down"
            },
            format: 'DD-MM-YYYY HH:mm:ss',
        });
    });
    $('body').on('click', '.removelist', function () {
        var rmvIdid = $(this).attr('rmvIdid');
        $('#' + rmvIdid).remove();
    });
    $("#addPackage").click(function () {
        var html = '';
        var uniqId = Math.floor((Math.random() * 1000000000) + 1);

        html = html + ' <div class="form-group col-sm-3 packageDiv" id="list-' + uniqId + '">';
        html = html + '    <select class="brdOrg form-control frmCtrlFxd addOnSelect packageWeightlist" id="packageWeight_'+uniqId+'" >';
        html = html + weightOption;

        html = html + '    </select>';
        html = html + '    <button class=" btn btnSec rounded addOnRmv remove removelist" type="button" rmvIdid="list-' + uniqId + '">X</button>';
        html = html + '</div> ';
        $('#packageList').append(html);

    });

    $('.container').on('click', '.remove', function () {
        var id = this.id;
        var split_id = id.split("_");
        var deleteindex = split_id[1];
        $("#div_" + deleteindex).remove();
    });

    $('.offers').bind('click', function () {
        $('.active').removeClass('active');
        $(this).addClass('active');
        var id = this.id;
        var split_id = id.split("_");
        var index = split_id[1];
        var paid = $('#paid_' + index).val();
        var getAmt = $('#get_' + index).val();
        $('#paidAmt').val(paid);
        $('#getAmt').val(getAmt);
        $('#offer_id').val(index);
        $('#offer_paid_amount').val(paid);
        $('#offer_get_amount').val(getAmt);
        $('#offerId').val(index);

    });

  $('.datatable').DataTable();
    var userDataTable = $('#customerTable').DataTable({
        'processing': true,
        'serverSide': true,
        'serverMethod': 'post',
        'ajax': {
            'url': 'Frontend/getValues'
        },
        'columns': [
            {data: 'TRACKING_ID'},
            {data: 'STATUS'},
            {data: 'RECIPIENT_NAME'},
            {data: 'RECIPIENT_COUNTRY'},
            {data: 'RECIPIENT_CITY'},
            {data: 'SENDER_NAME'},
            {data: 'SENDER_COUNTRY'},
            {data: 'SENDER_CITY'},
            {data: 'RECEIVED_BY'},
            {data: 'CHARGEABLE_WEIGHT'},
        ]
    });
    // Hide & show columns
    $('input[type="checkbox"]').click(function () {
        var checked_arr = [];
        var unchecked_arr = [];

        // Read all checked checkboxes
        $.each($('input[type="checkbox"]:checked'), function (key, value) {
            checked_arr.push(this.value);
        });

        // Read all unchecked checkboxes
        $.each($('input[type="checkbox"]:not(:checked)'), function (key, value) {
            unchecked_arr.push(this.value);
        });

        // Hide the checked columns
        userDataTable.columns(checked_arr).visible(false);

        // Show the unchecked columns
        userDataTable.columns(unchecked_arr).visible(true);
    });


    $('#individual_reg').off().click(function (e) {
        e.preventDefault();
        var password = $('#ind_password').val();
        var confirmPass = $('#ind_confPassword').val();

        if ($('#ind_fname').val() == '') {
            $('#ind_fname').focus();
            alertsimple('Please Enter First Name');
            return false;
        } else {
            var fname = $('#ind_fname').val();
        }

        if ($('#ind_lname').val() == '') {
            $('#ind_lname').focus();
            alertsimple('Please Enter Last Name');
            return false;
        } else {
            var lname = $('#ind_lname').val();
        }

        if ($('#ind_email').val() == '') {
            $('#ind_email').focus();
            alertsimple('Please Enter Email');
            return false;
        } else {
            if (!isValidEmailAddress($('#ind_email').val())) {
                $('#ind_email').focus();
                alertsimple('Please Enter Valid Email Id');
                return false;
            }
        }
        if ($('#ind_phone').val() == '') {
            $('#ind_phone').focus();
            alertsimple('Please Enter Mobile Number');
            return false;
        } else {
            var phone = $('#ind_phone').val();
        }

        if ($('#ind_password').val() == '') {
            $('#ind_password').focus();
            alertsimple('Please Enter Password');
            return false;
        } else {
            var password = $('#ind_password').val();
        }

        if ($('#ind_confPassword').val() == '') {
            $('#ind_confPassword').focus();
            alertsimple('Please Confirm Password');
            return false;
        } else {
            if (password != confirmPass) {
                $('#ind_confPassword').focus();
                alertsimple('Password Mismatch');
                return false;
            }
        }
        var email = $('#ind_email').val();
        var type = 1;
        var data = {
            type: type,
            ind_fname: fname,
            ind_lname: lname,
            ind_email: email,
            ind_phone: phone,
            ind_password: password
        }
        $.ajax({
            url: base_url + "Frontend/Register",
            type: 'POST',
            data: data,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == 'success') {
                        $('.register_message').css('color', 'green');
                        $('.register_message').html(json.message);
                        setTimeout(function () {
                            window.location = '';
                        }, 2000);
                    } else {
                        $('.register_message').css('color', 'red');
                        $('.register_message').html(json.message);
                    }
                } catch (e) {
                    alert(e)
                }
            }
        });
    });

    $('#comp_form').off().submit(function (e) {
        e.preventDefault();
        var password = $('#comp_password').val();
        var confirmPass = $('#comp_confPassword').val();

        if ($('#comp_fname').val() == '') {
            $('#comp_fname').focus();
            alertsimple('Please Enter First Name');
            return false;
        }

        if ($('#comp_lname').val() == '') {
            $('#comp_lname').focus();
            alertsimple('Please Enter Last Name');
            return false;
        }

        if ($('#comp_email').val() == '') {
            $('#comp_email').focus();
            alertsimple('Please Enter Email');
            return false;
        } else {
            if (!isValidEmailAddress($('#comp_email').val())) {
                $('#comp_email').focus();
                alertsimple('Please Enter Valid Email Id');
                return false;
            }
        }

        if ($('#comp_phone').val() == '') {
            $('#comp_phone').focus();
            alertsimple('Please Enter Mobile Number');
            return false;
        }

        if ($('#comp_landphone').val() == '') {
            $('#comp_landphone').focus();
            alertsimple('Please Enter Mobile Number');
            return false;
        }

        if ($('.comp_license').val() == '') {
            $('.comp_license').focus();
            alertsimple('Please Upload License');
            return false;
        }

        if ($('comp_vat').val() == '') {
            $('comp_vat').focus();
            alertsimple('Please Upload VAT');
            return false;
        }
        if ($('#comp_password').val() == '') {
            $('#comp_password').focus();
            alertsimple('Please Enter Password');
            return false;
        }
        if ($('#comp_confPassword').val() == '') {
            $('#comp_confPassword').focus();
            alertsimple('Please Confirm Password');
            return false;
        } else {
            if (password != confirmPass) {
                $('#comp_confPassword').focus();
                alertsimple('Password Mismatch');
                return false;
            }
        }

        $.ajax({
            url: base_url + "Frontend/Register",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == 'success') {
                        $('.register_message').css('color', 'green');
                        $('.register_message').html(json.message);
                        setTimeout(function () {
                            window.location = '';
                        }, 2000);
                    } else {
                        $('.register_message').css('color', 'red');
                        $('.register_message').html(json.message);
                    }
                } catch (e) {
                    alert(e)
                }
            }
        });
    });

    $('#business_reg').off().click(function (e) {
        e.preventDefault();
        var password = $('#bus_password').val();
        var confirmPass = $('#bus_confPassword').val();

        if ($('#bus_fname').val() == '') {
            $('#bus_fname').focus();
            alertsimple('Please Enter First Name');
            return false;
        } else {
            var fname = $('#bus_fname').val();
        }

        if ($('#bus_lname').val() == '') {
            $('#bus_lname').focus();
            alertsimple('Please Enter Last Name');
            return false;
        } else {
            var lname = $('#bus_lname').val();
        }

        if ($('#bus_email').val() == '') {
            $('#bus_email').focus();
            alertsimple('Please Enter Email');
            return false;
        } else {
            if (!isValidEmailAddress($('#bus_email').val())) {
                $('#bus_email').focus();
                alertsimple('Please Enter Valid Email Id');
                return false;
            }
        }

        if ($('#bus_phone').val() == '') {
            $('#bus_phone').focus();
            alertsimple('Please Enter Mobile Number');
            return false;
        } else {
            var phone = $('#bus_phone').val();
        }

        if ($('#bus_password').val() == '') {
            $('#bus_password').focus();
            alertsimple('Please Enter Password');
            return false;
        } else {
            var password = $('#bus_password').val();
        }

        if ($('#bus_confPassword').val() == '') {
            $('#bus_confPassword').focus();
            alertsimple('Please Confirm Password');
            return false;
        } else {
            if (password != confirmPass) {
                $('#bus_confPassword').focus();
                alertsimple('Password Mismatch');
                return false;
            }
        }
        var email = $('#bus_email').val();
        var type = 3;
        var data = {
            type: type,
            bus_fname: fname,
            bus_lname: lname,
            bus_email: email,
            bus_phone: phone,
            bus_password: password
        }
        $.ajax({
            url: base_url + "Frontend/Register",
            type: 'POST',
            data: data,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == 'success') {
                        $('.register_message').css('color', 'green');
                        $('.register_message').html(json.message);
                        setTimeout(function () {
                            window.location = '';
                        }, 2000);
                    } else {
                        $('.register_message').css('color', 'red');
                        $('.register_message').html(json.message);
                    }
                } catch (e) {
                    alert(e)
                }
            }
        });
    });

    $('#login_submit,#login_submit_page').on('click', function (e) {
        e.preventDefault();
        var myid = this.id == 'login_submit_page' ? '_page' : '';
        var user_email = $('#user_email');
        var user_password = $('#user_password');
        if (myid != undefined) {
            user_email = $('#user_email' + myid);
            user_password = $('#user_password' + myid);
        }

        if (user_email.val() == '') {
            user_email.css('border-color', 'red');
            user_email.focus();
            return false;
        } else {
            user_email.css('border-color', '');
            var email = user_email.val();
        }
        if (user_password.val() == '') {
            user_password.css('border-color', 'red');
            user_password.focus();
            return false;
        } else {
            user_password.css('border-color', '');
            var password = user_password.val();
        }
        var remember = $('#remember').is(':checked');
        var data = {email: email, password: password, remember: remember};
        console.log(data);
        $.ajax({
            url: base_url + "Frontend/login",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                $('.login_submit').html('Submit');
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == 'success') {
                        $('.login_message').css('color', 'green');
                        $('.login_message').html(json.message);
                        setTimeout(function () {
                            window.location = base_url;
                        }, 2000);

                    } else {
                        $('.login_message').css('color', 'red');
                        $('.login_message').html(json.message);
                    }
                } catch (e) {
                    alert(e)
                }
            }
        });
    });

    $('#save_address').off().click(function () {
        if ($('#sender_full_name').val() == '') {
            $('#sender_full_name').focus();
            alertsimple('Please Enter Name');
            return false;
        } else {
            var full_name = $('#sender_full_name').val();
        }

        if ($('#sender_phone').val() == '') {
            $('#sender_phone').focus();
            alertsimple('Please Enter Mobile Number');
            return false;
        } else {
            var phone = $('#sender_phone').val();
        }

        if ($('#sender_country').val() == '') {
            $('#sender_country').focus();
            alertsimple('Please Enter Country');
            return false;
        } else {
            var country = $('#sender_country').val();
        }

        if ($('#sender_city').val() == '') {
            $('#sender_city').focus();
            alertsimple('Please Enter city');
            return false;
        } else {
            var city = $('#sender_city').val();
        }
        var id = $('#addressIds').val();
        var data = {
            id: id,
            full_name: full_name,
            phone: phone,
            country: country,
            city: city,
        }
        $.ajax({
            url: base_url + "Frontend/saveSenderAddress",
            type: 'POST',
            data: data,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == true) {
                        window.location = '';
                    } else {
                    }
                } catch (e) {
                    alert(e)
                }
            }
        });
    });

    $(".edit_address").on("click", function () {
        var address_id = $(this).attr('data-address');
        var data = {function: 'GetSenderAddressByAddressId', addressId: address_id, encode: true};
        $.ajax({
            url: base_url + "Frontend/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.status == true) {
                    var address = json.data;

                    // $('#country').html($('#country').html().replace('selected', ''));
                    // $('#country option[value="' + address.country + '"]').attr('selected', true);
                    // if (address.country) {
                    //     USER.getcityBycountryId();
                    // }
                    // setTimeout(function () {
                    //     $('#city').html($('#city').html().replace('selected', ''));
                    //     $('#city option[value="' + address.city + '"]').attr('selected', true);
                    // }, 1000);

                    $('#sender_full_name').val(address.full_name);
                    $('#sender_phone').val(address.mobile);
                    $('#sender_country').val(address.country);
                    $('#sender_city').val(address.city);
                    $('#addressIds').val(address.id);
                }
            }
        });
    });

    $('.addnew_address').on("click", function () {
        $('#sender_full_name').val('');
        $('#sender_phone').val('');
        $('#sender_country').val('');
        $('#sender_city').val('');
        $('#save_address').attr("data-address", address_id);
    });

    $(".delete_address").click(function () {
        var addressId = $(this).attr('addressId');
        var data = {addressId: addressId};
        $.ajax({
            url: base_url + "Frontend/removeSenderAddress",
            type: 'POST',
            data: data,
            async: false,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == true) {
                        window.location = '';
                    } else {
                        if (json.isLoged == false) {
                            window.location = json.URL;
                        }
                    }
                } catch (e) {

                }
            }
        });
    });

    $('#save_recipient_address').off().click(function () {
        if ($('#recipient_full_name').val() == '') {
            $('#recipient_full_name').focus();
            alertsimple('Please Enter Name');
            return false;
        } else {
            var full_name = $('#recipient_full_name').val();
        }

        if ($('#recipient_phone').val() == '') {
            $('#recipient_phone').focus();
            alertsimple('Please Enter Mobile Number');
            return false;
        } else {
            var phone = $('#recipient_phone').val();
        }

        if ($('#recipient_country').val() == '') {
            $('#recipient_country').focus();
            alertsimple('Please Enter Country');
            return false;
        } else {
            var country = $('#recipient_country').val();
        }

        if ($('#recipient_city').val() == '') {
            $('#recipient_city').focus();
            alertsimple('Please Enter city');
            return false;
        } else {
            var city = $('#recipient_city').val();
        }
        var id = $('#recipientAddressIds').val();
        var data = {
            id: id,
            full_name: full_name,
            phone: phone,
            country: country,
            city: city,
        }
        $.ajax({
            url: base_url + "Frontend/saveRecipientAddress",
            type: 'POST',
            data: data,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == true) {
                        window.location = '';
                    } else {
                    }
                } catch (e) {
                    alert(e)
                }
            }
        });
    });

    $(".edit_recipient_address").on("click", function () {
        var address_id = $(this).attr('data-address');
        var data = {function: 'GetRecipientAddressByAddressId', addressId: address_id, encode: true};
        $.ajax({
            url: base_url + "Frontend/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                var json = jQuery.parseJSON(data);
                if (json.status == true) {
                    var address = json.data;

                    // $('#country').html($('#country').html().replace('selected', ''));
                    // $('#country option[value="' + address.country + '"]').attr('selected', true);
                    // if (address.country) {
                    //     USER.getcityBycountryId();
                    // }
                    // setTimeout(function () {
                    //     $('#city').html($('#city').html().replace('selected', ''));
                    //     $('#city option[value="' + address.city + '"]').attr('selected', true);
                    // }, 1000);

                    $('#recipient_full_name').val(address.full_name);
                    $('#recipient_phone').val(address.mobile);
                    $('#recipient_country').val(address.country);
                    $('#recipient_city').val(address.city);
                    $('#recipientAddressIds').val(address.id);
                }
            }
        });
    });

    $('.add_recipient_address').on("click", function () {
        $('#recipient_full_name').val('');
        $('#recipient_phone').val('');
        $('#recipient_country').val('');
        $('#recipient_city').val('');
        $('#save_recipient_address').attr("data-address", address_id);
    });

    $(".delete_recipient_address").click(function () {
        var addressId = $(this).attr('addressId');
        var data = {addressId: addressId};
        $.ajax({
            url: base_url + "Frontend/removeRecipientAddress",
            type: 'POST',
            data: data,
            async: false,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == true) {
                        window.location = '';
                    } else {
                        if (json.isLoged == false) {
                            window.location = json.URL;
                        }
                    }
                } catch (e) {

                }
            }
        });
    });

    $('#forgotPassword').on('click', function () {
        var email = $('#email').val();
        var user_type = $('#user_type').val();
        if ($('#email').val() === '') {
            $('#email').css('border-color', 'red');
            return false;
        } else {
            if (!isValidEmailAddress($('#email').val())) {
                $('#email').focus();
                $('#email').css('border-color', 'red');
                return false;
            } else {
                $('#email').css('border-color', '');
            }
        }
        var data = {email: email, user_type: user_type};
        $.ajax({
            url: base_url + "Frontend/forgotPassword/update"
            , async: true
            , type: 'POST'
            , data: data
            , success: function (resp) {
                var json = jQuery.parseJSON(resp);
                try {
                    var json = jQuery.parseJSON(resp);
                    if (json.status == 'success') {
                        $('.common_message').css('color', 'green');
                        $('.common_message').html(json.message);
                        setTimeout(function () {
                            window.location = base_url;
                        }, 2000);
                    } else {
                        $('.common_message').css('color', 'green');
                        $('.common_message').html(json.message);
                    }
                } catch (e) {
                    alert(e)
                }

            }
        });
    });

    $('#resetPassword').on('click', function () {
        var id = $('#userid').val();
        var password = $('#password').val();
        if ($('#password').val() === '') {
            $('#password').css('border-color', 'red');
            return false;
        } else {
            // if (!isValidPassword($('#password').val())) {
            //     $('#password').css('border-color', 'red');
            //     $('.common_message').css('color', 'red');
            //     $('.common_message').html('Password Should Be Alphanumeric');
            //     return false;
            // } else {
            $('#password').css('border-color', '');
            $('.common_message').html('');
            // }
        }
        if ($('#confPassword').val() === '') {
            $('#confPassword').css('border-color', 'red');
            return false;
        } else {
            if ($('#confPassword').val() != $('#password').val()) {
                $('#confPassword').css('border-color', 'red');
                $('.common_message').css('color', 'red');
                $('.common_message').html('Password Mismatch');
                return false;
            } else {
                $('#confPassword').css('border-color', '');
                $('.common_message').html('');
            }
        }
        $.ajax({
            url: base_url + "Frontend/resetPassword/update"
            , async: true
            , type: 'POST'
            , data: "password=" + password + "&id=" + id
            , success: function (data) {
                var json = jQuery.parseJSON(data);
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == 'success') {
                        $('.common_message').css('color', 'green');
                        $('.common_message').html(json.message);
                        setTimeout(function () {
                            window.location = base_url;
                        }, 2000);
                    } else {
                        $('.common_message').css('color', 'red');
                        $('.common_message').html(json.message);
                    }
                } catch (e) {
                    alert(e)
                }
            }
        });
    });

    $('#walletByCard').on('click', function () {
        var amount = $('#amount').val();
        var user_id = $('#user_id').val();
        if ($('#amount').val() === '') {
            $('#amount').css('border-color', 'red');
            return false;
        }
        var data = {amount: amount, user_id: user_id};
        $.ajax({
            url: base_url + "Frontend/UpdateWalletByCard"
            , async: true
            , type: 'POST'
            , data: data
            , success: function (resp) {
                var json = jQuery.parseJSON(resp);
                try {
                    var json = jQuery.parseJSON(resp);
                    if (json.status == 'success') {
                        $('.common_message').css('color', 'green');
                        $('.common_message').html(json.message);
                        setTimeout(function () {
                            window.location = '';
                        }, 2000);
                    } else {
                        $('.common_message').css('color', 'green');
                        $('.common_message').html(json.message);
                    }
                } catch (e) {
                    alert(e)
                }

            }
        });
    });

    $('#walletByTransafer').on('click', function () {
        $('#uploadDiv').removeClass('d-none');
        var amount = $('#amount').val();
        $('#transfer_amount').val(amount);
    });

    $('#bankTransaferForm').off().submit(function (e) {
        e.preventDefault();
        $('#uploadDiv').addClass('d-none');
        if ($('#amount').val() === '') {
            $('#amount').css('border-color', 'red');
            return false;
        }
        if ($('#walletReciept').val() === '') {
            $('#walletReciept').css('border-color', 'red');
            return false;
        }
        $.ajax({
            url: base_url + "Frontend/UpdateWalletByTransfer",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (resp) {
                var json = jQuery.parseJSON(resp);
                try {
                    var json = jQuery.parseJSON(resp);
                    if (json.status == 'success') {
                        $('.common_message').css('color', 'green');
                        $('.common_message').html(json.message);
                        setTimeout(function () {
                            window.location = '';
                        }, 2000);
                    } else {
                        $('.common_message').css('color', 'green');
                        $('.common_message').html(json.message);
                    }
                } catch (e) {
                    alert(e)
                }

            }
        });
    });

    $('#offerByTransfer').on('click', function () {
        $('#offerUploadDiv').removeClass('d-none');
    });

    $('#offerBankTransaferForm').off().submit(function (e) {
        e.preventDefault();
        $('#offerUploadDiv').addClass('d-none');
        if (($('#offer_paid_amount').val() === '') || ($('#offer_get_amount').val() === '') || ($('#offerId').val() === '')) {
            alertsimple('Please Select Offer');
            return false;
        }
        if ($('#offerWalletReciept').val() === '') {
            $('#offerWalletReciept').css('border-color', 'red');
            return false;
        }
        $.ajax({
            url: base_url + "Frontend/offerWalletByTransfer",
            method: "POST",
            data: new FormData(this),
            contentType: false,
            cache: false,
            processData: false,
            success: function (resp) {
                var json = jQuery.parseJSON(resp);
                try {
                    var json = jQuery.parseJSON(resp);
                    if (json.status == 'success') {
                        $('#common_message').css('color', 'green');
                        $('#common_message').html(json.message);
                        setTimeout(function () {
                            window.location = '';
                        }, 2000);
                    } else {
                        $('#common_message').css('color', 'green');
                        $('#common_message').html(json.message);
                    }
                } catch (e) {
                    alert(e)
                }

            }
        });
    });

    $('#offerByCard').on('click', function () {
        var paid_amount = $('#paidAmt').val();
        var get_amount = $('#getAmt').val();
        var offer_id = $('#offer_id').val();
        var user_id = $('#user_id').val();
        if (($('#paid_amount').val() === '') || ($('#get_amount').val() === '') || ($('#offer_id').val() === '')) {
            alertsimple('Please Select Offer');
            return false;
        }
        var data = {paid_amount: paid_amount, get_amount: get_amount, offer_id: offer_id, user_id: user_id};
        $.ajax({
            url: base_url + "Frontend/offerWalletByCard"
            , async: true
            , type: 'POST'
            , data: data
            , success: function (resp) {
                var json = jQuery.parseJSON(resp);
                try {
                    var json = jQuery.parseJSON(resp);
                    if (json.status == 'success') {
                        $('.common_message').css('color', 'green');
                        $('.common_message').html(json.message);
                        setTimeout(function () {
                            window.location = '';
                        }, 2000);
                    } else {
                        $('.common_message').css('color', 'green');
                        $('.common_message').html(json.message);
                    }
                } catch (e) {
                    alert(e)
                }

            }
        });
    });

});

function showAddressPopup(e) {
    $('#addAddress').modal('show');
    $('#addressType').val($(e).attr('prefix'));

    try {
        var json = $(e).attr('json');
        if (json != '') {
            json = jQuery.parseJSON(atob(json));
            $('#full_name').val(json.full_name);
            $('#father_name').val(json.father_name);
            $('#grand_father_name').val(json.grand_father_name);
            $('#family_name').val(json.family_name);
            $('#mobile').val(json.mobile);
            $('#email').val(json.email);
            $('#address').val(json.address);
            $('#company_name').val(json.company_name);
            $('#country').val(json.country).trigger('change');
            $('#city').val(json.city);
            $('#zip_code').val(json.zip_code);
            $('#primaryId').val(json.id);
            $('actionTitle').html('Update');

            console.log(json);
        } else {
            setNull();
        }
    } catch (e) {
        setNull();
    }

    function setNull() {
        $('actionTitle').html('Add New');
        $('#full_name').val('');
        $('#father_name').val('');
        $('#grand_father_name').val('');
        $('#family_name').val('');
        $('#mobile').val('');
        $('#email').val('');
        $('#address').val('');
        $('#company_name').val('');
        $('#country').val('').trigger('change');
        $('#city').val('');
        $('#zip_code').val('');
        $('#primaryId').val('');
    }
}
function savemyAddress() {
    var prefix = '';
    var json = {};

    json['full_name'] = $('#full_name' + prefix).val();
    json['father_name'] = $('#father_name' + prefix).val();
    json['grand_father_name'] = $('#grand_father_name' + prefix).val();
    json['family_name'] = $('#family_name' + prefix).val();
    json['mobile'] = $('#mobile' + prefix).val();
    json['email'] = $('#email' + prefix).val();
    json['address'] = $('#address' + prefix).val();
    json['company_name'] = $('#company_name' + prefix).val();

    json['country'] = $('#country' + prefix).val();
    json['city'] = $('#city' + prefix).val();
    json['zip_code'] = $('#zip_code' + prefix).val();
    json['type'] = $('#addressType').val();




    json['id'] = $('#primaryId').val();
    if ($('#full_name' + prefix).val() == '') {
        $('#full_name' + prefix).focus();
        return false;
    }
    if ($('#father_name' + prefix).val() == '') {
        $('#father_name' + prefix).focus();
        return false;
    }
    if ($('#grand_father_name' + prefix).val() == '') {
        $('#grand_father_name' + prefix).focus();
        return false;
    }
    if ($('#family_name' + prefix).val() == '') {
        $('#family_name' + prefix).focus();
        return false;
    }
    if ($('#mobile' + prefix).val() == '') {
        $('#mobile' + prefix).focus();
        return false;
    }
    if (!isValidEmailAddress($('#email' + prefix).val())) {
        $('#email' + prefix).focus();
        return false;
    }
    if ($('#address' + prefix).val() == '') {
        $('#address' + prefix).focus();
        return false;
    }
    if ($('#address' + prefix).val() == '') {
        $('#address' + prefix).focus();
        return false;
    }
    if ($('#address' + prefix).val() == '') {
        $('#address' + prefix).focus();
        return false;
    }
    var data = {function: 'savemyAddress', prefix: prefix, json: json};
    $.confirm({
        title: (!$.isNumeric(json['id']) ? 'Are you sure want to add new address' : 'Are you sure want to update address'),
        content: false,
        type: 'green',
        typeAnimated: true,
        buttons: {
            confirm: {
                text: 'Submit',
                btnClass: 'btn-green',
                action: function () {
                    b();
                }
            },
            cancel: {
                text: 'Cancel',
                btnClass: 'btn-warning',
                action: function () {
                }
            },
        }
    });
    function b() {
        $.ajax({
            url: base_url + "Frontend/Helper",
            type: 'POST',
            data: data,
            async: true,
            success: function (data) {
                try {
                    var json = jQuery.parseJSON(data);
                    if (json.status == true) {
                        window.location = '';
                    }
                } catch (e) {

                }
            }
        });
    }
}
function showRecipientAddressPopup(e, d) {
    if (e == false)
    {
        $('.modal-title').text('Add');
        $('#save_recipient_address').attr("data-address", '');
    } else {
        $('.modal-title').text("Edit");
        var address_id = d.getAttribute("data-address");
        $('#save_recipient_address').attr("data-address", address_id);
    }
}

function alertsimple(content) {
    $.confirm({
        closeIcon: true,
        title: false,
        content: content,
        type: 'blue',
        typeAnimated: true,
        buttons: {
            CLOSE: {
                text: 'CLOSE',
                action: function () {
                }
            }
        },
    });
}
function isValidEmailAddress($email) {
    var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
    return emailReg.test($email);
}

    