function loadTocity() {
    var code = $('#packageTo option:selected').attr('code');
    var input = document.getElementById('packageCityTo');
    var autocomplete = new google.maps.places.Autocomplete(input, {
        componentRestrictions: {country: [code]}

    });
    $('#packageCityTo').attr('placeholder', '');


    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();

        for (var i = 0; i < place.address_components.length; i++) {
            for (var j = 0; j < place.address_components[i].types.length; j++) {
                console.log(place.address_components[i].types[j]);
                if (place.address_components[i].types[j] == "postal_code") {
                    $('#packageToPincode').val(place.address_components[i].long_name);
                }
            }
        }
    });
}
function loadFromcity() {
    var code = $('#packageFrom option:selected').attr('code');
    var input = document.getElementById('packageCityFrom');
    var autocomplete = new google.maps.places.Autocomplete(input, {
        componentRestrictions: {country: [code]}
    });
    $('#packageCityFrom').attr('placeholder', '');
    google.maps.event.addListener(autocomplete, 'place_changed', function () {
        var place = autocomplete.getPlace();

        for (var i = 0; i < place.address_components.length; i++) {
            for (var j = 0; j < place.address_components[i].types.length; j++) {
                console.log(place.address_components[i].types[j]);
                if (place.address_components[i].types[j] == "postal_code") {
                    $('#packageFromPincode').val(place.address_components[i].long_name);
                }
            }
        }
    });
}