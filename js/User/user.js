$(document).ready(function () {

    $('.login_vendor').on('click', function () {
        VendorLogin();
    });
    
    // $("input").keypress(function(event) {
    //     if (event.which == 13) { // 13 is for enter button
    //         event.preventDefault();
    //         VendorLogin();
    //     }
    // });
    
    function VendorLogin()
    {
        if ($('#UserName').val() == '') {
            $('#UserName').css('border-color', 'red');
            $('#UserName').focus();
            return false;
        } else {
            $('#UserName').css('border-color', '');
            var email = $('#UserName').val();
        }
        if ($('#Password').val() == '') {
            $('#Password').css('border-color', 'red');
            $('#Password').focus();
            return false;
        } else {
            $('#Password').css('border-color', '');
            var password = $('#Password').val();
        }
        var data = {UserName: email, Password: password};

        // $('.login_vendor').html('<i class="fa fa-spinner fa-spin"></i> Submit');
        $.ajax({
            url: base_url + "Login/CheckVendorLogin",
            type: 'POST',
            async: false,
            data: data,
            success: function (resp) {
                // $('.login_vendor').html('Submit');
                try {
                    var json = jQuery.parseJSON(resp);
                    if (json.status == 'success') {
                        $('.login_message').css('color', 'green');
                        $('.login_message').html(json.msg);
                        setTimeout(function () {
                            window.location = json.RedirectURL;
                            // if (segment2 == 'callback') {
                            //     window.location = CallBack;
                            // }
                        }, 2000);
                    } else {
                        $('.login_message').css('color', 'red');
                        $('.login_message').html(json.msg);
                    }
                } catch (e) {
                    alert(e.message)
                }
            }
        });
    }
    
    $('.remove_user_doc').on('click',function(){
        var id = $(this).attr('id');
        var table = $(this).attr('table');
        var field = $(this).attr('field');
        var path = $(this).attr('path');
        var name = $(this).attr('name');
        
        console.log();
        
        var data = {function: 'removeUserDoc', id: id, table: table, field: field, path: path, name: name};
        var cmessage = $(this).attr('cmessage') != undefined ? $(this).attr('cmessage') : 'Are You Sure Want To Delete?';
        $.confirm({
            title: cmessage,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                tryAgain: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        $.ajax({
                            url: base_url + "Users/helper",
                            type: 'POST',
                            data: data,
                            async: false,
                            success: function (data) {
                                try {
                                    var json = jQuery.parseJSON(data);
                                    if (json.status == 'success') {
                                        window.location = '';
                                    }
                                } catch (e) {

                                }
                            }
                        });
                    }
                },
                close: {
                    text: 'Cancel',
                    btnClass: 'btn-red',
                }
            }
        });
    });
    
    
});
    
    // function ajaxpost(form, url) {
    //     var res = '';
    //     var xhr = new XMLHttpRequest();
    //     xhr.open('POST', base_url + url, false);
    //     xhr.onload = function () {
    //         res = xhr.responseText;
    //     };
    //     xhr.send(form);
    //     return res;
    // }
    function alertsimple(content) {
        $.confirm({
            closeIcon: true,
            title: false,
            content: content,
            type: 'green',
            typeAnimated: true,
            buttons: {
                CLOSE: {
                    text: 'CLOSE',
                    action: function () {
                    }
                }
            },
        });
    }