$(document).ready(function () {
    $('#NextYearCollection').click(function () {
        var NextYear = $('#NextYearCollection').attr('NextYear');
        OrderCollectionYear(NextYear);
    });
    $('#PreYearCollection').click(function () {
        var PreYear = $('#PreYearCollection').attr('PreYear');
        OrderCollectionYear(PreYear);
    });
    $('body').on('click', '.ReadMe', function () {
        var CallBackURL = $(this).attr('CallBackURL');
        var data = {function: 'ReadMe', tablename: $(this).attr('tablename'), 'fieldname': $(this).attr('fieldname'), primaryid: $(this).attr('primaryid'), PrimaryIdName: $(this).attr('PrimaryIdName')};
        $.ajax({
            url: base_url + "Admin/Helper",
            type: 'POST',
            async: false,
            data: data,
            success: function (data) {
                if (jQuery.parseJSON(data)) {
                    window.location = CallBackURL;
                }
            }
        });
    });
    $('.RejectApproveReview').click(function () {
        var status = $(this).attr('status');
        var primaryid = $(this).attr('primaryid');
        var message = status == 1 ? 'Are you sure want to approved.' : 'Are you sure want to rejected.'
        $.confirm({
            title: message,
            content: false,
            type: 'green',
            typeAnimated: true,
            buttons: {
                confirm: {
                    text: 'Submit',
                    btnClass: 'btn-success',
                    action: function () {
                        var data2 = {id: primaryid, status: status};
                        ChangeReviewStatus(data2, true);
                    }
                },
                cancel: {
                    text: 'cancel',
                    btnClass: 'btn-danger',
                    action: function () {
                    }
                },
            }
        });
    });
});










function LobiboxNotifyCompleted()
{
     var data = {function: 'LobiboxNotifyCompleted'};
    var OrderId = '';
var offer='';
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            if (jQuery.parseJSON(data)) {
                var json = jQuery.parseJSON(data);
                var NewOrder = json.NewOrder;
                if (!$('.jconfirm').hasClass('jconfirm-modern') && NewOrder.length > 0) {
                    var d = NewOrder[0];
                    playSound(base_url + 'files/quite-impressed-565.mp3');
                    OrderId = d.request_id;
                                        offer = d.offer;

                    $.confirm({
                        title: 'Request Completed!',
                        content: 'Request Completed  - Request Number:' + d.request_no + '.',
                        icon: 'fab fa-first-order',
                        theme: 'modern',
                        closeIcon: true,
                        animation: 'scale',
                        type: 'blue',
                        buttons: {
                            okay: {
                                text: 'Read More',
                                btnClass: 'btn-success',
                                action: function () {
                                    CloseNotifycomplete(OrderId,offer, true)
                                }
                            },
                            Read: {
                                text: 'Make AS READ',
                                btnClass: 'btn-danger',
                                action: function () {
                                    CloseNotifycomplete(OrderId, offer,false);
                                }
                            }
                        }
                    });
                }
            }
        }
    });
}
function LobiboxNotifyAppointment()
{
     var data = {function: 'LobiboxNotifyAppointment'};
    var OrderId = '';
var offer='';

    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            if (jQuery.parseJSON(data)) {
                var json = jQuery.parseJSON(data);
                var NewOrder = json.NewOrder;
                if (!$('.jconfirm').hasClass('jconfirm-modern') && NewOrder.length > 0) {
                    var d = NewOrder[0];
                    playSound(base_url + 'files/quite-impressed-565.mp3');
                    OrderId = d.request_id;
                                        offer = d.offer;

                    $.confirm({
                        title: 'Customer Accept Quotation!',
                        content: 'Customer Accept Quotation  ' + d.user_name + ' Request Number:' + d.request_no + '.',
                        icon: 'fab fa-first-order',
                        theme: 'modern',
                        closeIcon: true,
                        animation: 'scale',
                        type: 'blue',
                        buttons: {
                            okay: {
                                text: 'Read More',
                                btnClass: 'btn-success',
                                action: function () {
                                    CloseNotifyAppointment(OrderId,offer, true)
                                }
                            },
                            Read: {
                                text: 'Make AS READ',
                                btnClass: 'btn-danger',
                                action: function () {
                                    CloseNotifyAppointment(OrderId,offer, false);
                                }
                            }
                        }
                    });
                }
            }
        }
    });
}

function LobiboxNotifyOrder() {
    var data = {function: 'LobiboxNotifyOrder'};
    var OrderId = '';
var offer='';
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            if (jQuery.parseJSON(data)) {
                var json = jQuery.parseJSON(data);
                var NewOrder = json.NewOrder;
                if (!$('.jconfirm').hasClass('jconfirm-modern') && NewOrder.length > 0) {
                    var d = NewOrder[0];
                    playSound(base_url + 'files/quite-impressed-565.mp3');
                    OrderId = d.request_id;
                    offer=d.offer;
                    var newLine = "\r\n";
                    var message="";
                    if(d.category!='')
                    {
                 message ='Category: ' + d.category+ " \n";
                    }

                    $.confirm({
                        title: 'New Request!',
                         content: message +
    
     '<div class="form-group">' +
    'New Request has been received by ' + d.user_name  +  
    '</div>'+
     '<div class="form-group">' +
    'Request Number:' + d.request_no + 
    '</div>',

                        icon: 'fab fa-first-order',
                        theme: 'modern',
                        closeIcon: true,
                        animation: 'scale',
                        type: 'blue',
                        buttons: {
                            okay: {
                                text: 'Read More',
                                btnClass: 'btn-success',
                                action: function () {
                                    CloseNotifyOrder(OrderId,offer, true)
                                }
                            },
                            Read: {
                                text: 'Make AS READ',
                                btnClass: 'btn-danger',
                                action: function () {
                                    CloseNotifyOrder(OrderId,offer, false);
                                }
                            }
                        }
                    });
                }
            }
        }
    });
}
function playSound(url) {
  const audio = new Audio(url);
  audio.play();
}
function CloseNotifyOrder(OrderId, offer,IsRedirect = false) {
    var data = {function: 'CloseNotifyOrder', request_id: OrderId,offer:offer,IsRedirect:IsRedirect}
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            var json = jQuery.parseJSON(data);
            if (IsRedirect == true) {
                                            if (json.status == 'success') {

                window.location = base_url +  atob(json.redirect_url);
                                            }
            }
            else
            {
               window.location ="";  
            }
        }
    });
}



function CloseNotifyAppointment(OrderId, offer,IsRedirect = false) {
    var data = {function: 'CloseNotifyAppointment', request_id: OrderId,offer:offer,IsRedirect:IsRedirect}
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            var json = jQuery.parseJSON(data);
            if (IsRedirect == true) {
                                            if (json.status == 'success') {

                window.location = base_url +  atob(json.redirect_url);
                                            }
            }
        }
    });
}


function CloseNotifycomplete(OrderId,offer, IsRedirect = false) {
    var data = {function: 'CloseNotifycomplete', request_id: OrderId,offer:offer,IsRedirect:IsRedirect}
    $.ajax({
        url: base_url + "Admin/Helper",
        type: 'POST',
        async: false,
        data: data,
        success: function (data) {
            var json = jQuery.parseJSON(data);
            if (IsRedirect == true) {
                                            if (json.status == 'success') {

                window.location = base_url +  atob(json.redirect_url);
                                            }
            }
        }
    });
}










// Calling this All function

setInterval(function () {
    LobiboxNotifyOrder();
    LobiboxNotifyAppointment();
    LobiboxNotifyCompleted();
}, 40000);




