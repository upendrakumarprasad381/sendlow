(function ($) {
  
	var map,
    marker = false,
    geocoder;
	var initMap = function () {
	 var latitude=   $('#latitude').val();
	 	 var longitude=   $('#longitude').val();

		var mapCenter = new google.maps.LatLng(latitude,longitude);

		map = new google.maps.Map(document.getElementById('map'), {
			center: mapCenter,
			zoom: 12,
			draggable: true
		});

		$('#homebusiness_location_map').on('hidden.bs.modal', function (e) {
			$("#location").val($("#store-location").val());
		});
		$('#homebusiness_location_map').on('shown.bs.modal', function () {
			google.maps.event.trigger(map, "resize");
		});

		var input = document.getElementById('store-location');
		var autocomplete = new google.maps.places.Autocomplete(input);
		autocomplete.setComponentRestrictions({'country': 'ae'});
		
		marker = new google.maps.Marker({
			position: mapCenter,
			map: map,
			draggable: true
		});
		
        google.maps.event.addListener(marker, 'dragend', function (event) {
            geocodePositionByAddress(marker.getPosition());
			markerLocation();
        });
		
		google.maps.event.addListener(autocomplete, 'place_changed', function () {
			
			var place = autocomplete.getPlace();
            latlng = new google.maps.LatLng(place.geometry.location.lat(), place.geometry.location.lng());
    
            map.setCenter(latlng);
    
            map.setZoom(13);
    
			marker.setPosition(latlng);
			
			markerLocation();
		});
		map.setCenter(mapCenter);

	}

	function markerLocation() {
		var currentLocation = marker.getPosition();

		$('#latitude').val(currentLocation.lat()); //latitude
		$('#longitude').val(currentLocation.lng()); //latitude
// 		$('#location_lat').val(currentLocation.lat()); //latitude
// 		$('#location_lng').val(currentLocation.lng()); //latitude
// 		$('.latitude').val(currentLocation.lat()); //latitude
// 		$('.longitude').val(currentLocation.lng()); //latitude
		geocodePosition(currentLocation);
    }
    
    function geocodePosition(pos) {
        $('.delete-icon').css('display','block');
        $('.pointer').css('display','none');
        var geocoder = new google.maps.Geocoder();
        geocoder.geocode({
            latLng: pos
        }, function (responses) {
          
            var arr = [];
            var arr1 = [];
            if (responses && responses.length > 0) {
                for(i=0;i<responses.length;i++)
                {
                    var resp = responses[i].formatted_address;
                    arr.push(resp);
                   
                }
               
       
                var loc = document.getElementById('store-location').value;
                var loc1 = loc.split("-");
                var locality = arr[0].split("-");
                var array =[];
                if(loc1.length>2 || locality.length<3)
                {
                    for(i=0;i<responses.length;i++)
                    {

                        if(responses[i].geometry.location_type=='GEOMETRIC_CENTER')
                        {
                            var resp = responses[i].formatted_address;
                            array.push(resp);
    
                        }
                    }
                }
                $('#homebusinesslocation').val($('#store-location').val());
                return false;
                
               
                
              
            } else {
               
                document.getElementById('store-location').value = '';
            }
        });
    }
    function geocodePositionByAddress(pos) {
    var geocoder = new google.maps.Geocoder();
    geocoder.geocode({
        latLng: pos
    }, function (responses) {
        if (responses && responses.length > 0) {
            var addressName = responses[0].formatted_address;
            $('#store-location').val(addressName);
        } else {
            return '';
        }
    });
}

    var pacContainerInitialized = false;
	$('#store-location').keypress(function () {
		if (!pacContainerInitialized) {
			$('.pac-container').css('z-index', '9999');
			pacContainerInitialized = true;
		}
	});

	window.initMap = initMap;

})($);


/* $(document).ready(function () {
	initMap();
});
 */